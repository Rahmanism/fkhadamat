unit U_DemoForm1;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, Grids, DBGrids, DBTables,
  ComCtrls, ADODB;

type
  TF_DemoForm1 = class(TForm)
    PMain: TPanel;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    EName: TEdit;
    DSBrowse1: TDataSource;
    ECode: TEdit;
    Label1: TLabel;
    PButton: TPanel;
    BADD: TSpeedButton;
    BEDIT: TSpeedButton;
    BSAVE: TSpeedButton;
    BDEL: TSpeedButton;
    BCLOSE: TSpeedButton;
    SB1: TStatusBar;
    BCALC: TSpeedButton;
    BCANCEL: TSpeedButton;
    PCheckBoxes: TPanel;
    AutoNum: TCheckBox;
    AutoAdd: TCheckBox;
    BPRINT: TSpeedButton;
    BCOLOR: TSpeedButton;
    BFILTER: TSpeedButton;
    Splitter1: TSplitter;
    procedure FormShow(Sender: TObject);
    procedure BADDClick(Sender: TObject);
    procedure BSAVEClick(Sender: TObject);
    procedure BDELClick(Sender: TObject);
    procedure BEDITClick(Sender: TObject);
    procedure DSBrowse1DataChange(Sender: TObject; Field: TField);
    procedure ECodeExit(Sender: TObject);
    procedure ECodeKeyPress(Sender: TObject; var Key: Char);
    procedure BCANCELClick(Sender: TObject);
    procedure ENameKeyPress(Sender: TObject; var Key: Char);
  private
    procedure FBrowse1;
    procedure FullEdits;
    function CheckData: boolean;
    procedure InitializeDemoForm;
    { Private declarations }
  public
    { Public declarations }
    TableName : String;
    swAddOrEdit : TState;
    FocusedPanel : String;
    BookMark1 : integer;

    FilterStr : String;
    StrCodeName : String;
  end;

var
  F_DemoForm1: TF_DemoForm1;

implementation


{$R *.DFM}

procedure TF_DemoForm1.InitializeDemoForm;
begin
   Label1.Caption := '�� ' + StrCodeName ;
   Label2.Caption := '��� ' + StrCodeName ;
   DBGrid1.Columns[0].Title.Caption := '�� ' + StrCodeName ;
   DBGrid1.Columns[1].Title.Caption := '��� ' + StrCodeName ;
end;

function TF_DemoForm1.CheckData : boolean;
var
   CD : boolean;
begin
   CD := True;
   if (Trim(ECode.Text) = '') then
   begin
      ShowMessage('����� �� ���� ����');
      ECode.SetFocus;
      CD := False;
   end
   else
   if (Trim(EName.Text) = '') then
   begin
      ShowMessage('��� �� ���� ����');
      EName.SetFocus;
      CD := False;
   end;

   CheckData := CD;
end;

procedure TF_DemoForm1.FullEdits;
begin
   ECode.Text := F_Common.Browse1FCode.AsString;
   EName.Text := F_Common.Browse1FName.AsString;
end;

procedure TF_DemoForm1.FBrowse1;
begin
   BrowseRecord1(Self, TableName);
end;

procedure TF_DemoForm1.FormShow(Sender: TObject);
begin
   InitializeDemoForm;
   FocusedPanel := 'PMain';
   DBGrid1.SetFocus;
   FBrowse1;
end;

procedure TF_DemoForm1.BADDClick(Sender: TObject);
begin
   ChangeEnableButton(Self, 'bAdd', swAddOrEdit, FocusedPanel);
   if AutoNum.Checked then
   begin
      ECode.Text := GetMaxCodeRecord(TableName, '' , -1);
      if EName.Tag mod 6 = 0 then
         EName.SetFocus;
   end
   else
      if ECode.Tag mod 6 = 0 then
         ECode.SetFocus;
end;

procedure TF_DemoForm1.BSAVEClick(Sender: TObject);
var
   i : integer;
begin
   if not CheckData then
      exit;

   if swAddOrEdit = sAdd then
   begin
      InsertRecord1(Self, TableName, ECode.Text, EName.Text);
      ChangeEnableButton(Self, 'bSave', swAddOrEdit, FocusedPanel);
      DBGrid1.SetFocus;
      if AutoAdd.Checked then
         BADDClick(Sender);
   end

   else if swAddOrEdit = sEdit then
   begin
      UpdateRecord1(TableName, ECode.Text, EName.Text);
      ChangeEnableButton(Self, 'bSave', swAddOrEdit, FocusedPanel);
      DBGrid1.SetFocus;
   end;
end;

procedure TF_DemoForm1.BDELClick(Sender: TObject);
begin
   DeleteRecord1(Self, TableName, F_Common.Browse1FSerial.AsInteger, F_Common.Browse1);
end;

procedure TF_DemoForm1.BEDITClick(Sender: TObject);
begin
   if F_Common.Browse1.RecordCount = 0 then
   begin
     ShowMessage('������ ���� �����');
     exit;
   end
   else
   begin
      if ECode.Tag mod 6 = 0 then
         ECode.SetFocus;
      ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
   end;
end;

procedure TF_DemoForm1.DSBrowse1DataChange(Sender: TObject; Field: TField);
begin
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FullEdits;
end;

procedure TF_DemoForm1.ECodeExit(Sender: TObject);
begin
   if swAddOrEdit <> sFilter then
   if (ECode.Text <> '') and (F_Common.Browse1FCode.AsString <> ECode.Text) then
      F_Common.Browse1.Locate('FCode', ECode.Text, []);
end;

procedure TF_DemoForm1.ECodeKeyPress(Sender: TObject; var Key: Char);
begin
   if not(Key IN ValidKey) then
      Key := #0;
end;

procedure TF_DemoForm1.BCANCELClick(Sender: TObject);
begin
   F_Common.Browse1.Filtered := False;
   F_Common.Browse1.Filter := '';
   F_Common.Browse1.Filtered := True;

   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FullEdits;
end;

procedure TF_DemoForm1.ENameKeyPress(Sender: TObject; var Key: Char);
begin
   if key = #13 then
   if AutoAdd.Checked then
      if CheckData then
      begin
         BSAVEClick(Sender);
         BADDClick(Sender);
      end;
end;

end.
