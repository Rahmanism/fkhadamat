object F_Login: TF_Login
  Left = 280
  Top = 238
  BiDiMode = bdRightToLeft
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1605#1580#1608#1586' '#1608#1585#1608#1583' '#1576#1607' '#1587#1610#1587#1578#1605' '
  ClientHeight = 132
  ClientWidth = 285
  Color = 12615680
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 188
    Top = 21
    Width = 78
    Height = 16
    Caption = #1588#1606#1575#1587#1607' '#1603#1575#1585#1576#1585
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 202
    Top = 57
    Width = 52
    Height = 16
    Caption = #1585#1605#1586' '#1593#1576#1608#1585
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object C_User1: TEdit
    Left = 60
    Top = 17
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnKeyDown = C_User1KeyDown
    OnKeyPress = C_User1KeyPress
  end
  object Pass: TEdit
    Left = 60
    Top = 54
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = PassKeyDown
    OnKeyPress = PassKeyPress
  end
  object ok: TBitBtn
    Left = 54
    Top = 96
    Width = 75
    Height = 25
    Caption = #1578#1575#1610#1610#1583
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = okClick
  end
  object BitBtn2: TBitBtn
    Left = 152
    Top = 96
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1575#1606#1589#1585#1575#1601
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BitBtn2Click
  end
end
