unit U_SaveBeforClose;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TF_SaveBeforClose = class(TForm)
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    IsSave : integer;
  end;

var
  F_SaveBeforClose: TF_SaveBeforClose;

implementation

{$R *.DFM}

procedure TF_SaveBeforClose.FormShow(Sender: TObject);
begin
   BitBtn1.SetFocus;
   IsSave := 1;
end;

procedure TF_SaveBeforClose.BitBtn1Click(Sender: TObject);
begin
   IsSave := 1;
   Close;
end;

procedure TF_SaveBeforClose.BitBtn2Click(Sender: TObject);
begin
   IsSave := -1;
   Close;
end;

procedure TF_SaveBeforClose.BitBtn3Click(Sender: TObject);
begin
   IsSave := 0;
   Close;
end;

end.
