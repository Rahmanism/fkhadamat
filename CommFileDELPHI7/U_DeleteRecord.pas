






unit U_DeleteRecord;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TF_DeleteRecord = class(TForm)
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    LabelSrlDataSet: TLabel;
    LabelSrlRecord: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    IsDel : boolean;
  end;

var
  F_DeleteRecord: TF_DeleteRecord;

implementation

{$R *.DFM}

procedure TF_DeleteRecord.FormShow(Sender: TObject);
begin
   IsDel := False;
end;

procedure TF_DeleteRecord.BitBtn1Click(Sender: TObject);
begin
   IsDel := True;
end;

procedure TF_DeleteRecord.BitBtn2Click(Sender: TObject);
begin
   IsDel := False;
end;

end.
