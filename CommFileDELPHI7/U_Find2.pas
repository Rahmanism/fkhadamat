unit U_Find2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, Grids, DBGrids, ExtCtrls, Buttons;

type
  TF_Find2 = class(TForm)
    DSADOStoredProcFind: TDataSource;
    Label3: TLabel;
    EditFind: TEdit;
    DBGrid1: TDBGrid;
    LCount: TLabel;
    KFind: TRadioGroup;
    ADOStoredProcFind: TADOStoredProc;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure EditFindChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EditFindEnter(Sender: TObject);
    procedure EditFindExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KFindClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SrlDataSet : Integer;

    CFind : String;
    NFind : String;
    SrlFind : Integer;

    FormResult : integer;
  end;

var
   F_Find2: TF_Find2;

implementation

uses U_Common;


{$R *.DFM}

procedure TF_Find2.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
   if key = #13 then
      DBGrid1DblClick(Sender)
   else
   if key = #27 then
   begin
      FormResult := -1;
      Close;
   end;
end;

procedure TF_Find2.EditFindChange(Sender: TObject);
Var
   Str : String;
   ExactName : TField;
   ExactStr : String;
begin
   Str := '';
   if (EditFind.text <> '') then
   Begin
      ADOStoredProcFind.Filtered := False;

      if KFind.ItemIndex = 0 then
         Str := ' (StrFCode like ' + QuotedStr('%' + EditFind.text + '%')+ ')' + ' or ' +
            ' (StrFName like ' + QuotedStr('%' + EditFind.text + '%')+ ')'
      else if KFind.ItemIndex = 1 then
         Str := ' (StrFCode like ' + QuotedStr(EditFind.text + '%')+ ')'+ ' or ' +
            ' (StrFName like ' + QuotedStr(EditFind.text + '%')+ ')'
      else if KFind.ItemIndex = 2 then
         Str := ' (StrFCode like ' + QuotedStr('%' + EditFind.text + '%')+ ')' + ' or ' +
            ' (StrFName like ' + QuotedStr('%' + EditFind.text + '%')+ ')' + ' or ' +
            ' (ExactName like ' + QuotedStr('%' + GetExtractStr(EditFind.text) + '%')+ ')';

      ADOStoredProcFind.Filter := Str;
      ADOStoredProcFind.Filtered := True;
   End
   else
      ADOStoredProcFind.Filtered := False;

   LCount.Caption := IntToStr(ADOStoredProcFind.RecordCount);
end;

procedure TF_Find2.FormShow(Sender: TObject);
var
   i : integer;
   ExactName : TField;
   Fwidth : integer;
begin
{
   ADOStoredProcFind.Connection := F_Common.SystemConnection;
   ADOStoredProcFind.ProcedureName := 'sp_browse_T_Table2;1';
   ADOStoredProcFind.Parameters.Refresh;
}
   Fwidth := 0;
   DBGrid1.OnTitleClick := F_Common.FormDBGridTitleClick;

   try
      ADOStoredProcFind.Close;
      NullAdoSPParameters(ADOStoredProcFind);
      ADOStoredProcFind.Parameters.ParamByName('@Srl_Table').value := SrlDataSet;
      ADOStoredProcFind.Open;

      LCount.Caption := IntToStr(ADOStoredProcFind.RecordCount);

      for i := 0 to DBGrid1.Columns.Count - 1 do
      begin
         if ((DBGrid1.Columns[i].Field.DisplayName = 'FSerial')
            OR (DBGrid1.Columns[i].Field.DisplayName = 'StrFName')
            OR (DBGrid1.Columns[i].Field.DisplayName = 'ExactName')
            OR (DBGrid1.Columns[i].Field.DisplayName = 'StrFCode')) then
         DBGrid1.Columns[i].Visible := False;

         DBGrid1.Columns[i].Title.Alignment := taCenter;

         if DBGrid1.Columns[i].Field.DataType in NumberType then
            DBGrid1.Columns[i].Width := 100
         else //   StringType
            DBGrid1.Columns[i].Width := 200;

         if DBGrid1.Columns[i].Visible then
            Fwidth := Fwidth + DBGrid1.Columns[i].Width;
      end;

      width := Fwidth + 50; 
   except
      ShowMessage('Error in U_Find_FormShow ');
   end;

end;

procedure TF_Find2.DBGrid1DblClick(Sender: TObject);
begin
   CFind := ADOStoredProcFind.Fields.FieldByName('StrFCode').AsString;
   NFind := ADOStoredProcFind.Fields.FieldByName('StrFName').AsString;
   SrlFind := ADOStoredProcFind.Fields.FieldByName('FSerial').AsInteger;
   FormResult := 1;
   Close;
end;

procedure TF_Find2.EditFindEnter(Sender: TObject);
begin
   KeyPreview := False;
   DBGrid1.SetFocus;
end;

procedure TF_Find2.EditFindExit(Sender: TObject);
begin
   KeyPreview := True;
end;

procedure TF_Find2.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if Key IN ValidDigitAlphabet then
   Begin
         EditFind.Text := EditFind.Text + Key;
   End;
   if (ord(Key) = 8) then
      EditFind.Text := Copy(EditFind.Text,1,Length(EditFind.Text)-1);
end;

procedure TF_Find2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Shift = [ssCtrl]) and (Key = 96) then
   begin
      KFind.ItemIndex := (KFind.ItemIndex + 1) Mod 3;
      Key := 0;
   end;
end;

procedure TF_Find2.KFindClick(Sender: TObject);
begin
   EditFindChange(Sender);
end;

end.
