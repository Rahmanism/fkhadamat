object F_DisplayOption: TF_DisplayOption
  Left = 346
  Top = 177
  Width = 360
  Height = 309
  BiDiMode = bdRightToLeft
  Caption = #1575#1606#1578#1582#1575#1576#1607#1575
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ShapeButtonColor: TShape
    Left = 48
    Top = 88
    Width = 81
    Height = 25
  end
  object ShapeMainColor1: TShape
    Left = 48
    Top = 24
    Width = 81
    Height = 25
  end
  object Label1: TLabel
    Left = 132
    Top = 93
    Width = 106
    Height = 16
    Caption = #1585#1606#1711' '#1587#1578#1608#1606' '#1603#1604#1610#1583#1607#1575
  end
  object Label2: TLabel
    Left = 130
    Top = 29
    Width = 121
    Height = 16
    Caption = #1585#1606#1711' '#1586#1605#1610#1606#1607' '#1601#1585#1605' '#1601#1593#1575#1604
  end
  object SpeedButton1: TSpeedButton
    Left = 24
    Top = 90
    Width = 23
    Height = 22
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 24
    Top = 26
    Width = 23
    Height = 22
    OnClick = SpeedButton2Click
  end
  object ShapeMainColor2: TShape
    Left = 48
    Top = 56
    Width = 81
    Height = 25
  end
  object Label3: TLabel
    Left = 129
    Top = 61
    Width = 146
    Height = 16
    Caption = #1585#1606#1711' '#1586#1605#1610#1606#1607' '#1601#1585#1605' '#1594#1610#1585' '#1601#1593#1575#1604
  end
  object SpeedButton3: TSpeedButton
    Left = 24
    Top = 58
    Width = 23
    Height = 22
    OnClick = SpeedButton3Click
  end
  object Shape1: TShape
    Left = 48
    Top = 120
    Width = 81
    Height = 25
  end
  object Label4: TLabel
    Left = 132
    Top = 125
    Width = 109
    Height = 16
    Caption = #1585#1606#1711' '#1593#1606#1608#1575#1606' '#1587#1578#1608#1606#1607#1575
  end
  object SpeedButton4: TSpeedButton
    Left = 24
    Top = 122
    Width = 23
    Height = 22
    OnClick = SpeedButton4Click
  end
  object Shape2: TShape
    Left = 48
    Top = 152
    Width = 81
    Height = 25
  end
  object Label5: TLabel
    Left = 138
    Top = 157
    Width = 179
    Height = 16
    Caption = #1585#1606#1711' '#1593#1606#1608#1575#1606' '#1587#1578#1608#1606' '#1575#1606#1578#1582#1575#1576' '#1588#1583#1607
  end
  object SpeedButton5: TSpeedButton
    Left = 24
    Top = 154
    Width = 23
    Height = 22
    OnClick = SpeedButton5Click
  end
  object BitBtn1: TBitBtn
    Left = 64
    Top = 200
    Width = 75
    Height = 25
    Caption = #1578#1575#1610#1610#1583
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 152
    Top = 200
    Width = 75
    Height = 25
    Caption = #1581#1575#1604#1578' '#1575#1608#1604#1610#1607
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object ColorDialog1: TColorDialog
    Left = 120
    Top = 48
  end
end
