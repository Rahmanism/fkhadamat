object F_BackUp: TF_BackUp
  Left = 218
  Top = 107
  Width = 331
  Height = 299
  BiDiMode = bdRightToLeft
  Caption = #202#229#237#229' '#129#212#202#237#200#199#228
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clNavy
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 155
    Top = 8
    Width = 117
    Height = 16
    Caption = #227#211#237#209' '#221#199#237#225' '#129#212#202#237#200#199#228
  end
  object DirectoryListBox1: TDirectoryListBox
    Left = 44
    Top = 54
    Width = 229
    Height = 131
    ItemHeight = 16
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 152
    Top = 226
    Width = 121
    Height = 33
    Caption = #202#229#237#229' '#129#212#202#237#200#199#228
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 43
    Top = 226
    Width = 107
    Height = 33
    Caption = #206#209#230#204
    TabOrder = 2
    OnClick = BitBtn2Click
  end
  object DriveComboBox1: TDriveComboBox
    Left = 44
    Top = 30
    Width = 229
    Height = 22
    DirList = DirectoryListBox1
    TabOrder = 3
  end
  object BackUpName: TEdit
    Left = 45
    Top = 189
    Width = 227
    Height = 24
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 4
  end
  object BackUpDB: TADOStoredProc
    ProcedureName = 'BackUpDB;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BUPath'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@DBName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@BackUpName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 132
    Top = 118
  end
end
