unit U_Coding;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, Grids, DBGrids, DBTables,
  ComCtrls, ADODB;

type
  TF_Coding = class(TForm)
    PMain: TPanel;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    EName: TEdit;
    DSBrowse1: TDataSource;
    ECode: TEdit;
    Label1: TLabel;
    PButton: TPanel;
    BADD: TSpeedButton;
    BEDIT: TSpeedButton;
    BSAVE: TSpeedButton;
    BDEL: TSpeedButton;
    BCLOSE: TSpeedButton;
    SB1: TStatusBar;
    BCALC: TSpeedButton;
    BCANCEL: TSpeedButton;
    PCheckBoxes: TPanel;
    AutoNum: TCheckBox;
    AutoAdd: TCheckBox;
    BPRINT: TSpeedButton;
    BCOLOR: TSpeedButton;
    BFILTER: TSpeedButton;
    Splitter1: TSplitter;
    Delete1: TADOStoredProc;
    Browse1: TADOStoredProc;
    Insert1: TADOStoredProc;
    Update1: TADOStoredProc;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BADDClick(Sender: TObject);
    procedure BSAVEClick(Sender: TObject);
    procedure BDELClick(Sender: TObject);
    procedure BEDITClick(Sender: TObject);
    procedure DSBrowse1DataChange(Sender: TObject; Field: TField);
    procedure ECodeExit(Sender: TObject);
    procedure ECodeKeyPress(Sender: TObject; var Key: Char);
    procedure BCANCELClick(Sender: TObject);
    procedure ENameKeyPress(Sender: TObject; var Key: Char);
  private
    procedure FullEdits;
    function CheckData: boolean;
    procedure InitializeDemoForm;
    procedure BrowseCoding;
    { Private declarations }
  public
    { Public declarations }
    TableName : String;
    swAddOrEdit : TState;
    FocusedPanel : String;
    BookMark1 : integer;

    SrlDataSet : integer;
  end;

var
   F_Coding : TF_Coding;

implementation


{$R *.DFM}

procedure TF_Coding.BrowseCoding;
begin
   with Browse1 do
   Begin
      Connection := F_Common.SystemConnection;
      Close;
      NullAdoSPParameters(Browse1);
      Parameters.ParamByName('@Srl_Table').Value := SrlDataSet;
      try
         Open;
      except
         ShowMessage('Error In Browse1');
      end;
   end;
end;


procedure TF_Coding.InitializeDemoForm;
begin
   with Browse1 do
   begin
      Caption := Parameters.ParamByName('@Cap_DataSet').Value;

      Label1.Caption := Fields[1].DisplayName;
      Label2.Caption := Fields[2].DisplayName;

      DBGrid1.Columns[0].FieldName := Label1.Caption;
      DBGrid1.Columns[0].Title.Caption := Label1.Caption;

      DBGrid1.Columns[1].FieldName := Label2.Caption;
      DBGrid1.Columns[1].Title.Caption := Label2.Caption;
   end;
end;

function TF_Coding.CheckData : boolean;
var
   CD : boolean;
begin
   CD := True;
   if (Trim(ECode.Text) = '') then
   begin
      ShowMessage('����� �� ���� ����');
      ECode.SetFocus;
      CD := False;
   end
   else
   if (Trim(EName.Text) = '') then
   begin
      ShowMessage('��� �� ���� ����');
      EName.SetFocus;
      CD := False;
   end;

   CheckData := CD;
end;

procedure TF_Coding.FullEdits;
begin
   if ((Browse1.Active) and (Browse1.RecordCount > 0)) then
   with Browse1 do
   begin
      ECode.Text := Fields[1].Value;
      EName.Text := Fields[2].Value;
   end;
end;


procedure TF_Coding.FormShow(Sender: TObject);
begin
   FocusedPanel := 'PMain';
   DBGrid1.SetFocus;
   BrowseCoding;
   DSBrowse1.DataSet := Browse1;
   InitializeDemoForm;
end;

procedure TF_Coding.BADDClick(Sender: TObject);
begin
   ChangeEnableButton(Self, 'bAdd', swAddOrEdit, FocusedPanel);
   if AutoNum.Checked then
   begin
      ECode.Text := GetMaxCodeRecord(SrlDataSet);
      if EName.Enabled then
         EName.SetFocus;
   end
   else
      if ECode.Enabled then
         ECode.SetFocus;
end;

procedure TF_Coding.BSAVEClick(Sender: TObject);
begin
   if not CheckData then
      exit;

   if swAddOrEdit = sAdd then
   begin
      NullAdoSPParameters(Insert1);
      with Insert1.Parameters do
      begin
         ParamByName('@Srl_Table').Value := SrlDataSet;
         ParamByName('@FCode').Value := ECode.Text;
         ParamByName('@FName').Value := Trim(EName.Text);
         ParamByName('@SrlUser').Value := Srl_User;
      end;
      try
         Insert1.ExecProc;
         BookMark1 := Insert1.Parameters[0].Value;
         ChangeEnableButton(Self, 'bSave', swAddOrEdit, FocusedPanel);
         Browse1.Requery;
         Browse1.Locate(Browse1.Fields[0].DisplayName, BookMark1, []);
         DBGrid1.SetFocus;
         if AutoAdd.Checked then
            BADDClick(Sender);
      except
         ShowMessage('sp_insert_TDegree_1');
      end;
   end

   else if swAddOrEdit = sEdit then
   begin
      BookMark1 := Browse1.Fields[0].AsInteger;
      NullAdoSPParameters(Update1);
      with Update1.Parameters do
      begin
         ParamByName('@Srl_Table').Value := SrlDataSet;
         ParamByName('@FSerial').Value := BookMark1;
         ParamByName('@FCode').Value := ECode.Text;
         ParamByName('@FName').Value := Trim(EName.Text);
         ParamByName('@SrlUser').Value := Srl_User;
      end;
      try
         Update1.ExecProc;
         ChangeEnableButton(Self, 'bSave', swAddOrEdit, FocusedPanel);
         Browse1.Requery;
         Browse1.Locate(Browse1.Fields[0].DisplayName, BookMark1, []);
         DBGrid1.SetFocus;
      except
         ShowMessage('��� �� ������');
      end;
   end;
end;

procedure TF_Coding.BDELClick(Sender: TObject);
begin
   DeleteRecord1(SrlDataSet, Browse1.Fields[0].AsInteger, Browse1);
end;

procedure TF_Coding.BEDITClick(Sender: TObject);
begin
   if Browse1.RecordCount = 0 then
   begin
     ShowMessage('������ ���� �����');
     exit;
   end
   else
   begin
      if ECode.Enabled  then
         ECode.SetFocus;
      ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
   end;
end;

procedure TF_Coding.DSBrowse1DataChange(Sender: TObject; Field: TField);
begin
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FullEdits;
end;

procedure TF_Coding.ECodeExit(Sender: TObject);
begin
   if swAddOrEdit <> sFilter then
   if (ECode.Text <> '') and (Browse1.Fields[1].Value <> ECode.Text) then
      Browse1.Locate(Browse1.Fields[1].DisplayName, ECode.Text, []);
end;

procedure TF_Coding.ECodeKeyPress(Sender: TObject; var Key: Char);
begin
   if not(Key IN ValidKey) then
      Key := #0;
end;

procedure TF_Coding.BCANCELClick(Sender: TObject);
begin
   BookMark1 := Browse1.Fields[1].AsInteger;
   Browse1.Filtered := False;
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   Browse1.Locate(Browse1.Fields[1].DisplayName, BookMark1, []);
end;

procedure TF_Coding.ENameKeyPress(Sender: TObject; var Key: Char);
begin
   if key = #13 then
   if AutoAdd.Checked then
      if CheckData then
      begin
         BSAVEClick(Sender);
         BADDClick(Sender);
      end;
end;

end.
