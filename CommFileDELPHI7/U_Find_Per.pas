unit U_Find_Per;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, Grids, DBGrids, StdCtrls, ExtCtrls;

type
  TF_Find_Per = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Browse_T_StudentList_Find: TADOStoredProc;
    EName: TEdit;
    Label1: TLabel;
    EFamily: TEdit;
    Label2: TLabel;
    ENFather: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    ENumShen: TEdit;
    RGAndOr: TRadioGroup;
    Label5: TLabel;
    Browse_T_StudentList_FindFSerial: TAutoIncField;
    Browse_T_StudentList_FindFName: TStringField;
    Browse_T_StudentList_FindFName1: TStringField;
    Browse_T_StudentList_FindFName2: TStringField;
    Browse_T_StudentList_FindFCode: TIntegerField;
    Browse_T_StudentList_FindNum_Per: TIntegerField;
    Browse_T_StudentList_FindNum_Docum: TIntegerField;
    Browse_T_StudentList_FindNum_Cart: TIntegerField;
    Browse_T_StudentList_FindInput_Sal: TIntegerField;
    Browse_T_StudentList_FindShohrat: TStringField;
    Browse_T_StudentList_FindN_Father: TStringField;
    Browse_T_StudentList_FindFather_Work: TStringField;
    Browse_T_StudentList_FindNum_Shen: TStringField;
    Browse_T_StudentList_FindD_Brith: TStringField;
    Browse_T_StudentList_FindL_Brith: TStringField;
    Browse_T_StudentList_FindC_School: TIntegerField;
    Browse_T_StudentList_FindN_School: TStringField;
    Browse_T_StudentList_Findis_check: TIntegerField;
    procedure ENameChange(Sender: TObject);
    procedure EFamilyChange(Sender: TObject);
    procedure ENFatherChange(Sender: TObject);
    procedure ENumShenChange(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure RGAndOrClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    procedure FilterStudent;
    { Private declarations }
  public
    { Public declarations }
    CFind : String;
    NFind : String;
    SrlFind : Integer;
    FormResult : integer;

    FilterName : String;
    FilterFamily : String;
    FilterNFather : String;
    FilterNumShen : String;

  end;

var
  F_Find_Per: TF_Find_Per;

implementation

{$R *.DFM}

procedure TF_Find_Per.FilterStudent;
var
   TotalFilter : String;
begin
   TotalFilter := '';
   if Trim(FilterName) <> '' then
      TotalFilter := Trim(FilterName);

   if Trim(FilterFamily) <> '' then
   begin
      if Trim(TotalFilter) = '' then
         TotalFilter := Trim(FilterFamily)
      else
      begin
         if RGAndOr.ItemIndex = 0 then
            TotalFilter := TotalFilter + ' AND ' + Trim(FilterFamily)
         else
            TotalFilter := TotalFilter + ' OR ' + Trim(FilterFamily)
      end;
   end;

   if Trim(FilterNFather) <> '' then
   begin
      if Trim(TotalFilter) = '' then
         TotalFilter := Trim(FilterNFather)
      else
      begin
         if RGAndOr.ItemIndex = 0 then
            TotalFilter := TotalFilter + ' AND ' + Trim(FilterNFather)
         else
            TotalFilter := TotalFilter + ' OR ' + Trim(FilterNFather)
      end;
   end;

   if Trim(FilterNumShen) <> '' then
   begin
      if Trim(TotalFilter) = '' then
         TotalFilter := Trim(FilterNumShen)
      else
      begin
         if RGAndOr.ItemIndex = 0 then
            TotalFilter := TotalFilter + ' AND ' + Trim(FilterNumShen)
         else
            TotalFilter := TotalFilter + ' OR ' + Trim(FilterNumShen)
      end;
   end;



   Browse_T_StudentList_Find.Filtered := False;
   Browse_T_StudentList_Find.Filter := TotalFilter;
   Browse_T_StudentList_Find.Filtered := True;

{
    FilterNFather : String;
    FilterNumShen : String;
}
end;

procedure TF_Find_Per.ENameChange(Sender: TObject);
begin
   if Trim(EName.Text) <> '' then
      FilterName := 'FName1 Like ' + QuotedStr('%' + Trim(EName.Text) + '%')
   else
      FilterName := '';
   FilterStudent;
end;

procedure TF_Find_Per.EFamilyChange(Sender: TObject);
begin
   if Trim(EFamily.Text) <> '' then
      FilterFamily := 'FName2 Like ' + QuotedStr('%' + Trim(EFamily.Text) + '%')
   else
      FilterFamily := '';
   FilterStudent;
end;

procedure TF_Find_Per.ENFatherChange(Sender: TObject);
begin
   if Trim(ENFather.Text) <> '' then
      FilterNFather := 'N_Father Like ' + QuotedStr('%' + Trim(ENFather.Text) + '%')
   else
      FilterNFather := '';
   FilterStudent;
end;

procedure TF_Find_Per.ENumShenChange(Sender: TObject);
begin
   if Trim(ENumShen.Text) <> '' then
      FilterNumShen := 'Num_Shen Like ' + QuotedStr('%' + Trim(ENumShen.Text) + '%')
   else
      FilterNumShen := '';
   FilterStudent;
end;

procedure TF_Find_Per.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
   GlobalSrl_Student := Browse_T_StudentList_FindFSerial.AsInteger;
   Label5.Caption := IntToStr(Browse_T_StudentList_Find.RecordCount);
end;

procedure TF_Find_Per.FormShow(Sender: TObject);
begin
   NullAdoSPParameters(Browse_T_StudentList_Find);
   with Browse_T_StudentList_Find.Parameters do
   begin
      ParamByName('@SrlInputSal').value := -1;
      ParamByName('@KJens').value := -1;
      ParamByName('@KPer').value := -1;
   end;
   try
      Browse_T_StudentList_Find.Open;
   except

   end;
end;

procedure TF_Find_Per.RGAndOrClick(Sender: TObject);
begin
   FilterStudent;
end;

procedure TF_Find_Per.DBGrid1DblClick(Sender: TObject);
begin
   CFind := Browse_T_StudentList_FindFCode.AsString;
   NFind := Browse_T_StudentList_FindFName.AsString;
   SrlFind := Browse_T_StudentList_FindFSerial.AsInteger;
   FormResult := 1;
   Close;
end;

end.


{
CREATE PROCEDURE  Browse_T_StudentList_Find  @SrlInputSal int ,   @KJens int , @KPer  int
As

SELECT
	T_Student.FSerial ,
	T_Student.FName,
	T_Student.FName1,
	T_Student.FName2,
	T_Student.FCode,
	T_Student.Num_Per ,
	T_Student.Num_Docum,
	T_Student.Num_Cart ,
	T_Student.Input_Sal,
	T_Student.Shohrat,
	T_Student.N_Father,
	T_Student.Father_Work,
	T_Student.Num_Shen,
	T_Student.D_Brith,
	T_Student.L_Brith ,
	T_School.FCode AS C_School,
	T_School.FName AS N_School,
              T_Student.is_check
FROM T_Student
	LEFT OUTER JOIN    T_School ON T_Student.Srl_School = T_School.FSerial
WHERE
	 (T_Student.Input_Sal  =  @SrlInputSal  OR @SrlInputSal  = -1)
	AND (T_Student.K_Jens = @KJens  OR @KJens = -1)
	AND  (T_Student.Srl_KPer = @KPer  OR  @KPer  = -1)
Order By  T_Student.FCode
GO

}