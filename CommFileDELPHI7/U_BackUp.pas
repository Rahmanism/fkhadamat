unit U_BackUp;

interface

uses
  Windows, Variants, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Db, ADODB, FileCtrl;

type
  TF_BackUp = class(TForm)
    DirectoryListBox1: TDirectoryListBox;
    BackUpDB: TADOStoredProc;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DriveComboBox1: TDriveComboBox;
    Label1: TLabel;
    BackUpName: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_BackUp: TF_BackUp;

implementation

uses U_Common;

{$R *.DFM}

procedure TF_BackUp.BitBtn1Click(Sender: TObject);
var
   i : integer;
   BackUpPath : String;
begin
   try
      BitBtn1.Enabled := False;
      BackUpPath := DirectoryListBox1.Directory;
      for i := 0 to BackUpDB.Parameters.Count -1  do
         BackUpDB.Parameters[i].Value := null;

      BackUpDB.Parameters.ParamByName('@BUPath').Value := BackUpPath;
      BackUpDB.Parameters.ParamByName('@DBName').Value := N_DataBase;
      BackUpDB.Parameters.ParamByName('@BackUpName').Value := Trim(BackUpName.Text);
      BackUpDB.ExecProc;
      ShowMessage('����� ������ ���� �������');
      BitBtn1.Enabled := True;
   Except
      ShowMessage('��� �� ���� �������');
      BitBtn1.Enabled := True;
   end;
end;

procedure TF_BackUp.BitBtn2Click(Sender: TObject);
begin
   Close;   
end;

end.
