object F_Find_Per: TF_Find_Per
  Left = 118
  Top = 57
  Width = 812
  Height = 573
  BiDiMode = bdRightToLeft
  Caption = #1580#1587#1578#1580#1608#1610' '#1610#1603' '#1591#1604#1576#1607
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 664
    Top = 469
    Width = 17
    Height = 16
    Caption = #1606#1575#1605
  end
  object Label2: TLabel
    Left = 664
    Top = 497
    Width = 79
    Height = 16
    Caption = #1606#1575#1605' '#1582#1575#1606#1608#1575#1583#1711#1610
  end
  object Label3: TLabel
    Left = 413
    Top = 468
    Width = 38
    Height = 16
    Caption = #1606#1575#1605' '#1662#1583#1585
  end
  object Label4: TLabel
    Left = 406
    Top = 497
    Width = 108
    Height = 16
    Caption = #1588#1605#1575#1585#1607' '#1588#1606#1575#1587#1606#1575#1605#1607
  end
  object Label5: TLabel
    Left = 61
    Top = 424
    Width = 69
    Height = 25
    Caption = 'Label5'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clNavy
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBGrid1: TDBGrid
    Left = 32
    Top = 8
    Width = 763
    Height = 409
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = ARABIC_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Input_Sal'
        Title.Caption = #1608#1585#1608#1583
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FCode'
        Title.Caption = #1588#1605#1575#1585#1607' '#1591#1604#1576#1607
        Width = 96
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Num_Docum'
        Title.Caption = #1662#1585#1608#1606#1583#1607
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FName1'
        Title.Caption = #1606#1575#1605
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FName2'
        Title.Caption = #1606#1575#1605' '#1582#1575#1606#1608#1575#1583#1711#1610
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'N_Father'
        Title.Caption = #1606#1575#1605' '#1662#1583#1585
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Num_Shen'
        Title.Caption = #1588#1606#1575#1587#1606#1575#1605#1607
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_Brith'
        Title.Caption = #1578' '#1578#1608#1604#1583
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'L_Brith'
        Title.Caption = #1605' '#1578#1608#1604#1583
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Shohrat'
        Title.Caption = #1588#1607#1585#1578
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'C_School'
        Title.Caption = #1705#1583' '#1605#1583#1585#1587#1607
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'N_School'
        Title.Caption = #1606#1575#1605' '#1605#1583#1585#1587#1607
        Width = 40
        Visible = True
      end>
  end
  object EName: TEdit
    Left = 535
    Top = 467
    Width = 121
    Height = 24
    TabOrder = 1
    OnChange = ENameChange
  end
  object EFamily: TEdit
    Left = 536
    Top = 495
    Width = 121
    Height = 24
    TabOrder = 2
    OnChange = EFamilyChange
  end
  object ENFather: TEdit
    Left = 280
    Top = 466
    Width = 121
    Height = 24
    TabOrder = 3
    OnChange = ENFatherChange
  end
  object ENumShen: TEdit
    Left = 280
    Top = 495
    Width = 121
    Height = 24
    TabOrder = 4
    OnChange = ENumShenChange
  end
  object RGAndOr: TRadioGroup
    Left = 64
    Top = 456
    Width = 185
    Height = 73
    Caption = #1578#1585#1705#1610#1576' '#1588#1585#1591#1607#1575
    ItemIndex = 0
    Items.Strings = (
      #1607#1605#1607' '#1588#1585#1591#1607#1575
      #1581#1583#1575#1602#1604' '#1610#1705#1610' '#1575#1586' '#1588#1585#1591#1607#1575)
    TabOrder = 5
    OnClick = RGAndOrClick
  end
  object DataSource1: TDataSource
    DataSet = Browse_T_StudentList_Find
    OnDataChange = DataSource1DataChange
    Left = 264
    Top = 160
  end
  object Browse_T_StudentList_Find: TADOStoredProc
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_T_StudentList_Find'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlInputSal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end
      item
        Name = '@KJens'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end
      item
        Name = '@KPer'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end>
    Left = 264
    Top = 106
    object Browse_T_StudentList_FindFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse_T_StudentList_FindFName: TStringField
      FieldName = 'FName'
      ReadOnly = True
      Size = 303
    end
    object Browse_T_StudentList_FindFName1: TStringField
      FieldName = 'FName1'
      Size = 50
    end
    object Browse_T_StudentList_FindFName2: TStringField
      FieldName = 'FName2'
      Size = 100
    end
    object Browse_T_StudentList_FindFCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse_T_StudentList_FindNum_Per: TIntegerField
      FieldName = 'Num_Per'
    end
    object Browse_T_StudentList_FindNum_Docum: TIntegerField
      FieldName = 'Num_Docum'
    end
    object Browse_T_StudentList_FindNum_Cart: TIntegerField
      FieldName = 'Num_Cart'
    end
    object Browse_T_StudentList_FindInput_Sal: TIntegerField
      FieldName = 'Input_Sal'
    end
    object Browse_T_StudentList_FindShohrat: TStringField
      FieldName = 'Shohrat'
      Size = 100
    end
    object Browse_T_StudentList_FindN_Father: TStringField
      FieldName = 'N_Father'
      Size = 100
    end
    object Browse_T_StudentList_FindFather_Work: TStringField
      FieldName = 'Father_Work'
      Size = 100
    end
    object Browse_T_StudentList_FindNum_Shen: TStringField
      FieldName = 'Num_Shen'
      Size = 50
    end
    object Browse_T_StudentList_FindD_Brith: TStringField
      FieldName = 'D_Brith'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentList_FindL_Brith: TStringField
      FieldName = 'L_Brith'
      Size = 100
    end
    object Browse_T_StudentList_FindC_School: TIntegerField
      FieldName = 'C_School'
    end
    object Browse_T_StudentList_FindN_School: TStringField
      FieldName = 'N_School'
      Size = 100
    end
    object Browse_T_StudentList_Findis_check: TIntegerField
      FieldName = 'is_check'
    end
  end
end
