unit U_Docum;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, DBCtrls, ExtDlgs, Grids, DBGrids,
  Mask, ppPrnabl, ppClass, ppCtrls, ppBarCod, ppBands, ppCache, ppComm,
  ppRelatv, ppProd, ppReport, OleCtnrs;

type
  TF_Docum = class(TForm)
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    BitBtnAddPic: TBitBtn;
    BitBtnDelOne: TBitBtn;
    DBGrid1: TDBGrid;
    DBEdit1: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    ppReport1: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    BitBtnDelAll: TBitBtn;
    ppDBImage1: TppDBImage;
    OpenDialog1: TOpenDialog;
    OleContainer1: TOleContainer;
    Browse_T_Source_Pic: TADOStoredProc;
    Browse_T_Source_PicFSerial: TAutoIncField;
    Browse_T_Source_PicFCode: TIntegerField;
    Browse_T_Source_PicFName: TStringField;
    Browse_T_Source_PicSrl_Des_Pic: TIntegerField;
    Browse_T_Source_PicDes_Pic: TStringField;
    Browse_T_Source_PicPic_Path: TStringField;
    DSBrowse_T_Source_Pic: TDataSource;
    Delete1: TADOStoredProc;
    IsMoveFile: TCheckBox;
    ADOTable2: TADOTable;
    ADOTable2FSerial: TIntegerField;
    ADOTable2FCode: TIntegerField;
    ADOTable2FName: TWideStringField;
    ADOTable2U_DateTime: TDateTimeField;
    ADOTable2Srl_Connection: TIntegerField;
    ADOTable1: TADOTable;
    ADOTable1FSerial: TIntegerField;
    ADOTable1FCode: TIntegerField;
    ADOTable1FName: TWideStringField;
    ADOTable1U_DateTime: TDateTimeField;
    ADOTable1Srl_Connection: TIntegerField;
    IsViewFile: TCheckBox;
    DBText1: TDBText;
    BitBtnClose: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BitBtnAddPicClick(Sender: TObject);
    procedure DSBrowse_T_Source_PicDataChange(Sender: TObject;
      Field: TField);
    procedure BitBtnDelOneClick(Sender: TObject);
    procedure BitBtnDelAllClick(Sender: TObject);
    procedure DBLookupComboBox1Click(Sender: TObject);
    procedure IsViewFileClick(Sender: TObject);
    procedure BitBtnCloseClick(Sender: TObject);
  private
    procedure FBrowse1;
    procedure ShowFile;
    { Private declarations }
  public
    { Public declarations }
    BookMark1 : integer;
    SrlTable : integer;
  end;

var
  F_Docum: TF_Docum;

implementation

uses ViewWin;

{$R *.DFM}

procedure TF_Docum.FBrowse1;
begin
   with Browse_T_Source_Pic do
   begin
      NullAdoSPParameters(Browse_T_Source_Pic);
      try
         Parameters.ParamByName('@SrlSource').Value := GlobalSrl_Source;
         Parameters.ParamByName('@KSource').Value := GlobalK_Source;
         Parameters.ParamByName('@KFile').Value := 2;
         Open;
      except
         ShowMessage('��� ������ �����');
      end;
   end;
end;

procedure TF_Docum.FormShow(Sender: TObject);
begin
   SrlTable := SrlDataSetPicture;
   FBrowse1;
   ADOTable1.Open;
   ADOTable2.Open;
end;

procedure TF_Docum.ShowFile;
begin
   with Browse_T_Source_Pic do
   begin
      try
         if Not Browse_T_Source_PicPic_Path.IsNull then
         begin
            if IsViewFile.Checked then
               OleContainer1.CreateLinkToFile(Browse_T_Source_PicPic_Path.AsString,False);
         end
         else
//            OleContainer1. := nil;
      except
         ShowMessage('��� ������ ����');
      end;
   end;
end;


procedure TF_Docum.BitBtnAddPicClick(Sender: TObject);
{
   OldFullFileName : Current Path of File
   IsMoveFile : Delete File From Source Directory ?
   MyKFile : 1 - Picture, 2 - Other Files
   MyKSource : ... ,
   MySrlSource : Serial Parent of Picture
   Result : Is Moved File ?
}

var
   i : integer;
   NewFileName : String;
   OldFileName : String;
   FileIndex : integer;
   Result : integer;
begin
   OpenDialog1.Options := [ofAllowMultiSelect, ofFileMustExist];
   OpenDialog1.FilterIndex := 2; { start the dialog showing all files }
   OpenDialog1.Title := '���� ����� �� ������ ����';
   if OpenDialog1.Execute  then
   begin
      For i := 0 To OpenDialog1.Files.Count - 1 do
      begin
         FileIndex := 0;
         InsertPicture(OpenDialog1.Files.Strings[i], IsMoveFile.Checked,
               2, GlobalK_Source, GlobalSrl_Source, True,Result);

      end;
      FBrowse1;
      Browse_T_Source_Pic.Last;
   end;
end;

procedure TF_Docum.DSBrowse_T_Source_PicDataChange(Sender: TObject;
  Field: TField);
begin
   ShowFile;
end;

procedure TF_Docum.BitBtnDelOneClick(Sender: TObject);
begin
   DeleteRecord1(SrlTable, Browse_T_Source_PicFSerial.AsInteger, Browse_T_Source_Pic);
end;

procedure TF_Docum.BitBtnDelAllClick(Sender: TObject);
begin
   while Browse_T_Source_Pic.RecordCount > 0 do
      BitBtnDelOneClick(Sender);
end;

procedure TF_Docum.DBLookupComboBox1Click(Sender: TObject);
begin
   Browse_T_Source_Pic.Post;
   FBrowse1;
end;

procedure TF_Docum.IsViewFileClick(Sender: TObject);
begin
   ShowFile;
end;

procedure TF_Docum.BitBtnCloseClick(Sender: TObject);
begin
   Close;
end;

end.
