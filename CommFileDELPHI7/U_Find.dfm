object F_Find: TF_Find
  Left = 341
  Top = 318
  AutoScroll = False
  BiDiMode = bdRightToLeft
  BorderIcons = []
  Caption = '??? ??E??'
  ClientHeight = 313
  ClientWidth = 419
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnClick = FormClick
  OnHide = FormHide
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel
    Left = 311
    Top = 278
    Width = 40
    Height = 16
    AutoSize = False
    Caption = '??E??'
    Transparent = True
  end
  object LCount: TLabel
    Left = 47
    Top = 283
    Width = 45
    Height = 16
    Alignment = taCenter
    Caption = '...........'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 372
    Top = 275
    Width = 14
    Height = 14
    OnClick = SpeedButton1Click
  end
  object EditFind: TEdit
    Left = 102
    Top = 276
    Width = 202
    Height = 24
    ParentColor = True
    TabOrder = 0
    OnChange = EditFindChange
    OnDblClick = DBGrid1DblClick
    OnEnter = EditFindEnter
    OnExit = EditFindExit
    OnKeyPress = DBGrid1KeyPress
  end
  object KFind: TRadioGroup
    Left = 32
    Top = 218
    Width = 377
    Height = 40
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'OC??'
      'O??? EC'
      '??E??? I???')
    TabOrder = 2
    OnClick = KFindClick
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 419
    Height = 217
    Align = alTop
    DataSource = DSADOStoredProcFind
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = ARABIC_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    OnDblClick = DBGrid1DblClick
    OnKeyPress = DBGrid1KeyPress
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'FCode'
        Title.Alignment = taCenter
        Title.Caption = '?I'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FName'
        Title.Alignment = taCenter
        Title.Caption = '?C?'
        Width = 262
        Visible = True
      end
      item
        Expanded = False
        Visible = True
      end>
  end
  object ADOStoredProcFind: TADOStoredProc
    CursorType = ctStatic
    ProcedureName = 'sp_Browse_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 192
    Top = 104
  end
  object DSADOStoredProcFind: TDataSource
    DataSet = ADOStoredProcFind
    Left = 192
    Top = 152
  end
end
