unit U_System;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, ComCtrls, StdCtrls, DBCtrls, Mask, Grids, DBGrids, Db,
  ADODB,    Menus;

type
  TF_System = class(TForm)
    Panel1: TPanel;
    StatusBar: TStatusBar;
    Grid1: TDBGrid;
    N_System: TEdit;
    Pub_BROWSE_System: TADOStoredProc;
    DSPub_BROWSE_System: TDataSource;
    Pub_INSERT_System: TADOStoredProc;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    Pub_DELETE_System: TADOStoredProc;
    Pub_UPDATE_System: TADOStoredProc;
    Panel2: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Grid2: TDBGrid;
    Label7: TLabel;
    Cap_Form: TEdit;
    Pub_BROWSE_System_Form: TADOStoredProc;
    DSPub_BROWSE_System_Form: TDataSource;
    Pub_INSERT_System_Form: TADOStoredProc;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    IntegerField10: TIntegerField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    Pub_DELETE_System_Form: TADOStoredProc;
    Label2: TLabel;
    C_System: TEdit;
    N_Form: TEdit;
    Pub_UPDATE_System_Form: TADOStoredProc;
    Pub_BROWSE_System_FormSrl_Form: TAutoIncField;
    Pub_BROWSE_System_FormN_Form: TStringField;
    Pub_BROWSE_System_FormCap_Form: TStringField;
    Panel3: TPanel;
    Label3: TLabel;
    Grid3: TDBGrid;
    Cap_Field: TEdit;
    N_Field: TEdit;
    Pub_BROWSE_System_Form_Field: TADOStoredProc;
    DSPub_BROWSE_System_Form_Field: TDataSource;
    Pub_DELETE_System_Form_Field: TADOStoredProc;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    Pub_INSERT_System_Form_Field: TADOStoredProc;
    Pub_UPDATE_System_Form_Field: TADOStoredProc;
    Pub_BROWSE_System_Form_FieldSrl_Field: TAutoIncField;
    Pub_BROWSE_System_Form_FieldN_Field: TStringField;
    Pub_BROWSE_System_Form_FieldCap_Field: TStringField;
    Cap_System: TEdit;
    Pub_BROWSE_SystemSrl_System: TIntegerField;
    Pub_BROWSE_SystemN_System: TStringField;
    Pub_BROWSE_SystemCap_System: TStringField;
    Label10: TLabel;
    Label11: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Pub_BROWSE_System_FormK_Form: TSmallintField;
    PButton: TPanel;
    BADD: TSpeedButton;
    BEDIT: TSpeedButton;
    BSAVE: TSpeedButton;
    BDEL: TSpeedButton;
    BCLOSE: TSpeedButton;
    BFILTER: TSpeedButton;
    BCANCEL: TSpeedButton;
    BPRINT: TSpeedButton;
    BCOLOR: TSpeedButton;
    BCALC: TSpeedButton;
    K_Form: TComboBox;
    procedure FillEdits1;
    procedure FillEdits2;
    procedure Fill_Insert_Parameters1;
    procedure Fill_Insert_Parameters2;
    procedure AllTabOrderChange(Sw: Boolean; Parent : TWinControl);
    procedure GoNext(Sw: Byte);
    procedure FormShow(Sender: TObject);
    procedure bExitClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure DSPub_BROWSE_SystemDataChange(Sender: TObject; Field: TField);
    procedure bCancelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bAddClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure Panel1Enter(Sender: TObject);
    procedure Panel2Enter(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure DSPub_BROWSE_System_FormDataChange(Sender: TObject; Field: TField);
    procedure bEditClick(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure Panel3Enter(Sender: TObject);
    procedure DSPub_BROWSE_System_Form_FieldDataChange(Sender: TObject;
      Field: TField);
    procedure N_FieldEnter(Sender: TObject);
    procedure Cap_SystemEnter(Sender: TObject);
    procedure BCLOSEClick(Sender: TObject);
    procedure N_FormEnter(Sender: TObject);
    procedure N_SystemEnter(Sender: TObject);
  private
    procedure Fill_Update_Parameters1;
    procedure Fill_Update_Parameters2;
    procedure Fill_Insert_Parameters3;
    procedure FillEdits3;
    procedure Fill_Update_Parameters3;
    procedure PubBROWSESystem(Srl_Role : Integer);
    procedure PubBROWSESystemForm(Srl_System : Integer);
    procedure PubBROWSESystemFormField(Srl_Form: Integer);
    { Private declarations }
  public
   FocusedPanel : String;
   Str : String;
   BookMark1 : Integer;
   swAddOrEdit : TState;
  end;

var
   F_System: TF_System;

implementation



{$R *.DFM}


procedure TF_System.PubBROWSESystem(Srl_Role : Integer);
begin
   NullAdoSPParameters(Pub_BROWSE_System);
   Pub_BROWSE_System.Open;
end;

procedure TF_System.PubBROWSESystemForm(Srl_System : Integer);
begin
   NullAdoSPParameters(Pub_BROWSE_System_Form);
   Pub_BROWSE_System_Form.Parameters.ParamByName('@Srl_System').Value := Srl_System;
   Pub_BROWSE_System_Form.Open;
end;

procedure TF_System.PubBROWSESystemFormField(Srl_Form : Integer);
begin
   NullAdoSPParameters(Pub_BROWSE_System_Form_Field);
   Pub_BROWSE_System_Form_Field.Parameters.ParamByName('@Srl_Form').Value := Srl_Form;
   Pub_BROWSE_System_Form_Field.Open;
end;


procedure TF_System.FillEdits1;
begin
   CommonFullEdits(Self, Pub_BROWSE_System);
   C_System.Text := Pub_BROWSE_SystemSrl_System.AsString;
end;


procedure TF_System.FillEdits2;
begin
   CommonFullEdits(Self, Pub_BROWSE_System_Form);
end;

procedure TF_System.FillEdits3;
begin
   CommonFullEdits(Self, Pub_BROWSE_System_Form_Field);
end;

procedure TF_System.Fill_Insert_Parameters1;
Begin
   NullAdoSPParameters(Pub_INSERT_System);

   if trim(C_System.Text) <> '' then
      Pub_INSERT_System.Parameters.ParamByName('@Srl_System_1').VALUE := StrToInt(C_System.Text);
   Pub_INSERT_System.Parameters.ParamByName('@N_System_2').VALUE := Trim(N_System.Text);
   Pub_INSERT_System.Parameters.ParamByName('@Cap_System_3').VALUE := Trim(Cap_System.Text);
End;

procedure TF_System.Fill_Update_Parameters1;
Begin
   NullAdoSPParameters(Pub_UPDATE_System);

   Pub_UPDATE_System.Parameters.ParamByName('@Srl_System_1').VALUE := Pub_BROWSE_SystemSrl_System.AsInteger;
   if trim(N_System.Text) <> '' then
      Pub_UPDATE_System.Parameters.ParamByName('@N_System_2').VALUE := Trim(N_System.Text);
   if trim(C_System.Text) <> '' then
      Pub_UPDATE_System.Parameters.ParamByName('@C_System_3').VALUE := StrToInt(C_System.Text);
   if trim(Cap_System.Text) <> '' then
      Pub_UPDATE_System.Parameters.ParamByName('@Cap_System').VALUE := Trim(Cap_System.Text);
End;



procedure TF_System.Fill_Insert_Parameters2;
Begin
   NullAdoSPParameters(Pub_INSERT_System_Form);

   Pub_INSERT_System_Form.Parameters.ParamByName('@Srl_System_1').VALUE := Pub_BROWSE_SystemSrl_System.AsInteger;
   if trim(N_Form.Text) <> '' then
      Pub_INSERT_System_Form.Parameters.ParamByName('@N_Form_2').VALUE := Trim(N_Form.Text);
   if trim(Cap_Form.text) <> '' then
      Pub_INSERT_System_Form.Parameters.ParamByName('@Cap_Form_3').VALUE := Trim(Cap_Form.Text);

   Pub_INSERT_System_Form.Parameters.ParamByName('@K_Form').VALUE := K_Form.ItemIndex;
End;

procedure TF_System.Fill_Insert_Parameters3;
Begin
   NullAdoSPParameters(Pub_INSERT_System_Form_Field);

   Pub_INSERT_System_Form_Field.Parameters.ParamByName('@Srl_Form_1').VALUE := Pub_BROWSE_System_FormSrl_Form.AsInteger;
   if trim(N_Field.Text) <> '' then
      Pub_INSERT_System_Form_Field.Parameters.ParamByName('@N_Field_2').VALUE := Trim(N_Field.Text);
   if trim(Cap_Field.Text) <> '' then
      Pub_INSERT_System_Form_Field.Parameters.ParamByName('@Cap_Field_3').VALUE := Trim(Cap_Field.Text);
End;


procedure TF_System.Fill_Update_Parameters2;
Begin
   NullAdoSPParameters(Pub_UPDATE_System_Form);

   Pub_UPDATE_System_Form.Parameters.ParamByName('@Srl_Form_1').VALUE := Pub_BROWSE_System_FormSrl_Form.AsInteger;
   if trim(N_Form.Text) <> '' then
      Pub_UPDATE_System_Form.Parameters.ParamByName('@N_Form_3').VALUE := Trim(N_Form.Text);
   if trim(Cap_Form.text) <> '' then
      Pub_UPDATE_System_Form.Parameters.ParamByName('@Cap_Form_4').VALUE := Trim(Cap_Form.Text);

   Pub_UPDATE_System_Form.Parameters.ParamByName('@K_Form').VALUE := K_Form.ItemIndex;
End;


procedure TF_System.Fill_Update_Parameters3;
Begin
   NullAdoSPParameters(Pub_UPDATE_System_Form_Field);

   Pub_UPDATE_System_Form_Field.Parameters.ParamByName('@Srl_Field_1').VALUE := Pub_BROWSE_System_Form_FieldSrl_Field.AsInteger;
   if trim(N_Field.Text) <> '' then
      Pub_UPDATE_System_Form_Field.Parameters.ParamByName('@N_Field_3').VALUE := Trim(N_Field.Text);
   if trim(Cap_Field.text) <> '' then
      Pub_UPDATE_System_Form_Field.Parameters.ParamByName('@Cap_Field_4').VALUE := Trim(Cap_Field.Text);
End;

procedure TF_System.AllTabOrderChange(Sw: Boolean; Parent : TWinControl);
    var i: Integer;
begin
    for i:=0 to ComponentCount-1 do
    begin
        if (Components[i] is Tedit) then
        if ((Components[i] as Tedit).Parent=Parent) then (Components[i] as Tedit).TabStop := Sw;
        if (Components[i] is TPanel) then (Components[i] as TPanel).TabStop := Not Sw;
    end;
end;



procedure TF_System.GoNext(Sw: Byte);
var
   TWinC_Back,TWinC_Next : TWinControl;
begin
    TWinC_Back := FindNextControl(ActiveControl,False,false,false);
    TWinC_Next := FindNextControl(ActiveControl,true,false,false);
    if sw=1 then
    begin
        if  not (Twinc_Back is TPanel)  then
        begin
            TWinC_Back.TabStop := True;
            SelectNext(ActiveControl,false,True);
            TWinC_Back.TabStop := False;
        end
        else
        begin
            AllTabOrderChange(True,ActiveControl.Parent);
            TWinC_Back.TabStop := false;
            SelectNext(ActiveControl,false,True);
            TWinC_Back.TabStop := true;
            AllTabOrderChange(False,ActiveControl.Parent);
        end;
    end
    else
    begin
        if  not (Twinc_Next is TPanel)  then
        begin
            TWinC_Next.TabStop := True;
            Perform(CM_DIALOGKEY,VK_TAB,0);
            TWinC_Next.TabStop := False;
        end
        else
        begin
            AllTabOrderChange(True,ActiveControl.Parent);
            TWinC_Next.TabStop := false;
            SelectFirst;
            TWinC_Next.TabStop := true;
            AllTabOrderChange(False,ActiveControl.Parent);
        end;
    End;
end;



procedure TF_System.FormShow(Sender: TObject);
begin
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   StatusBar.Panels[0].Text := '����� :' + Str; {+ Copy(DateTimetoStr(UDateTime),1,10);}
   StatusBar.Panels[1].Text := '��� �����:';
   StatusBar.Panels[4].Text := '����� ������� :';
   PubBROWSESystem(-1);
   Grid1.SetFocus;
end;



procedure TF_System.bExitClick(Sender: TObject);
begin
   Close;
end;

procedure TF_System.bSaveClick(Sender: TObject);
begin
   if (swAddOrEdit = sAdd) and (FocusedPanel = 'Panel1') then  // bAdd is pressed
   Begin
      Fill_Insert_Parameters1;
      TRY
         Pub_INSERT_System.ExecProc;
         BookMark1 := StrToInt(C_System.Text);
         PubBROWSESystem(-1);
         ChangeEnableButton(Self, TComponent(Sender).Name, swAddOrEdit, FocusedPanel);
         StatusBar.Panels[4].Text := '����� ������� :' +
            inttostr(Pub_BROWSE_System.RecordCount);
         Pub_BROWSE_System.Locate('Srl_System',BookMark1,[]);
         Grid1.SetFocus;
      EXCEPT

      End;
   End;

   if (swAddOrEdit = sEdit) and (FocusedPanel = 'Panel1') then  // bAdd is pressed
   Begin
      Fill_Update_Parameters1;
      TRY
         BookMark1 := Pub_BROWSE_SystemSrl_System.AsInteger;
         Pub_UPDATE_System.ExecProc;
         PubBROWSESystem(-1);
         Pub_BROWSE_System.Locate('Srl_System',BookMark1,[]);
         ChangeEnableButton(Self, TComponent(Sender).Name, swAddOrEdit, FocusedPanel);
         StatusBar.Panels[4].Text := '����� ������� :' +
            inttostr(Pub_BROWSE_System.RecordCount);
         Grid1.SetFocus;
      EXCEPT
      End;
   End;

   if (swAddOrEdit = sAdd) and (FocusedPanel = 'Panel2') then  // bAdd is pressed
   Begin
      Fill_Insert_Parameters2;
      TRY
         Pub_INSERT_System_Form.ExecProc;
         BookMark1 := Pub_INSERT_System_Form.Parameters[0].Value;
         PubBROWSESystemForm(Pub_BROWSE_SystemSrl_System.AsInteger);
         Pub_BROWSE_System_Form.Locate('Srl_Form',BookMark1,[]);
         ChangeEnableButton(Self, TComponent(Sender).Name, swAddOrEdit, FocusedPanel);
         FillEdits2;
         StatusBar.Panels[4].Text := '����� ������� :' +
            inttostr(Pub_BROWSE_System_Form.RecordCount);
      EXCEPT
         IF Pub_INSERT_System_Form.Parameters.Count>0 THEN
         Begin
            IF Pub_INSERT_System_Form.Parameters[0].Value=2627 THEN
               ShowMessage('���� �� ������')
            else IF Pub_INSERT_System_Form.Parameters[0].Value=547 THEN
               ShowMessage('������ ��� �� ��� ����� ��� ����')
            else
               ShowMessage('��� �� ��� �������');
         End;
      End;
   End;

   if (swAddOrEdit = sEdit) and (FocusedPanel = 'Panel2') then  // bAdd is pressed
   Begin
      Fill_Update_Parameters2;
      TRY
         BookMark1 := Pub_BROWSE_System_FormSrl_Form.AsInteger;
         Pub_UPDATE_System_Form.ExecProc;
         PubBROWSESystemForm(Pub_BROWSE_SystemSrl_System.AsInteger);
         Pub_BROWSE_System_Form.Locate('Srl_Form',BookMark1,[]);
         ChangeEnableButton(Self, TComponent(Sender).Name, swAddOrEdit, FocusedPanel);
         FillEdits2;
         StatusBar.Panels[4].Text := '����� ������� :' +
            inttostr(Pub_BROWSE_System_Form.RecordCount);
      EXCEPT
         IF Pub_UPDATE_System_Form.Parameters.Count>0 THEN
         Begin
            IF Pub_UPDATE_System_Form.Parameters[0].Value=2627 THEN
               ShowMessage('���� �� ������')
            else IF Pub_UPDATE_System_Form.Parameters[0].Value=547 THEN
               ShowMessage('������ ��� �� ��� ����� ��� ����')
            else
               ShowMessage('��� �� ��� �������');
         End;
      End;
   End;

   if (swAddOrEdit = sAdd) and (FocusedPanel = 'Panel3') then  // bAdd is pressed
   Begin
      Fill_Insert_Parameters3;
      TRY
         Pub_INSERT_System_Form_Field.ExecProc;
         BookMark1 := Pub_INSERT_System_Form_Field.Parameters[0].Value;
         PubBROWSESystemFormField(Pub_BROWSE_System_FormSrl_Form.AsInteger);
         Pub_BROWSE_System_Form_Field.Locate('Srl_Field',BookMark1,[]);
         ChangeEnableButton(Self, TComponent(Sender).Name, swAddOrEdit, FocusedPanel);
         FillEdits3;
         StatusBar.Panels[4].Text := '����� ������� :' +
            inttostr(Pub_BROWSE_System_Form_Field.RecordCount);
      EXCEPT
         IF Pub_INSERT_System_Form_Field.Parameters.Count>0 THEN
         Begin
            IF Pub_INSERT_System_Form_Field.Parameters[0].Value=2627 THEN
               ShowMessage('���� �� ������')
            else IF Pub_INSERT_System_Form_Field.Parameters[0].Value=547 THEN
               ShowMessage('������ ��� �� ��� ����� ��� ����')
            else
               ShowMessage('��� �� ��� �������');
         End;
      End;
   End;

   if (swAddOrEdit = sEdit) and (FocusedPanel = 'Panel3') then  // bAdd is pressed
   Begin
      Fill_Update_Parameters3;
      TRY
         BookMark1 := Pub_BROWSE_System_Form_FieldSrl_Field.AsInteger;
         Pub_UPDATE_System_Form_Field.ExecProc;
         PubBROWSESystemFormField(Pub_BROWSE_System_FormSrl_Form.AsInteger);
         Pub_BROWSE_System_Form_Field.Locate('Srl_Field',BookMark1,[]);
         ChangeEnableButton(Self, 'bSave', swAddOrEdit, FocusedPanel);
         FillEdits3;
         StatusBar.Panels[4].Text := '����� ������� :' +
            inttostr(Pub_BROWSE_System_Form_Field.RecordCount);
      EXCEPT
         IF Pub_UPDATE_System_Form.Parameters.Count>0 THEN
         Begin
            IF Pub_UPDATE_System_Form_Field.Parameters[0].Value=2627 THEN
               ShowMessage('���� �� ������')
            else IF Pub_UPDATE_System_Form_Field.Parameters[0].Value=547 THEN
               ShowMessage('������ ��� �� ��� ����� ��� ����')
            else
               ShowMessage('��� �� ��� �������');
         End;
      End;
   End;

end;


procedure TF_System.DSPub_BROWSE_SystemDataChange(Sender: TObject;
  Field: TField);
begin
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FillEdits1;
   if Pub_BROWSE_System.RecordCount <> 0 then
      PubBROWSESystemForm(Pub_BROWSE_SystemSrl_System.AsInteger)
   else
   begin
      Pub_BROWSE_System_Form.Close;
      Pub_BROWSE_System_Form_Field.Close;
   end;

end;

procedure TF_System.bCancelClick(Sender: TObject);
begin
   FillEdits1;
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
end;


procedure TF_System.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (key=VK_RETURN) and (Shift=[ssshift]) then
        GoNext(1);
    if (key=VK_RETURN) and (Shift=[]) then
    begin
        Key:=0;
        GoNext(0);
    end;
    if Key = VK_F4 then
        if bAdd.Enabled = true then
            bAddClick(bAdd);
    if Key = VK_F8 then
        if bDel.Enabled = true then
            bDelClick(bDel);
    if Key = VK_F5 then
        if bEdit.Enabled = true then
            bEditClick(bEdit);
    if Key = VK_F2 then
        if bSave.Enabled = true then
            bSaveClick(bSave);
    if Key = VK_ESCAPE then
        if bCancel.Enabled = true then
            bCancelClick(bCancel);

end;

procedure TF_System.bAddClick(Sender: TObject);
begin
   if (FocusedPanel='Panel1') and (C_System.Tag Mod 6 = 0) then
      C_System.SetFocus
   else
   if (FocusedPanel='Panel2') and (N_Form.Tag Mod 6 = 0) then
      N_Form.SetFocus
   else
   if (FocusedPanel='Panel3') and (N_Field.Tag Mod 6 = 0) then
      N_Field.SetFocus;
   ChangeEnableButton(Self, 'bAdd', swAddOrEdit, FocusedPanel);
end;


procedure TF_System.bDelClick(Sender: TObject);
begin
   if FocusedPanel = 'Panel1' then
   Begin
      if Pub_BROWSE_System.RecordCount = 0 then Exit;
      if MessageDlg('��� ��� ����� ������ ��� �� ����� �� ����',
       mtConfirmation,[mbYes,mbNo],0) = mrNo then
          Exit;

      Pub_DELETE_System.Close;
      Pub_DELETE_System.Parameters.ParamByName('@Srl_System_1').VALUE := Pub_BROWSE_SystemSrl_System.AsInteger;
      TRY
         Pub_DELETE_System.ExecProc;
      EXCEPT
         IF Pub_DELETE_System.Parameters.Count>0 THEN
            ShowMessage('��� �� ��� �������');
      End;

      BookMark1 := Pub_BROWSE_System.RecNo;
      PubBROWSESystem(-1);
      if Pub_BROWSE_System.RecordCount > 0 then
         Pub_BROWSE_System.RecNo := BookMark1;
   End;
   ///////////////////////////////
   if FocusedPanel = 'Panel2' then
   Begin
      if Pub_BROWSE_System_Form.RecordCount = 0 then Exit;
      if MessageDlg('��� ��� ����� ������ ��� �� ����� �� ����',
       mtConfirmation,[mbYes,mbNo],0) = mrNo then
         Exit;

      Pub_DELETE_System_Form.Close;
      Pub_DELETE_System_Form.Parameters.ParamByName('@Srl_Form_1').VALUE := Pub_BROWSE_System_FormSrl_Form.AsInteger;
      TRY
         Pub_DELETE_System_Form.ExecProc;
      EXCEPT
         IF Pub_DELETE_System_Form.Parameters.Count>0 THEN
            ShowMessage('��� �� ��� �������');
      End;

      BookMark1 := Pub_BROWSE_System_Form.RecNo;
      PubBROWSESystemForm(Pub_BROWSE_SystemSrl_System.AsInteger);
      if Pub_BROWSE_System_Form.RecordCount > 0 then
         Pub_BROWSE_System_Form.RecNo := BookMark1;

   End;
   ///////////////////////////////
   if FocusedPanel = 'Panel3' then
   Begin
      if Pub_BROWSE_System_Form_Field.RecordCount = 0 then Exit;
      if MessageDlg('��� ��� ����� ������ ��� �� ����� �� ����',
       mtConfirmation,[mbYes,mbNo],0) = mrNo then
         Exit;

      Pub_DELETE_System_Form_Field.Close;
      Pub_DELETE_System_Form_Field.Parameters.ParamByName('@Srl_Field_1').VALUE := Pub_BROWSE_System_Form_FieldSrl_Field.AsInteger;
      TRY
         Pub_DELETE_System_Form_Field.ExecProc;
      EXCEPT
         IF Pub_DELETE_System_Form_Field.Parameters.Count>0 THEN
            ShowMessage('��� �� ��� �������');
      End;

      BookMark1 := Pub_BROWSE_System_Form_Field.RecNo;
      PubBROWSESystemFormField(Pub_BROWSE_System_FormSrl_Form.AsInteger);
      if Pub_BROWSE_System_Form_Field.RecordCount > 0 then
         Pub_BROWSE_System_Form_Field.RecNo := BookMark1;
   End;
end;


procedure TF_System.Panel1Enter(Sender: TObject);
begin
   FocusedPanel := TComponent(Sender).Name;
//   if (Grid1.Enabled) and (Grid1.Visible) then
//      Grid1.SetFocus;
   Panel1.Color := $00A7B097;
   Panel2.Color := clMenu;  
   Panel3.Color := clMenu;  
   StatusBar.Panels[4].Text := '����� ������� :' +
    inttostr(Pub_BROWSE_System.RecordCount);
   StatusBar.Panels[3].Text := '������� ����� ���� ��� :' +
    '���� �������';
end;

procedure TF_System.Panel2Enter(Sender: TObject);
begin
   FocusedPanel := TComponent(Sender).Name;
   if (Grid2.Enabled) and (Grid2.Visible) then
      Grid2.SetFocus;
   Panel1.Color := clMenu;
   Panel2.Color := $00A7B097;
   Panel3.Color := clMenu;
   if Pub_BROWSE_System_Form.Active then
      StatusBar.Panels[4].Text := '����� ������� :' +
         inttostr(Pub_BROWSE_System_Form.RecordCount)
   else
      StatusBar.Panels[4].Text := '����� ������� :' +
         inttostr(0);

   StatusBar.Panels[3].Text := '������� ����� ���� ��� :' +
    '������ ����� �� �����';
end;

procedure TF_System.Panel2Click(Sender: TObject);
begin
   Panel2Enter(Sender);
end;

procedure TF_System.Panel1Click(Sender: TObject);
begin
   Panel1Enter(Sender);
end;

procedure TF_System.DSPub_BROWSE_System_FormDataChange(Sender: TObject;
  Field: TField);
begin
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FillEdits2;
   if Pub_BROWSE_System_Form.RecordCount <> 0 then
      PubBROWSESystemFormField(Pub_BROWSE_System_FormSrl_Form.AsInteger)
   else
      Pub_BROWSE_System_Form_Field.Close;
end;

procedure TF_System.bEditClick(Sender: TObject);
Var
   Ret : Boolean;
begin
   if (FocusedPanel = 'Panel1') then
   Begin
      // ǐ� ���� ���� ��� ������ ����
      if Pub_BROWSE_System.RecordCount=0 then
         exit
      else
      begin
         ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
         if (C_System.Tag Mod 6 = 0) then
            C_System.SetFocus;
      end
   End;
   ///////////////////////////////////
   if FocusedPanel = 'Panel2' then
   Begin
      if Pub_BROWSE_System_Form.RecordCount=0 then
         Exit
      else
      begin
         ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
         if (N_Form.Tag Mod 6 = 0) then
            N_Form.SetFocus;
      End;
   End;
   ///////////////////////////////////
   if FocusedPanel = 'Panel3' then
   Begin
      if Pub_BROWSE_System_Form.RecordCount=0 then
         Exit
      else
      begin
         if (N_Field.Tag Mod 6 = 0) then
            N_Field.SetFocus;
         ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
      end;
   End;
end;

procedure TF_System.Panel3Click(Sender: TObject);
begin
   Panel3Enter(Sender);
end;

procedure TF_System.Panel3Enter(Sender: TObject);
begin
   FocusedPanel := 'Panel3';
   if (Grid3.Enabled) and (Grid3.Visible) then
      Grid3.SetFocus;
   Panel3.Color := $00A7B097;
   Panel2.Color := clMenu;
   Panel1.Color := clMenu;

   if Pub_BROWSE_System_Form_Field.Active then
      StatusBar.Panels[4].Text := '����� ������� :' +
         inttostr(Pub_BROWSE_System_Form_Field.RecordCount)
   else
      StatusBar.Panels[4].Text := '����� ������� :' +
         inttostr(0);
   StatusBar.Panels[3].Text := '������� ����� ���� ��� :' +
    '����� � �������� ����� �� �����';

end;

procedure TF_System.DSPub_BROWSE_System_Form_FieldDataChange(
  Sender: TObject; Field: TField);
begin
   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FillEdits3;
end;

procedure TF_System.N_FieldEnter(Sender: TObject);
begin
   LoadKeyboardLayout(PChar('00000409'), KLF_ACTIVATE);
end;

procedure TF_System.Cap_SystemEnter(Sender: TObject);
begin
   LoadKeyboardLayout(PChar('00000401'), KLF_ACTIVATE);
end;

procedure TF_System.BCLOSEClick(Sender: TObject);
begin
   Close;
end;

procedure TF_System.N_FormEnter(Sender: TObject);
begin
   LoadKeyboardLayout(PChar('00000409'), KLF_ACTIVATE);
end;

procedure TF_System.N_SystemEnter(Sender: TObject);
begin
   LoadKeyboardLayout(PChar('00000409'), KLF_ACTIVATE);
end;

end.
