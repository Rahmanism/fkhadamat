unit U_Common1111;

//F_Common.SystemConnection
//F_Common.PermisionConnection
//PCheckBoxes
//PCheckBoxes2
//PCheckBoxes3
//SrlSalJari

interface

uses
  Windows, Variants,Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, stdctrls, buttons, extctrls, comctrls, Menus, Mask, IniFiles,
  ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, ppDB, ppDBPipe, ppComm,
  ppRelatv, ppProd, ppReport, DBCtrls, ppDBBDE , Grids, DBGrids, DBTables, 
  FileCtrl,pbnumedit  ,ppTypes, ppVar, ExtDlgs;

Const
    ValidKey : Set of Char = ['0'..'9',#13,#8,#37,#32,#46,#39,'=','<','>'];
    ValidDigitAlphabet : Set of Char = ['0'..'9','a'..'z','A'..'Z','�'..'�','/','.',' ','-','_'];
    NumberType : Set of TFieldType = [ftCurrency, FtInteger, ftFloat, ftSmallint, ftWord, ftAutoInc, ftBCD];
    StringType : Set of TFieldType = [ftString, ftWideString, ftFixedChar];

    MonthNameArray : Array[1..12] of string = (
            '�������',            '��������',            '�����',
            '���',            '�����',            '������',
            '���',            '����',            '���',
            '��',            '����',            '�����' );

    DayNameArray : Array[1..7] of string = (
            '����',            '������',            '������',
            '�� ����',            '���� ����',            '��� ����',
            '����');

   DefaultPButtonColor = $00FF8080;
   DefaultPanelActiceColor = $00FF8080;
   DefaultPanelIdelColor = $00FF8080;

   SrlDataSetCity = 1;
   SrlDataSetDegreeU = 16;
   SrlDataSetReshtehU = 3;
   SrlDataSetDegreeH = 17;
   SrlDataSetReshtehH = 5;
   SrlDataSetGrpDegH = 18;
   SrlDataSetKEsar = 10;
   SrlDataSetOstan = 12;
   SrlDataSetHazineh = 15;
   SrlDataSetDocumWomen = 100;
   SrlDataSetPlace = 200;
   SrlDataSetAction = 202;
   SrlDataSetJens = 203;
   SrlDataSetSemat = 204;
   SrlDataSetPass = 205;
   SrlDataSetSchool = 9;
   SrlDataSetSchoolMen = 13;
   SrlDataSetSchoolWomen = 14;
   SrlDataSetSchoolSonat = 8;
   SrlDataSetSchoolList = 9202;
   SrlDataSetKService = 19;
   SrlDataSetKProperty = 20;
   SrlDataSetKSkill = 21;
   SrlDataSetKResearch = 22;
   SrlDataSetDeen = 24;
   SrlDataSetLang = 25;
   SrlDataSetCountry = 26;
   SrlDataSetMelli = 27;
   SrlDataSetMadrak = 29;
   SrlDataSetKNezam = 28;
   SrlDataSetTitle = 23;
   SrlDataSetStudent = 100;
   SrlDataSetStudent2 = 903;
   SrlDataSetLetter = 400;
   SrlDataSetSource = 1000;
   SrlDataSetDiscription = 9000;
   SrlDataSetDegHSonat = 904;
   SrlDataSetDprt = 907;
   SrlDataSetCycle = 910;
   SrlDataSetBook = 9095;
   SrlDataSetKJob = 9093;
   SrlDataSetKSchoolService = 912;
   SrlDataSetKPayShahr = 913;
   SrlDataSetDegSonat = 915;
   SrlDataSetPost = 9104;
   SrlDataSetTeacher = -1;
   SrlDataSetStudentShie = 101;
   SrlDataSetKPerson = 9208;

   SrlObjectPazireshPerson = 1;
   SrlObjectPazireshStudent = 2;
   SrlObjectSonat = 3;
   SrlObjectBedehi = 450;
   SrlDataSetPerson = 9090;
   SrlDataSetKLevelDarolhefz = 9098;
   SrlDataSetKArzyabi = 9203;

type
   T_Field = Record
      NFiled : String;
      CapField : String;
      KindField : integer;
      SizeField : integer;
      TableName : String;
   end;

type
    TState = (sView, sAdd, sEdit, sFilter);

type
    TKJens = (KjAll, kjMen, kjWomen);

type
    TKTahol = (sOne, sMarrid );

type
    TKSchool = (ksAll, ksMen, ksWomen, ksSonat);

type
    TKPer = (kpAll, kpMotmarkez, kpCommon, kpOther, kpSonat, kpNotIran);

type                  
    TKTitle = (ktAll, ktHalf, ktSali, ktCommon, ktShafahi, ktWomen,
               ktSonat1, kt38, ktCommon2, ktCommon3, ktCommon4, ktCommon5,
               ktCommon6, ktCommon7, ktCommon8, kt7_10, kt8_10,
               ktSonat2, ktSonat3, ktSonat4, ktSonat5, ktSonat6);

type
  TF_Common11111 = class(TDataModule)
    PermisionConnection: TADOConnection;
    Pub_BROWSE_Per: TADOStoredProc;
    Pub_BROWSE_Role_Field3: TADOStoredProc;
    Pub_BROWSE_Role_Field1: TADOStoredProc;
    Pub_BROWSE_Per1: TADOStoredProc;
    Pub_BROWSE_Per1srl_per1: TIntegerField;
    Pub_BROWSE_Per1srl_User: TIntegerField;
    Pub_BROWSE_Per1n_per: TStringField;
    Pub_BROWSE_Per1family: TStringField;
    Pub_BROWSE_Per1n_hazineh: TStringField;
    Pub_INSERT_System_Form_Field1: TADOStoredProc;
    sp_GetCurrentFarsiDate: TADOStoredProc;
    sp_Find_T_Table: TADOStoredProc;
    Delete1: TADOStoredProc;
    Sp_Get_Max: TADOStoredProc;
    Insert1: TADOStoredProc;
    Update1: TADOStoredProc;
    Browse1: TADOStoredProc;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TStringField;
    Browse2: TADOStoredProc;
    Browse2FSerial: TAutoIncField;
    Browse2FCode: TIntegerField;
    Browse2FName: TStringField;
    SystemConnection: TADOConnection;
    Pub_InitUser: TADOStoredProc;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField3: TIntegerField;
    StringField3: TStringField;
    INSERT_Form_Proc: TADOStoredProc;
    sp_Get_Max_T_Table2: TADOStoredProc;
    sp_Find_T_Table2: TADOStoredProc;
    Session1: TSession;
    Query1: TQuery;
    sp_FullCycleInfo: TADOStoredProc;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField4: TStringField;
    StringField5: TStringField;
    IntegerField6: TIntegerField;
    StringField6: TStringField;
    sp_Find_T_Table3: TADOStoredProc;
    SP_GetSystemVersion: TADOStoredProc;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    StringField10: TStringField;
    StringField11: TStringField;
    IntegerField12: TIntegerField;
    StringField12: TStringField;
    sp_Get_Max_T_Table3: TADOStoredProc;
    SP_GetSystemInfo: TADOStoredProc;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    StringField13: TStringField;
    StringField14: TStringField;
    IntegerField15: TIntegerField;
    StringField15: TStringField;
    Pub_BROWSE_Form_Fields: TADOStoredProc;
    Pub_BROWSE_Form_FieldsN_Field: TStringField;
    Pub_BROWSE_Form_FieldsK_Access: TIntegerField;
    sp_GetNextDate: TADOStoredProc;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    IntegerField9: TIntegerField;
    StringField9: TStringField;
    QueryConnection: TADOConnection;
    sp_SetUserWork: TADOStoredProc;
    Timer1: TTimer;
    Delete3: TADOStoredProc;
    sp_GetCurrentServerTime: TADOStoredProc;
    sp_GetSystemPermision: TADOStoredProc;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    StringField16: TStringField;
    StringField17: TStringField;
    IntegerField18: TIntegerField;
    StringField18: TStringField;
    insert_T_Letter_System: TADOStoredProc;
    Browse_Pub_Reports: TADOStoredProc;
    Browse_Pub_ReportsVal_Report: TMemoField;
    OpenPictureDialog1: TOpenPictureDialog;
    insert_T_Student_Pic_1: TADOStoredProc;
    BROWSE_Student_Pic: TADOStoredProc;
    BROWSE_Student_PicPic_Student: TBlobField;
    BROWSE_Student_PicFSerial: TAutoIncField;
    Browse_T_StudentOne: TADOStoredProc;
    Browse_T_StudentOneFSerial: TAutoIncField;
    Browse_T_StudentOneFName: TStringField;
    Browse_T_StudentOneFName1: TStringField;
    Browse_T_StudentOneFName2: TStringField;
    Browse_T_StudentOneFCode: TIntegerField;
    Browse_T_StudentOneK_Docum: TIntegerField;
    Browse_T_StudentOneK_StateDocum: TIntegerField;
    Browse_T_StudentOneNum_Per: TIntegerField;
    Browse_T_StudentOneNum_Cart: TIntegerField;
    Browse_T_StudentOneNum_Melli: TStringField;
    Browse_T_StudentOneSrl_KPer: TIntegerField;
    Browse_T_StudentOneNum_Docum: TIntegerField;
    Browse_T_StudentOneInput_Sal: TIntegerField;
    Browse_T_StudentOneShohrat: TStringField;
    Browse_T_StudentOneN_Father: TStringField;
    Browse_T_StudentOneNum_Shen: TStringField;
    Browse_T_StudentOneD_Brith: TStringField;
    Browse_T_StudentOneL_Brith: TStringField;
    Browse_T_StudentOneSrl_Sodoor: TIntegerField;
    Browse_T_StudentOneD_Sodoor: TStringField;
    Browse_T_StudentOneSrl_DegU: TIntegerField;
    Browse_T_StudentOneD_DegU: TStringField;
    Browse_T_StudentOneSrl_ReshU: TIntegerField;
    Browse_T_StudentOneSrl_DegH: TIntegerField;
    Browse_T_StudentOneSrl_ReshH: TIntegerField;
    Browse_T_StudentOneAvrg: TFloatField;
    Browse_T_StudentOneK_Maskan: TIntegerField;
    Browse_T_StudentOneK_Jens: TIntegerField;
    Browse_T_StudentOneK_Tahol: TIntegerField;
    Browse_T_StudentOneD_Marrid: TStringField;
    Browse_T_StudentOneK_Lebas: TIntegerField;
    Browse_T_StudentOneK_Moamam: TIntegerField;
    Browse_T_StudentOneD_Moamam: TStringField;
    Browse_T_StudentOneNum_Child: TIntegerField;
    Browse_T_StudentOneNum_Takafol: TIntegerField;
    Browse_T_StudentOneIs_Worker: TIntegerField;
    Browse_T_StudentOneK_Gharardad: TIntegerField;
    Browse_T_StudentOneP_Salary: TBCDField;
    Browse_T_StudentOneCurWork: TStringField;
    Browse_T_StudentOneFather_Work: TStringField;
    Browse_T_StudentOneHome_Adress: TStringField;
    Browse_T_StudentOneSrl_City: TIntegerField;
    Browse_T_StudentOneC_Post: TStringField;
    Browse_T_StudentOneHome_Tel: TStringField;
    Browse_T_StudentOneSrl_KNezam: TIntegerField;
    Browse_T_StudentOneD_Ezam: TStringField;
    Browse_T_StudentOneD_MohlatKNezam: TStringField;
    Browse_T_StudentOneWork_Adress: TStringField;
    Browse_T_StudentOneWork_Tel: TStringField;
    Browse_T_StudentOneC_Classe: TStringField;
    Browse_T_StudentOneD_Classe: TStringField;
    Browse_T_StudentOneD_StartStudy: TStringField;
    Browse_T_StudentOneD_Compelete: TStringField;
    Browse_T_StudentOneD_EndStudy: TStringField;
    Browse_T_StudentOneDis_Per: TStringField;
    Browse_T_StudentOneDay_Jebhe: TIntegerField;
    Browse_T_StudentOneDay_Esarat: TIntegerField;
    Browse_T_StudentOneDay_Zendan: TIntegerField;
    Browse_T_StudentOneIs_Basij: TIntegerField;
    Browse_T_StudentOneDay_Basij: TIntegerField;
    Browse_T_StudentOnePrc_Janbazi: TIntegerField;
    Browse_T_StudentOneExam_Mark: TFloatField;
    Browse_T_StudentOneDialog_Mark: TFloatField;
    Browse_T_StudentOneRank: TIntegerField;
    Browse_T_StudentOneC_KNezam: TIntegerField;
    Browse_T_StudentOneN_KNezam: TStringField;
    Browse_T_StudentOneC_Sodoor: TIntegerField;
    Browse_T_StudentOneN_Sodoor: TStringField;
    Browse_T_StudentOneC_DegU: TIntegerField;
    Browse_T_StudentOneN_DegU: TStringField;
    Browse_T_StudentOneC_ReshU: TIntegerField;
    Browse_T_StudentOneN_ReshU: TStringField;
    Browse_T_StudentOneC_ReshH: TIntegerField;
    Browse_T_StudentOneN_ReshH: TStringField;
    Browse_T_StudentOneC_DegH: TIntegerField;
    Browse_T_StudentOneN_DegH: TStringField;
    Browse_T_StudentOneC_City: TIntegerField;
    Browse_T_StudentOneN_City: TStringField;
    Browse_T_StudentOneC_KPer: TIntegerField;
    Browse_T_StudentOneN_KPer: TStringField;
    Browse_T_StudentOneSrl_SourceInput: TIntegerField;
    Browse_T_StudentOneC_SourceInput: TIntegerField;
    Browse_T_StudentOneN_SourceInput: TStringField;
    Browse_T_StudentOneSrl_Melli: TIntegerField;
    Browse_T_StudentOneC_Melli: TIntegerField;
    Browse_T_StudentOneN_Melli: TStringField;
    Browse_T_StudentOneSrl_Deen: TIntegerField;
    Browse_T_StudentOneC_Deen: TIntegerField;
    Browse_T_StudentOneN_Deen: TStringField;
    Browse_T_StudentOneSrl_Univer: TIntegerField;
    Browse_T_StudentOneC_Univer: TIntegerField;
    Browse_T_StudentOneN_Univer: TStringField;
    Browse_T_StudentOneSrl_School: TIntegerField;
    Browse_T_StudentOneC_School: TIntegerField;
    Browse_T_StudentOneN_School: TStringField;
    Browse_T_StudentOneSrl_KEsar: TIntegerField;
    Browse_T_StudentOneC_KEsar: TIntegerField;
    Browse_T_StudentOneN_KEsar: TStringField;
    Browse_T_StudentOneSrl_GroupShahr: TAutoIncField;
    Browse_T_StudentOneC_GroupShahr: TIntegerField;
    Browse_T_StudentOneN_GroupShahr: TStringField;
    Browse_T_StudentOneIs_Check: TIntegerField;
    Browse_T_StudentOneIs_Active: TIntegerField;
    Browse_T_StudentOneIs_EmamJam: TIntegerField;
    Browse_T_StudentOneIs_EmamJom: TIntegerField;
    Browse_T_StudentOneSrl_DprtDocum: TIntegerField;
    Browse_T_StudentOneD_EtebarResearch: TStringField;
    Browse_T_StudentOneC_DprtDocum: TIntegerField;
    Browse_T_StudentOneN_DprtDocum: TStringField;
    Browse_T_StudentOneC_Hozeh: TIntegerField;
    Browse_T_StudentOneIs_Mashmool: TIntegerField;
    Browse_T_StudentOneStrIs_Mashmool: TStringField;
    Browse_T_StudentOneIs_CheckMashmool: TIntegerField;
    Browse_T_StudentOneStrIs_CheckMashmool: TStringField;
    Browse_T_StudentOneIs_CheckResearch: TIntegerField;
    Browse_T_StudentOneStrIs_CheckResearch: TStringField;
    Browse_T_StudentOneIs_CheckPaziresh: TIntegerField;
    Browse_T_StudentOneStrIs_CheckPaziresh: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    Procedure UserCanAccessField(NObject, NParent : String; var K_Access : integer);
    procedure UserCanAccessSystem;
    procedure MyFormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDBGridTitleClick(Column: TColumn);
    procedure SystemConnectionWillExecute(Connection: TADOConnection;
      var CommandText: WideString; var CursorType: TCursorType;
      var LockType: TADOLockType; var CommandType: TCommandType;
      var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
      const Command: _Command; const Recordset: _Recordset);
    procedure SystemConnectionExecuteComplete(Connection: TADOConnection;
      RecordsAffected: Integer; const Error: Error;
      var EventStatus: TEventStatus; const Command: _Command;
      const Recordset: _Recordset);
    procedure DataModuleDestroy(Sender: TObject);
    procedure SystemConnectionAfterConnect(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    procedure FormDBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure FormCALCClick(Sender: TObject);
    procedure FormCLOSEClick(Sender: TObject);
    procedure FormFILTERClick(Sender: TObject);
    procedure FormCOLORClick(Sender: TObject);
    procedure FormPRINTClick(Sender: TObject);
    procedure EditSearchExit(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure EditFilterChange(Sender: TObject);
    procedure EditFilter2Change(Sender: TObject);
    Function  GetSystemVersion : integer;
    procedure GetSystemInfo;
    procedure ReadFromIniFile;
    procedure InitUser;
    procedure InitCycle;
    procedure InitSystem;
    procedure PerImagePrint(Sender: TObject);
    procedure CompanyMarkImagePrint(Sender: TObject);
    procedure ppLabelD_CurrentGetText(Sender: TObject; var Text: String);

    { Private declarations }
  public
    { Public declarations }
  end;

procedure BackUpDB;
procedure ChangePassword;
procedure EmptyFieldList;

procedure FilterDBGrid(MyDBGrid : TDBGrid);
procedure FilterDBGrid2(MyDBGrid : TDBGrid);
procedure FilterDBGrid3(MyDBGrid : TDBGrid);
procedure PrintDBGrid(MyDBGrid : TDBGrid);
procedure SearchDBGrid(MyDBGrid : TDBGrid);

procedure ChangeColorComponent(MyComp : TComponent);
procedure ChangeEnablePanel(Sender: TObject; sw : Boolean; FocusedPanel : String);
procedure ChangeReadOnlyPanel(Sender: TObject; sw : Boolean; FocusedPanel : String);
procedure EmptyEdits(Sender: TObject; FocusedPanel : String);
procedure Fill_Tags(Sender: TObject);
procedure VisibleComponents(Sender: TObject);
procedure EnableComponents(Sender: TObject);
procedure ReadOnlyComponents(Sender: TObject);
procedure ChangeEnableButton(Sender: TObject; N : String; var swAddOrEdit : TState; FocusedPanel : String);
procedure AddComponentToTable(Sender: TObject);
procedure AddFormSPToDB(NForm, CapForm, NSP, CapSP : String);
procedure ShowErrorMessage(MessageID : integer);

procedure PreviewReport(ppReport1 : TppReport);overload;
procedure PreviewReport(SrlReport : integer; MyDataSource : TDataSource; IsOneRecord : integer);overload;

procedure FindStudent;
procedure ShowFormOption(N_Form : String; Var FormPMainColor1, FormPMainColor2, FormPButtonColor : TColor);
procedure ShowCodingForm(FormTableName, FormCaption, FormCodeName : String);overload;
procedure ShowCodingForm(SrlDataSet : integer);overload;
function Is_DeleteRecord : boolean;
procedure ShowDynamicQueryForm(SrlObject : integer);
procedure AddFieldList(FieldKind, FieldSize : integer; TableName, FieldName, FieldCaption : String);


procedure EditCodeChange(MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; MySerial2 : integer; swState : TState; TableName , MyKindFieldName : String);overload;
procedure EditCodeChange(MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; swState : TState; TableName : String); overload;
procedure EditCodeChange(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet : integer);overload ;
procedure EditCodeChange(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;

procedure EditCodeExit(swState : TState; MyEdit : TEdit; MyLabel : TLabel);

procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; MySerial2, C_A_F : integer;   SpBrowseName, MyKindFieldName : String);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; C_A_F : integer;   SpBrowseName : String);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; SrlDataSet : integer);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;

procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; MySerial2, C_A_F : integer; TableName, MyKindFieldName : String);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; C_A_F : integer; TableName : String);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet : integer);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,   Val_Param4 : integer);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,   Val_Param4 : integer);overload;
procedure SBFindClickPer(swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer);

procedure EditCodeFind(var Key : Char; MyEdit : TEdit; MySerialKind, FieldCount : integer; TableName : String);

procedure NullAdoSPParameters(MyAdoSP : TADOStoredProc);
procedure BrowseRecord1(MyForm : TObject; TableName : String);
procedure BrowseRecord2(MyForm : TObject; TableName, FieldParam : String; KindParam : integer);
function  GetMaxCodeRecord(TableName, MyKindFieldName : String; MyKindFieldValue : integer) : String;overload;
function  GetMaxCodeRecord(TableName : String) : String; overload;
function  GetMaxCodeRecord(SrlDataSet : integer) : String; overload;
function  GetMaxCodeRecord(SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer) : String;overload;

procedure InsertRecord1(MyForm : TObject; TableName, MyCode, MyName : String);
procedure UpdateRecord1(TableName, MyCode, MyName : String);
procedure DeleteRecord1(MyForm : TObject; TableName : String; SrlRecord : integer; MyBrowse : TADOStoredProc);overload;
procedure DeleteRecord1(SrlDataSet, SrlRecord : integer; MyBrowse : TADOStoredProc);overload;
procedure MakeFilterString(KindField : Integer; ComboBIndex : integer; FieldName : String; FromV : String; ToV : String; var Filter : String);
function MakeFilterStringOne(KindField, ComboBIndex : integer; FieldName, FromV, ToV : String) : String;
procedure ChangeActivePanel(Sender: TObject; var FocusedPanel : String; Panel1 : String; ActicePanelColor, IdelPanelColor : TColor);
procedure AllTabOrderChange(Sender: TObject; Sw: Boolean; Parent : TWinControl);

Function GetNextDate(MyDate : String; MyDif : integer): String;
function IsCorrectDate(Date1 : String): integer;
function GetExtractStr(Str1 : String): String;
function CorrectString(Str1 : String): String;
function Number2Str(MyPrice : Currency) : String;
function Number2StrRil(MyPrice : Currency) : String;
function Number2StrDecimal(MyNumBer : real) : String;
procedure FormShowDM(MyForm : TObject; K_Form : integer; Is_Show : boolean);
function ShowFilter(var SqlFilter : String): Boolean;
procedure SortDBGrid1(MyColumn : TColumn);

procedure EditCodeKeyPress2(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; MySerial2, C_A_F : integer;
   SpBrowseName : String);

procedure SBFindClick2(swState : TState; MyEdit : TEdit;
   MyLabel : TLabel; var MySerial1 : integer; MySerial2, C_A_F : integer; SpBrowseName : String);

procedure FormShow2(MyForm, MainForm : String; K_Form : integer; Is_Show : boolean);
procedure CommonFullEdits(MyForm : TObject; MyBrowse : TADOStoredProc);

function ConvertToIranSystem1(Str : String) : String;
function ConvertFromIranSystem1(Str : String) : String;
function XConvert(tcStr: String; tcType: Char): String;
function ConvertFromIranSystem2(tcStr: String): String;
function ConvertToIranSystem2(tcStr: String): String;
procedure CalcCurrentDate;
function ServerTime: String;
function Is_Permision : Boolean;
procedure ExecSystem(MySystemName : String);
procedure InsertMyLetter(SrlTable, SrlRecord : integer);
procedure AssignPicToPer(SrlStudent, KStudent, KPicture : integer; PicStd : TImage);
procedure ViewPicPer(SrlStudent, KStudent, KPicture : integer;var PicStd : TImage);overload;
procedure ViewPicPer(SrlStudent, KStudent, KPicture : integer; PicStd : TppImage);overload;
function ShowCloseCycle: boolean;

procedure CreateDBFFile(Path1, FileName1, FileName2, DBFStruct1, DBFStruct2 : String);
procedure BrowseTStudentOne(SrlStudent : integer);

Const
   MaxField = 100;
var
   F_Common11111: TF_Common11111;
   K_Access : integer;
   OldSystem_Version : integer;
   NewSystem_Version : integer;
   Count_Param : integer;

   F1 : TIniFile;

   Srl_User : integer;
   Srl_Connection : integer;
   C_User : String;
   N_User : String;
   Pass_User : String;
   Msg_User : String;

   Srl_Employe : integer;
   Srl_Chart : integer;

   GlobalSrl_Dprt : integer;
   GlobalN_Dprt : String;
   srl_c_title:Integer;
   GlobalSrl_Person : integer;
   GlobalSrl_Student : integer;
   GlobalSrl_StudentPersonPic : integer;
   GlobalK_StudentPersonPic : integer;
   GlobalSrl_Employe : integer;
   GlobalSrl_Mosque : integer;
   GlobalSrl_Bakhsh : integer;
   GlobalSrl_School : integer;
   GlobalC_School : integer;
   GlobalC_City : integer;
   GlobalSrl_City : integer;
   GlobalSrl_Letter : integer;
   GlobalC_Letter : integer;
   GlobalK_State : integer;
   GlobalK_Book : integer;
   GlobalK_Title : TKTitle;
   GlobalN_Title : String;
   GlobalK_Register : integer;
   GlobalK_EkhtarTash : integer;
   GlobalSrl_Role : integer;
   GlobalSrl_Title : integer;
   GlobalSrl_Object : integer;
   GlobalSrl_Action : integer;
   GlobalN_Source : String;
   GlobalC_Source : String;
   Global_KOtherPerson : integer;
   Global_SrlKSemat : integer;
   Global_KExam : integer;
   GlobalSrl_Sanad : integer;
   GlobalKPerMadrak : integer;
   GlobalIs_Mashmool : integer;
   GlobalK_StudentPersonOther : integer;

   Run_Form_From_Main : Boolean;
   MyKind : integer;
   GlobalN_Student : String;
   GlobalK_Jalase : integer;
   Global_KGroupShahr : integer;
   Global_LocationSonat : integer;

   GlobalSerial : integer;
   GlobalFormCaption : String;
   GlobalK_Form  : integer;

   GlobalParaf : String;
   GlobalN_Per : String;
   GlobalF_Per : String;
   GlobalNum_Docum : String;
   GlobalN_Father : String;
   GlobalN_School : String;
   GlobalK_Per : TKPer;
   GlobalK_Jens : TKJens;
   GlobalK_School : TKSchool;
   GlobalK_Check : integer;
   GlobalK_Tahol : integer;
   GlobalK_Question : integer;
   GlobalN_DegH : String;

   SrlBaseSanad : integer;

   GlobalDBGrid: TDBGrid;
   GlobalColumnIndex : integer;

   Srl_System : integer;
   N_System : String;
   Cap_System : String;
   N_Server : String;
   N_Company : String;
   N_Station : String;
   N_DataBase : String;

   CheckPermission : Boolean;
   Visible_Components : boolean;
   Enable_Components : boolean;
   ReadOnly_Components : boolean;
   Add_Components : boolean;
   Add_StoredProcedure : boolean;
   IS_System_Periodic : Boolean;
   Is_ReConnect : Boolean;
   Is_LoadBMPFiles : Boolean;
   Is_SaveDBGrid : Boolean;

   FieldList : Array[1..MaxField] of T_Field;
   FieldCount : integer;
   CurDate : String;
   CurSal : Integer;
   CurTime : String;

   SrlSalJari : integer;
   CSalJari : integer;
   NSalJari : String;
   KSalJari : integer;
   ColorSalJari : TColor;
   SalJariFromDate : String;
   SalJariToDate : String;
   IsCloseSalJari : integer;

   SrlCurCycle : integer;
   CCurCycle : integer;
   NCurCycle : String;
   KCurCycle : integer;
   ColorCurCycle : TColor;
   CycleFromDate : String;
   CycleToDate : String;
   IsCloseCycle : integer;
   SystemIdelTime : TTime;

   SrlCurArs : integer;
   CCurArs : integer;
   NCurArs : String;
   srl_serial : integer;

   PButtonColor : TColor;
   PanelActiceColor : TColor;
   PanelIdelColor : TColor;
   SortColor : TColor;
   NotSortColor : TColor;
   PCheckBoxesColor : TColor;
   ReadOnlyTrueColor : TColor;
   ReadOnlyFalseColor : TColor;
   DefaultIsAutoNum : Boolean;
   DefaultIsAutoAdd : Boolean;

   StatusBarFont : TFont;
   DBGridTitleFont : TFont;
   DBGridDataFont : TFont;

   PrintNumberFont : TFont;
   PrintStrFont : TFont;
   PrintTitleFont : TFont;

   BetweenIndex : integer;

   FarsiFilter : TStrings;
   SelectStr : String;
   WhereStr : TStrings;
   OrderStr : String;
   BMPPath : String;
   CalcFileName : String;

   //F_WaitAvi : TF_WaitAvi;

   BADDbmp,
   BEDITbmp,
   BSAVEbmp,
   BDELbmp,
   BCANCELbmp,
   BCOLORbmp,
   BFILTERbmp,
   BPRINTbmp,
   BCLOSEbmp,
   BCALCbmp : TBitMap;
   ShowNotFoundSB : Boolean;


implementation

uses U_Report, U_Filter, U_Filter2, U_Filter3, U_Find, U_Find2, U_Find3, U_BackUp,
      U_ChangePassword, U_DisplayOption, U_Coding, U_DemoForm1,
      U_DynamicReport, U_DeleteRecord,  U_RegisterFormClass,
      U_Find_Per;

{$R *.DFM}




procedure CommonFullEdits(MyForm : TObject; MyBrowse : TADOStoredProc);
var
   i : integer;
begin
   with TForm(MyForm) Do
   begin
      for i := 0 To MyBrowse.FieldCount - 1 do
      begin
         if FindComponent(MyBrowse.Fields[i].FieldName) <> nil then
            if (FindComponent(MyBrowse.Fields[i].FieldName) is TCustomEdit) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TCustomEdit(FindComponent(MyBrowse.Fields[i].FieldName)).Text := MyBrowse.Fields[i].value
               else
                  TCustomEdit(FindComponent(MyBrowse.Fields[i].FieldName)).Text := '';
            end
            else if (FindComponent(MyBrowse.Fields[i].FieldName) is TComboBox) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TComboBox(FindComponent(MyBrowse.Fields[i].FieldName)).ItemIndex :=
                     MyBrowse.Fields[i].value
               else
                  TComboBox(FindComponent(MyBrowse.Fields[i].FieldName)).ItemIndex := -1;
            end
            else if (FindComponent(MyBrowse.Fields[i].FieldName) is TLabel) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TLabel(FindComponent(MyBrowse.Fields[i].FieldName)).Caption :=
                     MyBrowse.Fields[i].value
               else
                  TLabel(FindComponent(MyBrowse.Fields[i].FieldName)).Caption := '..........';
            end;
      end;
   end;
end;

procedure MakeFilterString(KindField : Integer; ComboBIndex : integer; FieldName : String;
                FromV : String; ToV : String; var Filter : String);
begin
   if KindField = 1 then          //  For Numeric Data Type
   begin
      Case ComboBIndex of
         0 : Filter := Filter + ' and ' + FieldName + ' = ' + FromV;
         1 : Filter := Filter + ' and ' + FieldName + ' > ' + FromV;
         2 : Filter := Filter + ' and ' + FieldName + ' >= ' + FromV;
         3 : Filter := Filter + ' and ' + FieldName + ' between ' + FromV + ' and ' + ToV;
         4 : Filter := Filter + ' and ' + FieldName + ' < ' + FromV;
         5 : Filter := Filter + ' and ' + FieldName + ' <= ' + FromV;
         6 : Filter := Filter + ' and ' + FieldName + ' <> ' + FromV;
      end;
   end
   else if KindField = 2 then  //  For String Data Type
   begin
      Case ComboBIndex of
         0 : Filter := Filter + ' and ' + FieldName + ' = "' + FromV + '"';
         1 : Filter := Filter + ' and ' + FieldName + ' > "' + FromV + '"';
         2 : Filter := Filter + ' and ' + FieldName + ' >= "' + FromV + '"';
         3 : Filter := Filter + ' and ' + FieldName + ' between "' + FromV + '" and "' + ToV + '"';
         4 : Filter := Filter + ' and ' + FieldName + ' like "%' +  FromV + '"';
         5 : Filter := Filter + ' and ' + FieldName + ' like "%' +  FromV + '%"';
         6 : Filter := Filter + ' and ' + FieldName + ' like "' +  FromV + '%"';
         7 : Filter := Filter + ' and ' + FieldName + ' < "' + FromV + '"';
         8 : Filter := Filter + ' and ' + FieldName + ' <= "' + FromV + '"';
         9 : Filter := Filter + ' and ' + FieldName + ' <> "' + FromV + '"';
      end;
   end;

end;
//------------------------------------------------------------------------------


function MakeFilterStringOne(KindField, ComboBIndex : integer;
         FieldName, FromV, ToV : String) : String;
var
   Filter : String;
begin
   if KindField = 1 then          //  For Numeric Data Type
   begin
      Case ComboBIndex of

         0 : Filter := '[' + FieldName + ']' + ' = ' + FromV;
         1 : Filter := '[' + FieldName + ']' + ' > ' + FromV;
         2 : Filter := '[' + FieldName + ']' + ' >= ' + FromV;
         3 : Filter := '[' + FieldName + ']' + ' >= ' + FromV + ' and ' + '[' + FieldName + ']' + ' <= ' + ToV;
         4 : Filter := '[' + FieldName + ']' + ' < ' + FromV;
         5 : Filter := '[' + FieldName + ']' + ' <= ' + FromV;
         6 : Filter := '[' + FieldName + ']' + ' <> ' + FromV;
      end;
   end
   else if KindField = 2 then  //  For String Data Type
   begin
      Case ComboBIndex of
         0 : Filter := '[' + FieldName + ']' + ' = ' +  QuotedStr(FromV);
         1 : Filter := '[' + FieldName + ']' + ' > ' + QuotedStr(FromV);
         2 : Filter := '[' + FieldName + ']' + ' >= ' + QuotedStr(FromV);
         3 : Filter := '[' + FieldName + ']' + ' >= ' + QuotedStr(FromV) + ' and ' + '[' + FieldName + ']' + ' <= ' + QuotedStr(ToV);
         4 : Filter := '[' + FieldName + ']' + ' like ' + QuotedStr('%' + FromV);
         5 : Filter := '[' + FieldName + ']' + ' like ' + QuotedStr('%' + FromV + '%');
         6 : Filter := '[' + FieldName + ']' + ' like ' + QuotedStr(FromV + '%');
         7 : Filter := '[' + FieldName + ']' + ' < ' + QuotedStr(FromV);
         8 : Filter := '[' + FieldName + ']' + ' <= ' + QuotedStr(FromV);
         9 : Filter := '[' + FieldName + ']' + ' <> ' + QuotedStr(FromV);
      end;
   end;
   Result := Filter;
end;
//------------------------------------------------------------------------------


procedure TF_Common11111.FormPRINTClick(Sender: TObject);
begin
   if ((Sender <> ni