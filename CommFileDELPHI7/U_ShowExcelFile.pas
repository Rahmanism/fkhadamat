unit U_ShowExcelFile;

interface

uses
  U_Common, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, ExtCtrls, Buttons;

type
  TF_ShowExcelFile = class(TForm)
    ReportGrid: TDBGrid;
    Panel1: TPanel;
    Bevel1: TBevel;
    Label17: TLabel;
    LabelCount: TLabel;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    BitBtn1: TBitBtn;
    ADOTable2: TADOTable;
    ADOTable2Srl_Charjh_Det: TAutoIncField;
    ADOTable2Srl_Charjh: TIntegerField;
    ADOTable2N_Per: TStringField;
    ADOTable2F_Per: TStringField;
    ADOTable2N_Father: TStringField;
    ADOTable2Num_Shen: TStringField;
    ADOTable2Num_Cart: TStringField;
    ADOTable2Num_Per: TFloatField;
    ADOTable2P_Charjh: TBCDField;
    ADOTable2K_CharjhSodoor: TIntegerField;
    TableListBox: TListBox;
    BitBtn2: TBitBtn;
    OpenDialog1: TOpenDialog;
    Exit: TSpeedButton;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure TableListBoxDblClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure ExitClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
{
var
  F_ShowExcelFile: TF_ShowExcelFile;
}
implementation

{$R *.dfm}

procedure TF_ShowExcelFile.BitBtn1Click(Sender: TObject);
begin
{
   ADOTable1.Open;
   ADOTable2.Open;
   ADOTable1.First;
   while Not ADOTable1.Eof do
   begin
      ADOTable2.Append;
      ADOTable2Srl_Charjh.AsInteger := StrToInt(Edit1.Text);
      if Not ADOTable1.Fields[0].IsNull then
         ADOTable2Num_Per.AsString := ADOTable1.Fields[0].Value
      else
         ADOTable2Num_Per.AsString := '1';

      ADOTable2N_Per.AsString := ADOTable1.Fields[1].Value;
      ADOTable2F_Per.AsString := ADOTable1.Fields[2].Value;
      ADOTable2Num_Cart.AsString := ADOTable1.Fields[3].Value;
      ADOTable2P_Charjh.AsInteger := ADOTable1.Fields[4].Value;
      ADOTable2.Post;
      ADOTable1.Next;
   end;
}   
end;

procedure TF_ShowExcelFile.TableListBoxDblClick(Sender: TObject);
var
   i : integer;
begin
   ADOTable1.Active := False;
   ADOTable1.Connection := F_Common.ExcelConnection;
   ADOTable1.TableName := TableListBox.Items[TableListBox.ItemIndex];
   ADOTable1.TableDirect := True;
   ADOTable1.Active := True;
   For i := 0 to ReportGrid.Columns.Count - 1 do
      ReportGrid.Columns[i].Width := 80;
end;

procedure TF_ShowExcelFile.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
   LabelCount.Caption := IntToStr(ADOTable1.RecordCount);

end;

procedure TF_ShowExcelFile.ExitClick(Sender: TObject);
begin
   Close;
end;

procedure TF_ShowExcelFile.BitBtn2Click(Sender: TObject);
var
   SL: TStrings;
   index: Integer;
begin
   if OpenDialog1.Execute then
   begin
      F_Common.ExcelConnection.Connected := false;
      F_Common.ExcelConnection.ConnectionString :=
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + Trim(OpenDialog1.FileName)
        + ';Extended Properties=Excel 8.0;Persist Security Info=False';
      F_Common.ExcelConnection.Connected := True;
   end;

   TableListBox.Clear;
   SL := TStringList.Create;
   try
      F_Common.ExcelConnection.GetTableNames(SL, False);
      for index := 0 to (SL.Count - 1) do begin
         TableListBox.Items.Add(SL[index]);
      end;
   finally
      SL.Free;
   end;
end;

end.
