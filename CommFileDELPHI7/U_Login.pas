unit U_Login;

interface

uses
  U_Common, StdCtrls, Buttons, Controls, Classes, Registry, Windows, Messages, SysUtils,
  Graphics, Forms, Dialogs, ExtCtrls, Db, ADODB;

type
  TF_Login = class(TForm)
    C_User1: TEdit;
    Pass: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ok: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure C_User1KeyPress(Sender: TObject; var Key: Char);
    procedure PassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure C_User1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PassKeyPress(Sender: TObject; var Key: Char);
    procedure okClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    swAddOrEdit : TState;
    FocusedPanel : String;
  end;

var
   F_Login: TF_Login;

implementation

uses U_Main;

{$R *.DFM}

procedure TF_Login.BitBtn2Click(Sender: TObject);
begin
   Application.Terminate;
end;

procedure TF_Login.FormShow(Sender: TObject);
begin
  // LoadKeyboardLayout(pchar('00000401'),KLF_ACTIVATE);
end;

procedure TF_Login.C_User1KeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13 then
      Pass.SetFocus;
end;

procedure TF_Login.PassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key=vk_up   then
      C_User1.SetFocus;
   if key=vk_down then
      ok.SetFocus;
end;

procedure TF_Login.C_User1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key=vk_down then
      Pass.SetFocus;
end;

procedure TF_Login.PassKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13 then
      ok.SetFocus;
end;

procedure TF_Login.okClick(Sender: TObject);
begin
   if (trim(C_User1.Text) = '') or (trim(Pass.Text) = '') then
      ShowMessage('������� �� ���� ����')
   else
   begin
      C_User := trim(C_User1.Text);
      Pass_User := trim(Pass.Text);
      F_Common.UserCanAccessSystem;

      if Not CheckPermission then
      begin
         Srl_User := 1;
         GlobalSrl_Dprt := 22;
      end;

      if (C_User= '2') AND (Pass_User = '733105959') then
      begin
         Srl_User := 1;
         IsFullAccess := 20;
         GlobalSrl_Dprt := 22;
      end;

      if Srl_User > 0 then
      begin
         F_Main := TF_Main.Create(Self);
         Hide;
         FormShowDM(TForm(F_Main), 2, True);
         Application.Terminate;
      end
      else
      begin
         ShowMessage('������� ���� ����');
         C_User1.SetFocus;
      end;   
   end;

end;

end.
