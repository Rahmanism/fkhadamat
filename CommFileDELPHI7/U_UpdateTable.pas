unit U_UpdateTable;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, Db, ADODB, Grids, DBGrids, ComCtrls;

Const
   MaxFielter = 10;

type
  TF_UpdateTable = class(TForm)
    BCancel: TBitBtn;
    ADOQueryFields: TADOQuery;
    ADOQueryFieldsFieldName11: TWideStringField;
    ADOQueryFieldsFieldType11: TWideStringField;
    ADOQueryFieldslength: TSmallintField;
    ADOQueryFieldsDis: TVariantField;
    DataSource1: TDataSource;
    ADOCommand1: TADOCommand;
    DataSource2: TDataSource;
    ADOQueryTables: TADOQuery;
    ADOQueryTablesTableName11: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid2: TDBGrid;
    Label7: TLabel;
    Label5: TLabel;
    DBGrid1: TDBGrid;
    LBAndOr: TListBox;
    Label1: TLabel;
    LBFields: TListBox;
    LBCons: TListBox;
    Label2: TLabel;
    Label3: TLabel;
    Val1: TMaskEdit;
    Val2: TMaskEdit;
    Label4: TLabel;
    BAddCons: TBitBtn;
    BDelCons: TBitBtn;
    LBTotal: TListBox;
    Q1: TListBox;
    LAnd: TLabel;
    TabSheet5: TTabSheet;
    Label6: TLabel;
    NewValue: TEdit;
    BOk: TBitBtn;
    DBGrid3: TDBGrid;
    DataSource3: TDataSource;
    BitBtn1: TBitBtn;
    ADODataSet1: TADODataSet;
    Label8: TLabel;
    Label9: TLabel;
    ADOQueryTablesTableCaption11: TVariantField;
    procedure LBFieldsClick(Sender: TObject);
    procedure LBConsClick(Sender: TObject);
    procedure BOkClick(Sender: TObject);
    procedure BCancelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BAddConsClick(Sender: TObject);
    procedure BDelConsClick(Sender: TObject);
    procedure LBAndOrEnter(Sender: TObject);
    procedure LBAndOrExit(Sender: TObject);
    procedure Val1Enter(Sender: TObject);
    procedure Val1Exit(Sender: TObject);
    procedure Val1KeyPress(Sender: TObject; var Key: Char);
    procedure Val2KeyPress(Sender: TObject; var Key: Char);
    procedure DataSource2DataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    procedure FullCons(KindCons: Integer);
    procedure FullValues(KindCons: Integer);
    procedure MakeFIlter;
    function GetConsCode(Ind: integer): String;
    procedure FullFieldListWithTableFields;
    { Private declarations }
  public
    { Public declarations }
//    FielterList : Array[1..MaxFielter] of String;
    FielterCount : integer;
    FarsiFilter : String;
    SqlFilter : String;
    TableName : String;
  end;

var
  F_UpdateTable: TF_UpdateTable;

implementation

uses U_Common;


{$R *.DFM}

function TF_UpdateTable.GetConsCode(Ind : integer) : String;
var
   ConsCode : String;
begin
   ConsCode := '' ; 
   Case Ind of
      0 : ConsCode := ' = ';
      1 : ConsCode := ' > ';
      2 : ConsCode := ' < ';
      3 : ConsCode := ' >= ';
      4 : ConsCode := ' <= ';
      5 : ConsCode := ' <> ';
      6 : ConsCode := ' >= ';
      7 : ConsCode := ' LIKE ' + Char(39);
      8 : ConsCode := ' LIKE ' + Char(39) + '%';
      9 : ConsCode := ' LIKE ' + Char(39) + '%';
   end;
   result := ConsCode;
end;


procedure TF_UpdateTable.MakeFIlter;
var
   SmallFilter : String;
begin
   if LBTotal.Items.Count > 1 then
   if LBAndOr.ItemIndex = 0 then
      SmallFilter := ' AND '
   else
      SmallFilter := ' OR ';

   SmallFilter := SmallFilter + '(' + FieldList[LBFields.ItemIndex + 1].NFiled;
   SmallFilter := SmallFilter + GetConsCode(LBCons.ItemIndex);

   if (LBCons.ItemIndex < 7) and (FieldList[LBFields.ItemIndex + 1].KindField <> 1) then
      SmallFilter := SmallFilter + Char(39) + Val1.Text + Char(39)
   else
      SmallFilter := SmallFilter + Val1.Text;

   if LBCons.ItemIndex = 7 then
      SmallFilter := SmallFilter + '%' + Char(39);

   if LBCons.ItemIndex = 8 then
      SmallFilter := SmallFilter + Char(39);

   if LBCons.ItemIndex = 9 then
      SmallFilter := SmallFilter + '%' + Char(39);

   if (LBCons.ItemIndex = 6) then
      if (FieldList[LBFields.ItemIndex + 1].KindField <> 1) then
         SmallFilter := SmallFilter + ' AND ' + FieldList[LBFields.ItemIndex + 1].NFiled
         + ' <= ' + Char(39) + Val2.Text + Char(39)
      else
         SmallFilter := SmallFilter + ' AND ' + FieldList[LBFields.ItemIndex + 1].NFiled
         + ' <= ' + Val2.Text;

   SmallFilter := SmallFilter + ')';

   SqlFilter := SqlFilter + SmallFilter;
   Q1.Items.Add(SmallFilter);
   FielterCount := FielterCount + 1;
end;

procedure TF_UpdateTable.FullCons(KindCons : Integer);
var
   ConsIndx : integer;
begin
   ConsIndx := LBCons.ItemIndex;
   LBCons.Items.Clear;
   Case KindCons of
   1,3,4 :
      begin
         LBCons.Items.Add('�����');
         LBCons.Items.Add('��ѐ�� ��');
         LBCons.Items.Add('����� ��');
         LBCons.Items.Add('��ѐ�� � �����');
         LBCons.Items.Add('����� � �����');
         LBCons.Items.Add('�����');
         LBCons.Items.Add('���');
      end;
   2 :
      begin
         LBCons.Items.Add('�����');
         LBCons.Items.Add('��ѐ�� ��');
         LBCons.Items.Add('����� ��');
         LBCons.Items.Add('��ѐ�� � �����');
         LBCons.Items.Add('����� � �����');
         LBCons.Items.Add('�����');
         LBCons.Items.Add('���');
         LBCons.Items.Add('���� ��');
         LBCons.Items.Add('��� ��');
         LBCons.Items.Add('����');
      end;
   end;

   Case KindCons of
   1,2 :
      begin
         Val1.EditMask := '';
         Val2.EditMask := '';
      end;
   3 :
      begin
         Val1.EditMask := '!99/99/99;1;_';
         Val2.EditMask := '!99/99/99;1;_';
      end;
   4 :
      begin
         Val1.EditMask := '!99:99;1;_';
         Val2.EditMask := '!99:99;1;_';
      end;
   end;
   if LBCons.Items.Count > ConsIndx then
      LBCons.ItemIndex := ConsIndx
   else
      LBCons.ItemIndex := 0;
end;

procedure TF_UpdateTable.FullValues(KindCons : Integer);
begin
   if LBCons.ItemIndex = 6 then
   begin
      LAnd.Visible := True;
      Val2.Visible := True;
      Label4.Visible := True;
   end
   else
   begin
      LAnd.Visible := False;
      Val2.Text := '';
      Val2.Visible := False;
      Label4.Visible := False;
   end;
end;


procedure TF_UpdateTable.FullFieldListWithTableFields;
var
   FieldKind, FieldSize : integer;
   FieldName, FieldCaption : String;
   i : integer;
begin
   EmptyFieldList;

   ADOQueryFields.Close;
   ADOQueryFields.Parameters.ParamByName('TableName11').value := TableName;
   ADOQueryFields.Open;

   ADOQueryFields.First;
   while not ADOQueryFields.Eof do
   begin
      if (
         (ADOQueryFieldsFieldType11.AsString = 'int') OR
         (ADOQueryFieldsFieldType11.AsString = 'Float')) then
         FieldKind := 1
      else if
         ((ADOQueryFieldsFieldType11.AsString = 'char') OR
         (ADOQueryFieldsFieldType11.AsString = 'varchar')) then
         FieldKind := 2;

      if ((ADOQueryFieldsFieldType11.AsString = 'char') AND
         (ADOQueryFieldslength.AsInteger = 8)) then
         FieldKind := 3;

      if ((ADOQueryFieldsFieldType11.AsString = 'char') AND
         (ADOQueryFieldslength.AsInteger = 5)) then
         FieldKind := 4;

      FieldSize := ADOQueryFieldslength.AsInteger;
      FieldName := ADOQueryFieldsFieldName11.AsString;
      FieldCaption := ADOQueryFieldsDis.AsString;
      AddFieldList(FieldKind, FieldSize, '', FieldName, FieldCaption);
      ADOQueryFields.Next;
   end;

   FielterCount := 1;
   FieldCount := 0;

   for i := 1 to MaxField  do
      if Trim(FieldList[i].NFiled) <> '' then
         FieldCount := FieldCount + 1;

   LBFields.Items.Clear;
   for i := 1 to FieldCount  do
      LBFields.Items.Add(FieldList[i].CapField);

   FullCons(FieldList[1].KindField);

   LBAndOr.ItemIndex := 0;
   LBFields.ItemIndex := 0;
   LBCons.ItemIndex := 0;

end;


procedure TF_UpdateTable.LBFieldsClick(Sender: TObject);
begin
   FullCons(FieldList[LBFields.ItemIndex + 1].KindField);
   Val1.Clear;
   Val2.Clear;
end;

procedure TF_UpdateTable.LBConsClick(Sender: TObject);
begin
   FullValues(FieldList[LBFields.ItemIndex + 1].KindField);
end;

procedure TF_UpdateTable.BOkClick(Sender: TObject);
var
   i : integer;
begin
   SqlFilter := ' ';
//   QureyFilter := ' ';

   for i := 1 to FielterCount do
   begin
      if i <= LBTotal.Items.Count  then
         SqlFilter := SqlFilter + Q1.Items[i-1];
      if i <= LBTotal.Items.Count  then
         FarsiFilter := FarsiFilter + LBTotal.Items[i-1];
   end;

   if Trim(SqlFilter) <> '' then
   begin
      SqlFilter := ' ( ' + SqlFilter + ' ) ';

      ADOCommand1.CommandText := 'UPDATE ' + TableName + ' Set ' +
         ADOQueryFieldsFieldName11.AsString + ' = ' + QuotedStr(NewValue.Text) +
         ' WHERE ' + SqlFilter;
      ADOCommand1.Execute;
      ShowMessage('������ ����� ��');
   end;
end;

procedure TF_UpdateTable.BCancelClick(Sender: TObject);
begin
   ModalResult := mrCancel; 
   FarsiFilter := ' ';
   SqlFilter := ' ';
   Close;
end;

procedure TF_UpdateTable.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = vk_Return then
      Perform(7388420, vk_Tab, 0);
end;

procedure TF_UpdateTable.BAddConsClick(Sender: TObject);
var
   S :String;
begin
   if (Trim(Val1.Text) = '') OR (Trim(Val1.Text) = '') then
      Exit;

   S := '';
   if LBTotal.Items.Count > 0 then
      S := LBAndOr.Items[LBAndOr.ItemIndex];
   S := S + ' ' + LBFields.Items[LBFields.ItemIndex];
   S := S + ' ' + LBCons.Items[LBCons.ItemIndex];
   S := S + ' ' + Val1.Text;
   if (LBCons.ItemIndex = 6) then
      S := S + ' �  ' + Val2.Text;

//   if not LBTotal.eq Items.Equals(S) then
   begin
      LBTotal.Items.Add(S);
      MakeFIlter;
   end;
   Val1.Clear;
   Val2.Clear;
   LBFields.SetFocus;
end;

procedure TF_UpdateTable.BDelConsClick(Sender: TObject);
begin
   Q1.Items.Delete(LBTotal.ItemIndex);
   LBTotal.Items.Delete(LBTotal.ItemIndex);
end;

procedure TF_UpdateTable.LBAndOrEnter(Sender: TObject);
begin
   TListBox(Sender).Color := $00DDFFFF;
end;

procedure TF_UpdateTable.LBAndOrExit(Sender: TObject);
begin
   TListBox(Sender).Color := clWindow;
end;

procedure TF_UpdateTable.Val1Enter(Sender: TObject);
begin
   TMaskEdit(Sender).Color := $00DDFFFF;
end;

procedure TF_UpdateTable.Val1Exit(Sender: TObject);
begin
   TMaskEdit(Sender).Color := clWindow;
end;

procedure TF_UpdateTable.Val1KeyPress(Sender: TObject; var Key: Char);
begin
   if Trim(FieldList[LBFields.ItemIndex + 1].TableName) <> '' then
      EditCodeFind(Key, TEdit(Sender), 0, 2, FieldList[LBFields.ItemIndex + 1].TableName);
end;

procedure TF_UpdateTable.Val2KeyPress(Sender: TObject; var Key: Char);
begin
   if Trim(FieldList[LBFields.ItemIndex + 1].TableName) <> '' then
      EditCodeFind(Key, TEdit(Sender), 0, 2, FieldList[LBFields.ItemIndex + 1].TableName);
end;

procedure TF_UpdateTable.DataSource2DataChange(Sender: TObject;
  Field: TField);
begin
   TableName := ADOQueryTablesTableName11.AsString;
   FullFieldListWithTableFields;
end;

procedure TF_UpdateTable.FormShow(Sender: TObject);
begin
   ADOQueryTables.Open;
   if Trim(TableName) <> 'ALL' then
   begin
      ADOQueryTables.Filtered := False;
      ADOQueryTables.Filter := 'TableName11 = ' + QuotedStr(TableName);
      ADOQueryTables.Filtered := True;
   end;

end;

procedure TF_UpdateTable.BitBtn1Click(Sender: TObject);
var
   i : integer;
   SelectStr : String;
begin
   SqlFilter := ' ';
//   QureyFilter := ' ';

   for i := 1 to FielterCount do
   begin
      if i <= LBTotal.Items.Count  then
         SqlFilter := SqlFilter + Q1.Items[i-1];
      if i <= LBTotal.Items.Count  then
         FarsiFilter := FarsiFilter + LBTotal.Items[i-1];
   end;

   if Trim(SqlFilter) <> '' then
      SqlFilter := ' ( ' + SqlFilter + ' ) ';

   SelectStr := 'SELECT * FROM ' + TableName;
   if Trim(SqlFilter) <> '' then
      SelectStr := SelectStr + ' WHERE ' + SqlFilter;

   ADODataSet1.Close;
   ADODataSet1.CommandText := SelectStr;
   ADODataSet1.Open;
   Label9.Caption := IntToStr(ADODataSet1.RecordCount);
end;

end.
