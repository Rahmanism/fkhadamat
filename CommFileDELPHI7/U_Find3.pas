unit U_Find3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, Grids, DBGrids, ExtCtrls, Buttons;

type
  TF_Find3 = class(TForm)
    DSADOStoredProcFind: TDataSource;
    Label3: TLabel;
    EditFind: TEdit;
    DBGrid1: TDBGrid;
    LCount: TLabel;
    KFind: TRadioGroup;
    sp_Browse_T_Table3: TADOStoredProc;
    Label1: TLabel;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure EditFindChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EditFindEnter(Sender: TObject);
    procedure EditFindExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KFindClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SrlDataSet : Integer;
    Val_Param1, Val_Param2, Val_Param3, Val_Param4 : Integer;

    CFind : String;
    NFind : String;
    SrlFind : Integer;

    FormResult : integer;
  end;

var
   F_Find3: TF_Find3;

implementation

uses U_Common;


{$R *.DFM}

procedure TF_Find3.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
   if key = #13 then
      DBGrid1DblClick(Sender)
   else
   if key = #27 then
   begin
      FormResult := -1;
      Close;
   end;
end;

procedure TF_Find3.EditFindChange(Sender: TObject);
Var
   Str : String;
   ExactName : TField;
   ExactStr : String;
   EditFindText : String;
   PosAnd : integer;
   PosOr : integer;
begin
   Str := '';
//
//   PosAnd := 0;
//   PosOr := 0;
//   EditFindText := Trim(EditFind.text);
//   PosAnd := Pos('&', EditFindText);
//   PosOr := Pos('|', EditFindText);
   EditFindText := EditFind.text;
   if Trim(EditFindText) <> '' then  // AND (PosAnd = 0) AND (PosOr = 0) then
   Begin
      sp_Browse_T_Table3.Filtered := False;

      if KFind.ItemIndex = 0 then
         Str := ' (StrFCode like ' + QuotedStr('%' + EditFindText + '%')+ ')' + ' or ' +
            ' (StrFName like ' + QuotedStr('%' + EditFindText + '%')+ ')'
      else if KFind.ItemIndex = 1 then
         Str := ' (StrFCode like ' + QuotedStr(EditFindText + '%')+ ')'+ ' or ' +
            ' (StrFName like ' + QuotedStr(EditFindText + '%')+ ')'
      else if KFind.ItemIndex = 2 then
         Str := ' (StrFCode like ' + QuotedStr('%' + EditFindText + '%')+ ')' + ' or ' +
            ' (StrFName like ' + QuotedStr('%' + EditFindText + '%')+ ')' + ' or ' +
            ' (ExactName like ' + QuotedStr('%' + GetExtractStr(EditFindText) + '%')+ ')';

      sp_Browse_T_Table3.Filter := Str;
      sp_Browse_T_Table3.Filtered := True;
   End
   else
      sp_Browse_T_Table3.Filtered := False;

   LCount.Caption := IntToStr(sp_Browse_T_Table3.RecordCount);
end;

procedure TF_Find3.FormShow(Sender: TObject);
var
   i : integer;
   ExactName : TField;
   Fwidth : integer;
begin
   Fwidth := 0;
   DBGrid1.OnTitleClick := F_Common.FormDBGridTitleClick;
   try
      sp_Browse_T_Table3.Close;
      NullAdoSPParameters(sp_Browse_T_Table3);
      sp_Browse_T_Table3.Parameters.ParamByName('@Srl_Table').value := SrlDataSet;
      sp_Browse_T_Table3.Parameters.ParamByName('@Val_Param1').value := Val_Param1;
      sp_Browse_T_Table3.Parameters.ParamByName('@Val_Param2').value := Val_Param2;
      sp_Browse_T_Table3.Parameters.ParamByName('@Val_Param3').value := Val_Param3;
      sp_Browse_T_Table3.Parameters.ParamByName('@Val_Param4').value := Val_Param4;
      sp_Browse_T_Table3.Open;

      LCount.Caption := IntToStr(sp_Browse_T_Table3.RecordCount);
      Caption := Caption + sp_Browse_T_Table3.Parameters.ParamByName('@Cap_DataSet').value;

      for i := 0 to DBGrid1.Columns.Count - 1 do
      begin
         if ((DBGrid1.Columns[i].Field.DisplayName = 'FSerial')
            OR (DBGrid1.Columns[i].Field.DisplayName = 'StrFName')
            OR (DBGrid1.Columns[i].Field.DisplayName = 'ExactName')
            OR (DBGrid1.Columns[i].Field.DisplayName = 'StrFCode')) then
         DBGrid1.Columns[i].Visible := False;

         DBGrid1.Columns[i].Title.Alignment := taCenter;

         if DBGrid1.Columns[i].Field.DataType in NumberType then
            DBGrid1.Columns[i].Width := 100
         else //   StringType
            DBGrid1.Columns[i].Width := 200;

         if DBGrid1.Columns[i].Visible then
            Fwidth := Fwidth + DBGrid1.Columns[i].Width;
      end;

      width := Fwidth + 50;
   except
      ShowMessage('Error in U_Find_FormShow ');
   end;

end;

procedure TF_Find3.DBGrid1DblClick(Sender: TObject);
begin
   CFind := sp_Browse_T_Table3.Fields.FieldByName('StrFCode').AsString;
   NFind := sp_Browse_T_Table3.Fields.FieldByName('StrFName').AsString;
   SrlFind := sp_Browse_T_Table3.Fields.FieldByName('FSerial').AsInteger;
   FormResult := 1;
   Close;
end;

procedure TF_Find3.EditFindEnter(Sender: TObject);
begin
   KeyPreview := False;
   DBGrid1.SetFocus;
end;

procedure TF_Find3.EditFindExit(Sender: TObject);
begin
   KeyPreview := True;
end;

procedure TF_Find3.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if Key IN ValidDigitAlphabet then
   Begin
         EditFind.Text := EditFind.Text + Key;
   End;
   if (ord(Key) = 8) then
      EditFind.Text := Copy(EditFind.Text,1,Length(EditFind.Text)-1);
end;

procedure TF_Find3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Shift = [ssCtrl]) and (Key = 96) then
   begin
      KFind.ItemIndex := (KFind.ItemIndex + 1) Mod 3;
      Key := 0;
   end;
end;

procedure TF_Find3.KFindClick(Sender: TObject);
begin
   EditFindChange(Sender);
end;

end.
