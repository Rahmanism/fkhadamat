object F_Filter: TF_Filter
  Left = 362
  Top = 193
  Width = 596
  Height = 445
  BiDiMode = bdRightToLeft
  Caption = '��� �����'
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 412
    Top = 18
    Width = 91
    Height = 16
    Caption = '���� ����� ��'
  end
  object Label2: TLabel
    Left = 326
    Top = 16
    Width = 32
    Height = 16
    Caption = '���'
  end
  object LAnd: TLabel
    Left = 214
    Top = 84
    Width = 7
    Height = 16
    Caption = '�'
    Visible = False
  end
  object Label3: TLabel
    Left = 164
    Top = 45
    Width = 44
    Height = 16
    Caption = '����� 1'
  end
  object Label4: TLabel
    Left = 164
    Top = 85
    Width = 44
    Height = 16
    Caption = '����� 2'
    Visible = False
  end
  object LBFields: TListBox
    Left = 360
    Top = 37
    Width = 145
    Height = 160
    BorderStyle = bsNone
    IntegralHeight = True
    ItemHeight = 16
    TabOrder = 1
    OnClick = LBFieldsClick
    OnEnter = LBAndOrEnter
    OnExit = LBAndOrExit
  end
  object LBCons: TListBox
    Left = 237
    Top = 37
    Width = 121
    Height = 160
    BorderStyle = bsNone
    IntegralHeight = True
    ItemHeight = 16
    TabOrder = 2
    OnClick = LBConsClick
    OnEnter = LBAndOrEnter
    OnExit = LBAndOrExit
  end
  object Val1: TMaskEdit
    Left = 64
    Top = 40
    Width = 97
    Height = 24
    EditMask = '!99:99;1;_'
    MaxLength = 5
    TabOrder = 3
    Text = '  :  '
    OnEnter = Val1Enter
    OnExit = Val1Exit
    OnKeyPress = Val1KeyPress
  end
  object Val2: TMaskEdit
    Left = 64
    Top = 80
    Width = 97
    Height = 24
    TabOrder = 4
    Visible = False
    OnEnter = Val1Enter
    OnExit = Val1Exit
    OnKeyPress = Val2KeyPress
  end
  object LBTotal: TListBox
    Left = 40
    Top = 205
    Width = 505
    Height = 132
    IntegralHeight = True
    ItemHeight = 16
    Style = lbOwnerDrawVariable
    TabOrder = 7
    OnEnter = LBAndOrEnter
    OnExit = LBAndOrExit
  end
  object LBAndOr: TListBox
    Left = 507
    Top = 101
    Width = 33
    Height = 36
    BorderStyle = bsNone
    Color = 14548991
    ItemHeight = 16
    Items.Strings = (
      '�'
      '��')
    Sorted = True
    Style = lbOwnerDrawVariable
    TabOrder = 0
    TabWidth = 5
    OnEnter = LBAndOrEnter
    OnExit = LBAndOrExit
  end
  object Q1: TListBox
    Left = 40
    Top = 336
    Width = 506
    Height = 20
    BiDiMode = bdLeftToRight
    IntegralHeight = True
    ItemHeight = 16
    ParentBiDiMode = False
    TabOrder = 8
    Visible = False
    OnEnter = LBAndOrEnter
    OnExit = LBAndOrExit
  end
  object BOk: TBitBtn
    Left = 344
    Top = 368
    Width = 75
    Height = 31
    Caption = '�����'
    TabOrder = 6
    OnClick = BOkClick
  end
  object BCancel: TBitBtn
    Left = 168
    Top = 368
    Width = 75
    Height = 32
    Caption = '������'
    TabOrder = 9
    OnClick = BCancelClick
  end
  object BNewCons: TBitBtn
    Left = 256
    Top = 368
    Width = 75
    Height = 31
    Caption = '��� ����'
    TabOrder = 11
    Visible = False
    OnClick = BNewConsClick
  end
  object BAddCons: TBitBtn
    Left = 144
    Top = 136
    Width = 75
    Height = 33
    Caption = '����� ���'
    TabOrder = 5
    OnClick = BAddConsClick
  end
  object BDelCons: TBitBtn
    Left = 40
    Top = 136
    Width = 89
    Height = 33
    Caption = '��� ���'
    TabOrder = 10
    OnClick = BDelConsClick
  end
end
