unit U_Find;

interface

uses
  Windows, Variants, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, Grids, DBGrids, ExtCtrls, Buttons;

type
  TF_Find = class(TForm)
    ADOStoredProcFind: TADOStoredProc;
    DSADOStoredProcFind: TDataSource;
    Label3: TLabel;
    EditFind: TEdit;
    DBGrid1: TDBGrid;
    LCount: TLabel;
    KFind: TRadioGroup;
    SpeedButton1: TSpeedButton;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure EditFindChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EditFindEnter(Sender: TObject);
    procedure EditFindExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KFindClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    TableName : String;

    CFind : String;
    NFind : String;
    SrlFind : Integer;
    KindParam : integer;
    FieldParam : String;

    FormResult : integer;
    Count_Add_Fields : integer;
  end;

var
   F_Find: TF_Find;

implementation

uses U_Common;


{$R *.DFM}

procedure TF_Find.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
   if key = #13 then
      DBGrid1DblClick(Sender)
   else
   if key = #27 then
   begin
      FormResult := -1;
      Close;
   end;
end;

procedure TF_Find.EditFindChange(Sender: TObject);
Var
   Str : String;
   ExactName : TField;
   ExactStr : String;
begin
   Str := '';
   ADOStoredProcFind.Filtered := False;
   if (EditFind.text <> '') then
   Begin
      if KFind.ItemIndex = 0 then
//         Str := ' (FCode like ' + QuotedStr('%' + EditFind.text + '%')+ ')' + ' or ' +
         Str := ' (FName like ' + QuotedStr('%' + EditFind.text + '%')+ ')'
      else if KFind.ItemIndex = 1 then
//         Str := ' (FCode like ' + QuotedStr(EditFind.text + '%')+ ')'+ ' or ' +
         Str := ' (FName like ' + QuotedStr(EditFind.text + '%')+ ')'
      else if KFind.ItemIndex = 2 then
      begin
         ExactName := ADOStoredProcFind.FindField('ExactName');
         if ExactName <> nil then
         Begin
            ExactStr := GetExtractStr(Trim(EditFind.text));
//            Str := ' (FCode like ' + QuotedStr('%' + ExactStr + '%')+ ')' + ' or ' +
            Str := ' (ExactName like ' + QuotedStr('%' + ExactStr + '%')+ ')'
         end
         ELSE
            Str := ' (FName like ' + QuotedStr('%' + EditFind.text + '%')+ ')'

      end;
   End;

   if Str = '' then Exit;
   ADOStoredProcFind.Filter := Str;
   ADOStoredProcFind.Filtered := True;
   LCount.Caption := IntToStr(ADOStoredProcFind.RecordCount);
end;

procedure TF_Find.FormShow(Sender: TObject);
var
   i : integer;
begin
   ADOStoredProcFind.Connection := F_Common.SystemConnection;
   try
      ADOStoredProcFind.Parameters.Refresh;
      ADOStoredProcFind.Close;
      for i := 0 to ADOStoredProcFind.Parameters.Count -1  do
         ADOStoredProcFind.Parameters[i].value := null;

      if ADOStoredProcFind.Parameters.FindParam('@TableName') <> nil then
         ADOStoredProcFind.Parameters.ParamByName('@TableName').value := TableName;

      if ADOStoredProcFind.Parameters.FindParam('@FieldParam') <> nil then
         ADOStoredProcFind.Parameters.ParamByName('@FieldParam').value := FieldParam;

      if ADOStoredProcFind.Parameters.FindParam('@KindParam') <> nil then
         ADOStoredProcFind.Parameters.ParamByName('@KindParam').value := KindParam;

      ADOStoredProcFind.Open;
      LCount.Caption := IntToStr(ADOStoredProcFind.RecordCount);
      if ADOStoredProcFind.Fields.Count > Count_Add_Fields then
      for i := 2 to Count_Add_Fields - 1 do
      begin
         DBGrid1.Columns.Add;
         DBGrid1.Columns[i].Field := ADOStoredProcFind.Fields[i + 1];
         DBGrid1.Columns[i].Width := 140;
         Width := Width + 140;
      end;
   except
      ShowMessage('Error in U_Find_FormShow ');
   end;

end;

procedure TF_Find.DBGrid1DblClick(Sender: TObject);
begin
   CFind := ADOStoredProcFind.Fields.FieldByName('FCode').AsString;
   NFind := ADOStoredProcFind.Fields.FieldByName('FName').AsString;
   SrlFind := ADOStoredProcFind.Fields.FieldByName('FSerial').AsInteger;
   srl_serial:=ADOStoredProcFind.Fields.FieldByName('FSerial').AsInteger;
   FormResult := 1;
   Close;
end;

procedure TF_Find.EditFindEnter(Sender: TObject);
begin
   KeyPreview := False;
   DBGrid1.SetFocus;
end;

procedure TF_Find.EditFindExit(Sender: TObject);
begin
   KeyPreview := True;
end;

procedure TF_Find.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if Key IN ValidDigitAlphabet then
   Begin
         EditFind.Text := EditFind.Text + Key;
   End;
   if (ord(Key) = 8) then
      EditFind.Text := Copy(EditFind.Text,1,Length(EditFind.Text)-1);

end;

procedure TF_Find.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if (Shift = [ssCtrl]) and (Key = 96) then
   begin
      KFind.ItemIndex := (KFind.ItemIndex + 1) Mod 3;
      Key := 0;
   end;
      srl_serial:=ADOStoredProcFind.Fields.FieldByName('FSerial').AsInteger;
   end;

procedure TF_Find.KFindClick(Sender: TObject);
begin
   EditFindChange(Sender);
end;

procedure TF_Find.SpeedButton1Click(Sender: TObject);
begin
{
   ShowCodingForm(-1);
   ADOStoredProcFind.Requery;
}
end;

procedure TF_Find.FormClick(Sender: TObject);
begin
srl_serial:=ADOStoredProcFind.Fields.FieldByName('FSerial').AsInteger;
end;

procedure TF_Find.FormHide(Sender: TObject);
begin
srl_serial:=ADOStoredProcFind.Fields.FieldByName('FSerial').AsInteger;
end;

end.
