object F_ChangePassword: TF_ChangePassword
  Left = 415
  Top = 351
  Width = 312
  Height = 197
  BiDiMode = bdRightToLeft
  Caption = #1578#1594#1610#1610#1585' '#1585#1605#1586' '#1593#1576#1608#1585
  Color = 10520173
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object Label1111: TLabel
    Left = 167
    Top = 21
    Width = 67
    Height = 16
    Caption = #1585#1605#1586' '#1593#1576#1608#1585' '#1602#1576#1604#1610
    Font.Charset = ARABIC_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2222: TLabel
    Left = 172
    Top = 51
    Width = 67
    Height = 16
    Caption = #1585#1605#1586' '#1593#1576#1608#1585' '#1580#1583#1610#1583
    Font.Charset = ARABIC_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3333: TLabel
    Left = 172
    Top = 81
    Width = 95
    Height = 16
    Caption = #1578#1603#1585#1575#1585' '#1585#1605#1586' '#1593#1576#1608#1585' '#1580#1583#1610#1583
    Font.Charset = ARABIC_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LastPass: TEdit
    Left = 44
    Top = 17
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 0
    OnKeyDown = LastPassKeyDown
    OnKeyPress = LastPassKeyPress
  end
  object NewPass: TEdit
    Left = 44
    Top = 48
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = NewPassKeyDown
    OnKeyPress = NewPassKeyPress
  end
  object Ok: TBitBtn
    Left = 70
    Top = 126
    Width = 75
    Height = 25
    Caption = #1578#1575#1610#1610#1583
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = OkClick
  end
  object BitBtn2: TBitBtn
    Left = 168
    Top = 126
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1575#1606#1589#1585#1575#1601
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Windows UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object NewPass2: TEdit
    Left = 44
    Top = 78
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 4
    OnKeyDown = NewPass2KeyDown
    OnKeyPress = NewPass2KeyPress
  end
  object Pub_UPDATE_User1: TADOStoredProc
    ProcedureName = 'Pub_UPDATE_User1;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srl_User_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Pass_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 80
    Top = 56
  end
end
