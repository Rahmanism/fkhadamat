unit U_Picture;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, ADODB, DBCtrls, ExtDlgs, Grids, DBGrids,
  Mask, ppPrnabl, ppClass, ppCtrls, ppBarCod, ppBands, ppCache, ppComm,
  ppRelatv, ppProd, ppReport, Printers;

type
  TF_Picture = class(TForm)
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    Browse_T_Source_Pic: TADOStoredProc;
    Browse_T_Source_PicFSerial: TAutoIncField;
    DSBrowse_T_Source_Pic: TDataSource;
    BitBtnAddPic: TBitBtn;
    BitBtnDelOne: TBitBtn;
    OpenPictureDialog1: TOpenPictureDialog;
    DBGrid1: TDBGrid;
    Browse_T_Source_PicFCode: TIntegerField;
    Image1: TImage;
    Delete1: TADOStoredProc;
    Browse_T_Source_PicFName: TStringField;
    DBEdit1: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    Browse_T_Source_PicSrl_Des_Pic: TIntegerField;
    Browse_T_Source_PicDes_Pic: TStringField;
    ADOTable2: TADOTable;
    DataSource2: TDataSource;
    BitBtnViewPic: TBitBtn;
    BitBtnPrintPic: TBitBtn;
    BitBtnDelAll: TBitBtn;
    Browse_T_Source_PicPic_Path: TStringField;
    PrintDialog1: TPrintDialog;
    BitBtnEditPic: TBitBtn;
    ADOTable2FSerial: TIntegerField;
    ADOTable2FCode: TIntegerField;
    ADOTable2FName: TWideStringField;
    ADOTable2U_DateTime: TDateTimeField;
    ADOTable2Srl_Connection: TIntegerField;
    ADOTable1FSerial: TIntegerField;
    ADOTable1FCode: TIntegerField;
    ADOTable1FName: TWideStringField;
    ADOTable1U_DateTime: TDateTimeField;
    ADOTable1Srl_Connection: TIntegerField;
    IsMoveFile: TCheckBox;
    DBText1: TDBText;
    BitBtnClose: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BitBtnAddPicClick(Sender: TObject);
    procedure DSBrowse_T_Source_PicDataChange(Sender: TObject;
      Field: TField);
    procedure BitBtnDelOneClick(Sender: TObject);
    procedure BitBtnViewPicClick(Sender: TObject);
    procedure BitBtnPrintPicClick(Sender: TObject);
    procedure BitBtnDelAllClick(Sender: TObject);
    procedure DBLookupComboBox1Click(Sender: TObject);
    procedure BitBtnEditPicClick(Sender: TObject);
    procedure BitBtnCloseClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure FBrowse1;
    procedure LoadPictureToImage;
    { Private declarations }
  public
    { Public declarations }
    BookMark1 : integer;
    SrlTable : integer;
  end;

var
  F_Picture: TF_Picture;

implementation

uses ViewWin;

{$R *.DFM}

procedure TF_Picture.FBrowse1;
begin
   with Browse_T_Source_Pic do
   begin
      NullAdoSPParameters(Browse_T_Source_Pic);
      try
         Parameters.ParamByName('@SrlSource').Value := GlobalSrl_Source;
         Parameters.ParamByName('@KSource').Value := GlobalK_Source;
         Parameters.ParamByName('@KFile').Value := 1;
         Open;
      except
         ShowMessage('��� ������ �����');
      end;
   end;
end;

procedure TF_Picture.FormShow(Sender: TObject);
begin
   SrlTable := SrlDataSetPicture;
   FBrowse1;
   ADOTable1.Open;
   ADOTable2.Open;
end;

procedure TF_Picture.BitBtnAddPicClick(Sender: TObject);
var
   i : integer;
   NewFileName : String;
   OldFileName : String;
   FileIndex : integer;
   Result : integer;
begin
   OpenPictureDialog1.Options := [ofAllowMultiSelect, ofFileMustExist];
   OpenPictureDialog1.FilterIndex := 2; { start the dialog showing all files }
   OpenPictureDialog1.Title := '���� ����� �� ������ ����';
   if OpenPictureDialog1.Execute  then
   begin
      For i := 0 To OpenPictureDialog1.Files.Count - 1 do
      begin
         FileIndex := 0;
         Image1.Picture.LoadFromFile(OpenPictureDialog1.Files.Strings[i]);

         InsertPicture(OpenPictureDialog1.Files.Strings[i], IsMoveFile.Checked,
               1, GlobalK_Source, GlobalSrl_Source, True,Result);

      end;
      FBrowse1;
      Browse_T_Source_Pic.Last;
   end;
end;

procedure TF_Picture.LoadPictureToImage;
var
   FName : String;
   a : integer;
begin
      try
         Image1.Picture := nil;
         a := 0;
         if Not Browse_T_Source_PicPic_Path.IsNull then
         begin
            FName := ExtractFileName(Browse_T_Source_PicPic_Path.AsString);
            if FileExists(PChar(Browse_T_Source_PicPic_Path.AsString)) then
            begin
               Image1.Picture.LoadFromFile(Browse_T_Source_PicPic_Path.AsString);
               a := 1;
            end
            else
               Image1.Picture := nil;
{
            if a = 0 then
            begin
               PName := '\\pserver\picture$\AllPicPaz\';

               FullName := PName + FName;
               if FileExists(PChar(FullName)) then
               begin
                  Image1.Picture.LoadFromFile(FullName);
                  a := 1;
               end
               else
                  Image1.Picture := nil;
            end;

            if a = 0 then
            begin
               PName := '\\pserver\picture$\Parsian84\';

               FullName := PName + FName;
               if FileExists(PChar(FullName)) then
               begin
                  Image1.Picture.LoadFromFile(FullName);
                  a := 1;
               end
               else
                  Image1.Picture := nil;
            end;
}
         end
         else
            Image1.Picture := nil;
      except
         ShowMessage('��� ������ �����');
      end;
end;

procedure TF_Picture.DSBrowse_T_Source_PicDataChange(Sender: TObject;
  Field: TField);
begin
   LoadPictureToImage;
end;

procedure TF_Picture.BitBtnDelOneClick(Sender: TObject);
begin
   DeleteRecord1(SrlTable, Browse_T_Source_PicFSerial.AsInteger, Browse_T_Source_Pic);
end;

procedure TF_Picture.BitBtnViewPicClick(Sender: TObject);
var
  FileExt: string[4];
  FormCaption : String;
  FViewForm : TFViewForm;
begin
  FViewForm := TFViewForm.Create(Self);

  FileExt := AnsiUpperCase(ExtractFileExt(Browse_T_Source_PicPic_Path.AsString));
  if (FileExt = '.BMP') or (FileExt = '.ICO') or (FileExt = '.WMF') or
    (FileExt = '.EMF') then
  begin
    Image1.Picture.LoadFromFile(Browse_T_Source_PicPic_Path.AsString);
    Caption := FormCaption + ExtractFilename(Browse_T_Source_PicPic_Path.AsString);
    if (FileExt = '.BMP') then
    begin
      Caption := Caption +
        Format(' (%d x %d)', [Image1.Picture.Width, Image1.Picture.Height]);
      FViewForm.Image1.Picture := Image1.Picture;
      FViewForm.Caption := Caption;
    end;
    if FileExt = '.ICO' then
    begin
      Icon := Image1.Picture.Icon;
      FViewForm.Image1.Picture.Icon := Icon;
    end;
    if (FileExt = '.WMF') or (FileExt = '.EMF') then
      FViewForm.Image1.Picture.Metafile := Image1.Picture.Metafile;
  end;

   FViewForm.Image1.Picture := Image1.Picture;
   FViewForm.Image1.AutoSize := True;
   FViewForm.HorzScrollBar.Range := Image1.Picture.Width;
   FViewForm.VertScrollBar.Range := Image1.Picture.Height;
   FViewForm.Caption := Caption;
   FViewForm.WindowState := wsMaximized;
   FViewForm.ShowModal;
   FViewForm.Free;
end;

procedure TF_Picture.BitBtnPrintPicClick(Sender: TObject);
var
  AspectRatio: Single;
  OutputWidth, OutputHeight: Single;
begin
  if not PrintDialog1.Execute then Exit;
  Printer.BeginDoc;
  try
    OutputWidth := Image1.Picture.Width;
    OutputHeight := Image1.Picture.Height;
    AspectRatio := OutputWidth / OutputHeight;
    if (OutputWidth < Printer.PageWidth) and
      (OutputHeight < Printer.PageHeight) then
    begin
      if OutputWidth < OutputHeight then

      begin
        OutputHeight := Printer.PageHeight;
        OutputWidth := OutputHeight * AspectRatio;
      end
      else
      begin
        OutputWidth := Printer.PageWidth;
        OutputHeight := OutputWidth / AspectRatio;
      end
    end;
    if OutputWidth > Printer.PageWidth then
    begin
      OutputWidth := Printer.PageWidth;
      OutputHeight := OutputWidth / AspectRatio;
    end;
    if OutputHeight > Printer.PageHeight then

    begin
      OutputHeight := Printer.PageHeight;
      OutputWidth := OutputHeight * AspectRatio;
    end;
    Printer.Canvas.StretchDraw(Rect(0,0,
      Trunc(OutputWidth), Trunc(OutputHeight)),
      Image1.Picture.Graphic);
  finally
    Printer.EndDoc;
  end;

end;

procedure TF_Picture.BitBtnDelAllClick(Sender: TObject);
begin
   while Browse_T_Source_Pic.RecordCount > 0 do
      BitBtnDelOneClick(Sender);
end;

procedure TF_Picture.DBLookupComboBox1Click(Sender: TObject);
begin
   Browse_T_Source_Pic.Post;
   FBrowse1;
end;

procedure TF_Picture.BitBtnEditPicClick(Sender: TObject);
begin
   WinExec(PChar('mspaint.exe "' + Browse_T_Source_PicPic_Path.AsString + '"'), 3);
end;

procedure TF_Picture.BitBtnCloseClick(Sender: TObject);
begin
   Close;
end;

procedure TF_Picture.FormActivate(Sender: TObject);
begin
   LoadPictureToImage;
end;

procedure TF_Picture.BitBtn4Click(Sender: TObject);
var
  MyRect: TRect;
begin
  MyRect := Panel1.BoundsRect;
  MyRect.Right := MyRect.Left + Round((MyRect.Right - MyRect.Left) * 1.2);
  MyRect.Bottom := MyRect.Top + Round((MyRect.Bottom - MyRect.Top) * 1.2);
  Panel1.BoundsRect := MyRect;
end;

procedure TF_Picture.BitBtn5Click(Sender: TObject);
var
  MyRect: TRect;
begin
  MyRect := Panel1.BoundsRect;
  MyRect.Right := MyRect.Left + Round((MyRect.Right - MyRect.Left) * 10/12);
  MyRect.Bottom := MyRect.Top + Round((MyRect.Bottom - MyRect.Top) * 10/12);
  Panel1.BoundsRect := MyRect;
end;

procedure TF_Picture.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key = 107 then
      BitBtn4Click(Sender)
   else if Key = 109 then
      BitBtn5Click(Sender)
   else if Key = 39 then
      Browse_T_Source_Pic.Next
   else if Key = 37 then
      Browse_T_Source_Pic.Prior;
end;

end.
