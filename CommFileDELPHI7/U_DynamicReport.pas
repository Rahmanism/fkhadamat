unit U_DynamicReport;

interface

uses
  U_Common, Variants, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBGrids, StdCtrls, ExtCtrls, Grids, ComCtrls, Buttons, Mask, Db, ADODB,
  ppViewr, ppDB, ppDBPipe, ppDBBDE, ppBands, ppCtrls, ppVar, ppStrtch,
  ppMemo, ppPrnabl, ppClass, ppCache, ppComm, ppRelatv, ppProd, ppReport,
  Menus, ppTypes, Printers, CheckLst, TXtraDev, ppRegion, TeEngine, Series,
  TeeProcs, Chart, DBChart, DBCtrls, Spin;


type
  TF_DynamicReport = class(TForm)
    FontDialog1: TFontDialog;
    BrowseQuery: TADOStoredProc;
    Insert1: TADOStoredProc;
    Delete1: TADOStoredProc;
    Update1: TADOStoredProc;
    DSBrowseQuery: TDataSource;
    DSBrowseField: TDataSource;
    BrowseField: TADOStoredProc;
    BrowseFieldFSerial: TAutoIncField;
    BrowseFieldCap_Field: TStringField;
    BrowseFieldSize_Field: TIntegerField;
    BrowseFieldK_Field: TIntegerField;
    DSBrowseQuerySelect: TDataSource;
    BrowseQuerySelect: TADOStoredProc;
    InsertQuerySelect: TADOStoredProc;
    UpdateQuerySelect: TADOStoredProc;
    DeleteQuerySelect: TADOStoredProc;
    BrowseQuerySelectFSerial: TAutoIncField;
    BrowseQuerySelectCap_Field: TStringField;
    BrowseQuerySelectSize_Field: TIntegerField;
    updateQuerySelect2: TADOStoredProc;
    DSbrowseOperator: TDataSource;
    browseOperator: TADOStoredProc;
    browseOperatorFSerial: TAutoIncField;
    browseOperatorL_Name1: TStringField;
    browseOperatorF_Name: TStringField;
    DSbrowseQueryWhere: TDataSource;
    browseQueryWhere: TADOStoredProc;
    insertQueryWhere: TADOStoredProc;
    updateQueryWhere: TADOStoredProc;
    deleteQueryWhere: TADOStoredProc;
    DSExecQuery: TDataSource;
    MakeQuery: TADOStoredProc;
    browseQueryWhereFSerial: TAutoIncField;
    browseQueryWhereSrl_Field1: TIntegerField;
    browseQueryWhereN_Field: TStringField;
    browseQueryWhereCap_Field: TStringField;
    browseQueryWhereSrl_Operator: TIntegerField;
    browseQueryWhereL_Name1: TStringField;
    browseQueryWhereN_Operator: TStringField;
    browseQueryWhereValue1: TStringField;
    browseQueryWhereValue2: TStringField;
    browseQueryWhereValue_In: TStringField;
    browseQueryWhereAnd_Or: TIntegerField;
    ExecQuery: TADOStoredProc;
    DSbrowseQuerySort: TDataSource;
    browseQuerySort: TADOStoredProc;
    insertQuerySort: TADOStoredProc;
    updateQuerySort: TADOStoredProc;
    deleteQuerySort: TADOStoredProc;
    browseQuerySortFSerial: TAutoIncField;
    browseQuerySortFName: TStringField;
    browseQuerySortCap_Field: TStringField;
    browseQuerySortOrder_Field: TIntegerField;
    browseQuerySortIs_Desc: TIntegerField;
    Panel6: TPanel;
    PageControl1: TPageControl;
    TabResult: TTabSheet;
    PanelResult: TPanel;
    ReportGrid: TDBGrid;
    TabOption: TTabSheet;
    Group: TRadioButton;
    Normal: TRadioButton;
    PanelOption: TPanel;
    Label8: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PrinterOrientation: TRadioGroup;
    EditHedCaption: TEdit;
    PSize: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    PaperHeight: TEdit;
    PaperWidth: TEdit;
    CheckBoxRadif: TCheckBox;
    MemoTitCaption: TMemo;
    TabSort: TTabSheet;
    PanelSort: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    Is_Desc: TRadioGroup;
    Panel4: TPanel;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    TabWhere: TTabSheet;
    PanelWhere: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    And_Or: TRadioGroup;
    DBGrid7: TDBGrid;
    DBGrid8: TDBGrid;
    Value1: TMaskEdit;
    Value2: TMaskEdit;
    DBGrid9: TDBGrid;
    Value_In: TMaskEdit;
    TabSelect: TTabSheet;
    PanelSelect: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    TabReports: TTabSheet;
    PanelQuery: TPanel;
    Label16: TLabel;
    DBGrid10: TDBGrid;
    EName: TEdit;
    PButton: TPanel;
    BADD: TSpeedButton;
    BEDIT: TSpeedButton;
    BSAVE: TSpeedButton;
    BDEL: TSpeedButton;
    BCLOSE: TSpeedButton;
    BCANCEL: TSpeedButton;
    StatusBar1: TStatusBar;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    pnlPreviewBar: TPanel;
    SpeedButton11: TSpeedButton;
    spbPreviewWhole: TSpeedButton;
    spbPreviewWidth: TSpeedButton;
    spbPreview100Percent: TSpeedButton;
    Label18: TLabel;
    Label19: TLabel;
    TotalPage: TLabel;
    Label20: TLabel;
    SpeedButton12: TSpeedButton;
    SpeedButton13: TSpeedButton;
    bEnd: TSpeedButton;
    bNext: TSpeedButton;
    bPrev: TSpeedButton;
    bFirst: TSpeedButton;
    SpeedButton15: TSpeedButton;
    mskPreviewPercentage: TMaskEdit;
    mskPreviewPage: TMaskEdit;
    ppViewer1: TppViewer;
    Panel3: TPanel;
    GridReport: TppReport;
    ppTitleBand1: TppTitleBand;
    ppLabel1: TppLabel;
    TitCaption: TppMemo;
    ppHeaderBand1: TppHeaderBand;
    ppShape3: TppShape;
    ppShape1: TppShape;
    ppLabel201: TppLabel;
    ppLabel202: TppLabel;
    ppLabel203: TppLabel;
    ppLabel204: TppLabel;
    ppLabel205: TppLabel;
    ppLabel206: TppLabel;
    ppLabel207: TppLabel;
    ppLabel208: TppLabel;
    ppLine201: TppLine;
    ppLine202: TppLine;
    ppLine203: TppLine;
    ppLine204: TppLine;
    ppLine205: TppLine;
    ppLine206: TppLine;
    ppLine207: TppLine;
    ppLine208: TppLine;
    ppLabel209: TppLabel;
    ppLabel210: TppLabel;
    ppLine209: TppLine;
    HedCaption: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppSystemVariable2: TppSystemVariable;
    ppLabel18: TppLabel;
    ppLineRadifHed: TppLine;
    ppLine210: TppLine;
    ppLabel211: TppLabel;
    ppLine211: TppLine;
    ppLabel212: TppLabel;
    ppLine212: TppLine;
    ppLabel213: TppLabel;
    ppLine213: TppLine;
    ppLabel214: TppLabel;
    ppLine214: TppLine;
    ppLabel215: TppLabel;
    ppLine215: TppLine;
    ppLabel216: TppLabel;
    ppLine216: TppLine;
    ppLabel217: TppLabel;
    ppLine217: TppLine;
    ppLabel218: TppLabel;
    ppLine218: TppLine;
    ppLabel219: TppLabel;
    ppLine219: TppLine;
    ppLabel220: TppLabel;
    ppLine220: TppLine;
    ppDetailBand1: TppDetailBand;
    RowLine: TppLine;
    ppDBText201: TppDBText;
    ppDBText202: TppDBText;
    ppDBText203: TppDBText;
    ppDBText204: TppDBText;
    ppDBText205: TppDBText;
    ppDBText206: TppDBText;
    ppDBText207: TppDBText;
    ppDBText208: TppDBText;
    ppLine301: TppLine;
    ppLine302: TppLine;
    ppLine303: TppLine;
    ppLine304: TppLine;
    ppLine305: TppLine;
    ppLine306: TppLine;
    ppLine307: TppLine;
    ppLine308: TppLine;
    ppLine309: TppLine;
    ppDBText209: TppDBText;
    ppDBText210: TppDBText;
    ppDBCalc2: TppDBCalc;
    ppLineRadifDet: TppLine;
    ppLine310: TppLine;
    ppLine311: TppLine;
    ppLine312: TppLine;
    ppLine313: TppLine;
    ppLine314: TppLine;
    ppLine315: TppLine;
    ppLine316: TppLine;
    ppLine317: TppLine;
    ppLine318: TppLine;
    ppLine319: TppLine;
    ppLine320: TppLine;
    ppDBText211: TppDBText;
    ppDBText212: TppDBText;
    ppDBText213: TppDBText;
    ppDBText214: TppDBText;
    ppDBText215: TppDBText;
    ppDBText216: TppDBText;
    ppDBText217: TppDBText;
    ppDBText218: TppDBText;
    ppDBText219: TppDBText;
    ppDBText220: TppDBText;
    ppFooterBand1: TppFooterBand;
    FooterLine: TppLine;
    ppSummaryBand1: TppSummaryBand;
    ppShape4: TppShape;
    SummaryLine: TppLine;
    ppLabel19: TppLabel;
    ppDBCalc3: TppDBCalc;
    ppBDEGridReport: TppBDEPipeline;
    Label21: TLabel;
    CountRecord: TEdit;
    Is_Distinct: TCheckBox;
    Label17: TLabel;
    LabelCount: TLabel;
    Splitter1: TSplitter;
    Memo1: TMemo;
    CapField: TEdit;
    SizeField: TEdit;
    updateQuerySelect3: TADOStoredProc;
    ppRegion1: TppRegion;
    ppShape2: TppShape;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel15: TppLabel;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    CBLineStyle: TComboBox;
    Label4: TLabel;
    Aggregate: TRadioGroup;
    BrowseQuerySelectSrlAggregate: TIntegerField;
    BrowseQuerySelectCapAggregate: TStringField;
    DSBrowseField2: TDataSource;
    BrowseField2: TADOStoredProc;
    BrowseField2FSerial: TAutoIncField;
    BrowseField2Cap_Field: TStringField;
    BrowseField2Size_Field: TIntegerField;
    BrowseField2K_Field: TIntegerField;
    BrowseField2SrlAggregate: TIntegerField;
    Str_Field: TEdit;
    IsCalc: TCheckBox;
    BitBtn11: TBitBtn;
    FindField1: TEdit;
    FindField2: TEdit;
    FindReport: TEdit;
    Label22: TLabel;
    BrowseField2N_TableLookup: TStringField;
    N_Value1: TLabel;
    N_Value2: TLabel;
    SpeedButton14: TSpeedButton;
    SpeedButton16: TSpeedButton;
    Edit1: TEdit;
    SpeedButton17: TSpeedButton;
    SpeedButton18: TSpeedButton;
    SpeedButton19: TSpeedButton;
    SpeedButton20: TSpeedButton;
    SpeedButton21: TSpeedButton;
    SpeedButton22: TSpeedButton;
    SpeedButton23: TSpeedButton;
    Label23: TLabel;
    N_Report: TLabel;
    SpeedButton2: TSpeedButton;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Panel5: TPanel;
    Label25: TLabel;
    SalSpinEdit: TSpinEdit;
    Label24: TLabel;
    Sal: TEdit;
    BrowseQueryFSerial: TAutoIncField;
    BrowseQueryFCode: TIntegerField;
    BrowseQuerySrl_Sal: TIntegerField;
    BrowseQueryFName: TStringField;
    BrowseQueryTitle1: TStringField;
    BrowseQueryTitle2: TStringField;
    BrowseQueryK_Line: TIntegerField;
    BrowseQueryK_Page: TIntegerField;
    BrowseQueryHeight: TFloatField;
    BrowseQueryWidth: TFloatField;
    BrowseQueryIs_Counter: TBooleanField;
    BrowseQueryIs_Distinct: TBooleanField;
    BrowseQueryCount_Record: TIntegerField;
    procedure BADDClick(Sender: TObject);
    procedure BEDITClick(Sender: TObject);
    procedure BSAVEClick(Sender: TObject);
    procedure BDELClick(Sender: TObject);
    procedure BCANCELClick(Sender: TObject);
    procedure BCLOSEClick(Sender: TObject);
    procedure DSBrowseQueryDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure DBGrid4DblClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure DBGrid5DblClick(Sender: TObject);
    procedure DBGrid6DblClick(Sender: TObject);
    procedure DSbrowseOperatorDataChange(Sender: TObject; Field: TField);
    procedure DSbrowseQueryWhereDataChange(Sender: TObject; Field: TField);
    procedure TabSortEnter(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure spbPreviewWholeClick(Sender: TObject);
    procedure spbPreviewWidthClick(Sender: TObject);
    procedure spbPreview100PercentClick(Sender: TObject);
    procedure mskPreviewPercentageKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure bFirstClick(Sender: TObject);
    procedure bPrevClick(Sender: TObject);
    procedure bNextClick(Sender: TObject);
    procedure bEndClick(Sender: TObject);
    procedure mskPreviewPageKeyPress(Sender: TObject; var Key: Char);
    procedure ppViewer1PageChange(Sender: TObject);
    procedure ppViewer1StatusChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabResultExit(Sender: TObject);
    procedure ppLabel15GetText(Sender: TObject; var Text: String);
    procedure ppLabel17GetText(Sender: TObject; var Text: String);
    procedure PrinterOrientationClick(Sender: TObject);
    procedure ppLabel1GetText(Sender: TObject; var Text: String);
    procedure CheckBoxRadifClick(Sender: TObject);
    procedure DSBrowseQuerySelectDataChange(Sender: TObject;
      Field: TField);
    procedure DSBrowseField2DataChange(Sender: TObject; Field: TField);
    procedure BitBtn11Click(Sender: TObject);
    procedure FindField1Change(Sender: TObject);
    procedure FindField1Enter(Sender: TObject);
    procedure FindField1Exit(Sender: TObject);
    procedure DBGrid3KeyPress(Sender: TObject; var Key: Char);
    procedure FindField2Exit(Sender: TObject);
    procedure FindField2Enter(Sender: TObject);
    procedure FindField2Change(Sender: TObject);
    procedure DBGrid7KeyPress(Sender: TObject; var Key: Char);
    procedure FindReportChange(Sender: TObject);
    procedure FindReportEnter(Sender: TObject);
    procedure FindReportExit(Sender: TObject);
    procedure DBGrid10KeyPress(Sender: TObject; var Key: Char);
    procedure Value1KeyPress(Sender: TObject; var Key: Char);
    procedure Value2KeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton14Click(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure SpeedButton17Click(Sender: TObject);
    procedure SpeedButton18Click(Sender: TObject);
    procedure SpeedButton19Click(Sender: TObject);
    procedure SpeedButton20Click(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure SpeedButton22Click(Sender: TObject);
    procedure SpeedButton23Click(Sender: TObject);
    procedure DBGrid7Enter(Sender: TObject);
    procedure DBGrid7Exit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SalSpinEditChange(Sender: TObject);
  private
    procedure FullReportEdits;
    procedure FullWhereEdits;
    procedure ChengeSelectOrder(KChange: integer);
    procedure FBrowseFields;
    procedure FBrowseQuery;
    procedure FBrowseQuerySelect;
    procedure FBrowseOperator;
    procedure FBrowseQueryWhere;
    procedure FBrowseQuerySort;
    procedure PrintGrid;
    procedure FullSelectEdits;
    procedure FBrowseFields2;
    { Private declarations }
  public
    { Public declarations }
    swAddOrEdit : TState;
    FocusedPanel : String;
    BookMark1 : integer;

    SortOrder : integer;
    SrlObject : integer;
    DelAllRecord : Boolean;

    WidthPaper : integer;
    RadifWidth : integer;
    ColumnCount : integer;

    PPLableArray : Array[1..20] of TppLabel;
    PPDataArray  : Array[1..20] of TppDBText;
    PPHeaderLine : Array[1..20] of TppLine;
    PPDetailLine : Array[1..20] of TppLine;
    CountPrintGrid : Integer;
    SaveFileNeme : String;

    SrlValue1, SrlValue2 : integer;
  end;

var
  F_DynamicReport: TF_DynamicReport;

implementation

uses untDm;


{$R *.DFM}

{
procedure TF_Report.FormShow(Sender: TObject);
var
   Vk : Char;
begin
   Vk := #13;
   mskPreviewPage.Text := IntToStr(ppViewer1.AbsolutePageNo);
   mskPreviewPercentage.Text :='100';
   mskPreviewPercentageKeyPress(Self,VK);
   spbPreviewWidthClick(Self);
   CountPrintGrid := 0;
end;

}

procedure TF_DynamicReport.FBrowseQuery;
begin
   NullAdoSPParameters(BrowseQuery);
   BrowseQuery.Close;
   BrowseQuery.Parameters.ParamByName('@SrlObject').Value := SrlObject;
   BrowseQuery.Parameters.ParamByName('@SrlSal').Value := SalSpinEdit.Value;
   BrowseQuery.Open;
end;

procedure TF_DynamicReport.FBrowseFields;
begin
   BrowseField.Close;
   BrowseField.Parameters.ParamByName('@SrlObject').Value := SrlObject;
   BrowseField.Open;
end;

procedure TF_DynamicReport.FBrowseFields2;
begin
   if BrowseQuery.Active then
   begin
      BrowseField2.Close;
      BrowseField2.Parameters.ParamByName('@SrlObject').Value := SrlObject;
      BrowseField2.Parameters.ParamByName('@SrlQuery').Value := BrowseQueryFSerial.AsInteger;
      BrowseField2.Open;
   end;   
end;


procedure TF_DynamicReport.FBrowseQuerySelect;
begin
   BrowseQuerySelect.Close;
   BrowseQuerySelect.Parameters.ParamByName('@SrlQuery').Value := BrowseQueryFSerial.AsInteger;
   BrowseQuerySelect.Open;
end;

procedure TF_DynamicReport.FBrowseQuerySort;
begin
   BrowseQuerySort.Close;
   BrowseQuerySort.Parameters.ParamByName('@SrlQuery').Value := BrowseQueryFSerial.AsInteger;
   BrowseQuerySort.Open;
end;

procedure TF_DynamicReport.FBrowseOperator;
begin
   browseOperator.Close;
   browseOperator.Parameters.ParamByName('@KField').Value := BrowseField2K_Field.AsInteger;
   browseOperator.Open;
end;

procedure TF_DynamicReport.FBrowseQueryWhere;
begin
   browseQueryWhere.Close;
   browseQueryWhere.Parameters.ParamByName('@SrlQuery').Value := BrowseQueryFSerial.AsInteger;
   browseQueryWhere.Open;
end;


procedure TF_DynamicReport.PrintGrid;
var
   i1, j1 : integer;
   LeftObject : integer;
   TotalWidth : integer;
   MaxColumn : integer;
   MyColumn : TColumn;
begin
   CountPrintGrid := CountPrintGrid + 1;

   MaxColumn := 20;
   TotalWidth := 0;
   LeftObject := 0;

   ppBDEGridReport.DataSource := ReportGrid.DataSource;

   TitCaption.Left := WidthPaper Div 2 - TitCaption.spWidth Div 2;
   HedCaption.Left := WidthPaper Div 2 - HedCaption.spWidth Div 2;
   ppLabel1.Left := WidthPaper Div 2 - ppLabel1.spWidth Div 2;
   ppRegion1.Left := WidthPaper Div 2 - ppRegion1.spWidth Div 2;

   ColumnCount := 0;

   for i1 := 0 to ReportGrid.Columns.Count -1 do
      ColumnCount := ColumnCount + 1;

   if ColumnCount > MaxColumn then
      ColumnCount := MaxColumn;

   for i1 := 1 to ReportGrid.Columns.Count  do
      TotalWidth := TotalWidth + ReportGrid.Columns[i1-1].Width;


   LeftObject := WidthPaper Div 2;
   LeftObject := LeftObject + (TotalWidth Div 2) - (RadifWidth Div 2);

   TotalWidth := TotalWidth + RadifWidth;

   ppLabel18.Left := LeftObject;
   ppLabel18.Width := RadifWidth;
   ppLabel18.TextAlignment := taCentered;
   ppDBCalc2.Left := LeftObject;
   ppDBCalc2.Width := RadifWidth;
   ppDBCalc2.TextAlignment := taCentered;

   ppLineRadifHed.Left := LeftObject;
   ppLineRadifDet.Left := LeftObject + RadifWidth - 1;


   for i1 := 1 to MaxColumn do
   begin
      PPLableArray[i1] := Nil;
      PPDataArray[i1] := Nil;
      PPHeaderLine[i1] := Nil;
      PPDetailLine[i1] := Nil;

      if FindComponent('ppLabel' + IntToStr(i1 + 200)) <> nil then
      begin
         PPLableArray[i1] :=  TppLabel(FindComponent('ppLabel' + IntToStr(i1 + 200)));
         PPLableArray[i1].Top := ppLabel18.Top;
      end;
      if FindComponent('ppDBText' + IntToStr(i1 + 200)) <> nil then
      begin
         PPDataArray[i1] := TppDBText(FindComponent('ppDBText' + IntToStr(i1 + 200)));
         PPDataArray[i1].Top := 2;
      end;
      if FindComponent('ppLine' + IntToStr(i1 + 200)) <> nil then
      begin
         PPHeaderLine[i1] := TppLine(FindComponent('ppLine' + IntToStr(i1 + 200)));
         PPHeaderLine[i1].Top := 69;
      end;
      if FindComponent('ppLine' + IntToStr(i1 + 300)) <> nil then
      begin
         PPDetailLine[i1] := TppLine(FindComponent('ppLine' + IntToStr(i1 + 300)));
         PPDetailLine[i1].ParentHeight := True;
      end;

      PPDataArray[i1].DataField := '';

   end;

   ppDetailBand1.Height := 25;

   j1 := 1;
   for i1 := 1 to ReportGrid.Columns.Count  do
   begin
      MyColumn := ReportGrid.Columns[i1-1];

      PPDataArray[j1].Visible  := True;
      PPLableArray[j1].Visible := True;
      PPHeaderLine[j1].Visible := True;
      PPDetailLine[j1].Visible := True;

      PPDataArray[j1].DataField := MyColumn.FieldName;
      PPDataArray[j1].AutoSize := False;

      if CountPrintGrid = 1 then
         if ReportGrid.Columns.Items[i1-1].Field <> nil  then
            if (ReportGrid.Columns.Items[i1-1].Field.DataType in NumberType) OR
              (ReportGrid.Columns.Items[i1-1].Field.Name = 'Browse1FCode') OR
              ((ReportGrid.Columns.Items[i1-1].Field.DataType = ftString) AND
              (ReportGrid.Columns.Items[i1-1].Field.Size = 8))
            then
               PPDataArray[j1].Font := ppDBCalc2.Font
            else
               PPDataArray[j1].Font := ppLabel13.Font;

      if ReportGrid.Columns.Items[i1-1].Field <> nil then
         if ((ReportGrid.Columns.Items[i1-1].Field.DataType = ftCurrency) OR
            (ReportGrid.Columns.Items[i1-1].Field.DataType = ftBCD)) then
             PPDataArray[j1].DisplayFormat := '#,0;-#,0'
         else
             PPDataArray[j1].DisplayFormat := '';

      if ReportGrid.Columns.Items[i1-1].Field <> nil then
         if (ReportGrid.Columns.Items[i1-1].Field.DataType in NumberType) then
         begin
            if MyColumn.Alignment = taLeftJustify then
               PPDataArray[j1].TextAlignment := taLeftJustified
            else if MyColumn.Alignment = taRightJustify then
               PPDataArray[j1].TextAlignment := taRightJustified
            else if MyColumn.Alignment = taCenter then
               PPDataArray[j1].TextAlignment := taCentered;
         end
         else
         begin
            if MyColumn.Alignment = taLeftJustify then
               PPDataArray[j1].TextAlignment := taRightJustified
            else if MyColumn.Alignment = taRightJustify then
               PPDataArray[j1].TextAlignment := taLeftJustified
            else if MyColumn.Alignment = taCenter then
               PPDataArray[j1].TextAlignment := taCentered;
         end;

      PPDataArray[j1].Width := MyColumn.Width - 10 ;
      PPDataArray[j1].Left := LeftObject - MyColumn.Width + 5;

      PPHeaderLine[j1].Left := LeftObject;
      PPDetailLine[j1].Left := LeftObject;

      PPLableArray[j1].AutoSize := False;
      PPLableArray[j1].Caption := Trim(MyColumn.Title.Caption);
      PPLableArray[j1].TextAlignment := taCentered;
      PPLableArray[j1].AutoSize := False;
      PPLableArray[j1].Width := MyColumn.Width;

      LeftObject := LeftObject - MyColumn.Width;

      PPLableArray[j1].Left := LeftObject;
      j1 := j1 + 1;
   end;

   for i1 := j1 to MaxColumn do
   begin
      PPDataArray[i1].Visible := False;
      PPLableArray[i1].Visible := False;
      PPHeaderLine[i1].Visible := False;
      PPDetailLine[i1].Visible := False;
   end;

   PPDetailLine[j1].Visible := True;
   PPDetailLine[j1].Left := LeftObject;

   ppShape1.Width := TotalWidth;
   ppShape1.Left := LeftObject;
   SummaryLine.Width := TotalWidth;
   SummaryLine.Left := LeftObject;
   FooterLine.Width := TotalWidth;
   FooterLine.Left := LeftObject;
   RowLine.Left := LeftObject;
   RowLine.Width := TotalWidth;

   
end;



procedure TF_DynamicReport.ChengeSelectOrder(KChange : integer);
var
   i : integer;
begin
   with updateQuerySelect2 do
   begin
      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@FSerial_1').Value := BrowseQuerySelectFSerial.AsInteger;
      Parameters.ParamByName('@KindOrder').Value := KChange;
      Parameters.ParamByName('@Srl_User_4').Value := Srl_User;
      try
         ExecProc;
         BookMark1 := BrowseQuerySelectFSerial.AsInteger;
         //ChangeEnableButton(Self, 'bSave', swAddOrEdit, FocusedPanel);
         FBrowseQuerySelect;
         BrowseQuerySelect.Locate('FSerial', BookMark1, []);
         DBGrid4.SetFocus;
      except
         ShowErrorMessage(Parameters[0].Value)
      end;
   end;
end;


procedure TF_DynamicReport.FullReportEdits;
begin
   EName.Text := BrowseQueryFName.AsString;
 //  Sal.Text := BrowseQuerySrl_Sal.AsString;
   N_Report.Caption := BrowseQueryFName.AsString;
   MemoTitCaption.Text := BrowseQueryTitle1.AsString;
   EditHedCaption.Text := BrowseQueryTitle2.AsString;
   PrinterOrientation.ItemIndex := BrowseQueryK_Page.AsInteger;
   CountRecord.Text := BrowseQueryCount_Record.AsString;
   Is_Distinct.Checked := BrowseQueryIs_Distinct.AsBoolean;
   CBLineStyle.ItemIndex := BrowseQueryK_Line.AsInteger;
   CheckBoxRadif.Checked := BrowseQueryIs_Counter.AsBoolean;
   if BrowseQueryIs_Counter.AsBoolean then
      RadifWidth := 40
   else
      RadifWidth := 1;

end;

procedure TF_DynamicReport.FullWhereEdits;
begin
   And_Or.ItemIndex := browseQueryWhereAnd_Or.AsInteger;
   Value1.Text := browseQueryWhereValue1.AsString;
   Value2.Text := browseQueryWhereValue2.AsString;
   Value_In.Text := browseQueryWhereValue_In.AsString;
end;

procedure TF_DynamicReport.FullSelectEdits;
begin
   CapField.Text := BrowseQuerySelectCap_Field.AsString;
   SizeField.Text := BrowseQuerySelectSize_Field.AsString;
   Aggregate.ItemIndex := BrowseQuerySelectSrlAggregate.AsInteger;
   FBrowseFields2;
end;


procedure TF_DynamicReport.BADDClick(Sender: TObject);
begin
   if PageControl1.ActivePage = TabWhere then
   begin
     // ChangeEnableButton(Self, 'bAdd', swAddOrEdit, FocusedPanel);
      And_Or.ItemIndex := 0;
      if DBGrid7.Enabled then
         DBGrid7.SetFocus;
   end
   else if PageControl1.ActivePage = TabSelect then
   begin
     // ChangeEnableButton(Self, 'bAdd', swAddOrEdit, FocusedPanel);
      Aggregate.ItemIndex := 0;
      if DBGrid3.Enabled then
         DBGrid3.SetFocus;
   end
   else if PageControl1.ActivePage = TabReports then
   begin
    //  ChangeEnableButton(Self, 'bAdd', swAddOrEdit, FocusedPanel);
      EName.SetFocus;
   end;
   swAddOrEdit := sAdd;
end;

procedure TF_DynamicReport.BEDITClick(Sender: TObject);
begin
   if PageControl1.ActivePage = TabResult then
//      ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel)
   else if PageControl1.ActivePage = TabOption then
   begin
     // ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
      MemoTitCaption.SetFocus;
   end
   else if PageControl1.ActivePage = TabSort then
  //    ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel)
   else if PageControl1.ActivePage = TabWhere then
   begin
      if browseQueryWhere.RecordCount = 0 then
      begin
         ShowMessage('������ ���� ������ ���� ���� �����');
         exit;
      end
      else
      begin
  //       ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
         Value1.SetFocus;
      end;
   end
   else if PageControl1.ActivePage = TabSelect then
   begin
      if BrowseQuerySelect.RecordCount = 0 then
      begin
         ShowMessage('������ ���� ������ ���� ���� �����');
         exit;
      end
      else
      begin
    //     ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
         CapField.SetFocus;
      end;
   end
   else if PageControl1.ActivePage = TabReports then
   begin
      if BrowseQuery.RecordCount = 0 then
      begin
         ShowMessage('������ ���� ������ ���� ���� �����');
         exit;
      end
      else
      begin
         //ChangeEnableButton(Self, 'bEdit', swAddOrEdit, FocusedPanel);
         EName.SetFocus;
      end;
   end;
end;

procedure TF_DynamicReport.BSAVEClick(Sender: TObject);
var
   i,j : integer;
begin
   if PageControl1.ActivePage = TabResult then
      begin
         if swAddOrEdit = sEdit then
         begin
            for j := 0 to ReportGrid.Columns.Count -1 do
               with updateQuerySelect3 do
               begin
                  for i := 0 to Parameters.Count -1  do
                     Parameters[i].Value := null;

                  Parameters.ParamByName('@SrlQuery').Value := BrowseQueryFSerial.AsInteger;
                  Parameters.ParamByName('@Cap_Field').Value := ReportGrid.Columns[j].FieldName;
                  Parameters.ParamByName('@SizeField').Value := ReportGrid.Columns[j].Width;
                  Parameters.ParamByName('@OrderField').Value := j;
                  Parameters.ParamByName('@Srl_User_4').Value := Srl_User;
                  try
                     ExecProc;
                  except
                     ShowErrorMessage(Parameters[0].Value)
                  end;
               end;
         FBrowseQuerySelect;
         end;
      end

   else if (PageControl1.ActivePage = TabOption) OR (PageControl1.ActivePage = TabReports) then
      begin
         if swAddOrEdit = sAdd then
         begin
            NullAdoSPParameters(Insert1);

            Insert1.Parameters.ParamByName('@FName_2').Value := Trim(EName.Text);
            Insert1.Parameters.ParamByName('@Srl_Sal').Value := Trim(Sal.Text);
            Insert1.Parameters.ParamByName('@Srl_Object_3').Value := SrlObject;
            Insert1.Parameters.ParamByName('@Title1_4').Value := Trim(EName.Text);
            Insert1.Parameters.ParamByName('@Title2_5').Value := '';
            Insert1.Parameters.ParamByName('@IsDistinct').Value := True;
            Insert1.Parameters.ParamByName('@CountRecord').Value := 99999999;
            Insert1.Parameters.ParamByName('@K_Line_7').Value := 0;
            Insert1.Parameters.ParamByName('@K_Page_8').Value := 0;
         if Trim(PaperHeight.Text) <> '' then
               Insert1.Parameters.ParamByName('@Height_9').Value := 11;
         if Trim(PaperWidth.Text) <> '' then
               Insert1.Parameters.ParamByName('@Width_10').Value := 8.5;
            Insert1.Parameters.ParamByName('@Is_Counter_11').Value := True;
            try
               Insert1.ExecProc;
               BookMark1 := Insert1.Parameters[0].Value;
               FBrowseQuery;
               BrowseQuery.Locate('FSerial', BookMark1, []);
               DBGrid10.SetFocus;
            except
               ShowErrorMessage(Insert1.Parameters[0].Value)
            end;
         end

         else if swAddOrEdit = sEdit then
         begin
            BookMark1 := BrowseQueryFSerial.AsInteger;

            NullAdoSPParameters(Update1);

            Update1.Parameters.ParamByName('@FSerial_1').Value := BookMark1;
            Update1.Parameters.ParamByName('@SrlSal').Value := Trim(Sal.Text);
            Update1.Parameters.ParamByName('@FName_2').Value := Trim(EName.Text);
            Update1.Parameters.ParamByName('@Title1_3').Value := Trim(MemoTitCaption.Text);
            Update1.Parameters.ParamByName('@Title2_4').Value := Trim(EditHedCaption.Text);
            Update1.Parameters.ParamByName('@IsDistinct').Value := Is_Distinct.Checked;
            if Trim(CountRecord.Text) <> '' then
               Update1.Parameters.ParamByName('@CountRecord').Value := CountRecord.Text;
            Update1.Parameters.ParamByName('@K_Line_6').Value := CBLineStyle.ItemIndex;
            Update1.Parameters.ParamByName('@K_Page_7').Value := PrinterOrientation.ItemIndex;
{
            if Trim(PaperHeight.Text) <> '' then
               Update1.Parameters.ParamByName('@Height_8').Value := StrToFloat(Trim(PaperHeight.Text));
            if Trim(PaperWidth.Text) <> '' then
               Update1.Parameters.ParamByName('@Width_9').Value := StrToFloat(Trim(PaperWidth.Text));
}
            Update1.Parameters.ParamByName('@Is_Counter_10').Value := CheckBoxRadif.Checked;
            try
               Update1.ExecProc;
               FBrowseQuery;
               BrowseQuery.Locate('FSerial', BookMark1, []);
               if PrinterOrientation.ItemIndex = 6 then
                  DBGrid10.SetFocus;
            except
               ShowErrorMessage(Update1.Parameters[0].Value)
            end;
         end;
      end

   else if PageControl1.ActivePage = TabSelect then
      begin
         if swAddOrEdit = sEdit then
         begin
            BookMark1 := BrowseQuerySelectFSerial.AsInteger;

            for i := 0 to UpdateQuerySelect.Parameters.Count -1  do
               UpdateQuerySelect.Parameters[i].Value := null;

            UpdateQuerySelect.Parameters.ParamByName('@FSerial_1').Value := BookMark1;
            UpdateQuerySelect.Parameters.ParamByName('@Cap_Field_3').Value := Trim(CapField.Text);
            UpdateQuerySelect.Parameters.ParamByName('@Size_Field_4').Value := StrToInt(SizeField.Text);
            UpdateQuerySelect.Parameters.ParamByName('@Srl_Aggregate').Value := Aggregate.ItemIndex;
            UpdateQuerySelect.Parameters.ParamByName('@Srl_User_4').Value := Srl_User;

            try
               UpdateQuerySelect.ExecProc;
               FBrowseQuerySelect;
               BrowseQuerySelect.Locate('FSerial', BookMark1, []);
               DBGrid4.SetFocus;
            except
               ShowErrorMessage(UpdateQuerySelect.Parameters[0].Value)
            end;
         end;
      end;
end;

procedure TF_DynamicReport.BDELClick(Sender: TObject);
var
   i : integer;
begin
   if BrowseQuery.RecordCount = 0 then
   begin
     ShowMessage('������ ���� ��� ���� ���� �����');
     exit;
   end
   else
      if MessageDlg('��� ���� ��� ����� �����',
         mtConfirmation, [mbYes, mbNo], 0) = mrYes  then
      begin
         for i := 0 to Delete1.Parameters.Count -1  do
            Delete1.Parameters[i].Value := null;

         Delete1.Parameters.ParamByName('@FSerial_1').Value := BrowseQueryFSerial.AsInteger;
         try
            Delete1.ExecProc;
            BookMark1 := BrowseQuery.RecNo;
            FBrowseQuery;
            if BrowseQuery.RecordCount > 0 then
               BrowseQuery.RecNo := BookMark1;
         except
            ShowMessage(' ����� ������ ��� ���� ��� �������  ');
         end;
      end;
end;

procedure TF_DynamicReport.BCANCELClick(Sender: TObject);
begin
   //ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FullReportEdits;
end;

procedure TF_DynamicReport.BCLOSEClick(Sender: TObject);
begin
   Close;
end;

procedure TF_DynamicReport.DSBrowseQueryDataChange(Sender: TObject;
  Field: TField);
begin
//   ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);
   FullReportEdits;
   FBrowseQuerySelect;
   FBrowseQueryWhere;
   FBrowseQuerySort;
end;

procedure TF_DynamicReport.FormShow(Sender: TObject);
begin
   SrlObject:=6;
   SalSpinEdit.Value := StrToInt(Copy(CurDate,1,2));
   PageControl1.ActivePage := TabReports;
   FocusedPanel := 'PanelQuery';
   FBrowseQuery;
   FBrowseFields;
   FBrowseFields2;
   DelAllRecord := False;
end;


procedure TF_DynamicReport.DBGrid3DblClick(Sender: TObject);
begin
   SpeedButton14Click(Sender);
end;

procedure TF_DynamicReport.DBGrid4DblClick(Sender: TObject);
begin
   SpeedButton16Click(Sender);
end;

procedure TF_DynamicReport.SpeedButton3Click(Sender: TObject);
begin
   ChengeSelectOrder(1);
end;

procedure TF_DynamicReport.SpeedButton5Click(Sender: TObject);
begin
   ChengeSelectOrder(2);
end;

procedure TF_DynamicReport.SpeedButton4Click(Sender: TObject);
begin
   ChengeSelectOrder(3);
end;

procedure TF_DynamicReport.SpeedButton1Click(Sender: TObject);
begin
   ChengeSelectOrder(4);
end;

procedure TF_DynamicReport.DBGrid5DblClick(Sender: TObject);
begin
   SpeedButton17Click(Sender);
end;

procedure TF_DynamicReport.DBGrid6DblClick(Sender: TObject);
begin
   SpeedButton18Click(Sender);
end;

procedure TF_DynamicReport.DSbrowseOperatorDataChange(Sender: TObject;
  Field: TField);
begin
   Case browseOperatorFSerial.AsInteger of
   1..6 :
      begin
         Value1.Visible := True;
         Value2.Visible := False; Label14.Visible := False; N_Value2.Visible := False;
         Value_In.Visible := False;
      end;
   7 :
      begin
         Value1.Visible := True;
         Value2.Visible := True; Label14.Visible := True; N_Value2.Visible := True;
         Value_In.Visible := False;
      end;
   8..10 :
      begin
         Value1.Visible := True;
         Value2.Visible := False;  Label14.Visible := False; N_Value2.Visible := False;
         Value_In.Visible := False;
      end;
   11 :
      begin
         Value1.Visible := False;
         Value2.Visible := False;  Label14.Visible := False;
         Value_In.Visible := True;
      end;
   end;
end;

procedure TF_DynamicReport.DSbrowseQueryWhereDataChange(Sender: TObject;
  Field: TField);
begin
   FullWhereEdits;
end;

procedure TF_DynamicReport.TabSortEnter(Sender: TObject);
begin
   FocusedPanel := 'Panel5';
   //ChangeEnableButton(Self, 'bFilter', swAddOrEdit, FocusedPanel);
   And_Or.ItemIndex := 0;
end;

procedure TF_DynamicReport.PageControl1Change(Sender: TObject);
begin
   if PageControl1.ActivePage = TabResult then
      FocusedPanel := 'PanelResult'
   else if PageControl1.ActivePage = TabOption then
      FocusedPanel := 'PanelOption'
   else if PageControl1.ActivePage = TabSort then
      FocusedPanel := 'PanelSort'
   else if PageControl1.ActivePage = TabWhere then
      FocusedPanel := 'PanelWhere'
   else if PageControl1.ActivePage = TabSelect then
      FocusedPanel := 'PanelSelect'
   else if PageControl1.ActivePage = TabReports then
      FocusedPanel := 'PanelQuery';

  // ChangeEnableButton(Self, 'bCancel', swAddOrEdit, FocusedPanel);

   if PageControl1.ActivePage = TabSheet1 then
   begin
      if ExecQuery.Active then
      begin
         if Trim(MemoTitCaption.Text) <> '' then
         begin
            ppTitleBand1.Visible := True;
            TitCaption.Visible := True;
            TitCaption.Text := MemoTitCaption.Text;
         end
         else
         begin
            ppTitleBand1.Visible := False;
            TitCaption.Caption := '';
         end;

         HedCaption.Caption := BrowseQueryTitle2.AsString;

         if BrowseQueryK_Line.AsInteger <> 0 then
            RowLine.Visible := True
         else
            RowLine.Visible := FALSE;

         Case BrowseQueryK_Line.AsInteger of
            0 : RowLine.Pen.Style := psClear;
            1 : RowLine.Pen.Style := psSolid;
            2 : RowLine.Pen.Style := psDot;
            3 : RowLine.Pen.Style := psDash;
            4 : RowLine.Pen.Style := psDashDot;
            5 : RowLine.Pen.Style := psDashDotDot;
         end;

         if BrowseQueryK_Page.AsInteger = 0 then
         begin
            ppViewer1.Report.PrinterSetup.Orientation := poPortrait;
            WidthPaper := 800;
         end
         else if BrowseQueryK_Page.AsInteger = 1 then
         begin
            ppViewer1.Report.PrinterSetup.Orientation := poLandscape;
            WidthPaper := 1200;
         end
         else
         begin
            GridReport.PrinterSetup.PaperHeight := BrowseQueryHeight.AsInteger;
            GridReport.PrinterSetup.PaperWidth := BrowseQueryWidth.AsInteger;
         end;


         ppViewer1.ZoomSetting := zsPageWidth;
         mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
         pnlPreviewBar.SetFocus;

         ExecQuery.DisableControls;
         PrintGrid;
         ppViewer1.LastPage;
         ppViewer1.FirstPage;
         GridReport.PrintToDevices;
         ExecQuery.EnableControls;
      end
   end
   else if PageControl1.ActivePage = TabResult then
      BEDITClick(Sender)
   else if PageControl1.ActivePage = TabOption then
      BADD.Enabled := False
   else if PageControl1.ActivePage = TabSort then
      BEDITClick(Sender)
   else if PageControl1.ActivePage = TabWhere then
      BEDITClick(Sender)
   else if PageControl1.ActivePage = TabSelect then
      BEDITClick(Sender);
end;

procedure TF_DynamicReport.SpeedButton11Click(Sender: TObject);
begin
   ppViewer1.Print;
end;

procedure TF_DynamicReport.spbPreviewWholeClick(Sender: TObject);
begin
   ppViewer1.ZoomSetting := zsWholePage;
   mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   pnlPreviewBar.SetFocus;
end;

procedure TF_DynamicReport.spbPreviewWidthClick(Sender: TObject);
begin
   ppViewer1.ZoomSetting := zsPageWidth;
   mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   pnlPreviewBar.SetFocus;
end;

procedure TF_DynamicReport.spbPreview100PercentClick(Sender: TObject);
begin
   ppViewer1.ZoomSetting := zs100Percent;
   mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   pnlPreviewBar.SetFocus;
end;

procedure TF_DynamicReport.mskPreviewPercentageKeyPress(Sender: TObject;
  var Key: Char);
var
  liPercentage: Integer;
begin
   if (Key = #13) then
   begin
      liPercentage := StrToIntDef(mskPreviewPercentage.Text, 100);

      ppViewer1.ZoomPercentage := liPercentage;

      spbPreviewWhole.Down := False;
      spbPreviewWidth.Down := False;
      spbPreview100Percent.Down := False;

      mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   end; {if, carriage return pressed}
end;

procedure TF_DynamicReport.SpeedButton12Click(Sender: TObject);
var
   liPercentage: Integer;
begin
   if ppViewer1.CalculatedZoom > 50 then
   begin
      liPercentage := ppViewer1.CalculatedZoom - 20;
      ppViewer1.ZoomPercentage := liPercentage;
      mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   end;
end;

procedure TF_DynamicReport.SpeedButton13Click(Sender: TObject);
var
   liPercentage: Integer;
begin
   if ppViewer1.CalculatedZoom <= 200 then
   begin
      liPercentage := ppViewer1.CalculatedZoom + 20;
      ppViewer1.ZoomPercentage := liPercentage;
      mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   end;
end;

procedure TF_DynamicReport.bFirstClick(Sender: TObject);
begin
   ppViewer1.FirstPage;
end;

procedure TF_DynamicReport.bPrevClick(Sender: TObject);
begin
   ppViewer1.PriorPage;
end;

procedure TF_DynamicReport.bNextClick(Sender: TObject);
begin
   ppViewer1.NextPage;
end;

procedure TF_DynamicReport.bEndClick(Sender: TObject);
begin
   ppViewer1.LastPage;
end;

procedure TF_DynamicReport.mskPreviewPageKeyPress(Sender: TObject;
  var Key: Char);
var
   liPage : Longint;
begin
   if (Key = #13) then
   begin
      liPage := StrToInt(mskPreviewPage.Text);
      ppViewer1.GotoPage(liPage);
   end; {if, carriage return pressed}
end;

procedure TF_DynamicReport.ppViewer1PageChange(Sender: TObject);
begin
   mskPreviewPage.Text := IntToStr(ppViewer1.AbsolutePageNo);
   mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
end;

procedure TF_DynamicReport.ppViewer1StatusChange(Sender: TObject);
begin
   Panel3.Caption := ppViewer1.Status;
end;

procedure TF_DynamicReport.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key = vk_F10 then
      SpeedButton2Click(Sender);

   if PageControl1.ActivePage = TabSheet1 then
      begin
         Case Key of
            13, 80  : ppViewer1.Print;
            33  : ppViewer1.PriorPage;
            34  : ppViewer1.NextPage;
            35  : ppViewer1.LastPage;
            36  : ppViewer1.FirstPage;
            38  : ppViewer1.Perform(EM_LINESCROLL,0,0);
            40  : ppViewer1.Perform(EM_LINESCROLL,0,0);
            106 : spbPreviewWidthClick(Self);
         end;
      end
      else
         if key = vk_Return then
         begin
            Perform(7388420, vk_Tab, 0);
            key := 0;
         end;
end;

procedure TF_DynamicReport.TabResultExit(Sender: TObject);
Var
   MyPenStyle : TPenStyle;
begin
{

}
end;

procedure TF_DynamicReport.ppLabel15GetText(Sender: TObject;
  var Text: String);
begin
   Text := CurDate;
end;

procedure TF_DynamicReport.ppLabel17GetText(Sender: TObject;
  var Text: String);
begin
   Text := N_User;
end;

procedure TF_DynamicReport.PrinterOrientationClick(Sender: TObject);
begin
   if PrinterOrientation.ItemIndex = 2 then
      PSize.Visible := True
   else
      PSize.Visible := False;

   Case PrinterOrientation.ItemIndex of
      0 : begin PaperHeight.Text := '11'; PaperWidth.Text := '8.5'; end;
      1 : begin PaperHeight.Text := '11'; PaperWidth.Text := '8.5'; end;
      2 : begin PaperHeight.Text := ''; PaperWidth.Text := ''; end;
   end

end;

procedure TF_DynamicReport.ppLabel1GetText(Sender: TObject;
  var Text: String);
begin
   Text := N_Company;
end;

procedure TF_DynamicReport.CheckBoxRadifClick(Sender: TObject);
begin
   if CheckBoxRadif.Checked then
   begin
      ppLabel18.Visible := True;
      ppDBCalc2.Visible := True;
   end
   else
   begin
      ppLabel18.Visible := False;
      ppDBCalc2.Visible := False;
   end
end;

procedure TF_DynamicReport.DSBrowseQuerySelectDataChange(Sender: TObject;
  Field: TField);
begin
   FullSelectEdits;
end;

procedure TF_DynamicReport.DSBrowseField2DataChange(Sender: TObject;
  Field: TField);
begin
   FBrowseOperator;
   Case BrowseField2K_Field.AsInteger of
   3 :
      begin
         Value1.EditMask := '!99/99/99;1;_';
         Value2.EditMask := '!99/99/99;1;_';
      end;
   4 :
      begin
         Value1.EditMask := '!90:00;1;_';
         Value2.EditMask := '!90:00;1;_';
      end;
   else
      begin
         Value1.EditMask := '';
         Value2.EditMask := '';
      end;
   end;
end;

procedure TF_DynamicReport.BitBtn11Click(Sender: TObject);
begin
   Str_Field.Text := Str_Field.Text + '<' + BrowseFieldCap_Field.AsString + '>';
end;

procedure TF_DynamicReport.FindField1Change(Sender: TObject);
Var
   Str : String;
begin
   Str := '';
   BrowseField.Filtered := False;
   if (FindField1.text <> '') then
   Begin
      Str := ' (Cap_Field like ' + QuotedStr('%' + FindField1.text + '%')+ ')';
   End;
   if Str = '' then Exit;

   BrowseField.Filter := Str;
   BrowseField.Filtered := True;
end;

procedure TF_DynamicReport.FindField1Enter(Sender: TObject);
begin
   KeyPreview := False;
   DBGrid3.SetFocus;
end;

procedure TF_DynamicReport.FindField1Exit(Sender: TObject);
begin
   KeyPreview := True;
end;

procedure TF_DynamicReport.DBGrid3KeyPress(Sender: TObject; var Key: Char);
begin
   if Key IN ValidDigitAlphabet then
      FindField1.Text := FindField1.Text + Key;

   if (ord(Key) = 8) then
      FindField1.Text := Copy(FindField1.Text,1,Length(FindField1.Text)-1);
end;

procedure TF_DynamicReport.FindField2Exit(Sender: TObject);
begin
   KeyPreview := True;
end;

procedure TF_DynamicReport.FindField2Enter(Sender: TObject);
begin
   KeyPreview := False;
   DBGrid7.SetFocus;
end;

procedure TF_DynamicReport.FindField2Change(Sender: TObject);
Var
   Str : String;
begin
   Str := '';
   BrowseField2.Filtered := False;
   if (FindField2.text <> '') then
   Begin
      Str := ' (Cap_Field like ' + QuotedStr('%' + FindField2.text + '%')+ ')';
   End;
   if Str = '' then Exit;

   BrowseField2.Filter := Str;
   BrowseField2.Filtered := True;
end;

procedure TF_DynamicReport.DBGrid7KeyPress(Sender: TObject; var Key: Char);
begin
   if Key IN ValidDigitAlphabet then
      FindField2.Text := FindField2.Text + Key;

   if (ord(Key) = 8) then
      FindField2.Text := Copy(FindField2.Text,1,Length(FindField2.Text)-1);
end;

procedure TF_DynamicReport.FindReportChange(Sender: TObject);
Var
   Str : String;
begin
   Str := '';
   BrowseQuery.Filtered := False;
   if (FindReport.text <> '') then
   Begin
      Str := ' (FName like ' + QuotedStr('%' + FindReport.text + '%')+ ')';
   End;
   if Str = '' then Exit;

   BrowseQuery.Filter := Str;
   BrowseQuery.Filtered := True;
end;

procedure TF_DynamicReport.FindReportEnter(Sender: TObject);
begin
   KeyPreview := False;
   DBGrid10.SetFocus;
end;

procedure TF_DynamicReport.FindReportExit(Sender: TObject);
begin
   KeyPreview := True;
end;

procedure TF_DynamicReport.DBGrid10KeyPress(Sender: TObject; var Key: Char);
begin
   if Key IN ValidDigitAlphabet then
      FindReport.Text := FindReport.Text + Key;

   if (ord(Key) = 8) then
      FindReport.Text := Copy(FindReport.Text,1,Length(FindReport.Text)-1);
end;

procedure TF_DynamicReport.Value1KeyPress(Sender: TObject; var Key: Char);
begin
   if (Key = #13) and (Trim(Value1.Text) <> '') then
      SpeedButton19Click(Sender)
   else
   if Trim(BrowseField2N_TableLookup.AsString) <> '' then
      EditCodeKeyPress(Key, swAddOrEdit, TEdit(Value1), N_Value1,
         SrlValue1, -1, 2, Trim(BrowseField2N_TableLookup.AsString), '')
end;

procedure TF_DynamicReport.Value2KeyPress(Sender: TObject; var Key: Char);
begin
   if Trim(BrowseField2N_TableLookup.AsString) <> '' then
      EditCodeKeyPress(Key, swAddOrEdit, TEdit(Value2), N_Value2,
         SrlValue2, -1, 2, Trim(BrowseField2N_TableLookup.AsString), '')
end;

procedure TF_DynamicReport.SpeedButton14Click(Sender: TObject);
var
   i : integer;
begin
   if BrowseQuerySelect.Locate('Cap_Field;SrlAggregate',
      VarArrayOf([BrowseFieldCap_Field.AsString, Aggregate.ItemIndex]), []) then
   begin
      ShowMessage('����� ���� �� ���� ������� ������� ���� ���� ' +
      #13 + #13 +
      '      ����� ���� ����� �� ��� ����');
      exit;
   end;

   NullAdoSPParameters(InsertQuerySelect);
   With InsertQuerySelect do
   begin
      Parameters.ParamByName('@Srl_Query_1').Value := BrowseQueryFSerial.AsInteger;
  //    DFGDFG
      Parameters.ParamByName('@Srl_Field_2').Value := BrowseFieldFSerial.AsInteger;
//      FGDF
      Parameters.ParamByName('@Srl_Aggregate').Value := Aggregate.ItemIndex;
      Parameters.ParamByName('@IsCalc').Value := IsCalc.Checked;
      Parameters.ParamByName('@Str_Field').Value := Trim(Str_Field.Text);
      Parameters.ParamByName('@Srl_User_6').Value := Srl_User;
      try
         ExecProc;
         BookMark1 := Parameters[0].Value;
         FBrowseQuerySelect;
         BrowseQuerySelect.Locate('FSerial', BookMark1, []);
         DBGrid4.SetFocus;
      except
         ShowErrorMessage(Parameters[0].Value)
      end;
   end;
end;

procedure TF_DynamicReport.SpeedButton16Click(Sender: TObject);
var
   i : integer;
begin
   if BrowseQuerySelect.RecordCount = 0 then
   begin
     ShowMessage('������ ���� ��� ���� ���� �����');
     exit;
   end
   else
//      if MessageDlg('��� ���� ��� ����� �����',
//         mtConfirmation, [mbYes, mbNo], 0) = mrYes  then
      begin
         for i := 0 to DeleteQuerySelect.Parameters.Count -1  do
            DeleteQuerySelect.Parameters[i].Value := null;

         DeleteQuerySelect.Parameters.ParamByName('@FSerial_1').Value := BrowseQuerySelectFSerial.AsInteger;
         try
            DeleteQuerySelect.ExecProc;
            BookMark1 := BrowseQuerySelect.RecNo;
            if not DelAllRecord then
            begin
               FBrowseQuerySelect;
               FBrowseQuerySort;
               FBrowseQueryWhere;
            end;
            if BrowseQuerySelect.RecordCount > 0 then
               BrowseQuerySelect.RecNo := BookMark1;

         except
            ShowMessage(' ����� ������ ��� ���� ��� �������  ');
         end;
      end;
end;

procedure TF_DynamicReport.SpeedButton17Click(Sender: TObject);
var
   i : integer;
begin
   With insertQuerySort do
   begin
      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@Srl_Query_1').Value := BrowseQueryFSerial.AsInteger;
      Parameters.ParamByName('@Srl_Field_2').Value := BrowseField2FSerial.AsInteger;
      Parameters.ParamByName('@Srl_Aggregate').Value := BrowseField2SrlAggregate.AsInteger;
      Parameters.ParamByName('@Is_Desc_3').Value := Is_Desc.ItemIndex;
      Parameters.ParamByName('@Srl_User_5').Value := Srl_User;
      try
         ExecProc;
         BookMark1 := Parameters[0].Value;
         FBrowseQuerySort;
         BrowseQuerySort.Locate('FSerial', BookMark1, []);
         DBGrid6.SetFocus;
      except
         ShowErrorMessage(Parameters[0].Value)
      end;
   end;
end;

procedure TF_DynamicReport.SpeedButton18Click(Sender: TObject);
var
   i : integer;
begin
   if BrowseQuerySort.RecordCount = 0 then
   begin
     ShowMessage('������ ���� ��� ���� ���� �����');
     exit;
   end
   else
//      if MessageDlg('��� ���� ��� ����� �����',
//         mtConfirmation, [mbYes, mbNo], 0) = mrYes  then
      begin
         for i := 0 to DeleteQuerySort.Parameters.Count -1  do
            DeleteQuerySort.Parameters[i].Value := null;

         DeleteQuerySort.Parameters.ParamByName('@FSerial_1').Value := BrowseQuerySortFSerial.AsInteger;
         try
            DeleteQuerySort.ExecProc;
            BookMark1 := BrowseQuerySort.RecNo;
            if not DelAllRecord then
               FBrowseQuerySort;
            if BrowseQuerySort.RecordCount > 0 then
               BrowseQuerySort.RecNo := BookMark1;
         except
            ShowMessage(' ����� ������ ��� ���� ��� �������  ');
         end;
      end;
end;

procedure TF_DynamicReport.SpeedButton19Click(Sender: TObject);
var
   i : integer;
begin
   With insertQueryWhere do
   begin
      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@Srl_Query_1').Value := BrowseQueryFSerial.AsInteger;
      Parameters.ParamByName('@Sub_Level_2').Value := 1;
      Parameters.ParamByName('@Srl_Field1_3').Value := BrowseField2FSerial.AsInteger;
      Parameters.ParamByName('@Srl_Aggregate').Value := BrowseField2SrlAggregate.AsInteger;
      Parameters.ParamByName('@Srl_Operator_6').Value := browseOperatorFSerial.AsInteger;
      Parameters.ParamByName('@Value1_7').Value := Value1.Text;
      Parameters.ParamByName('@Value2_8').Value := Value2.Text;
      Parameters.ParamByName('@Value_In_10').Value := Value_In.Text;
      Parameters.ParamByName('@And_Or_11').Value := And_Or.ItemIndex;
      Parameters.ParamByName('@Srl_User_12').Value := Srl_User;
      try
         ExecProc;
         BookMark1 := Parameters[0].Value;
         FBrowseQueryWhere;
         browseQueryWhere.Locate('FSerial', BookMark1, []);
         BADDClick(Sender);
         DBGrid9.SetFocus;
      except
         ShowErrorMessage(Parameters[0].Value)
      end;
   end;
end;

procedure TF_DynamicReport.SpeedButton20Click(Sender: TObject);
var
   i : integer;
begin
   if browseQueryWhere.RecordCount = 0 then
   begin
     ShowMessage('������ ���� ��� ���� ���� �����');
     exit;
   end
   else
//      if MessageDlg('��� ���� ��� ����� �����',
//         mtConfirmation, [mbYes, mbNo], 0) = mrYes  then
      begin
         for i := 0 to deleteQueryWhere.Parameters.Count -1  do
            deleteQueryWhere.Parameters[i].Value := null;

         deleteQueryWhere.Parameters.ParamByName('@FSerial_1').Value := BrowseQueryWhereFSerial.AsInteger;
         try
            deleteQueryWhere.ExecProc;
            BookMark1 := browseQueryWhere.RecNo;
            if not DelAllRecord then
               FBrowseQueryWhere;
            if browseQueryWhere.RecordCount > 0 then
               browseQueryWhere.RecNo := BookMark1;
         except
            ShowMessage(' ����� ������ ��� ���� ��� �������  ');
         end;
      end;
end;

procedure TF_DynamicReport.SpeedButton21Click(Sender: TObject);
var
   j : integer;
begin
   DelAllRecord := True;

   if browseQueryWhere.RecordCount > 0 then
   begin
      browseQueryWhere.First;
      for j := 0 to browseQueryWhere.RecordCount - 1 do
      begin
         SpeedButton20Click(Sender);
         browseQueryWhere.Next;
      end;
   end;

   DelAllRecord := False;
   FBrowseQueryWhere;
end;

procedure TF_DynamicReport.SpeedButton22Click(Sender: TObject);
var
   j : integer;
begin
   DelAllRecord := True;

   if BrowseQuerySelect.RecordCount > 0 then
   begin
      BrowseQuerySelect.First;
      for j := 0 to BrowseQuerySelect.RecordCount - 1 do
      begin
         SpeedButton16Click(Sender);
         BrowseQuerySelect.Next;
      end;
   end;

   DelAllRecord := False;
   FBrowseQuerySelect;
   FBrowseQuerySort;
   FBrowseQueryWhere;
end;

procedure TF_DynamicReport.SpeedButton23Click(Sender: TObject);
var
   j : integer;
begin
   DelAllRecord := True;

   if BrowseQuerySort.RecordCount > 0 then
   begin
      BrowseQuerySort.First;
      for j := 0 to BrowseQuerySort.RecordCount - 1 do
      begin
         SpeedButton18Click(Sender);
         BrowseQuerySort.Next;
      end;
   end;

   DelAllRecord := False;
   FBrowseQuerySort;
end;

procedure TF_DynamicReport.DBGrid7Enter(Sender: TObject);
begin
   TDBGrid(Sender).Color := $00FFFF80;
end;

procedure TF_DynamicReport.DBGrid7Exit(Sender: TObject);
begin
   TDBGrid(Sender).Color := clWhite;
end;

procedure TF_DynamicReport.SpeedButton2Click(Sender: TObject);
var
   i : integer;
   StrQuery : String;
begin
//   StartWaitAvi;
   if BrowseQuerySelect.RecordCount = 0 then
   begin
      ShowMessage('�� ����� ���� ����� ���� ������ ���� ���');
      exit;
   end;

   NullAdoSPParameters(MakeQuery);
   With MakeQuery do
   begin
      Parameters.ParamByName('@SrlQuery').Value := BrowseQueryFSerial.AsInteger;
      try
         ExecProc;
         StrQuery := Parameters.ParamByName('@StrQuery').Value;
         Memo1.Text := StrQuery;
      except
         ShowErrorMessage(Parameters[0].Value)
      end;
   end;

   ExecQuery.Connection := F_Common.SystemConnection;
   NullAdoSPParameters(ExecQuery);
   With ExecQuery do
   begin
      Parameters.ParamByName('@StrQuery').Value := StrQuery;
      try
         Open;
         LabelCount.Caption := IntToStr(RecordCount);

         for i := 0 to ReportGrid.Columns.Count -1 do
            ReportGrid.Columns.Delete(0);

         for i := 0 to FieldCount -1 do
         begin
            With ReportGrid.Columns.Add do
            begin
               FieldName := Fields[i].FieldName;
               Title.Caption := FieldName;
               if BrowseQuerySelect.Locate('Cap_Field', FieldName, []) then
                  Width := BrowseQuerySelectSize_Field.AsInteger
               else
                  Width := 75;
            end;
         end;
      except
         ShowErrorMessage(Parameters[0].Value)
      end;
   end;
   PageControl1.ActivePage := TabResult;
//   EndWaitAvi;
end;

procedure TF_DynamicReport.SalSpinEditChange(Sender: TObject);
begin
   FBrowseQuery;
end;

end.
