object F_Common: TF_Common
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 106
  Top = 98
  Height = 551
  Width = 729
  object Pub_BROWSE_Per: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Pub_BROWSE_Per;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CPer'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@SrlPer'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Name1'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 240
    Top = 128
  end
  object Pub_BROWSE_Role_Field3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Pub_BROWSE_Role_Field3;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@N_Filed'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Srl_User'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@KAccess'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 72
  end
  object Pub_BROWSE_Role_Field1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Pub_BROWSE_Role_Field1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@C_User'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Pass'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@KAccess'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 176
  end
  object Pub_BROWSE_Per1: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Pub_BROWSE_Per1;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 10
      end>
    Left = 248
    Top = 72
    object Pub_BROWSE_Per1srl_per1: TIntegerField
      FieldName = 'srl_per1'
    end
    object Pub_BROWSE_Per1srl_User: TIntegerField
      FieldName = 'srl_User'
    end
    object Pub_BROWSE_Per1n_per: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object Pub_BROWSE_Per1family: TStringField
      FieldName = 'family'
      Size = 25
    end
    object Pub_BROWSE_Per1c_hazineh: TIntegerField
      FieldName = 'c_hazineh'
    end
    object Pub_BROWSE_Per1n_hazineh: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object Pub_INSERT_System_Form_Field1: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Pub_INSERT_System_Form_Field1;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_Form_1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Cap_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@N_Field_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Cap_Field_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 198
    Top = 199
  end
  object sp_GetCurrentFarsiDate: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_GetCurrentFarsiDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CurDate'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 8
        Value = Null
      end>
    Left = 184
    Top = 376
  end
  object sp_Find_T_Table: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Find_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FieldParamName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FieldParamValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 496
    Top = 154
  end
  object Delete1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Delete_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 160
  end
  object Sp_Get_Max: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Get_Max_T_Table;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FieldParamName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FieldParamValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 8
  end
  object Insert1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Insert_T_Table;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 64
  end
  object Update1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Update_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 112
  end
  object Browse1: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_Browse_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'T_Kol'
      end
      item
        Name = '@FieldParam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@KindParam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 16
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TStringField
      FieldName = 'FName'
      Size = 100
    end
  end
  object Browse2: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_Browse_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'T_Moeen'
      end
      item
        Name = '@FieldParam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'Srl_Kol'
      end
      item
        Name = '@KindParam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end>
    Left = 384
    Top = 16
    object Browse2FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse2FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse2FName: TStringField
      FieldName = 'FName'
      Size = 100
    end
  end
  object SystemConnection: TADOConnection
    ConnectionTimeout = 10
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AfterConnect = SystemConnectionAfterConnect
    OnExecuteComplete = SystemConnectionExecuteComplete
    OnWillExecute = SystemConnectionWillExecute
    Left = 56
    Top = 16
  end
  object Pub_InitUser: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Pub_InitUser;1'
    Parameters = <>
    Left = 408
    Top = 224
    object IntegerField1: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField2: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField1: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField2: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField3: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField3: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object INSERT_Form_Proc: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Common_SP_Insert_Pub_System_Form_Proc_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_Form'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 200
        Value = Null
      end
      item
        Name = '@Cap_Form'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 200
        Value = Null
      end
      item
        Name = '@N_Proc_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@Cap_Proc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@Dis_Proc_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@Srl_Connection'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@K_Proc_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 70
    Top = 231
  end
  object sp_Get_Max_T_Table2: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Get_Max_T_Table2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 56
  end
  object sp_Find_T_Table2: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Find_T_Table2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 496
    Top = 202
  end
  object Session1: TSession
    Left = 72
    Top = 392
  end
  object Query1: TQuery
    Left = 320
    Top = 256
  end
  object sp_FullCycleInfo: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_FullCycleInfo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlCurCycle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CCurCycle'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NCurCycle'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@KCurCycle'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 408
    Top = 272
    object IntegerField4: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField5: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField4: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField5: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField6: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField6: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object sp_Find_T_Table3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Mysp_Find_T_Table3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 496
    Top = 250
  end
  object SP_GetSystemVersion: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'SP_GetSystemVersion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Last_Version'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 256
    object IntegerField10: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField11: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField10: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField11: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField12: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField12: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object sp_Get_Max_T_Table3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Mysp_Get_Max_T_Table3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 104
  end
  object SP_GetSystemInfo: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'SP_GetSystemInfo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_System'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@N_Company'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@Cap_System'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@Is_Periodic'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 408
    Top = 176
    object IntegerField13: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField14: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField13: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField14: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField15: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField15: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object Pub_BROWSE_Form_Fields: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Pub_BROWSE_Form_Fields;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@N_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = '1'
      end
      item
        Name = '@Srl_User'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 72
    Top = 128
    object Pub_BROWSE_Form_FieldsN_Field: TStringField
      FieldName = 'N_Field'
      Size = 50
    end
    object Pub_BROWSE_Form_FieldsK_Access: TIntegerField
      FieldName = 'K_Access'
    end
  end
  object sp_GetNextDate: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_GetNextDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MyDate'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MyDif'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NextDate'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 8
        Value = Null
      end>
    Left = 72
    Top = 328
    object IntegerField7: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField8: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField7: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField8: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField9: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField9: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object QueryConnection: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnExecuteComplete = SystemConnectionExecuteComplete
    OnWillExecute = SystemConnectionWillExecute
    Left = 168
    Top = 16
  end
  object sp_SetUserWork: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_SetUserWork'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KindWork'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Srl_System_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_User_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Connect'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 78
    Top = 279
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 592
    Top = 288
  end
  object Delete3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Mysp_Delete_T_Table3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SerialValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 208
  end
  object sp_GetCurrentServerTime: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_GetCurrentServerTime;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CurTime'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 5
        Value = Null
      end>
    Left = 408
    Top = 328
  end
  object sp_GetSystemPermision: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_GetSystemPermision;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlSystem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Is_Permision'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 304
    object IntegerField16: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField17: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField16: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField17: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField18: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField18: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object insert_T_Letter_System: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'insert_T_Letter_System;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Record'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_User'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 304
  end
  object Browse_Pub_Reports: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_Pub_Reports'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlReport'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 496
    Top = 352
    object Browse_Pub_ReportsVal_Report: TMemoField
      FieldName = 'Val_Report'
      BlobType = ftMemo
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'JPEG Image File (*.jpg)|*.jpg'
    Left = 592
    Top = 232
  end
  object insert_T_Student_Pic_1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'insert_T_Student_Pic_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@Srl_Student_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Pic_Student_4'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end>
    Left = 408
    Top = 376
  end
  object BROWSE_Student_Pic: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_T_Student_Pic;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Srl_Student'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 280
    Top = 355
    object BROWSE_Student_PicPic_Student: TBlobField
      FieldName = 'Pic_Student'
    end
    object BROWSE_Student_PicFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
  end
  object Browse_T_StudentOne: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_T_StudentOne;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlStudent'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 254
      end>
    Left = 328
    Top = 304
    object Browse_T_StudentOneFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse_T_StudentOneFName: TStringField
      FieldName = 'FName'
      ReadOnly = True
      Size = 303
    end
    object Browse_T_StudentOneFName1: TStringField
      FieldName = 'FName1'
      Size = 50
    end
    object Browse_T_StudentOneFName2: TStringField
      FieldName = 'FName2'
      Size = 100
    end
    object Browse_T_StudentOneFCode: TIntegerField
      FieldName = 'FCode'
      ReadOnly = True
    end
    object Browse_T_StudentOneK_Docum: TIntegerField
      FieldName = 'K_Docum'
    end
    object Browse_T_StudentOneK_StateDocum: TIntegerField
      FieldName = 'K_StateDocum'
    end
    object Browse_T_StudentOneNum_Per: TIntegerField
      FieldName = 'Num_Per'
    end
    object Browse_T_StudentOneNum_Cart: TIntegerField
      FieldName = 'Num_Cart'
    end
    object Browse_T_StudentOneNum_Melli: TStringField
      FieldName = 'Num_Melli'
      Size = 100
    end
    object Browse_T_StudentOneSrl_KPer: TIntegerField
      FieldName = 'Srl_KPer'
    end
    object Browse_T_StudentOneNum_Docum: TIntegerField
      FieldName = 'Num_Docum'
    end
    object Browse_T_StudentOneInput_Sal: TIntegerField
      FieldName = 'Input_Sal'
    end
    object Browse_T_StudentOneShohrat: TStringField
      FieldName = 'Shohrat'
      Size = 100
    end
    object Browse_T_StudentOneN_Father: TStringField
      FieldName = 'N_Father'
      Size = 100
    end
    object Browse_T_StudentOneNum_Shen: TStringField
      FieldName = 'Num_Shen'
      Size = 50
    end
    object Browse_T_StudentOneD_Brith: TStringField
      FieldName = 'D_Brith'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneL_Brith: TStringField
      FieldName = 'L_Brith'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Sodoor: TIntegerField
      FieldName = 'Srl_Sodoor'
    end
    object Browse_T_StudentOneD_Sodoor: TStringField
      FieldName = 'D_Sodoor'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneSrl_DegU: TIntegerField
      FieldName = 'Srl_DegU'
    end
    object Browse_T_StudentOneD_DegU: TStringField
      FieldName = 'D_DegU'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneSrl_ReshU: TIntegerField
      FieldName = 'Srl_ReshU'
    end
    object Browse_T_StudentOneSrl_DegH: TIntegerField
      FieldName = 'Srl_DegH'
    end
    object Browse_T_StudentOneSrl_ReshH: TIntegerField
      FieldName = 'Srl_ReshH'
    end
    object Browse_T_StudentOneAvrg: TFloatField
      FieldName = 'Avrg'
    end
    object Browse_T_StudentOneK_Maskan: TIntegerField
      FieldName = 'K_Maskan'
    end
    object Browse_T_StudentOneK_Jens: TIntegerField
      FieldName = 'K_Jens'
    end
    object Browse_T_StudentOneK_Tahol: TIntegerField
      FieldName = 'K_Tahol'
    end
    object Browse_T_StudentOneD_Marrid: TStringField
      FieldName = 'D_Marrid'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneK_Lebas: TIntegerField
      FieldName = 'K_Lebas'
    end
    object Browse_T_StudentOneK_Moamam: TIntegerField
      FieldName = 'K_Moamam'
    end
    object Browse_T_StudentOneD_Moamam: TStringField
      FieldName = 'D_Moamam'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneNum_Child: TIntegerField
      FieldName = 'Num_Child'
    end
    object Browse_T_StudentOneNum_Takafol: TIntegerField
      FieldName = 'Num_Takafol'
    end
    object Browse_T_StudentOneIs_Worker: TIntegerField
      FieldName = 'Is_Worker'
    end
    object Browse_T_StudentOneK_Gharardad: TIntegerField
      FieldName = 'K_Gharardad'
    end
    object Browse_T_StudentOneP_Salary: TBCDField
      FieldName = 'P_Salary'
      Precision = 19
    end
    object Browse_T_StudentOneCurWork: TStringField
      FieldName = 'CurWork'
      Size = 100
    end
    object Browse_T_StudentOneFather_Work: TStringField
      FieldName = 'Father_Work'
      Size = 100
    end
    object Browse_T_StudentOneHome_Adress: TStringField
      FieldName = 'Home_Adress'
      Size = 200
    end
    object Browse_T_StudentOneSrl_City: TIntegerField
      FieldName = 'Srl_City'
    end
    object Browse_T_StudentOneC_Post: TStringField
      FieldName = 'C_Post'
      Size = 100
    end
    object Browse_T_StudentOneHome_Tel: TStringField
      FieldName = 'Home_Tel'
      Size = 100
    end
    object Browse_T_StudentOneSrl_KNezam: TIntegerField
      FieldName = 'Srl_KNezam'
    end
    object Browse_T_StudentOneD_Ezam: TStringField
      FieldName = 'D_Ezam'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_MohlatKNezam: TStringField
      FieldName = 'D_MohlatKNezam'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneWork_Adress: TStringField
      FieldName = 'Work_Adress'
      Size = 200
    end
    object Browse_T_StudentOneWork_Tel: TStringField
      FieldName = 'Work_Tel'
      Size = 100
    end
    object Browse_T_StudentOneC_Classe: TStringField
      FieldName = 'C_Classe'
      Size = 50
    end
    object Browse_T_StudentOneD_Classe: TStringField
      FieldName = 'D_Classe'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_StartStudy: TStringField
      FieldName = 'D_StartStudy'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_Compelete: TStringField
      FieldName = 'D_Compelete'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_EndStudy: TStringField
      FieldName = 'D_EndStudy'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneDis_Per: TStringField
      FieldName = 'Dis_Per'
      Size = 200
    end
    object Browse_T_StudentOneDay_Jebhe: TIntegerField
      FieldName = 'Day_Jebhe'
    end
    object Browse_T_StudentOneDay_Esarat: TIntegerField
      FieldName = 'Day_Esarat'
    end
    object Browse_T_StudentOneDay_Zendan: TIntegerField
      FieldName = 'Day_Zendan'
    end
    object Browse_T_StudentOneIs_Basij: TIntegerField
      FieldName = 'Is_Basij'
    end
    object Browse_T_StudentOneDay_Basij: TIntegerField
      FieldName = 'Day_Basij'
    end
    object Browse_T_StudentOnePrc_Janbazi: TIntegerField
      FieldName = 'Prc_Janbazi'
    end
    object Browse_T_StudentOneExam_Mark: TFloatField
      FieldName = 'Exam_Mark'
    end
    object Browse_T_StudentOneDialog_Mark: TFloatField
      FieldName = 'Dialog_Mark'
    end
    object Browse_T_StudentOneRank: TIntegerField
      FieldName = 'Rank'
    end
    object Browse_T_StudentOneC_KNezam: TIntegerField
      FieldName = 'C_KNezam'
    end
    object Browse_T_StudentOneN_KNezam: TStringField
      FieldName = 'N_KNezam'
      Size = 100
    end
    object Browse_T_StudentOneC_Sodoor: TIntegerField
      FieldName = 'C_Sodoor'
    end
    object Browse_T_StudentOneN_Sodoor: TStringField
      FieldName = 'N_Sodoor'
      Size = 100
    end
    object Browse_T_StudentOneC_DegU: TIntegerField
      FieldName = 'C_DegU'
    end
    object Browse_T_StudentOneN_DegU: TStringField
      FieldName = 'N_DegU'
      Size = 100
    end
    object Browse_T_StudentOneC_ReshU: TIntegerField
      FieldName = 'C_ReshU'
    end
    object Browse_T_StudentOneN_ReshU: TStringField
      FieldName = 'N_ReshU'
      Size = 100
    end
    object Browse_T_StudentOneC_ReshH: TIntegerField
      FieldName = 'C_ReshH'
    end
    object Browse_T_StudentOneN_ReshH: TStringField
      FieldName = 'N_ReshH'
      Size = 100
    end
    object Browse_T_StudentOneC_DegH: TIntegerField
      FieldName = 'C_DegH'
    end
    object Browse_T_StudentOneN_DegH: TStringField
      FieldName = 'N_DegH'
      Size = 100
    end
    object Browse_T_StudentOneC_City: TIntegerField
      FieldName = 'C_City'
    end
    object Browse_T_StudentOneN_City: TStringField
      FieldName = 'N_City'
      Size = 100
    end
    object Browse_T_StudentOneC_KPer: TIntegerField
      FieldName = 'C_KPer'
    end
    object Browse_T_StudentOneN_KPer: TStringField
      FieldName = 'N_KPer'
      Size = 100
    end
    object Browse_T_StudentOneSrl_SourceInput: TIntegerField
      FieldName = 'Srl_SourceInput'
    end
    object Browse_T_StudentOneC_SourceInput: TIntegerField
      FieldName = 'C_SourceInput'
    end
    object Browse_T_StudentOneN_SourceInput: TStringField
      FieldName = 'N_SourceInput'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Melli: TIntegerField
      FieldName = 'Srl_Melli'
    end
    object Browse_T_StudentOneC_Melli: TIntegerField
      FieldName = 'C_Melli'
    end
    object Browse_T_StudentOneN_Melli: TStringField
      FieldName = 'N_Melli'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Deen: TIntegerField
      FieldName = 'Srl_Deen'
    end
    object Browse_T_StudentOneC_Deen: TIntegerField
      FieldName = 'C_Deen'
    end
    object Browse_T_StudentOneN_Deen: TStringField
      FieldName = 'N_Deen'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Univer: TIntegerField
      FieldName = 'Srl_Univer'
    end
    object Browse_T_StudentOneC_Univer: TIntegerField
      FieldName = 'C_Univer'
    end
    object Browse_T_StudentOneN_Univer: TStringField
      FieldName = 'N_Univer'
      Size = 100
    end
    object Browse_T_StudentOneSrl_School: TIntegerField
      FieldName = 'Srl_School'
    end
    object Browse_T_StudentOneC_School: TIntegerField
      FieldName = 'C_School'
    end
    object Browse_T_StudentOneN_School: TStringField
      FieldName = 'N_School'
      Size = 100
    end
    object Browse_T_StudentOneSrl_KEsar: TIntegerField
      FieldName = 'Srl_KEsar'
    end
    object Browse_T_StudentOneC_KEsar: TIntegerField
      FieldName = 'C_KEsar'
    end
    object Browse_T_StudentOneN_KEsar: TStringField
      FieldName = 'N_KEsar'
      Size = 100
    end
    object Browse_T_StudentOneSrl_GroupShahr: TAutoIncField
      FieldName = 'Srl_GroupShahr'
      ReadOnly = True
    end
    object Browse_T_StudentOneC_GroupShahr: TIntegerField
      FieldName = 'C_GroupShahr'
    end
    object Browse_T_StudentOneN_GroupShahr: TStringField
      FieldName = 'N_GroupShahr'
      Size = 100
    end
    object Browse_T_StudentOneIs_Check: TIntegerField
      FieldName = 'Is_Check'
    end
    object Browse_T_StudentOneIs_Active: TIntegerField
      FieldName = 'Is_Active'
    end
    object Browse_T_StudentOneIs_EmamJam: TIntegerField
      FieldName = 'Is_EmamJam'
    end
    object Browse_T_StudentOneIs_EmamJom: TIntegerField
      FieldName = 'Is_EmamJom'
    end
    object Browse_T_StudentOneSrl_DprtDocum: TIntegerField
      FieldName = 'Srl_DprtDocum'
    end
    object Browse_T_StudentOneD_EtebarResearch: TStringField
      FieldName = 'D_EtebarResearch'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneC_DprtDocum: TIntegerField
      FieldName = 'C_DprtDocum'
    end
    object Browse_T_StudentOneN_DprtDocum: TStringField
      FieldName = 'N_DprtDocum'
      Size = 200
    end
    object Browse_T_StudentOneC_Hozeh: TIntegerField
      FieldName = 'C_Hozeh'
    end
    object Browse_T_StudentOneIs_Mashmool: TIntegerField
      FieldName = 'Is_Mashmool'
    end
    object Browse_T_StudentOneStrIs_Mashmool: TStringField
      FieldName = 'StrIs_Mashmool'
      ReadOnly = True
      Size = 3
    end
    object Browse_T_StudentOneIs_CheckMashmool: TIntegerField
      FieldName = 'Is_CheckMashmool'
    end
    object Browse_T_StudentOneStrIs_CheckMashmool: TStringField
      FieldName = 'StrIs_CheckMashmool'
      ReadOnly = True
      Size = 3
    end
    object Browse_T_StudentOneIs_CheckResearch: TIntegerField
      FieldName = 'Is_CheckResearch'
    end
    object Browse_T_StudentOneStrIs_CheckResearch: TStringField
      FieldName = 'StrIs_CheckResearch'
      ReadOnly = True
      Size = 3
    end
    object Browse_T_StudentOneIs_CheckPaziresh: TIntegerField
      FieldName = 'Is_CheckPaziresh'
    end
    object Browse_T_StudentOneStrIs_CheckPaziresh: TStringField
      FieldName = 'StrIs_CheckPaziresh'
      ReadOnly = True
      Size = 3
    end
  end
  object XPManifest1: TXPManifest
    Left = 592
    Top = 184
  end
  object insert_T_Source_Pic_1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Common_SP_Insert_Pub_Source_Pic_1'
    Parameters = <>
    Left = 576
    Top = 32
  end
  object Common_SP_GetSystemVersion: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Common_SP_GetSystemVersion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Last_Version'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 592
    Top = 80
    object IntegerField19: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField20: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField19: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField20: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField21: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField21: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object Exam_Student_Can_Register: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Exam_Student_Can_Register;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KCheck'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlStudent'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlCycleOld'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlCycleNew'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ResRegister'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 592
    Top = 128
  end
  object T_K_Cycle: TADOTable
    Connection = SystemConnection
    CursorType = ctStatic
    TableName = 'T_K_Cycle'
    Left = 608
    Top = 352
    object T_K_CycleFCode: TIntegerField
      FieldName = 'FCode'
    end
    object T_K_CycleFName: TStringField
      FieldName = 'FName'
      Size = 100
    end
    object T_K_CycleGrp_Cycle: TIntegerField
      FieldName = 'Grp_Cycle'
    end
    object T_K_CycleC_KTitle: TIntegerField
      FieldName = 'C_KTitle'
    end
    object T_K_CycleC_KJens: TIntegerField
      FieldName = 'C_KJens'
    end
    object T_K_CycleC_KSchool: TIntegerField
      FieldName = 'C_KSchool'
    end
    object T_K_CycleSrl_PrintCart1: TIntegerField
      FieldName = 'Srl_PrintCart1'
    end
    object T_K_CycleSrl_PrintCart2: TIntegerField
      FieldName = 'Srl_PrintCart2'
    end
    object T_K_CycleSrl_PrintKarname1: TIntegerField
      FieldName = 'Srl_PrintKarname1'
    end
    object T_K_CycleSrl_PrintKarname2: TIntegerField
      FieldName = 'Srl_PrintKarname2'
    end
    object T_K_CycleSrl_User: TIntegerField
      FieldName = 'Srl_User'
    end
    object T_K_CycleN_Station: TStringField
      FieldName = 'N_Station'
      Size = 100
    end
    object T_K_CycleU_DateTime: TDateTimeField
      FieldName = 'U_DateTime'
    end
  end
  object DST_K_Cycle: TDataSource
    DataSet = T_K_Cycle
    Left = 608
    Top = 400
  end
  object PopupMenu1: TPopupMenu
    Left = 256
    Top = 16
    object N1: TMenuItem
      Caption = #1585#1601#1578#1606' '#1576#1607' '#1575#1608#1604#1610#1606' '#1585#1603#1608#1585#1583
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1585#1601#1578#1606' '#1576#1607' '#1570#1582#1585#1610#1606' '#1585#1603#1608#1585#1583
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1585#1601#1578#1606' '#1576#1607' '#1585#1603#1608#1585#1583' ....'
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = 'F10  '#1670#1575#1662' '#1580#1583#1608#1604
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = 'F12  '#1578#1594#1610#1610#1585' '#1587#1578#1608#1606#1607#1575#1610' '#1580#1583#1608#1604
      OnClick = N5Click
    end
    object N6: TMenuItem
      Caption = #1581#1584#1601' '#1588#1585#1591#1607#1575' '#1608' '#1605#1588#1575#1607#1583#1607' '#1603#1604#1610#1607' '#1575#1591#1604#1575#1593#1575#1578
      OnClick = N6Click
    end
    object N7: TMenuItem
      Caption = 'Ctrl + A  '#1575#1606#1578#1582#1575#1576' '#1607#1605#1607' '#1585#1603#1608#1585#1583#1607#1575
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = #1605#1588#1575#1607#1583#1607' '#1585#1603#1608#1585#1583#1607#1575#1610' '#1575#1606#1578#1582#1575#1576' '#1588#1583#1607
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #1580#1605#1593
      OnClick = N9Click
    end
    object Access1: TMenuItem
      Caption = 'Access '#1575#1606#1578#1602#1575#1604' '#1576#1607' '
      OnClick = Access1Click
    end
    object Excel1: TMenuItem
      Caption = 'Excel '#1575#1606#1578#1602#1575#1604' '#1576#1607' '
      OnClick = Excel1Click
    end
    object WORD1: TMenuItem
      Caption = 'WORD  '#1575#1606#1578#1602#1575#1604' '#1576#1607'  '
      OnClick = WORD1Click
    end
    object N10: TMenuItem
      Caption = #1575#1606#1583#1575#1586#1607' '#1587#1578#1608#1606#1607#1575
      OnClick = N10Click
    end
  end
  object ADOCommand1: TADOCommand
    Parameters = <>
    Left = 510
    Top = 352
  end
  object ADOConnection1: TADOConnection
    Left = 510
    Top = 304
  end
  object ADOTable1: TADOTable
    Left = 510
    Top = 400
  end
  object ExcelConnection: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    OnExecuteComplete = SystemConnectionExecuteComplete
    OnWillExecute = SystemConnectionWillExecute
    Left = 400
    Top = 88
  end
  object Common_SP_GetPRocedureText: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Common_SP_GetPRocedureText;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@NForm'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@NProc'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@TxtProc'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 7000
        Value = Null
      end
      item
        Name = '@IsExists'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 408
  end
end
