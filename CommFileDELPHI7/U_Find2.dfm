object F_Find2: TF_Find2
  Left = 517
  Top = 236
  AutoScroll = False
  BiDiMode = bdRightToLeft
  BorderIcons = []
  Caption = #1601#1585#1605' '#1580#1587#1578#1580#1608
  ClientHeight = 291
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label3: TLabel
    Left = 288
    Top = 264
    Width = 61
    Height = 16
    AutoSize = False
    Caption = #1580#1587#1578#1580#1608
    Transparent = True
  end
  object LCount: TLabel
    Left = 23
    Top = 267
    Width = 45
    Height = 16
    Alignment = taCenter
    Caption = '...........'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object EditFind: TEdit
    Left = 80
    Top = 260
    Width = 202
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    OnChange = EditFindChange
    OnDblClick = DBGrid1DblClick
    OnEnter = EditFindEnter
    OnExit = EditFindExit
    OnKeyPress = DBGrid1KeyPress
  end
  object KFind: TRadioGroup
    Left = 0
    Top = 217
    Width = 362
    Height = 40
    Align = alTop
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      #1588#1575#1605#1604
      #1588#1585#1608#1593' '#1576#1575
      #1580#1587#1578#1580#1608#1610' '#1583#1602#1610#1602)
    TabOrder = 2
    OnClick = KFindClick
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 362
    Height = 217
    Align = alTop
    DataSource = DSADOStoredProcFind
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = ARABIC_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    OnDblClick = DBGrid1DblClick
    OnKeyPress = DBGrid1KeyPress
  end
  object DSADOStoredProcFind: TDataSource
    DataSet = ADOStoredProcFind
    Left = 184
    Top = 112
  end
  object ADOStoredProcFind: TADOStoredProc
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    ProcedureName = 'SP_Browse_T_Table2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Cap_DataSet'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 300
      end>
    Left = 184
    Top = 64
  end
end
