unit U_DBGrid_Option;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, Grids, DBGrids, Spin, ComCtrls;

type
  TF_DBGrid_Option = class(TForm)
    BitBtn1: TBitBtn;
    Label2: TLabel;
    BitBtn2: TBitBtn;
    SpeedButton2: TSpeedButton;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    FontDialog1: TFontDialog;
    FieldCaptionList: TCheckListBox;
    ColumnWidth: TTrackBar;
    Label3: TLabel;
    BitBtn3: TBitBtn;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    ComboBoxFields: TComboBox;
    Label4: TLabel;
    SpeedButton13: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FieldCaptionListClickCheck(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FieldCaptionListClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ColumnWidthChange(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure ComboBoxFieldsClick(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
  private
    procedure FillFieldCaptionList;
    { Private declarations }
  public
    { Public declarations }
    MyDbGrid2 : TDBGrid;
    PosList : integer;
  end;

var
  F_DBGrid_Option: TF_DBGrid_Option;

implementation

uses U_Common;

{$R *.DFM}

procedure TF_DBGrid_Option.FillFieldCaptionList;
var
   i  : integer;
begin
   FieldCaptionList.Items.Clear;
   for i := 0 to MyDbGrid2.Columns.Count - 1 do
   begin
      FieldCaptionList.Items.Add(MyDbGrid2.Columns[i].Title.Caption);
      FieldCaptionList.Checked[i] := MyDbGrid2.Columns[i].Visible;
   end;
   FieldCaptionList.ItemIndex := PosList;
end;

procedure TF_DBGrid_Option.FormShow(Sender: TObject);
begin
   PosList := 0;
   ComboBoxFields.Items := MyDbGrid2.DataSource.DataSet.FieldList;
   FillFieldCaptionList;
   FieldCaptionListClick(Sender);
end;

procedure TF_DBGrid_Option.BitBtn1Click(Sender: TObject);
var
   i  : integer;
begin
   ReSizeDBGrid(MyDbGrid2);
   FillFieldCaptionList;
end;

procedure TF_DBGrid_Option.FieldCaptionListClickCheck(Sender: TObject);
var
   i  : integer;
begin
   for i := 0 to MyDbGrid2.Columns.Count - 1 do
      MyDbGrid2.Columns[i].Visible := FieldCaptionList.Checked[i];
end;

procedure TF_DBGrid_Option.BitBtn2Click(Sender: TObject);
begin
   Close;
end;

procedure TF_DBGrid_Option.SpeedButton2Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
   begin
      FontDialog1.Font := MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Font;
      if FontDialog1.Execute then
         MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Font := FontDialog1.Font;
   end;
end;

procedure TF_DBGrid_Option.FieldCaptionListClick(Sender: TObject);
begin
   ColumnWidth.Position := MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Width;
   ComboBoxFields.ItemIndex := ComboBoxFields.Items.IndexOf(MyDbGrid2.Columns[FieldCaptionList.ItemIndex].FieldName);
end;

procedure TF_DBGrid_Option.SpeedButton1Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
   begin
      FontDialog1.Font := MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Title.Font;
      if FontDialog1.Execute then
         MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Title.Font := FontDialog1.Font;
   end;
end;

procedure TF_DBGrid_Option.ColumnWidthChange(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
      MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Width := ColumnWidth.Position;

   Label3.Caption := IntToStr(ColumnWidth.Position);
end;

procedure TF_DBGrid_Option.SpeedButton7Click(Sender: TObject);
var
   i : integer;
begin
   for i := 0 to FieldCaptionList.Items.Count -1 do
      FieldCaptionList.Checked[i] := True;
   FieldCaptionListClickCheck(Sender);
end;

procedure TF_DBGrid_Option.SpeedButton8Click(Sender: TObject);
var
   i : integer;
begin
   for i := 0 to FieldCaptionList.Items.Count -1 do
      FieldCaptionList.Checked[i] := False;
   FieldCaptionListClickCheck(Sender);
end;

procedure TF_DBGrid_Option.SpeedButton6Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
   begin
      MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Index := MyDbGrid2.Columns.Count -1;
      PosList := MyDbGrid2.Columns.Count -1;
      FillFieldCaptionList;
   end;
end;

procedure TF_DBGrid_Option.SpeedButton4Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 1 then
   begin
      MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Index := FieldCaptionList.ItemIndex - 1;
      PosList := FieldCaptionList.ItemIndex - 1;
      FillFieldCaptionList;
   end;
end;

procedure TF_DBGrid_Option.SpeedButton5Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex < MyDbGrid2.Columns.Count - 1 then
   begin
      MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Index := FieldCaptionList.ItemIndex + 1;
      PosList := FieldCaptionList.ItemIndex + 1;
      FillFieldCaptionList;
   end;
end;

procedure TF_DBGrid_Option.SpeedButton3Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
   begin
      MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Index := 0;
      PosList := 0;
      FillFieldCaptionList;
   end;
end;

procedure TF_DBGrid_Option.SpeedButton9Click(Sender: TObject);
var
   MyColumn : TColumn;
begin
   MyColumn := MyDbGrid2.Columns.Add;
   MyColumn.Width := 75;
   MyColumn.Title.Caption := InputBox('����� ���� ����', '', '');
   FillFieldCaptionList;
end;

procedure TF_DBGrid_Option.SpeedButton10Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
//   if Is_DeleteRecord(0) then
   begin
      MyDbGrid2.Columns.Delete(FieldCaptionList.ItemIndex);
      FillFieldCaptionList;
   end
end;

procedure TF_DBGrid_Option.SpeedButton11Click(Sender: TObject);
begin
   if FieldCaptionList.ItemIndex >= 0 then
   begin
      MyDbGrid2.Columns[FieldCaptionList.ItemIndex].Title.Caption := InputBox('����� ����', '', '');
      FillFieldCaptionList;
   end
end;

procedure TF_DBGrid_Option.ComboBoxFieldsClick(Sender: TObject);
begin
   MyDbGrid2.Columns[FieldCaptionList.ItemIndex].FieldName := ComboBoxFields.Text;
end;

procedure TF_DBGrid_Option.SpeedButton13Click(Sender: TObject);
begin
   MyDbGrid2.Tag := -1;
end;

end.
