unit U_Report;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ppViewr, StdCtrls, Mask, Buttons, ComCtrls, Menus, TXtraDev, ppBands,
  ppCtrls, ppClass, ppCache, ppProd, ppReport, ppComm, ppRelatv,
  ppDB, ppDBPipe, ppDBBDE,  Grids, DBGrids, ppTypes, ppPrnabl, DB, ppVar, Printers,
  CheckLst, ppStrtch, ppMemo, ppEndUsr, ADODB, ppRegion;

type
  TF_Report = class(TForm)
    ppViewer1: TppViewer;
    pnlPreviewBar: TPanel;
    SpeedButton3: TSpeedButton;
    spbPreviewWhole: TSpeedButton;
    spbPreviewWidth: TSpeedButton;
    spbPreview100Percent: TSpeedButton;
    Label1: TLabel;
    Label6: TLabel;
    TotalPage: TLabel;
    Label7: TLabel;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    bEnd: TSpeedButton;
    bNext: TSpeedButton;
    bPrev: TSpeedButton;
    bFirst: TSpeedButton;
    SpeedButton9: TSpeedButton;
    mskPreviewPercentage: TMaskEdit;
    mskPreviewPage: TMaskEdit;
    Panel1: TPanel;
    POption: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    PaperHeight: TEdit;
    PaperWidth: TEdit;
    GridReport: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppShape1: TppShape;
    ppLabel201: TppLabel;
    ppLabel202: TppLabel;
    ppLabel203: TppLabel;
    ppLabel204: TppLabel;
    ppLabel205: TppLabel;
    ppLabel206: TppLabel;
    ppLabel207: TppLabel;
    ppLabel208: TppLabel;
    ppLine201: TppLine;
    ppLine202: TppLine;
    ppLine203: TppLine;
    ppLine204: TppLine;
    ppLine205: TppLine;
    ppLine206: TppLine;
    ppLine207: TppLine;
    ppLine208: TppLine;
    ppDetailBand1: TppDetailBand;
    ppDBText201: TppDBText;
    ppDBText202: TppDBText;
    ppDBText203: TppDBText;
    ppDBText204: TppDBText;
    ppDBText205: TppDBText;
    ppDBText206: TppDBText;
    ppDBText207: TppDBText;
    ppDBText208: TppDBText;
    ppLine209: TppLine;
    ppLine301: TppLine;
    ppLine302: TppLine;
    ppLine303: TppLine;
    ppLine304: TppLine;
    ppLine305: TppLine;
    ppLine306: TppLine;
    ppFooterBand1: TppFooterBand;
    ppLabel209: TppLabel;
    ppLabel210: TppLabel;
    ppLine307: TppLine;
    ppLine308: TppLine;
    ppLine309: TppLine;
    ppDBText209: TppDBText;
    ppDBText210: TppDBText;
    ppTitleBand1: TppTitleBand;
    ppSummaryBand1: TppSummaryBand;
    FooterLine: TppLine;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    HedCaption: TppLabel;
    Label3: TLabel;
    EditHedCaption: TEdit;
    ppLabel11: TppLabel;
    ppLabel12: TppLabel;
    ppLabel13: TppLabel;
    ppLabel14: TppLabel;
    ppLabel15: TppLabel;
    ppSystemVariable1: TppSystemVariable;
    ppSystemVariable2: TppSystemVariable;
    ppLabel16: TppLabel;
    ppLabel17: TppLabel;
    ppSystemVariable3: TppSystemVariable;
    ppShape2: TppShape;
    PrinterOrientation: TRadioGroup;
    Label4: TLabel;
    PSize: TPanel;
    ppShape3: TppShape;
    ppLabel18: TppLabel;
    ppLabel19: TppLabel;
    ppDBCalc40: TppDBCalc;
    ppDBCalc30: TppDBCalc;
    ppLineRadifHed: TppLine;
    ppLineRadifDet: TppLine;
    ppShape4: TppShape;
    ppLine210: TppLine;
    ppLabel211: TppLabel;
    ppLine211: TppLine;
    ppLabel212: TppLabel;
    ppLine212: TppLine;
    ppLabel213: TppLabel;
    ppLine213: TppLine;
    ppLabel214: TppLabel;
    ppLine214: TppLine;
    ppLabel215: TppLabel;
    ppLine215: TppLine;
    ppLabel216: TppLabel;
    ppLine216: TppLine;
    ppLabel217: TppLabel;
    ppLine217: TppLine;
    ppLabel218: TppLabel;
    ppLine218: TppLine;
    ppLabel219: TppLabel;
    ppLine219: TppLine;
    ppLabel220: TppLabel;
    ppLine220: TppLine;
    ppLine310: TppLine;
    ppLine311: TppLine;
    ppLine312: TppLine;
    ppLine313: TppLine;
    ppLine314: TppLine;
    ppLine315: TppLine;
    ppLine316: TppLine;
    ppLine317: TppLine;
    ppLine318: TppLine;
    ppLine319: TppLine;
    ppLine320: TppLine;
    ppDBText211: TppDBText;
    ppDBText212: TppDBText;
    ppDBText213: TppDBText;
    ppDBText214: TppDBText;
    ppDBText215: TppDBText;
    ppDBText216: TppDBText;
    ppDBText217: TppDBText;
    ppDBText218: TppDBText;
    ppDBText219: TppDBText;
    ppDBText220: TppDBText;
    BSAVE: TSpeedButton;
    ListBoxColumns: TCheckListBox;
    RowLine: TppLine;
    ppLabel1: TppLabel;
    CheckBoxRadif: TCheckBox;
    MemoTitCaption: TMemo;
    TitCaption: TppMemo;
    SpeedButton2: TSpeedButton;
    FontDialog1: TFontDialog;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    CheckBox1: TCheckBox;
    Label5: TLabel;
    CBLineStyle: TListBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Print1: TMenuItem;
    MenuItem1: TMenuItem;
    Exit1: TMenuItem;
    Options2: TMenuItem;
    Options1: TMenuItem;
    RTF: TMenuItem;
    HTML: TMenuItem;
    CSS2: TMenuItem;
    Bitmap: TMenuItem;
    JPEG: TMenuItem;
    Lotus: TMenuItem;
    Quattro: TMenuItem;
    Excel: TMenuItem;
    PDF: TMenuItem;
    MenuItem2: TMenuItem;
    Screen: TMenuItem;
    Printer: TMenuItem;
    Archive: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N1: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    ppDBCalc401: TppDBCalc;
    ppDBCalc404: TppDBCalc;
    ppDBCalc405: TppDBCalc;
    ppDBCalc406: TppDBCalc;
    ppDBCalc407: TppDBCalc;
    ppDBCalc408: TppDBCalc;
    ppDBCalc409: TppDBCalc;
    ppDBCalc410: TppDBCalc;
    ppDBCalc411: TppDBCalc;
    ppDBCalc412: TppDBCalc;
    ppDBCalc413: TppDBCalc;
    ppDBCalc414: TppDBCalc;
    ppDBCalc403: TppDBCalc;
    ppDBCalc402: TppDBCalc;
    ppDBCalc415: TppDBCalc;
    ppDBCalc416: TppDBCalc;
    ppDBCalc417: TppDBCalc;
    ppDBCalc418: TppDBCalc;
    ppDBCalc419: TppDBCalc;
    ppDBCalc420: TppDBCalc;
    ppLabel2: TppLabel;
    ppShape5: TppShape;
    N2: TMenuItem;
    ppDesigner1: TppDesigner;
    insert_Pub_Reports_1: TADOStoredProc;
    insert_Pub_Reports_1Val_Report: TMemoField;
    update_Pub_Reports_1: TADOStoredProc;
    MemoField1: TMemoField;
    SpeedButton6: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    N3: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    ppBDEGridReport: TppBDEPipeline;
    StudentPic1: TppImage;
    ppBDEGridReport2: TppBDEPipeline;
    ppBDEGridReport3: TppBDEPipeline;
    procedure spbPreviewPrintClick(Sender: TObject);
    procedure spbPreviewWholeClick(Sender: TObject);
    procedure spbPreviewWidthClick(Sender: TObject);
    procedure spbPreview100PercentClick(Sender: TObject);
    procedure mskPreviewPageKeyPress(Sender: TObject; var Key: Char);
    procedure spbPreviewLastClick(Sender: TObject);
    procedure mskPreviewPercentageKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure ppViewer1PageChange(Sender: TObject);
    procedure bExitClick(Sender: TObject);
    procedure ppViewer1StatusChange(Sender: TObject);
    procedure bFirstClick(Sender: TObject);
    procedure bPrevClick(Sender: TObject);
    procedure bNextClick(Sender: TObject);
    procedure bEndClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure ppLabel15GetText(Sender: TObject; var Text: String);
    procedure ppLabel17GetText(Sender: TObject; var Text: String);
    procedure PrinterOrientationClick(Sender: TObject);
    procedure POptionExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBoxColumnsClickCheck(Sender: TObject);
    procedure ppViewer1Click(Sender: TObject);
    procedure ppLabel1GetText(Sender: TObject; var Text: String);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure PrintGrid;
    procedure CheckBoxRadifClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure Print1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure RTFClick(Sender: TObject);
    procedure HTMLClick(Sender: TObject);
    procedure CSS2Click(Sender: TObject);
    procedure BitmapClick(Sender: TObject);
    procedure JPEGClick(Sender: TObject);
    procedure LotusClick(Sender: TObject);
    procedure QuattroClick(Sender: TObject);
    procedure ExcelClick(Sender: TObject);
    procedure PDFClick(Sender: TObject);
    procedure ScreenClick(Sender: TObject);
    procedure PrinterClick(Sender: TObject);
    procedure ArchiveClick(Sender: TObject);
    procedure BSAVEClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
  private
    procedure AddFildsToList1;
    { Private declarations }
  public
    { Public declarations }
//    KindShow : Integer;
    ReportGrid : TDBGrid;
    WidthPaper : integer;
    ColumnCount : integer;
    PPLableArray : Array[1..20] of TppLabel;
    PPDataArray  : Array[1..20] of TppDBText;
    PPHeaderLine : Array[1..20] of TppLine;
    PPDetailLine : Array[1..20] of TppLine;
    PPSumArray : Array[1..20] of TppDBCalc;
    CountPrintGrid : Integer;
    SaveFileNeme : String;
    Srl_Report : integer;
  end;

var
  F_Report: TF_Report;

implementation

{$R *.DFM}

procedure TF_Report.spbPreviewPrintClick(Sender: TObject);
begin
   ppViewer1.Print;
end;

procedure TF_Report.spbPreviewWholeClick(Sender: TObject);
begin
  ppViewer1.ZoomSetting := zsWholePage;
  mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
  pnlPreviewBar.SetFocus;
end;

procedure TF_Report.spbPreviewWidthClick(Sender: TObject);
begin
  ppViewer1.ZoomSetting := zsPageWidth;

  mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);

  pnlPreviewBar.SetFocus;

end;

procedure TF_Report.spbPreview100PercentClick(Sender: TObject);
begin
  ppViewer1.ZoomSetting := zs100Percent;
  mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
  pnlPreviewBar.SetFocus;
end;

procedure TF_Report.mskPreviewPageKeyPress(Sender: TObject; var Key: Char);
var
  liPage: Longint;
begin

  if (Key = #13) then
    begin
      liPage := StrToInt(mskPreviewPage.Text);
      ppViewer1.GotoPage(liPage);
    end; {if, carriage return pressed}


end;

procedure TF_Report.spbPreviewLastClick(Sender: TObject);
begin
  ppViewer1.LastPage;
end;

procedure TF_Report.mskPreviewPercentageKeyPress(Sender: TObject;
  var Key: Char);
var
  liPercentage: Integer;
begin
  if (Key = #13) then
    begin
      liPercentage := StrToIntDef(mskPreviewPercentage.Text, 100);

      ppViewer1.ZoomPercentage := liPercentage;

      spbPreviewWhole.Down := False;
      spbPreviewWidth.Down := False;
      spbPreview100Percent.Down := False;

      mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
    end; {if, carriage return pressed}

end;

procedure TF_Report.FormShow(Sender: TObject);
var
   Vk : Char;
begin
   Caption := Caption + '          ' + intToStr(Srl_Report);
   Vk := #13;
{
   ppDBCalc40.Font := PrintNumberFont;
   ppSystemVariable1.Font := PrintNumberFont;
   ppSystemVariable2.Font := PrintNumberFont;
   ppDBCalc30.Font := PrintNumberFont;
}
   mskPreviewPage.Text := IntToStr(ppViewer1.AbsolutePageNo);
   mskPreviewPercentage.Text :='100';
   mskPreviewPercentageKeyPress(Self,VK);
   spbPreviewWidthClick(Self);
   CountPrintGrid := 0;
end;

procedure TF_Report.ppViewer1PageChange(Sender: TObject);
begin
  mskPreviewPage.Text := IntToStr(ppViewer1.AbsolutePageNo);
  mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
end;

procedure TF_Report.bExitClick(Sender: TObject);
var
   i : integer;
begin

   if ReportGrid <> nil then
      for i := 0 to ReportGrid.Columns.Count -1 do
         ReportGrid.Columns[i].ReadOnly := True;
   Close;
end;

procedure TF_Report.ppViewer1StatusChange(Sender: TObject);
begin
   Panel1.Caption := ppViewer1.Status;
end;

procedure TF_Report.bFirstClick(Sender: TObject);
begin
   ppViewer1.FirstPage;
end;

procedure TF_Report.bPrevClick(Sender: TObject);
begin
   ppViewer1.PriorPage;
end;

procedure TF_Report.bNextClick(Sender: TObject);
begin
   ppViewer1.NextPage;
end;

procedure TF_Report.bEndClick(Sender: TObject);
begin
   ppViewer1.LastPage;
end;

procedure TF_Report.FormKeyPress(Sender: TObject; var Key: Char);
begin
{
   if (Key = #13)  then
   begin
      if PaperHeight.Text <> '' then
         ppViewer1.Report.PrinterSetup.PaperHeight := StrToFloat(PaperHeight.Text)
      else
         ppViewer1.Report.PrinterSetup.PaperHeight := 11;

      if PaperWidth.Text <> '' then
         ppViewer1.Report.PrinterSetup.PaperWidth := StrToFloat(PaperWidth.Text)
      else
         ppViewer1.Report.PrinterSetup.PaperWidth := 8.5;
   end;
}
end;

procedure TF_Report.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

   Case Key of
      13  : begin
               ppViewer1.Print;
               Close;
            end;

      27  : Close;
      33  : ppViewer1.PriorPage;
      34  : ppViewer1.NextPage;
      35  : if not POption.Visible then ppViewer1.LastPage;
      36  : if not POption.Visible then ppViewer1.FirstPage;
      38  : ppViewer1.Perform(EM_LINESCROLL,0,0);
      40  : ppViewer1.Perform(EM_LINESCROLL,0,0);
      109 : SpeedButton2Click(Sender);
      106 : spbPreviewWidthClick(Self);
   end;
end;

procedure TF_Report.PrintGrid;
var
   i1, j1 : integer;
   LeftObject : integer;
   TotalWidth : integer;
   MaxColumn : integer;
   RadifWidth : integer;
   MyColumn : TColumn;
begin
   CountPrintGrid := CountPrintGrid + 1;

   MaxColumn := 20;
   RadifWidth := 40;
   TotalWidth := 0;
   LeftObject := 0;

   ppBDEGridReport.DataSource := ReportGrid.DataSource;

   TitCaption.Left := WidthPaper Div 2 - TitCaption.spWidth Div 2;
   HedCaption.Left := WidthPaper Div 2 - HedCaption.spWidth Div 2;


   ColumnCount := 0;

   for i1 := 0 to ReportGrid.Columns.Count -1 do
      if ReportGrid.Columns[i1].Visible then
         ColumnCount := ColumnCount + 1;

   if ColumnCount > MaxColumn then
      ColumnCount := MaxColumn;

   for i1 := 1 to ReportGrid.Columns.Count  do
   if ReportGrid.Columns[i1-1].Visible then
      TotalWidth := TotalWidth + ReportGrid.Columns[i1-1].Width;


   LeftObject := WidthPaper Div 2;
   LeftObject := LeftObject + (TotalWidth Div 2) - (RadifWidth Div 2);

   TotalWidth := TotalWidth + RadifWidth;

   ppLabel18.Left := LeftObject;
   ppLabel18.Width := RadifWidth;
   ppLabel18.TextAlignment := taCentered;
   ppDBCalc40.Left := LeftObject;
   ppDBCalc40.Width := RadifWidth;
   ppDBCalc40.TextAlignment := taCentered;

   ppLabel2.Left := LeftObject;
   ppLabel2.Width := RadifWidth;

   ppLineRadifHed.Left := LeftObject;
   ppLineRadifDet.Left := LeftObject + RadifWidth - 1;


   for i1 := 1 to MaxColumn do
   begin
      PPLableArray[i1] := Nil;
      PPDataArray[i1] := Nil;
      PPHeaderLine[i1] := Nil;
      PPDetailLine[i1] := Nil;
      PPSumArray[i1] := Nil;

      if FindComponent('ppLabel' + IntToStr(i1 + 200)) <> nil then
      begin
         PPLableArray[i1] :=  TppLabel(FindComponent('ppLabel' + IntToStr(i1 + 200)));
         PPLableArray[i1].Top := ppLabel18.Top;
      end;
      if FindComponent('ppDBText' + IntToStr(i1 + 200)) <> nil then
      begin
         PPDataArray[i1] := TppDBText(FindComponent('ppDBText' + IntToStr(i1 + 200)));
         PPDataArray[i1].Top := 2;
      end;
      if FindComponent('ppDBCalc' + IntToStr(i1 + 400)) <> nil then
      begin
         PPSumArray[i1] := TppDBCalc(FindComponent('ppDBCalc' + IntToStr(i1 + 400)));
         PPSumArray[i1].Top := 2;
      end;
      if FindComponent('ppLine' + IntToStr(i1 + 200)) <> nil then
      begin
         PPHeaderLine[i1] := TppLine(FindComponent('ppLine' + IntToStr(i1 + 200)));
         PPHeaderLine[i1].Top := 69;
      end;
      if FindComponent('ppLine' + IntToStr(i1 + 300)) <> nil then
      begin
         PPDetailLine[i1] := TppLine(FindComponent('ppLine' + IntToStr(i1 + 300)));
         PPDetailLine[i1].ParentHeight := True;
      end;
   end;

   ppDetailBand1.Height := 25;

   j1 := 1;
   for i1 := 1 to ReportGrid.Columns.Count  do
   if ReportGrid.Columns[i1-1].Visible then
   begin
      MyColumn := ReportGrid.Columns[i1-1];

      PPDataArray[j1].Visible  := True;
      PPLableArray[j1].Visible := True;
      PPHeaderLine[j1].Visible := True;
      PPDetailLine[j1].Visible := True;
      PPSumArray[j1].Visible  := True;

      PPDataArray[j1].DataField := MyColumn.FieldName;
      PPDataArray[j1].AutoSize := False;
      PPSumArray[j1].DataField := MyColumn.FieldName;

      if CountPrintGrid = 1 then
         if ReportGrid.Columns.Items[i1-1].Field <> nil  then
            if (ReportGrid.Columns.Items[i1-1].Field.DataType in NumberType) OR
              (ReportGrid.Columns.Items[i1-1].Field.Name = 'Browse1FCode') OR
              ((ReportGrid.Columns.Items[i1-1].Field.DataType = ftString) AND
              (ReportGrid.Columns.Items[i1-1].Field.Size = 8)) OR
              ((ReportGrid.Columns.Items[i1-1].Field.DataType = ftString) AND
              (ReportGrid.Columns.Items[i1-1].Field.Size = 10))
            then
            begin
               PPLableArray[j1].Font := PrintTitleFont;
               PPDataArray[j1].Font := PrintNumberFont;
               PPSumArray[j1].Font := PrintNumberFont;
            end
            else
            begin
               PPLableArray[j1].Font := PrintTitleFont;
               PPDataArray[j1].Font := PrintStrFont;
               PPSumArray[j1].Font := PrintStrFont;
            end;

      if ReportGrid.Columns.Items[i1-1].Field <> nil then
         if ((ReportGrid.Columns.Items[i1-1].Field.DataType = ftCurrency) OR
            (ReportGrid.Columns.Items[i1-1].Field.DataType = ftBCD)) then
            begin
             PPDataArray[j1].DisplayFormat := '#,0;-#,0';
             PPSumArray[j1].DisplayFormat := '#,0;-#,0';
            end
         else
            begin
             PPDataArray[j1].DisplayFormat := '';
             PPSumArray[j1].DisplayFormat := '';
            end;

      if ReportGrid.Columns.Items[i1-1].Field <> nil then
         if (ReportGrid.Columns.Items[i1-1].Field.DataType in NumberType) then
         begin
            if MyColumn.Alignment = taLeftJustify then
            begin
               PPDataArray[j1].TextAlignment := taLeftJustified;
               PPSumArray[j1].TextAlignment := taLeftJustified;
            end
            else if MyColumn.Alignment = taRightJustify then
            begin
               PPDataArray[j1].TextAlignment := taRightJustified;
               PPSumArray[j1].TextAlignment := taRightJustified;
            end
            else if MyColumn.Alignment = taCenter then
            begin
               PPDataArray[j1].TextAlignment := taCentered;
               PPSumArray[j1].TextAlignment := taCentered;
            end
         end
         else
         begin
            if MyColumn.Alignment = taLeftJustify then
               PPDataArray[j1].TextAlignment := taRightJustified
            else if MyColumn.Alignment = taRightJustify then
               PPDataArray[j1].TextAlignment := taLeftJustified
            else if MyColumn.Alignment = taCenter then
               PPDataArray[j1].TextAlignment := taCentered;
         end;

      PPDataArray[j1].Width := MyColumn.Width - 10 ;
      PPDataArray[j1].Left := LeftObject - MyColumn.Width + 5;
      PPSumArray[j1].Width := MyColumn.Width - 10 ;
      PPSumArray[j1].Left := LeftObject - MyColumn.Width + 5;


      PPHeaderLine[j1].Left := LeftObject;
      PPDetailLine[j1].Left := LeftObject;

      PPLableArray[j1].AutoSize := False;
      PPLableArray[j1].Caption := Trim(MyColumn.Title.Caption);
      PPLableArray[j1].TextAlignment := taCentered;
      PPLableArray[j1].AutoSize := False;
      PPLableArray[j1].Width := MyColumn.Width;

      LeftObject := LeftObject - MyColumn.Width;

      PPLableArray[j1].Left := LeftObject;
      j1 := j1 + 1;
   end;

   for i1 := j1 to MaxColumn do
   begin
      PPDataArray[i1].Visible := False;
      PPLableArray[i1].Visible := False;
      PPHeaderLine[i1].Visible := False;
      PPDetailLine[i1].Visible := False;
      PPSumArray[i1].Visible := False;
   end;

   PPDetailLine[j1].Visible := True;
   PPDetailLine[j1].Left := LeftObject;

   ppShape1.Width := TotalWidth;
   ppShape1.Left := LeftObject;
   ppShape5.Width := TotalWidth;
   ppShape5.Left := LeftObject;
   FooterLine.Width := TotalWidth;
   FooterLine.Left := LeftObject;
   RowLine.Left := LeftObject;
   RowLine.Width := TotalWidth;
end;


procedure TF_Report.BitBtn1Click(Sender: TObject);
Var
   MyPenStyle : TPenStyle;
begin
   POption.Visible := False;

   Case CBLineStyle.ItemIndex of
      1 : RowLine.Pen.Style := psSolid;
      2 : RowLine.Pen.Style := psDash;
      3 : RowLine.Pen.Style := psDot;
      4 : RowLine.Pen.Style := psDashDotDot;
      5 : RowLine.Pen.Style := psClear;
      6 : RowLine.Pen.Style := psInsideFrame;
   end;

   if CBLineStyle.ItemIndex <> 0 then
      RowLine.Visible := True
   else
      RowLine.Visible := FALSE;

   if Trim(MemoTitCaption.Text) <> '' then
   begin
      ppTitleBand1.Visible := True;
      TitCaption.Lines := MemoTitCaption.Lines;
   end
   else
   begin
      ppTitleBand1.Visible := False;
      TitCaption.Caption := '';
   end;

   HedCaption.Caption := Trim(EditHedCaption.Text);


   if PrinterOrientation.ItemIndex = 0 then
      ppViewer1.Report.PrinterSetup.Orientation := poPortrait
   else if PrinterOrientation.ItemIndex = 1 then
      ppViewer1.Report.PrinterSetup.Orientation := poLandscape
   else
   begin
      ppViewer1.Report.PrinterSetup.PaperHeight := StrToFloat(PaperHeight.Text);
      ppViewer1.Report.PrinterSetup.PaperWidth := StrToFloat(PaperWidth.Text);
   end;

   PrintGrid;
   ppViewer1.Report.PrintToDevices;
end;

procedure TF_Report.AddFildsToList1;
var
   i : integer;
begin
   if ReportGrid = nil then
      exit;
      
   ListBoxColumns.Clear;
   for i := 0 to ReportGrid.Columns.Count - 1 do
   begin
      ListBoxColumns.Items.Add(Trim(ReportGrid.Columns[i].Title.Caption));
      ListBoxColumns.Checked[i] := ReportGrid.Columns[i].Visible;
   end;

end;


procedure TF_Report.SpeedButton8Click(Sender: TObject);
var
   liPercentage: Integer;
begin
   if ppViewer1.CalculatedZoom <= 200 then
   begin
      liPercentage := ppViewer1.CalculatedZoom + 20;
      ppViewer1.ZoomPercentage := liPercentage;
      mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   end;
end;

procedure TF_Report.ppLabel15GetText(Sender: TObject; var Text: String);
begin
   Text := CurDate;
end;

procedure TF_Report.ppLabel17GetText(Sender: TObject; var Text: String);
begin
   Text := N_User;
end;

procedure TF_Report.PrinterOrientationClick(Sender: TObject);
begin
   if PrinterOrientation.ItemIndex = 2 then
      PSize.Visible := False
   else
      PSize.Visible := True;

   Case PrinterOrientation.ItemIndex of
      0 : WidthPaper := 800;
      1 : WidthPaper := 1200;
      2 : WidthPaper := 800;
   end
end;

procedure TF_Report.POptionExit(Sender: TObject);
begin
   POption.Visible := False;
end;

procedure TF_Report.FormCreate(Sender: TObject);
begin
   WidthPaper := 800;
end;

procedure TF_Report.ListBoxColumnsClickCheck(Sender: TObject);
begin
   ReportGrid.Columns[ListBoxColumns.ItemIndex].ReadOnly :=
      ListBoxColumns.Checked[ListBoxColumns.ItemIndex];
end;

procedure TF_Report.ppViewer1Click(Sender: TObject);
begin
   POption.Visible := False;
end;

procedure TF_Report.ppLabel1GetText(Sender: TObject; var Text: String);
begin
   Text := N_Company;
end;

procedure TF_Report.SpeedButton7Click(Sender: TObject);
var
   liPercentage: Integer;
begin
   if ppViewer1.CalculatedZoom > 50 then
   begin
      liPercentage := ppViewer1.CalculatedZoom - 20;
      ppViewer1.ZoomPercentage := liPercentage;
      mskPreviewPercentage.Text := IntToStr(ppViewer1.CalculatedZoom);
   end;
end;

procedure TF_Report.SpeedButton2Click(Sender: TObject);
begin
   if ListBoxColumns.ItemIndex <> -1 then
      if FontDialog1.Execute then
         PPDataArray[ListBoxColumns.ItemIndex + 1].Font := FontDialog1.Font;
end;

procedure TF_Report.CheckBoxRadifClick(Sender: TObject);
begin
   if CheckBoxRadif.Checked then
   begin
      ppLabel18.Visible := True;
      ppDBCalc30.Visible := True;
   end
   else
   begin
      ppLabel18.Visible := False;
      ppDBCalc30.Visible := False;
   end
end;

procedure TF_Report.CheckBox1Click(Sender: TObject);
var
   MyEmptyColumn : TColumn;
begin
   MyEmptyColumn := ReportGrid.Columns.Add;
   MyEmptyColumn.Title.Caption := Edit1.Text;
   MyEmptyColumn.Width := StrToInt(Edit2.Text);
end;

procedure TF_Report.Exit1Click(Sender: TObject);
var
   i : integer;
begin
   if ReportGrid <> nil then
      for i := 0 to ReportGrid.Columns.Count -1 do
         ReportGrid.Columns[i].ReadOnly := True;
   Close;
end;

procedure TF_Report.N7Click(Sender: TObject);
begin
   AddFildsToList1;
   POption.Visible := True;
   MemoTitCaption.SetFocus;

   MemoTitCaption.Lines := TitCaption.Lines;
   EditHedCaption.Text := Trim(HedCaption.Caption);

   PaperHeight.Text := FloatToStr(ppViewer1.Report.PrinterSetup.PaperHeight);
   PaperWidth.Text := FloatToStr(ppViewer1.Report.PrinterSetup.PaperWidth);
end;

procedure TF_Report.Print1Click(Sender: TObject);
begin
   ppViewer1.Print;
end;

procedure TF_Report.N4Click(Sender: TObject);
begin
   if SaveDialog1.Execute then
   begin
      TppReport(ppViewer1.Report).Template.FileName := SaveDialog1.FileName;
      TppReport(ppViewer1.Report).Template.SaveToFile;
   end;
end;

procedure TF_Report.N5Click(Sender: TObject);
begin
   if OpenDialog1.Execute then
   begin
      TppReport(ppViewer1.Report).Template.FileName := OpenDialog1.FileName;
      TppReport(ppViewer1.Report).Template.LoadFromFile;
      ppViewer1.Report.PrintToDevices;
   end;
end;

procedure TF_Report.RTFClick(Sender: TObject);
begin
   RTF.Checked      := True;
   ppViewer1.Report.DeviceType   := 'RTFFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.RTF';
end;

procedure TF_Report.HTMLClick(Sender: TObject);
begin
   //
   // For HTML files, the file path specified in TextFileName is only used
   // since each page is generated as a separate HTML file. After printing
   // there will be a series of RPTxxxx.HTM and IMGxxxx.JPG if images are in the
   // report in the destination directory.
   //

   HTML.Checked     := True;
   ppViewer1.Report.DeviceType   := 'HTMLFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.HTM';
end;

procedure TF_Report.CSS2Click(Sender: TObject);
begin
   CSS2.Checked := True;
   ppViewer1.Report.DeviceType   := 'CSS2File';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.HTM';
end;

procedure TF_Report.BitmapClick(Sender: TObject);
begin
   //
   // For Graphic files, the file path specified in TextFileName is only used
   // since each page is generated as a separate image file. After printing
   // there will be a series of IMGxxxx.BMP images in the destination directory.
   //

   Bitmap.Checked := True;
   ppViewer1.Report.DeviceType   := 'GraphicFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.BMP';
end;

procedure TF_Report.JPEGClick(Sender: TObject);
begin
   //
   // For Graphic files, the file path specified in TextFileName is only used
   // since each page is generated as a separate image file. After printing
   // there will be a series of IMGxxxx.JPG images in the destination directory.
   //

   JPEG.Checked := True;
   ppViewer1.Report.DeviceType   := 'GraphicFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.JPG';
end;

procedure TF_Report.LotusClick(Sender: TObject);
begin
   Lotus.Checked    := True;
   ppViewer1.Report.DeviceType   := 'LotusFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.WK1';
end;

procedure TF_Report.QuattroClick(Sender: TObject);
begin
   Quattro.Checked  := True;
   ppViewer1.Report.DeviceType   := 'QuattroFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.WQ1';
end;

procedure TF_Report.ExcelClick(Sender: TObject);
begin
   Excel.Checked    := True;
   ppViewer1.Report.DeviceType   := 'ExcelFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.XLS';
end;

procedure TF_Report.PDFClick(Sender: TObject);
begin
   PDF.Checked := True;
   ppViewer1.Report.DeviceType   := 'PDFFile';
   ppViewer1.Report.TextFileName := ExtractFilePath(ParamStr(0)) + 'TEST.PDF';
end;

procedure TF_Report.ScreenClick(Sender: TObject);
begin
   Screen.Checked := True;
   ppViewer1.Report.DeviceType := 'Screen';
end;

procedure TF_Report.PrinterClick(Sender: TObject);
begin
   Printer.Checked := True;
   ppViewer1.Report.DeviceType  := 'Printer';
end;

procedure TF_Report.ArchiveClick(Sender: TObject);
begin
   Archive.Checked := True;
   ppViewer1.Report.DeviceType  := 'ArchiveFile';
   ppViewer1.Report.ArchiveFileName := ExtractFilePath(ParamStr(0)) + 'TEST.RAF';
end;

procedure TF_Report.BSAVEClick(Sender: TObject);
var
   MyStreem : TMemoryStream;
begin
   MyStreem := TMemoryStream.Create;
   TppReport(ppViewer1.Report).Template.SaveToStream(MyStreem);
   MyStreem.Position := 0;
{
   TppReport(ppViewer1.Report).Template.FileName := 'C:\Temp\A12356';
   TppReport(ppViewer1.Report).Template.SaveToFile;
}
   if Srl_Report > 0 then
   begin
      NullAdoSPParameters(update_Pub_Reports_1);
      update_Pub_Reports_1.Parameters.ParamByName('@Srl_Report_1').Value := Srl_Report;
//      update_Pub_Reports_1.Parameters.ParamByName('@Val_Report_2').LoadFromFile('C:\Temp\A12356', ftMemo);
      update_Pub_Reports_1.Parameters.ParamByName('@Val_Report_2').LoadFromStream(MyStreem, ftMemo);
      update_Pub_Reports_1.ExecProc;
   end
   else
   begin
      NullAdoSPParameters(insert_Pub_Reports_1);
//      insert_Pub_Reports_1.Parameters.ParamByName('@Val_Report_1').LoadFromFile('C:\Temp\A12356', ftMemo);
      insert_Pub_Reports_1.Parameters.ParamByName('@Val_Report_1').LoadFromStream(MyStreem, ftMemo);
      Srl_System := 1;
      insert_Pub_Reports_1.Parameters.ParamByName('@Srl_System').Value := Srl_System;
      insert_Pub_Reports_1.Parameters.ParamByName('@N_Report').Value := GlobalFormCaption;
      insert_Pub_Reports_1.ExecProc;
      Srl_Report := insert_Pub_Reports_1.Parameters[0].Value;
      Caption := Caption + '          ' + intToStr(Srl_Report);
   end;
   MyStreem.Free;
end;

procedure TF_Report.SpeedButton6Click(Sender: TObject);
begin
   ppDesigner1.Report := TppReport(ppViewer1.Report);
   ppDesigner1.Show;
end;

procedure TF_Report.SpeedButton4Click(Sender: TObject);
begin
   WinExec(PChar(CalcFileName), 2);
end;

procedure TF_Report.N3Click(Sender: TObject);
begin
   BSAVEClick(Sender);
end;

procedure TF_Report.N8Click(Sender: TObject);
begin
   SpeedButton8Click(Sender);
end;

procedure TF_Report.N9Click(Sender: TObject);
begin
   SpeedButton7Click(Sender);
end;

end.


