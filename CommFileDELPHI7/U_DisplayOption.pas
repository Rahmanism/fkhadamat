unit U_DisplayOption;

interface

uses
  U_Common, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type
  TF_DisplayOption = class(TForm)
    ColorDialog1: TColorDialog;
    ShapeButtonColor: TShape;
    ShapeMainColor1: TShape;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ShapeMainColor2: TShape;
    Label3: TLabel;
    SpeedButton3: TSpeedButton;
    Shape1: TShape;
    Label4: TLabel;
    SpeedButton4: TSpeedButton;
    Shape2: TShape;
    Label5: TLabel;
    SpeedButton5: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
      N_Form : String;
      FDOPButtonColor : TColor;
      FDOPMainColor1 : TColor;
      FDOPMainColor2 : TColor;
      TempColor : TColor;
  end;

var
  F_DisplayOption: TF_DisplayOption;

implementation


{$R *.DFM}

procedure TF_DisplayOption.FormShow(Sender: TObject);
begin
   ShapeMainColor1.Brush.Color := FDOPMainColor1;
   ShapeMainColor2.Brush.Color := FDOPMainColor2;
   ShapeButtonColor.Brush.Color := FDOPButtonColor;
 //  Shape1.Brush.Color := TitleNotSortColor;
  // Shape2.Brush.Color := TitleSortColor;
end;

procedure TF_DisplayOption.SpeedButton1Click(Sender: TObject);
begin
   if ColorDialog1.Execute then
   begin
      FDOPButtonColor := ColorDialog1.Color;
      F1.WriteInteger(N_System, N_Form + 'PButtonColor', FDOPButtonColor);
      ShapeButtonColor.Brush.Color := FDOPButtonColor;
   end;
end;

procedure TF_DisplayOption.SpeedButton2Click(Sender: TObject);
begin
   if ColorDialog1.Execute then
   begin
      FDOPMainColor1 := ColorDialog1.Color;
      F1.WriteInteger(N_System, N_Form + 'PMainColor1', FDOPMainColor1);
      ShapeMainColor1.Brush.Color := FDOPMainColor1;
   end;
end;

procedure TF_DisplayOption.BitBtn1Click(Sender: TObject);
begin
   Close;
end;

procedure TF_DisplayOption.BitBtn2Click(Sender: TObject);
begin
   FDOPMainColor1 := PanelActiceColor;
   FDOPMainColor2 := PanelIdelColor;
   F1.WriteInteger(N_System, N_Form + 'PMainColor1', FDOPMainColor1);
   F1.WriteInteger(N_System, N_Form + 'PMainColor2', FDOPMainColor2);
   ShapeMainColor1.Brush.Color := FDOPMainColor1;
   ShapeMainColor2.Brush.Color := FDOPMainColor2;

   FDOPButtonColor := PButtonColor;
   F1.WriteInteger(N_System, N_Form + 'PButtonColor', FDOPButtonColor);
   ShapeButtonColor.Brush.Color := FDOPButtonColor;
end;

procedure TF_DisplayOption.SpeedButton3Click(Sender: TObject);
begin
   if ColorDialog1.Execute then
   begin
      FDOPMainColor2 := ColorDialog1.Color;
      F1.WriteInteger(N_System, N_Form + 'PMainColor2', FDOPMainColor2);
      ShapeMainColor2.Brush.Color := FDOPMainColor2;
   end;

end;

procedure TF_DisplayOption.SpeedButton4Click(Sender: TObject);
begin
   if ColorDialog1.Execute then
   begin
      TempColor := ColorDialog1.Color;
      F1.WriteInteger('Option', 'NotSortColor', TempColor);
      Shape1.Brush.Color := TempColor;
     // TitleNotSortColor := TempColor;
   end;
end;

procedure TF_DisplayOption.SpeedButton5Click(Sender: TObject);
begin
   if ColorDialog1.Execute then
   begin
      TempColor := ColorDialog1.Color;
      F1.WriteInteger('Option', 'SortColor', TempColor);
      Shape2.Brush.Color := TempColor;
    //  TitleSortColor := TempColor;
   end;
end;

end.
