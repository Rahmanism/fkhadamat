object F_Common11111: TF_Common11111
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 204
  Top = 134
  Height = 598
  Width = 812
  object PermisionConnection: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initi' +
      'al Catalog=Permision;Data Source=HOZEH-S\SERVER;Use Procedure fo' +
      'r Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=' +
      'M3;Use Encryption for Data=False;Tag with column collation when ' +
      'possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 168
    Top = 24
  end
  object Pub_BROWSE_Per: TADOStoredProc
    Connection = PermisionConnection
    ProcedureName = 'Pub_BROWSE_Per;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CPer'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@SrlPer'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@Name1'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 192
    Top = 136
  end
  object Pub_BROWSE_Role_Field3: TADOStoredProc
    Connection = PermisionConnection
    ProcedureName = 'Pub_BROWSE_Role_Field3;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@N_Filed'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Srl_User'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@KAccess'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 72
    Top = 72
  end
  object Pub_BROWSE_Role_Field1: TADOStoredProc
    Connection = PermisionConnection
    ProcedureName = 'Pub_BROWSE_Role_Field1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@C_User'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Pass'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@KAccess'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 184
  end
  object Pub_BROWSE_Per1: TADOStoredProc
    Connection = PermisionConnection
    CursorType = ctStatic
    ProcedureName = 'Pub_BROWSE_Per1;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 10
      end>
    Left = 192
    Top = 88
    object Pub_BROWSE_Per1srl_per1: TIntegerField
      FieldName = 'srl_per1'
    end
    object Pub_BROWSE_Per1srl_User: TIntegerField
      FieldName = 'srl_User'
    end
    object Pub_BROWSE_Per1n_per: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object Pub_BROWSE_Per1family: TStringField
      FieldName = 'family'
      Size = 25
    end
    object Pub_BROWSE_Per1c_hazineh: TIntegerField
      FieldName = 'c_hazineh'
    end
    object Pub_BROWSE_Per1n_hazineh: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object Pub_INSERT_System_Form_Field1: TADOStoredProc
    Connection = PermisionConnection
    CursorType = ctStatic
    ProcedureName = 'Pub_INSERT_System_Form_Field1;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_Form_1'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Cap_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@N_Field_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@Cap_Field_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 206
    Top = 191
  end
  object sp_GetCurrentFarsiDate: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_GetCurrentFarsiDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CurDate'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 8
        Value = Null
      end>
    Left = 152
    Top = 376
  end
  object sp_Find_T_Table: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Find_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FieldParamName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FieldParamValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 504
    Top = 170
  end
  object Delete1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Delete_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 160
  end
  object Sp_Get_Max: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Get_Max_T_Table;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FieldParamName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@FieldParamValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 8
  end
  object Insert1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Insert_T_Table;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 64
  end
  object Update1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Update_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@SrlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 112
  end
  object Browse1: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_Browse_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'T_Kol'
      end
      item
        Name = '@FieldParam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@KindParam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 16
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TStringField
      FieldName = 'FName'
      Size = 100
    end
  end
  object Browse2: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_Browse_T_Table;1'
    Parameters = <
      item
        Name = 'RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'T_Moeen'
      end
      item
        Name = '@FieldParam'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = 'Srl_Kol'
      end
      item
        Name = '@KindParam'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 255
      end>
    Left = 384
    Top = 16
    object Browse2FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse2FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse2FName: TStringField
      FieldName = 'FName'
      Size = 100
    end
  end
  object SystemConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=Shora;Data Source=.;Use Proc' +
      'edure for Prepare=1;Auto Translate=True;Packet Size=4096;Worksta' +
      'tion ID=SERVER;Use Encryption for Data=False;Tag with column col' +
      'lation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AfterConnect = SystemConnectionAfterConnect
    OnExecuteComplete = SystemConnectionExecuteComplete
    OnWillExecute = SystemConnectionWillExecute
    Left = 56
    Top = 16
  end
  object Pub_InitUser: TADOStoredProc
    Connection = PermisionConnection
    CursorType = ctStatic
    ProcedureName = 'Pub_InitUser;1'
    Parameters = <>
    Left = 400
    Top = 272
    object IntegerField1: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField2: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField1: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField2: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField3: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField3: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object INSERT_Form_Proc: TADOStoredProc
    ConnectionString = 
      'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=SA;Initi' +
      'al Catalog=MyHome;Data Source=Server'
    CursorType = ctStatic
    ProcedureName = 'INSERT_Form_Proc;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Cap_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@N_SP'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Cap_SP'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end>
    Left = 62
    Top = 287
  end
  object sp_Get_Max_T_Table2: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Get_Max_T_Table2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 56
  end
  object sp_Find_T_Table2: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_Find_T_Table2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 504
    Top = 218
  end
  object Session1: TSession
    SessionName = 'MySession'
    Left = 152
    Top = 328
  end
  object Query1: TQuery
    Left = 312
    Top = 280
  end
  object sp_FullCycleInfo: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_FullCycleInfo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlCurCycle'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CCurCycle'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NCurCycle'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@KCurCycle'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 400
    Top = 320
    object IntegerField4: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField5: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField4: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField5: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField6: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField6: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object sp_Find_T_Table3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Mysp_Find_T_Table3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 100
        Value = Null
      end>
    Left = 504
    Top = 266
  end
  object SP_GetSystemVersion: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'SP_GetSystemVersion;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Last_Version'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 144
    Top = 240
    object IntegerField10: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField11: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField10: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField11: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField12: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField12: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object sp_Get_Max_T_Table3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Mysp_Get_Max_T_Table3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Val_Param4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 496
    Top = 104
  end
  object SP_GetSystemInfo: TADOStoredProc
    Connection = PermisionConnection
    CursorType = ctStatic
    ProcedureName = 'SP_GetSystemInfo;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@N_System'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@N_Company'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@Cap_System'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end
      item
        Name = '@Is_Periodic'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 400
    Top = 224
    object IntegerField13: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField14: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField13: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField14: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField15: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField15: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object Pub_BROWSE_Form_Fields: TADOStoredProc
    Connection = PermisionConnection
    ProcedureName = 'Pub_BROWSE_Form_Fields;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Srl_System'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@N_Form'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = '1'
      end
      item
        Name = '@Srl_User'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 72
    Top = 128
    object Pub_BROWSE_Form_FieldsN_Field: TStringField
      FieldName = 'N_Field'
      Size = 50
    end
    object Pub_BROWSE_Form_FieldsK_Access: TIntegerField
      FieldName = 'K_Access'
    end
  end
  object sp_GetNextDate: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_GetNextDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@MyDate'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MyDif'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NextDate'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 8
        Value = Null
      end>
    Left = 56
    Top = 400
    object IntegerField7: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField8: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField7: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField8: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField9: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField9: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object QueryConnection: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Initia' +
      'l Catalog=Query;Data Source=HOZEH-S\SERVER;Use Procedure for Pre' +
      'pare=1;Auto Translate=True;Packet Size=4096;Workstation ID=M3;Us' +
      'e Encryption for Data=False;Tag with column collation when possi' +
      'ble=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    OnExecuteComplete = SystemConnectionExecuteComplete
    OnWillExecute = SystemConnectionWillExecute
    Left = 264
    Top = 24
  end
  object sp_SetUserWork: TADOStoredProc
    Connection = PermisionConnection
    CursorType = ctStatic
    ProcedureName = 'sp_SetUserWork'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KindWork'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@Srl_System_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_User_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Connect'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 62
    Top = 343
  end
  object Timer1: TTimer
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 496
    Top = 328
  end
  object Delete3: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'Mysp_Delete_T_Table3;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@SerialValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 216
  end
  object sp_GetCurrentServerTime: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'sp_GetCurrentServerTime;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CurTime'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 5
        Value = Null
      end>
    Left = 320
    Top = 400
  end
  object sp_GetSystemPermision: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'sp_GetSystemPermision;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SrlSystem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Is_Permision'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 224
    Top = 264
    object IntegerField16: TIntegerField
      FieldName = 'srl_per1'
    end
    object IntegerField17: TIntegerField
      FieldName = 'srl_User'
    end
    object StringField16: TStringField
      FieldName = 'n_per'
      Size = 15
    end
    object StringField17: TStringField
      FieldName = 'family'
      Size = 25
    end
    object IntegerField18: TIntegerField
      FieldName = 'c_hazineh'
    end
    object StringField18: TStringField
      FieldName = 'n_hazineh'
      Size = 50
    end
  end
  object insert_T_Letter_System: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'insert_T_Letter_System;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Table'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_Record'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Srl_User'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 400
  end
  object Browse_Pub_Reports: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_Pub_Reports'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlReport'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 56
    Top = 448
    object Browse_Pub_ReportsVal_Report: TMemoField
      FieldName = 'Val_Report'
      BlobType = ftMemo
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'JPEG Image File (*.jpg)|*.jpg'
    Left = 256
    Top = 336
  end
  object insert_T_Student_Pic_1: TADOStoredProc
    Connection = SystemConnection
    ProcedureName = 'insert_T_Student_Pic_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 200
        Value = Null
      end
      item
        Name = '@Srl_Student_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Pic_Student_4'
        Attributes = [paNullable]
        DataType = ftVarBytes
        Size = 2147483647
        Value = Null
      end>
    Left = 296
    Top = 464
  end
  object BROWSE_Student_Pic: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_T_Student_Pic;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Srl_Student'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end>
    Left = 392
    Top = 443
    object BROWSE_Student_PicPic_Student: TBlobField
      FieldName = 'Pic_Student'
    end
    object BROWSE_Student_PicFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
  end
  object Browse_T_StudentOne: TADOStoredProc
    Connection = SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_T_StudentOne;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlStudent'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 254
      end>
    Left = 192
    Top = 432
    object Browse_T_StudentOneFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse_T_StudentOneFName: TStringField
      FieldName = 'FName'
      ReadOnly = True
      Size = 303
    end
    object Browse_T_StudentOneFName1: TStringField
      FieldName = 'FName1'
      Size = 50
    end
    object Browse_T_StudentOneFName2: TStringField
      FieldName = 'FName2'
      Size = 100
    end
    object Browse_T_StudentOneFCode: TIntegerField
      FieldName = 'FCode'
      ReadOnly = True
    end
    object Browse_T_StudentOneK_Docum: TIntegerField
      FieldName = 'K_Docum'
    end
    object Browse_T_StudentOneK_StateDocum: TIntegerField
      FieldName = 'K_StateDocum'
    end
    object Browse_T_StudentOneNum_Per: TIntegerField
      FieldName = 'Num_Per'
    end
    object Browse_T_StudentOneNum_Cart: TIntegerField
      FieldName = 'Num_Cart'
    end
    object Browse_T_StudentOneNum_Melli: TStringField
      FieldName = 'Num_Melli'
      Size = 100
    end
    object Browse_T_StudentOneSrl_KPer: TIntegerField
      FieldName = 'Srl_KPer'
    end
    object Browse_T_StudentOneNum_Docum: TIntegerField
      FieldName = 'Num_Docum'
    end
    object Browse_T_StudentOneInput_Sal: TIntegerField
      FieldName = 'Input_Sal'
    end
    object Browse_T_StudentOneShohrat: TStringField
      FieldName = 'Shohrat'
      Size = 100
    end
    object Browse_T_StudentOneN_Father: TStringField
      FieldName = 'N_Father'
      Size = 100
    end
    object Browse_T_StudentOneNum_Shen: TStringField
      FieldName = 'Num_Shen'
      Size = 50
    end
    object Browse_T_StudentOneD_Brith: TStringField
      FieldName = 'D_Brith'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneL_Brith: TStringField
      FieldName = 'L_Brith'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Sodoor: TIntegerField
      FieldName = 'Srl_Sodoor'
    end
    object Browse_T_StudentOneD_Sodoor: TStringField
      FieldName = 'D_Sodoor'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneSrl_DegU: TIntegerField
      FieldName = 'Srl_DegU'
    end
    object Browse_T_StudentOneD_DegU: TStringField
      FieldName = 'D_DegU'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneSrl_ReshU: TIntegerField
      FieldName = 'Srl_ReshU'
    end
    object Browse_T_StudentOneSrl_DegH: TIntegerField
      FieldName = 'Srl_DegH'
    end
    object Browse_T_StudentOneSrl_ReshH: TIntegerField
      FieldName = 'Srl_ReshH'
    end
    object Browse_T_StudentOneAvrg: TFloatField
      FieldName = 'Avrg'
    end
    object Browse_T_StudentOneK_Maskan: TIntegerField
      FieldName = 'K_Maskan'
    end
    object Browse_T_StudentOneK_Jens: TIntegerField
      FieldName = 'K_Jens'
    end
    object Browse_T_StudentOneK_Tahol: TIntegerField
      FieldName = 'K_Tahol'
    end
    object Browse_T_StudentOneD_Marrid: TStringField
      FieldName = 'D_Marrid'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneK_Lebas: TIntegerField
      FieldName = 'K_Lebas'
    end
    object Browse_T_StudentOneK_Moamam: TIntegerField
      FieldName = 'K_Moamam'
    end
    object Browse_T_StudentOneD_Moamam: TStringField
      FieldName = 'D_Moamam'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneNum_Child: TIntegerField
      FieldName = 'Num_Child'
    end
    object Browse_T_StudentOneNum_Takafol: TIntegerField
      FieldName = 'Num_Takafol'
    end
    object Browse_T_StudentOneIs_Worker: TIntegerField
      FieldName = 'Is_Worker'
    end
    object Browse_T_StudentOneK_Gharardad: TIntegerField
      FieldName = 'K_Gharardad'
    end
    object Browse_T_StudentOneP_Salary: TBCDField
      FieldName = 'P_Salary'
      Precision = 19
    end
    object Browse_T_StudentOneCurWork: TStringField
      FieldName = 'CurWork'
      Size = 100
    end
    object Browse_T_StudentOneFather_Work: TStringField
      FieldName = 'Father_Work'
      Size = 100
    end
    object Browse_T_StudentOneHome_Adress: TStringField
      FieldName = 'Home_Adress'
      Size = 200
    end
    object Browse_T_StudentOneSrl_City: TIntegerField
      FieldName = 'Srl_City'
    end
    object Browse_T_StudentOneC_Post: TStringField
      FieldName = 'C_Post'
      Size = 100
    end
    object Browse_T_StudentOneHome_Tel: TStringField
      FieldName = 'Home_Tel'
      Size = 100
    end
    object Browse_T_StudentOneSrl_KNezam: TIntegerField
      FieldName = 'Srl_KNezam'
    end
    object Browse_T_StudentOneD_Ezam: TStringField
      FieldName = 'D_Ezam'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_MohlatKNezam: TStringField
      FieldName = 'D_MohlatKNezam'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneWork_Adress: TStringField
      FieldName = 'Work_Adress'
      Size = 200
    end
    object Browse_T_StudentOneWork_Tel: TStringField
      FieldName = 'Work_Tel'
      Size = 100
    end
    object Browse_T_StudentOneC_Classe: TStringField
      FieldName = 'C_Classe'
      Size = 50
    end
    object Browse_T_StudentOneD_Classe: TStringField
      FieldName = 'D_Classe'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_StartStudy: TStringField
      FieldName = 'D_StartStudy'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_Compelete: TStringField
      FieldName = 'D_Compelete'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneD_EndStudy: TStringField
      FieldName = 'D_EndStudy'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneDis_Per: TStringField
      FieldName = 'Dis_Per'
      Size = 200
    end
    object Browse_T_StudentOneDay_Jebhe: TIntegerField
      FieldName = 'Day_Jebhe'
    end
    object Browse_T_StudentOneDay_Esarat: TIntegerField
      FieldName = 'Day_Esarat'
    end
    object Browse_T_StudentOneDay_Zendan: TIntegerField
      FieldName = 'Day_Zendan'
    end
    object Browse_T_StudentOneIs_Basij: TIntegerField
      FieldName = 'Is_Basij'
    end
    object Browse_T_StudentOneDay_Basij: TIntegerField
      FieldName = 'Day_Basij'
    end
    object Browse_T_StudentOnePrc_Janbazi: TIntegerField
      FieldName = 'Prc_Janbazi'
    end
    object Browse_T_StudentOneExam_Mark: TFloatField
      FieldName = 'Exam_Mark'
    end
    object Browse_T_StudentOneDialog_Mark: TFloatField
      FieldName = 'Dialog_Mark'
    end
    object Browse_T_StudentOneRank: TIntegerField
      FieldName = 'Rank'
    end
    object Browse_T_StudentOneC_KNezam: TIntegerField
      FieldName = 'C_KNezam'
    end
    object Browse_T_StudentOneN_KNezam: TStringField
      FieldName = 'N_KNezam'
      Size = 100
    end
    object Browse_T_StudentOneC_Sodoor: TIntegerField
      FieldName = 'C_Sodoor'
    end
    object Browse_T_StudentOneN_Sodoor: TStringField
      FieldName = 'N_Sodoor'
      Size = 100
    end
    object Browse_T_StudentOneC_DegU: TIntegerField
      FieldName = 'C_DegU'
    end
    object Browse_T_StudentOneN_DegU: TStringField
      FieldName = 'N_DegU'
      Size = 100
    end
    object Browse_T_StudentOneC_ReshU: TIntegerField
      FieldName = 'C_ReshU'
    end
    object Browse_T_StudentOneN_ReshU: TStringField
      FieldName = 'N_ReshU'
      Size = 100
    end
    object Browse_T_StudentOneC_ReshH: TIntegerField
      FieldName = 'C_ReshH'
    end
    object Browse_T_StudentOneN_ReshH: TStringField
      FieldName = 'N_ReshH'
      Size = 100
    end
    object Browse_T_StudentOneC_DegH: TIntegerField
      FieldName = 'C_DegH'
    end
    object Browse_T_StudentOneN_DegH: TStringField
      FieldName = 'N_DegH'
      Size = 100
    end
    object Browse_T_StudentOneC_City: TIntegerField
      FieldName = 'C_City'
    end
    object Browse_T_StudentOneN_City: TStringField
      FieldName = 'N_City'
      Size = 100
    end
    object Browse_T_StudentOneC_KPer: TIntegerField
      FieldName = 'C_KPer'
    end
    object Browse_T_StudentOneN_KPer: TStringField
      FieldName = 'N_KPer'
      Size = 100
    end
    object Browse_T_StudentOneSrl_SourceInput: TIntegerField
      FieldName = 'Srl_SourceInput'
    end
    object Browse_T_StudentOneC_SourceInput: TIntegerField
      FieldName = 'C_SourceInput'
    end
    object Browse_T_StudentOneN_SourceInput: TStringField
      FieldName = 'N_SourceInput'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Melli: TIntegerField
      FieldName = 'Srl_Melli'
    end
    object Browse_T_StudentOneC_Melli: TIntegerField
      FieldName = 'C_Melli'
    end
    object Browse_T_StudentOneN_Melli: TStringField
      FieldName = 'N_Melli'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Deen: TIntegerField
      FieldName = 'Srl_Deen'
    end
    object Browse_T_StudentOneC_Deen: TIntegerField
      FieldName = 'C_Deen'
    end
    object Browse_T_StudentOneN_Deen: TStringField
      FieldName = 'N_Deen'
      Size = 100
    end
    object Browse_T_StudentOneSrl_Univer: TIntegerField
      FieldName = 'Srl_Univer'
    end
    object Browse_T_StudentOneC_Univer: TIntegerField
      FieldName = 'C_Univer'
    end
    object Browse_T_StudentOneN_Univer: TStringField
      FieldName = 'N_Univer'
      Size = 100
    end
    object Browse_T_StudentOneSrl_School: TIntegerField
      FieldName = 'Srl_School'
    end
    object Browse_T_StudentOneC_School: TIntegerField
      FieldName = 'C_School'
    end
    object Browse_T_StudentOneN_School: TStringField
      FieldName = 'N_School'
      Size = 100
    end
    object Browse_T_StudentOneSrl_KEsar: TIntegerField
      FieldName = 'Srl_KEsar'
    end
    object Browse_T_StudentOneC_KEsar: TIntegerField
      FieldName = 'C_KEsar'
    end
    object Browse_T_StudentOneN_KEsar: TStringField
      FieldName = 'N_KEsar'
      Size = 100
    end
    object Browse_T_StudentOneSrl_GroupShahr: TAutoIncField
      FieldName = 'Srl_GroupShahr'
      ReadOnly = True
    end
    object Browse_T_StudentOneC_GroupShahr: TIntegerField
      FieldName = 'C_GroupShahr'
    end
    object Browse_T_StudentOneN_GroupShahr: TStringField
      FieldName = 'N_GroupShahr'
      Size = 100
    end
    object Browse_T_StudentOneIs_Check: TIntegerField
      FieldName = 'Is_Check'
    end
    object Browse_T_StudentOneIs_Active: TIntegerField
      FieldName = 'Is_Active'
    end
    object Browse_T_StudentOneIs_EmamJam: TIntegerField
      FieldName = 'Is_EmamJam'
    end
    object Browse_T_StudentOneIs_EmamJom: TIntegerField
      FieldName = 'Is_EmamJom'
    end
    object Browse_T_StudentOneSrl_DprtDocum: TIntegerField
      FieldName = 'Srl_DprtDocum'
    end
    object Browse_T_StudentOneD_EtebarResearch: TStringField
      FieldName = 'D_EtebarResearch'
      FixedChar = True
      Size = 8
    end
    object Browse_T_StudentOneC_DprtDocum: TIntegerField
      FieldName = 'C_DprtDocum'
    end
    object Browse_T_StudentOneN_DprtDocum: TStringField
      FieldName = 'N_DprtDocum'
      Size = 200
    end
    object Browse_T_StudentOneC_Hozeh: TIntegerField
      FieldName = 'C_Hozeh'
    end
    object Browse_T_StudentOneIs_Mashmool: TIntegerField
      FieldName = 'Is_Mashmool'
    end
    object Browse_T_StudentOneStrIs_Mashmool: TStringField
      FieldName = 'StrIs_Mashmool'
      ReadOnly = True
      Size = 3
    end
    object Browse_T_StudentOneIs_CheckMashmool: TIntegerField
      FieldName = 'Is_CheckMashmool'
    end
    object Browse_T_StudentOneStrIs_CheckMashmool: TStringField
      FieldName = 'StrIs_CheckMashmool'
      ReadOnly = True
      Size = 3
    end
    object Browse_T_StudentOneIs_CheckResearch: TIntegerField
      FieldName = 'Is_CheckResearch'
    end
    object Browse_T_StudentOneStrIs_CheckResearch: TStringField
      FieldName = 'StrIs_CheckResearch'
      ReadOnly = True
      Size = 3
    end
    object Browse_T_StudentOneIs_CheckPaziresh: TIntegerField
      FieldName = 'Is_CheckPaziresh'
    end
    object Browse_T_StudentOneStrIs_CheckPaziresh: TStringField
      FieldName = 'StrIs_CheckPaziresh'
      ReadOnly = True
      Size = 3
    end
  end
end
