unit U_ChangePassword;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, Buttons;

type
  TF_ChangePassword = class(TForm)
    Label1111: TLabel;
    Label2222: TLabel;
    Label3333: TLabel;
    LastPass: TEdit;
    NewPass: TEdit;
    Ok: TBitBtn;
    BitBtn2: TBitBtn;
    NewPass2: TEdit;
    Pub_UPDATE_User1: TADOStoredProc;
    procedure LastPassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LastPassKeyPress(Sender: TObject; var Key: Char);
    procedure NewPassKeyPress(Sender: TObject; var Key: Char);
    procedure NewPassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NewPass2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NewPass2KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn2Click(Sender: TObject);
    procedure OkClick(Sender: TObject);
  private
    function CorrectData: Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ChangePassword: TF_ChangePassword;

implementation

uses U_Common;

{$R *.DFM}

Function TF_ChangePassword.CorrectData : Boolean;
var
   Rs : boolean;
begin
   Rs := False;
   if (Pass_User = Trim(LastPass.Text))
    and (Trim(NewPass.Text) = Trim(NewPass2.Text)) then
      Rs := True;
   Result := Rs;
end;

procedure TF_ChangePassword.LastPassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key=vk_down then
      NewPass.SetFocus;
end;

procedure TF_ChangePassword.LastPassKeyPress(Sender: TObject;
  var Key: Char);
begin
 if key=#13 then
    NewPass.SetFocus;
end;

procedure TF_ChangePassword.NewPassKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13 then
      NewPass2.SetFocus;
end;

procedure TF_ChangePassword.NewPassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key=vk_up then
      LastPass.SetFocus;
   if key=vk_down then
      NewPass.SetFocus;

end;

procedure TF_ChangePassword.NewPass2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = vk_up then
      NewPass.SetFocus;
   if key = vk_down then
      Ok.SetFocus;
end;

procedure TF_ChangePassword.NewPass2KeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13 then
      Ok.SetFocus;
end;

procedure TF_ChangePassword.BitBtn2Click(Sender: TObject);
begin
   Close;
end;

procedure TF_ChangePassword.OkClick(Sender: TObject);
begin
   if CorrectData then
   begin
      NullAdoSPParameters(Pub_UPDATE_User1);

      Pub_UPDATE_User1.Parameters.ParamByName('@srl_User_1').value := Srl_User;
      Pub_UPDATE_User1.Parameters.ParamByName('@Pass_3').value := Trim(NewPass.Text);
      try
         Pub_UPDATE_User1.ExecProc;
         Pass_User := Trim(NewPass.Text);
         ShowMessage('��� ��� �� ������ ����� ����');
         Close;
      except
         ShowMessage('��� �� ����� �������');
      end;
   end
   else
      ShowMessage('������� �� ����� ����');
end;

end.
