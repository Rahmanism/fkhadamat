unit U_Filter3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask;

type
  TF_Filter3 = class(TForm)
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    procedure MakeFIlter;
    { Private declarations }
  public
    { Public declarations }
    SqlFilter : String;
    ConsFieldCount : integer;
  end;

var
  F_Filter3: TF_Filter3;

implementation

uses U_Common;

{$R *.DFM}


procedure TF_Filter3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = vk_Return then
   begin
      if (Shift = [ssCtrl]) then
         BitBtn1Click(Sender)
      else
      begin
         Key := 0;
         Perform(7388420, vk_Tab, 0);
      end;
   end;
end;

procedure TF_Filter3.BitBtn2Click(Sender: TObject);
begin
   ModalResult := mrCancel;
   SqlFilter := ' ';
   Close;
end;

procedure TF_Filter3.MakeFIlter;
var
   SmallFilter : String;
   i : integer;

   L1 : TLabel;
   ME1 : TMaskEdit;

begin
   SqlFilter := '';
   for i := 1 to ConsFieldCount do
   begin
      L1 := TLabel(FindComponent('Label' + IntToStr(i)));
      ME1 := TMaskEdit(FindComponent('MaskEdit' + IntToStr(i)));

      if (L1 <> nil) and (ME1 <> nil) then
      begin
         SmallFilter := '';
         if (Trim(ME1.Text) <> '') and (Trim(ME1.Text) <> '/  /') and (Trim(ME1.Text) <> ':') then
            if ME1.Tag = 1 then
               SmallFilter := MakeFilterStringOne(ME1.Tag, 0, L1.Hint, ME1.Text, '')
            else if ME1.Tag = 2 then
               SmallFilter := MakeFilterStringOne(ME1.Tag, 5, L1.Hint, ME1.Text, '');

         if SmallFilter <> '' then
            if SqlFilter = '' then
               SqlFilter := SmallFilter
            else
               SqlFilter := SqlFilter + ' AND ' + SmallFilter;
      end;
   end;
end;


procedure TF_Filter3.BitBtn1Click(Sender: TObject);
begin
   MakeFIlter;
   ModalResult := mrOk;
end;

end.
