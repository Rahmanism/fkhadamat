object F_UpdateTable: TF_UpdateTable
  Left = 252
  Top = 379
  Width = 630
  Height = 451
  BiDiMode = bdRightToLeft
  Caption = #1601#1585#1605' '#1601#1610#1604#1578#1585
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object BCancel: TBitBtn
    Left = 16
    Top = 384
    Width = 75
    Height = 32
    Caption = #1582#1585#1608#1580
    TabOrder = 0
    OnClick = BCancelClick
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 622
    Height = 377
    ActivePage = TabSheet5
    Align = alTop
    TabOrder = 1
    object TabSheet5: TTabSheet
      Caption = #1608#1610#1585#1575#1610#1588
      ImageIndex = 4
      object Label6: TLabel
        Left = 291
        Top = 88
        Width = 61
        Height = 16
        Caption = #1605#1602#1583#1575#1585' '#1580#1583#1610#1583
      end
      object NewValue: TEdit
        Left = 160
        Top = 88
        Width = 121
        Height = 24
        TabOrder = 0
      end
      object BOk: TBitBtn
        Left = 216
        Top = 195
        Width = 137
        Height = 38
        Caption = #1575#1606#1580#1575#1605' '#1608#1610#1585#1575#1610#1588
        TabOrder = 1
        OnClick = BOkClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1604#1610#1587#1578' '#1601#1610#1604#1583#1607#1575
      Font.Charset = ARABIC_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ImageIndex = 2
      ParentFont = False
      object Label5: TLabel
        Left = 340
        Top = 8
        Width = 260
        Height = 16
        Caption = #1601#1610#1604#1583' '#1605#1608#1585#1583' '#1606#1592#1585' '#1580#1607#1578' '#1608#1610#1585#1575#1610#1588' '#1585#1575' '#1575#1606#1578#1582#1575#1576' '#1606#1605#1575#1574#1610#1583
      end
      object DBGrid1: TDBGrid
        Left = 240
        Top = 64
        Width = 145
        Height = 209
        DataSource = DataSource1
        Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = ARABIC_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        Columns = <
          item
            Expanded = False
            FieldName = 'Dis'
            Width = 100
            Visible = True
          end>
      end
    end
    object TabSheet1: TTabSheet
      Caption = #1582#1585#1608#1580#1610
      object Label8: TLabel
        Left = 123
        Top = 320
        Width = 69
        Height = 16
        Caption = #1578#1593#1583#1575#1583' '#1585#1603#1608#1585#1583
      end
      object Label9: TLabel
        Left = 49
        Top = 320
        Width = 31
        Height = 16
        Caption = '------'
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 614
        Height = 313
        DataSource = DataSource3
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
      end
      object BitBtn1: TBitBtn
        Left = 256
        Top = 320
        Width = 75
        Height = 25
        Caption = #1605#1588#1575#1607#1583#1607
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1588#1585#1575#1610#1591
      ImageIndex = 1
      object Label1: TLabel
        Left = 443
        Top = 26
        Width = 84
        Height = 16
        Caption = #1604#1610#1587#1578' '#1711#1586#1610#1606#1607' '#1607#1575
      end
      object Label2: TLabel
        Left = 356
        Top = 24
        Width = 26
        Height = 16
        Caption = #1588#1585#1591
      end
      object Label3: TLabel
        Left = 189
        Top = 53
        Width = 43
        Height = 16
        Caption = #1605#1602#1583#1575#1585' 1'
      end
      object Label4: TLabel
        Left = 189
        Top = 93
        Width = 43
        Height = 16
        Caption = #1605#1602#1583#1575#1585' 2'
        Visible = False
      end
      object LAnd: TLabel
        Left = 133
        Top = 72
        Width = 8
        Height = 16
        Caption = #1608
        Visible = False
      end
      object LBAndOr: TListBox
        Left = 531
        Top = 109
        Width = 33
        Height = 36
        Style = lbOwnerDrawVariable
        BorderStyle = bsNone
        Color = 14548991
        ItemHeight = 16
        Items.Strings = (
          #1608
          #1610#1575)
        Sorted = True
        TabOrder = 0
        TabWidth = 5
        OnEnter = LBAndOrEnter
        OnExit = LBAndOrExit
      end
      object LBFields: TListBox
        Left = 384
        Top = 45
        Width = 145
        Height = 160
        BorderStyle = bsNone
        IntegralHeight = True
        ItemHeight = 16
        TabOrder = 1
        OnClick = LBFieldsClick
        OnEnter = LBAndOrEnter
        OnExit = LBAndOrExit
      end
      object LBCons: TListBox
        Left = 261
        Top = 45
        Width = 121
        Height = 160
        BorderStyle = bsNone
        IntegralHeight = True
        ItemHeight = 16
        TabOrder = 2
        OnClick = LBConsClick
        OnEnter = LBAndOrEnter
        OnExit = LBAndOrExit
      end
      object Val1: TMaskEdit
        Left = 88
        Top = 48
        Width = 97
        Height = 24
        EditMask = '!99:99;1;_'
        MaxLength = 5
        TabOrder = 3
        Text = '  :  '
        OnEnter = Val1Enter
        OnExit = Val1Exit
        OnKeyPress = Val1KeyPress
      end
      object Val2: TMaskEdit
        Left = 88
        Top = 88
        Width = 97
        Height = 24
        TabOrder = 4
        Visible = False
        OnEnter = Val1Enter
        OnExit = Val1Exit
        OnKeyPress = Val2KeyPress
      end
      object BAddCons: TBitBtn
        Left = 160
        Top = 144
        Width = 75
        Height = 33
        Caption = #1575#1590#1575#1601#1607' '#1588#1608#1583
        TabOrder = 5
        OnClick = BAddConsClick
      end
      object BDelCons: TBitBtn
        Left = 64
        Top = 144
        Width = 89
        Height = 33
        Caption = #1581#1584#1601' '#1588#1608#1583
        TabOrder = 6
        OnClick = BDelConsClick
      end
      object LBTotal: TListBox
        Left = 64
        Top = 213
        Width = 505
        Height = 100
        Style = lbOwnerDrawVariable
        IntegralHeight = True
        ItemHeight = 16
        TabOrder = 7
        OnEnter = LBAndOrEnter
        OnExit = LBAndOrExit
      end
      object Q1: TListBox
        Left = 64
        Top = 310
        Width = 506
        Height = 20
        BiDiMode = bdLeftToRight
        IntegralHeight = True
        ItemHeight = 16
        ParentBiDiMode = False
        TabOrder = 8
        Visible = False
        OnEnter = LBAndOrEnter
        OnExit = LBAndOrExit
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1604#1610#1587#1578' '#1576#1575#1606#1603#1607#1575
      ImageIndex = 3
      object Label7: TLabel
        Left = 347
        Top = 8
        Width = 254
        Height = 16
        Caption = #1576#1575#1606#1603' '#1605#1608#1585#1583' '#1606#1592#1585' '#1580#1607#1578' '#1608#1610#1585#1575#1610#1588' '#1585#1575' '#1575#1606#1578#1582#1575#1576' '#1606#1605#1575#1574#1610#1583
      end
      object DBGrid2: TDBGrid
        Left = 80
        Top = 48
        Width = 489
        Height = 273
        DataSource = DataSource2
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = [fsBold]
        Columns = <
          item
            Expanded = False
            FieldName = 'TableName11'
            Width = 207
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TableCaption11'
            Width = 176
            Visible = True
          end>
      end
    end
  end
  object ADOQueryFields: TADOQuery
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'TableName11'
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 128
        Value = 'T_City'
      end>
    SQL.Strings = (
      'SELECT'
      '         dbo.syscolumns.name AS FieldName11,'
      '         dbo.systypes.name AS FieldType11,'
      '         dbo.syscolumns.length,'
      '         dbo.sysproperties.[value] AS Dis'
      '      FROM         dbo.syscolumns INNER JOIN'
      
        '                      dbo.sysobjects ON dbo.syscolumns.id = dbo.' +
        'sysobjects.id INNER JOIN'
      
        '                      dbo.sysproperties ON dbo.syscolumns.id = d' +
        'bo.sysproperties.id AND dbo.syscolumns.colid = dbo.sysproperties' +
        '.smallid INNER JOIN'
      
        '                      dbo.systypes ON dbo.syscolumns.xtype = dbo' +
        '.systypes.xtype'
      '      WHERE'
      '         (dbo.sysobjects.xtype = '#39'U'#39')'
      '         AND (dbo.sysobjects.name = :TableName11) '
      'ORDER BY dbo.sysobjects.name, dbo.syscolumns.name ')
    Left = 272
    Top = 88
    object ADOQueryFieldsFieldName11: TWideStringField
      FieldName = 'FieldName11'
      Size = 128
    end
    object ADOQueryFieldsFieldType11: TWideStringField
      FieldName = 'FieldType11'
      Size = 128
    end
    object ADOQueryFieldslength: TSmallintField
      FieldName = 'length'
    end
    object ADOQueryFieldsDis: TVariantField
      FieldName = 'Dis'
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOQueryFields
    Left = 272
    Top = 144
  end
  object ADOCommand1: TADOCommand
    Connection = F_Common.SystemConnection
    Parameters = <>
    Left = 48
    Top = 88
  end
  object DataSource2: TDataSource
    DataSet = ADOQueryTables
    OnDataChange = DataSource2DataChange
    Left = 376
    Top = 144
  end
  object ADOQueryTables: TADOQuery
    Active = True
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT     '
      '                   dbo.sysobjects.name AS TableName11 ,'
      '                   dbo.sysproperties.[value] AS TableCaption11 '
      'FROM         dbo.sysobjects LEFT OUTER JOIN'
      
        '                      dbo.sysproperties ON dbo.sysobjects.id = d' +
        'bo.sysproperties.id'
      
        'WHERE     (dbo.sysobjects.xtype = '#39'U'#39') AND (dbo.sysproperties.sm' +
        'allid = 0 OR'
      '                      dbo.sysproperties.smallid IS NULL)'
      'ORDER BY dbo.sysobjects.name'
      '')
    Left = 376
    Top = 88
    object ADOQueryTablesTableName11: TWideStringField
      FieldName = 'TableName11'
      Size = 128
    end
    object ADOQueryTablesTableCaption11: TVariantField
      FieldName = 'TableCaption11'
    end
  end
  object DataSource3: TDataSource
    DataSet = ADODataSet1
    Left = 160
    Top = 152
  end
  object ADODataSet1: TADODataSet
    Connection = F_Common.SystemConnection
    Parameters = <>
    Left = 164
    Top = 91
  end
end
