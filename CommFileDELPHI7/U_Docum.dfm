object F_Docum: TF_Docum
  Left = 70
  Top = 94
  Width = 875
  Height = 549
  BiDiMode = bdRightToLeft
  Caption = #1604#1610#1587#1578' '#1601#1575#1610#1604#1607#1575
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object DBText1: TDBText
    Left = 29
    Top = 488
    Width = 60
    Height = 19
    AutoSize = True
    BiDiMode = bdLeftToRight
    DataField = 'Pic_Path'
    DataSource = DSBrowse_T_Source_Pic
    ParentBiDiMode = False
  end
  object Panel1: TPanel
    Left = 17
    Top = 4
    Width = 525
    Height = 391
    TabOrder = 0
    object OleContainer1: TOleContainer
      Left = 8
      Top = 8
      Width = 513
      Height = 377
      Caption = 'OleContainer1'
      TabOrder = 0
    end
  end
  object DBNavigator1: TDBNavigator
    Left = 168
    Top = 400
    Width = 220
    Height = 33
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbPost]
    TabOrder = 1
  end
  object BitBtnAddPic: TBitBtn
    Left = 16
    Top = 400
    Width = 137
    Height = 33
    Caption = #1575#1590#1575#1601#1607' '#1603#1585#1583#1606' '#1601#1575#1610#1604
    TabOrder = 2
    OnClick = BitBtnAddPicClick
  end
  object BitBtnDelOne: TBitBtn
    Left = 408
    Top = 400
    Width = 113
    Height = 33
    Caption = #1581#1584#1601' '#1601#1575#1610#1604
    TabOrder = 3
    OnClick = BitBtnDelOneClick
  end
  object DBGrid1: TDBGrid
    Left = 552
    Top = 8
    Width = 297
    Height = 385
    DataSource = DSBrowse_T_Source_Pic
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = ARABIC_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'FCode'
        Title.Caption = #1588#1605#1575#1585#1607
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Des_Pic'
        Title.Caption = #1605#1608#1590#1608#1593
        Width = 181
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FName'
        Title.Caption = #1578#1608#1590#1610#1581#1575#1578
        Width = 8
        Visible = True
      end>
  end
  object DBEdit1: TDBEdit
    Left = 552
    Top = 400
    Width = 302
    Height = 27
    BiDiMode = bdRightToLeft
    DataField = 'FName'
    ParentBiDiMode = False
    TabOrder = 5
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 552
    Top = 432
    Width = 305
    Height = 27
    DataField = 'Srl_Des_Pic'
    KeyField = 'FSerial'
    ListField = 'FName'
    ListSource = DataSource1
    TabOrder = 6
    OnClick = DBLookupComboBox1Click
  end
  object BitBtnDelAll: TBitBtn
    Left = 208
    Top = 440
    Width = 153
    Height = 35
    Caption = #1581#1584#1601' '#1607#1605#1607' '#1601#1575#1610#1604#1607#1575
    TabOrder = 7
    OnClick = BitBtnDelAllClick
  end
  object IsMoveFile: TCheckBox
    Left = 687
    Top = 461
    Width = 169
    Height = 23
    Caption = #1570#1610#1575' '#1601#1575#1610#1604' '#1605#1606#1578#1602#1604' '#1588#1608#1583
    TabOrder = 8
  end
  object IsViewFile: TCheckBox
    Left = 479
    Top = 461
    Width = 169
    Height = 23
    Caption = #1570#1610#1575' '#1601#1575#1610#1604' '#1605#1588#1575#1607#1583#1607' '#1588#1608#1583
    TabOrder = 9
    OnClick = IsViewFileClick
  end
  object BitBtnClose: TBitBtn
    Left = 16
    Top = 438
    Width = 97
    Height = 33
    Caption = #1582#1585#1608#1580
    TabOrder = 10
    OnClick = BitBtnCloseClick
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 648
    Top = 296
  end
  object DataSource2: TDataSource
    DataSet = ADOTable2
    Left = 632
    Top = 144
  end
  object ppReport1: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Letter 8 1/2 x 11 in'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    DeviceType = 'Screen'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = False
    OutlineSettings.Visible = False
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = False
    Left = 433
    Top = 100
    Version = '7.03'
    mmColumnWidth = 0
    object ppHeaderBand1: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 0
      mmPrintPosition = 0
    end
    object ppDetailBand1: TppDetailBand
      mmBottomOffset = 0
      mmHeight = 80963
      mmPrintPosition = 0
      object ppDBImage1: TppDBImage
        UserName = 'DBImage1'
        MaintainAspectRatio = False
        GraphicType = 'Bitmap'
        mmHeight = 66675
        mmLeft = 37835
        mmTop = 11377
        mmWidth = 132821
        BandType = 4
      end
    end
    object ppFooterBand1: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 353
    Top = 60
  end
  object Browse_T_Source_Pic: TADOStoredProc
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Common_SP_Browse_T_Source_Pic;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlSource'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 194487
      end
      item
        Name = '@KFile'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@KSource'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 4009
      end>
    Left = 160
    Top = 35
    object Browse_T_Source_PicFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse_T_Source_PicFCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse_T_Source_PicFName: TStringField
      FieldName = 'FName'
      Size = 200
    end
    object Browse_T_Source_PicSrl_Des_Pic: TIntegerField
      FieldName = 'Srl_Des_Pic'
    end
    object Browse_T_Source_PicDes_Pic: TStringField
      FieldName = 'Des_Pic'
      Size = 100
    end
    object Browse_T_Source_PicPic_Path: TStringField
      FieldName = 'Pic_Path'
      Size = 200
    end
  end
  object DSBrowse_T_Source_Pic: TDataSource
    DataSet = Browse_T_Source_Pic
    OnDataChange = DSBrowse_T_Source_PicDataChange
    Left = 161
    Top = 84
  end
  object Delete1: TADOStoredProc
    ProcedureName = 'delete_T_Source_Pic_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 136
  end
  object ADOTable2: TADOTable
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    TableName = 'Pub_Des_Picture'
    Left = 632
    Top = 96
    object ADOTable2FSerial: TIntegerField
      FieldName = 'FSerial'
    end
    object ADOTable2FCode: TIntegerField
      FieldName = 'FCode'
    end
    object ADOTable2FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object ADOTable2U_DateTime: TDateTimeField
      FieldName = 'U_DateTime'
    end
    object ADOTable2Srl_Connection: TIntegerField
      FieldName = 'Srl_Connection'
    end
  end
  object ADOTable1: TADOTable
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    TableName = 'Pub_Des_Picture'
    Left = 640
    Top = 248
    object ADOTable1FSerial: TIntegerField
      FieldName = 'FSerial'
    end
    object ADOTable1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object ADOTable1FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object ADOTable1U_DateTime: TDateTimeField
      FieldName = 'U_DateTime'
    end
    object ADOTable1Srl_Connection: TIntegerField
      FieldName = 'Srl_Connection'
    end
  end
end
