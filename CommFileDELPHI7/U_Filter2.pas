unit U_Filter2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask;

type
  TF_Filter2 = class(TForm)
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    procedure MakeFIlter;
    { Private declarations }
  public
    { Public declarations }
    SqlFilter : String;
    ConsFieldCount : integer;
  end;

var
  F_Filter2: TF_Filter2;

implementation

uses U_Common;

{$R *.DFM}


procedure TF_Filter2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = vk_Return then
   begin
      if (Shift = [ssCtrl]) then
         BitBtn1Click(Sender)
      else
      begin
         Key := 0;
         Perform(7388420, vk_Tab, 0);
      end;
   end;
end;

procedure TF_Filter2.BitBtn2Click(Sender: TObject);
begin
   ModalResult := mrCancel;
   SqlFilter := ' ';
   Close;
end;

procedure TF_Filter2.MakeFIlter;
var
   SmallFilter : String;
   i : integer;

   L1 : TLabel;
   CB1 : TComboBox;
   ME1 : TMaskEdit;
   ME2 : TMaskEdit;

begin
   SqlFilter := '';
   for i := 1 to ConsFieldCount do
   begin
      L1 := TLabel(FindComponent('Label' + IntToStr(i)));
      CB1 := TComboBox(FindComponent('ComboBox' + IntToStr(i)));
      ME1 := TMaskEdit(FindComponent('MaskEdit' + IntToStr(i)));
      ME2 := TMaskEdit(FindComponent('MaskEdit' + IntToStr(i+100)));

      if (L1 <> nil) and (CB1 <> nil) and (ME1 <> nil) and (ME2 <> nil) then
      begin
         SmallFilter := '';
         if (CB1.ItemIndex <> -1) and (Trim(ME1.Text) <> '')
            and (Trim(ME1.Text) <> '/  /') and (Trim(ME1.Text) <> ':') then
               SmallFilter := MakeFilterStringOne(CB1.Tag, CB1.ItemIndex, L1.Hint, ME1.Text, ME2.Text);

         if SmallFilter <> '' then
            if SqlFilter = '' then
               SqlFilter := SmallFilter
            else
               SqlFilter := SqlFilter + ' AND ' + SmallFilter;
      end;
   end;
end;


procedure TF_Filter2.BitBtn1Click(Sender: TObject);
begin
   MakeFIlter;
   ModalResult := mrOk;
end;

end.
