object F_DBGrid_Option: TF_DBGrid_Option
  Left = 238
  Top = 136
  Width = 582
  Height = 476
  BiDiMode = bdRightToLeft
  Caption = #1605#1588#1582#1589#1575#1578' '#1580#1583#1608#1604
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  OldCreateOrder = False
  ParentBiDiMode = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object Label2: TLabel
    Left = 307
    Top = 12
    Width = 173
    Height = 19
    Caption = #1587#1578#1608#1606#1607#1575#1610' '#1602#1575#1576#1604' '#1605#1588#1575#1607#1583#1607
  end
  object SpeedButton2: TSpeedButton
    Left = 8
    Top = 48
    Width = 201
    Height = 33
    Caption = #1601#1608#1606#1578' '#1583#1585' '#1580#1583#1608#1604
    OnClick = SpeedButton2Click
  end
  object Label1: TLabel
    Left = 444
    Top = 244
    Width = 84
    Height = 19
    Caption = #1591#1608#1604' '#1587#1578#1608#1606
  end
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 8
    Width = 201
    Height = 33
    Caption = #1601#1608#1606#1578' '#1593#1606#1608#1575#1606
    OnClick = SpeedButton1Click
  end
  object Label3: TLabel
    Left = 268
    Top = 248
    Width = 10
    Height = 19
    Caption = '0'
  end
  object SpeedButton3: TSpeedButton
    Left = 486
    Top = 40
    Width = 53
    Height = 49
    Caption = #1575#1608#1604#1610#1606
    OnClick = SpeedButton3Click
  end
  object SpeedButton4: TSpeedButton
    Left = 486
    Top = 88
    Width = 53
    Height = 49
    Caption = #1602#1576#1604#1610
    OnClick = SpeedButton4Click
  end
  object SpeedButton5: TSpeedButton
    Left = 486
    Top = 136
    Width = 53
    Height = 49
    Caption = #1576#1593#1583#1610
    OnClick = SpeedButton5Click
  end
  object SpeedButton6: TSpeedButton
    Left = 486
    Top = 184
    Width = 53
    Height = 49
    Caption = #1570#1582#1585#1610#1606
    OnClick = SpeedButton6Click
  end
  object SpeedButton7: TSpeedButton
    Left = 8
    Top = 128
    Width = 201
    Height = 33
    Caption = #1575#1606#1578#1582#1575#1576' '#1607#1605#1607
    OnClick = SpeedButton7Click
  end
  object SpeedButton8: TSpeedButton
    Left = 8
    Top = 168
    Width = 201
    Height = 33
    Caption = #1581#1584#1601' '#1607#1605#1607
    OnClick = SpeedButton8Click
  end
  object SpeedButton9: TSpeedButton
    Left = 8
    Top = 208
    Width = 201
    Height = 33
    Caption = #1587#1578#1608#1606' '#1580#1583#1610#1583
    OnClick = SpeedButton9Click
  end
  object SpeedButton10: TSpeedButton
    Left = 8
    Top = 248
    Width = 201
    Height = 33
    Caption = #1581#1584#1601' '#1587#1578#1608#1606
    OnClick = SpeedButton10Click
  end
  object SpeedButton11: TSpeedButton
    Left = 8
    Top = 88
    Width = 201
    Height = 33
    Caption = #1608#1610#1585#1575#1610#1588' '#1593#1606#1608#1575#1606' '#1587#1578#1608#1606
    OnClick = SpeedButton11Click
  end
  object Label4: TLabel
    Left = 448
    Top = 332
    Width = 97
    Height = 19
    Caption = #1606#1575#1605' '#1604#1575#1578#1610#1606' '#1601#1610#1604#1583
  end
  object SpeedButton13: TSpeedButton
    Left = 8
    Top = 288
    Width = 201
    Height = 33
    Caption = #1581#1584#1601' '#1603#1604#1610#1607' '#1578#1594#1610#1610#1585#1575#1578' '#1580#1583#1608#1604
    OnClick = SpeedButton13Click
  end
  object BitBtn1: TBitBtn
    Left = 240
    Top = 384
    Width = 105
    Height = 33
    Caption = #1581#1575#1604#1578' '#1575#1608#1604#1610#1607
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 352
    Top = 384
    Width = 105
    Height = 33
    Cancel = True
    Caption = #1578#1575#1574#1610#1583
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  object FieldCaptionList: TCheckListBox
    Left = 216
    Top = 40
    Width = 265
    Height = 194
    OnClickCheck = FieldCaptionListClickCheck
    IntegralHeight = True
    ItemHeight = 19
    Items.Strings = (
      'a'
      'b'
      'c')
    TabOrder = 2
    OnClick = FieldCaptionListClick
  end
  object ColumnWidth: TTrackBar
    Left = 224
    Top = 272
    Width = 329
    Height = 45
    Max = 300
    Frequency = 10
    TabOrder = 3
    TickMarks = tmBoth
    OnChange = ColumnWidthChange
  end
  object BitBtn3: TBitBtn
    Left = 128
    Top = 384
    Width = 105
    Height = 33
    Cancel = True
    Caption = #1582#1585#1608#1580
    TabOrder = 4
    OnClick = BitBtn2Click
  end
  object ComboBoxFields: TComboBox
    Left = 216
    Top = 328
    Width = 225
    Height = 27
    ItemHeight = 19
    Sorted = True
    TabOrder = 5
    OnClick = ComboBoxFieldsClick
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 328
    Top = 112
  end
end
