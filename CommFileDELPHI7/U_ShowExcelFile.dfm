object F_ShowExcelFile: TF_ShowExcelFile
  Left = 289
  Top = 130
  Width = 696
  Height = 457
  BiDiMode = bdRightToLeft
  Caption = #1605#1588#1575#1607#1583#1607' '#1601#1575#1610#1604' '#1575#1603#1587#1604
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  PixelsPerInch = 96
  TextHeight = 13
  object ReportGrid: TDBGrid
    Left = 137
    Top = 47
    Width = 551
    Height = 329
    Align = alClient
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 376
    Width = 688
    Height = 47
    Align = alBottom
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Bevel1: TBevel
      Left = 72
      Top = 8
      Width = 177
      Height = 33
    end
    object Label17: TLabel
      Left = 141
      Top = 17
      Width = 93
      Height = 19
      Caption = #1578#1593#1583#1575#1583' '#1585#1603#1608#1585#1583' :'
    end
    object LabelCount: TLabel
      Left = 74
      Top = 17
      Width = 72
      Height = 16
      Alignment = taCenter
      Caption = '000000000'
      Font.Charset = ARABIC_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Exit: TSpeedButton
      Left = 8
      Top = 6
      Width = 57
      Height = 33
      Caption = #1582#1585#1608#1580
      Flat = True
      Font.Charset = ARABIC_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Compset'
      Font.Style = [fsBold]
      Layout = blGlyphTop
      NumGlyphs = 4
      ParentFont = False
      OnClick = ExitClick
    end
    object BitBtn1: TBitBtn
      Left = 304
      Top = 8
      Width = 107
      Height = 33
      Caption = #1575#1606#1578#1602#1575#1604
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 511
      Top = 9
      Width = 170
      Height = 30
      Caption = #1575#1606#1578#1582#1575#1576' '#1601#1575#1610#1604' Excel'
      TabOrder = 1
      OnClick = BitBtn2Click
    end
  end
  object TableListBox: TListBox
    Left = 0
    Top = 47
    Width = 137
    Height = 329
    Align = alLeft
    ItemHeight = 13
    TabOrder = 2
    OnDblClick = TableListBoxDblClick
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 47
    Align = alTop
    Font.Charset = ARABIC_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 106
      Height = 19
      Caption = #1604#1610#1587#1578' '#1589#1601#1581#1575#1578
    end
    object Label2: TLabel
      Left = 356
      Top = 16
      Width = 114
      Height = 19
      Caption = #1605#1581#1578#1608#1610#1575#1578' '#1589#1601#1581#1607
    end
  end
  object ADOTable1: TADOTable
    TableDirect = True
    Left = 240
    Top = 80
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    OnDataChange = DataSource1DataChange
    Left = 240
    Top = 136
  end
  object ADOTable2: TADOTable
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    TableName = 'T_Organ_Charjh_Det'
    Left = 440
    Top = 112
    object ADOTable2Srl_Charjh_Det: TAutoIncField
      FieldName = 'Srl_Charjh_Det'
      ReadOnly = True
    end
    object ADOTable2Srl_Charjh: TIntegerField
      FieldName = 'Srl_Charjh'
    end
    object ADOTable2N_Per: TStringField
      FieldName = 'N_Per'
      Size = 50
    end
    object ADOTable2F_Per: TStringField
      FieldName = 'F_Per'
      Size = 50
    end
    object ADOTable2N_Father: TStringField
      FieldName = 'N_Father'
      Size = 50
    end
    object ADOTable2Num_Shen: TStringField
      FieldName = 'Num_Shen'
    end
    object ADOTable2Num_Cart: TStringField
      FieldName = 'Num_Cart'
      FixedChar = True
      Size = 16
    end
    object ADOTable2Num_Per: TFloatField
      FieldName = 'Num_Per'
    end
    object ADOTable2P_Charjh: TBCDField
      FieldName = 'P_Charjh'
      Precision = 19
    end
    object ADOTable2K_CharjhSodoor: TIntegerField
      FieldName = 'K_CharjhSodoor'
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 353
    Top = 154
  end
end
