unit U_Common;

//F_Common.SystemConnection
//F_Common.PermisionConnection
//PCheckBoxes
//PCheckBoxes2
//PCheckBoxes3
//SrlSalJari

interface

uses
  Windows, Variants, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, stdctrls, buttons, extctrls, comctrls, Menus, Mask, IniFiles,
  ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, ppDB, ppDBPipe, ppComm,
  ppRelatv, ppProd, ppReport, DBCtrls, ppDBBDE , Grids, DBGrids, DBTables, 
  FileCtrl, pbnumedit, ppTypes, ppVar, ExtDlgs, XPMan, OleServer,  ComObj ;

Const
    ValidKey : Set of Char = ['0'..'9',#13,#8,#37,#32,#46,#39,'=','<','>'];
    ValidDigitAlphabet : Set of Char = ['0'..'9','a'..'z','A'..'Z','�'..'�','/','.',' ','-','_'];
    NumberType : Set of TFieldType = [ftCurrency, FtInteger, ftFloat, ftSmallint, ftWord, ftAutoInc, ftBCD];
    StringType : Set of TFieldType = [ftString, ftWideString, ftFixedChar];

    MonthNameArray : Array[1..12] of string = (
            '�������',            '��������',            '�����',
            '���',            '�����',            '������',
            '���',            '����',            '���',
            '��',            '����',            '�����' );

    DayNameArray : Array[1..7] of string = (
            '����',            '������',            '������',
            '�� ����',            '���� ����',            '��� ����',
            '����');

   DefaultPButtonColor = $00FF8080;
   DefaultPanelActiceColor = $00BBFFF7;
   DefaultPanelIdelColor = $00FFFFD2;

   SrlDataSetCity = 1;
   SrlDataSetDegreeU = 16;
   SrlDataSetReshtehU = 3;
   SrlDataSetDegreeH = 17;
   SrlDataSetReshtehH = 5;
   SrlDataSetGrpDegH = 18;
   SrlDataSetKEsar = 10;
   SrlDataSetOstan = 12;
   SrlDataSetHazineh = 15;
   SrlDataSetDocumWomen = 100;
   SrlDataSetPlace = 200;
   SrlDataSetAction = 202;
   SrlDataSetJens = 203;
   SrlDataSetSemat = 204;
   SrlDataSetPass = 205;
   SrlDataSetSchool = 9;
   SrlDataSetSchoolMen = 13;
   SrlDataSetSchoolWomen = 14;
   SrlDataSetSchoolSonat = 8;
   SrlDataSetSchoolList = 9202;
   SrlDataSetKService = 19;
   SrlDataSetKProperty = 20;
   SrlDataSetKSkill = 21;
   SrlDataSetKResearch = 22;
   SrlDataSetDeen = 24;
   SrlDataSetLang = 25;
   SrlDataSetCountry = 26;
   SrlDataSetMelli = 27;
   SrlDataSetMadrak = 29;
   SrlDataSetKNezam = 28;
   SrlDataSetTitle = 23;

   SrlDataSetStudent1 = 100;
   SrlDataSetStudent2 = 101;
   SrlDataSetStudent3 = 102;
   SrlDataSetStudent4 = 103;
   SrlDataSetStudent5 = 104;
   SrlDataSetStudent6 = 105;
   SrlDataSetStudentSonat = 106;
   SrlDataSetStudentActive = 107;
//   SrlDataSetStudentALL = 109;

   SrlDataSetOtherPerson = 9003;
   SrlDataSetLetter = 400;
   SrlDataSetSource = 1000;
   SrlDataSetDiscription = 9000;
   SrlDataSetDegHSonat = 904;
   SrlDataSetDprt = 907;
   SrlDataSetCycle = 910;
   SrlDataSetBook = 9095;
   SrlDataSetKJob = 9093;
   SrlDataSetKSchoolService = 912;
   SrlDataSetKPayShahr = 913;
   SrlDataSetDegSonat = 915;
   SrlDataSetPost = 9104;
   SrlDataSetTeacher = 9204;
   SrlDataSetPicture = 1;

   SrlObjectPazireshPerson = 1;
   SrlObjectPazireshStudent = 2;
   SrlObjectSonat = 3;
   SrlObjectBedehi = 450;
   SrlDataSetPerson = 9090;
   SrlDataSetKLevelDarolhefz = 9098;
   SrlDataSetKArzyabi = 9203;
   SrlDataSetKPerson = 101;

type
   T_Field = Record
      NFiled : String;
      CapField : String;
      KindField : integer;
      SizeField : integer;
      TableName : String;
   end;

type
    TState = (sView, sAdd, sEdit, sFilter);

type
    TKJens = (KjAll, kjMen, kjWomen);

type
    TKTahol = (sOne, sMarrid);

type
    TKSchool = (ksAll, ksMen, ksWomen, ksSonat);

type
    TKPer = (kpAll, kpMotmarkez, kpCommon, kpOther, kpSonat, kpNotIran);

type                  
    TKTitle = (ktAll, ktHalf, ktSali, ktCommon, ktShafahi, ktWomen,
               ktSonat1, kt38, ktCommon2, ktCommon3, ktCommon4, ktCommon5,
               ktCommon6, ktCommon7, ktCommon8, kt7_10, kt8_10,
               ktSonat2, ktSonat3, ktSonat4, ktSonat5, ktSonat6, kt7);

type
  TF_Common = class(TDataModule)
    Pub_BROWSE_Per: TADOStoredProc;
    Pub_BROWSE_Role_Field3: TADOStoredProc;
    Pub_BROWSE_Role_Field1: TADOStoredProc;
    Pub_BROWSE_Per1: TADOStoredProc;
    Pub_BROWSE_Per1srl_per1: TIntegerField;
    Pub_BROWSE_Per1srl_User: TIntegerField;
    Pub_BROWSE_Per1n_per: TStringField;
    Pub_BROWSE_Per1family: TStringField;
    Pub_BROWSE_Per1n_hazineh: TStringField;
    Pub_INSERT_System_Form_Field1: TADOStoredProc;
    sp_GetCurrentFarsiDate: TADOStoredProc;
    sp_Find_T_Table: TADOStoredProc;
    Delete1: TADOStoredProc;
    Sp_Get_Max: TADOStoredProc;
    Insert1: TADOStoredProc;
    Update1: TADOStoredProc;
    Browse1: TADOStoredProc;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TStringField;
    Browse2: TADOStoredProc;
    Browse2FSerial: TAutoIncField;
    Browse2FCode: TIntegerField;
    Browse2FName: TStringField;
    SystemConnection: TADOConnection;
    Pub_InitUser: TADOStoredProc;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField3: TIntegerField;
    StringField3: TStringField;
    INSERT_Form_Proc: TADOStoredProc;
    sp_Get_Max_T_Table2: TADOStoredProc;
    sp_Find_T_Table2: TADOStoredProc;
    Session1: TSession;
    Query1: TQuery;
    sp_FullCycleInfo: TADOStoredProc;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField4: TStringField;
    StringField5: TStringField;
    IntegerField6: TIntegerField;
    StringField6: TStringField;
    sp_Find_T_Table3: TADOStoredProc;
    SP_GetSystemVersion: TADOStoredProc;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    StringField10: TStringField;
    StringField11: TStringField;
    IntegerField12: TIntegerField;
    StringField12: TStringField;
    sp_Get_Max_T_Table3: TADOStoredProc;
    SP_GetSystemInfo: TADOStoredProc;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    StringField13: TStringField;
    StringField14: TStringField;
    IntegerField15: TIntegerField;
    StringField15: TStringField;
    Pub_BROWSE_Form_Fields: TADOStoredProc;
    Pub_BROWSE_Form_FieldsN_Field: TStringField;
    Pub_BROWSE_Form_FieldsK_Access: TIntegerField;
    sp_GetNextDate: TADOStoredProc;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    IntegerField9: TIntegerField;
    StringField9: TStringField;
    QueryConnection: TADOConnection;
    sp_SetUserWork: TADOStoredProc;
    Timer1: TTimer;
    Delete3: TADOStoredProc;
    sp_GetCurrentServerTime: TADOStoredProc;
    sp_GetSystemPermision: TADOStoredProc;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    StringField16: TStringField;
    StringField17: TStringField;
    IntegerField18: TIntegerField;
    StringField18: TStringField;
    insert_T_Letter_System: TADOStoredProc;
    Browse_Pub_Reports: TADOStoredProc;
    Browse_Pub_ReportsVal_Report: TMemoField;
    OpenPictureDialog1: TOpenPictureDialog;
    insert_T_Student_Pic_1: TADOStoredProc;
    BROWSE_Student_Pic: TADOStoredProc;
    BROWSE_Student_PicPic_Student: TBlobField;
    BROWSE_Student_PicFSerial: TAutoIncField;
    Browse_T_StudentOne: TADOStoredProc;
    Browse_T_StudentOneFSerial: TAutoIncField;
    Browse_T_StudentOneFName: TStringField;
    Browse_T_StudentOneFName1: TStringField;
    Browse_T_StudentOneFName2: TStringField;
    Browse_T_StudentOneFCode: TIntegerField;
    Browse_T_StudentOneK_Docum: TIntegerField;
    Browse_T_StudentOneK_StateDocum: TIntegerField;
    Browse_T_StudentOneNum_Per: TIntegerField;
    Browse_T_StudentOneNum_Cart: TIntegerField;
    Browse_T_StudentOneNum_Melli: TStringField;
    Browse_T_StudentOneSrl_KPer: TIntegerField;
    Browse_T_StudentOneNum_Docum: TIntegerField;
    Browse_T_StudentOneInput_Sal: TIntegerField;
    Browse_T_StudentOneShohrat: TStringField;
    Browse_T_StudentOneN_Father: TStringField;
    Browse_T_StudentOneNum_Shen: TStringField;
    Browse_T_StudentOneD_Brith: TStringField;
    Browse_T_StudentOneL_Brith: TStringField;
    Browse_T_StudentOneSrl_Sodoor: TIntegerField;
    Browse_T_StudentOneD_Sodoor: TStringField;
    Browse_T_StudentOneSrl_DegU: TIntegerField;
    Browse_T_StudentOneD_DegU: TStringField;
    Browse_T_StudentOneSrl_ReshU: TIntegerField;
    Browse_T_StudentOneSrl_DegH: TIntegerField;
    Browse_T_StudentOneSrl_ReshH: TIntegerField;
    Browse_T_StudentOneAvrg: TFloatField;
    Browse_T_StudentOneK_Maskan: TIntegerField;
    Browse_T_StudentOneK_Jens: TIntegerField;
    Browse_T_StudentOneK_Tahol: TIntegerField;
    Browse_T_StudentOneD_Marrid: TStringField;
    Browse_T_StudentOneK_Lebas: TIntegerField;
    Browse_T_StudentOneK_Moamam: TIntegerField;
    Browse_T_StudentOneD_Moamam: TStringField;
    Browse_T_StudentOneNum_Child: TIntegerField;
    Browse_T_StudentOneNum_Takafol: TIntegerField;
    Browse_T_StudentOneIs_Worker: TIntegerField;
    Browse_T_StudentOneK_Gharardad: TIntegerField;
    Browse_T_StudentOneP_Salary: TBCDField;
    Browse_T_StudentOneCurWork: TStringField;
    Browse_T_StudentOneFather_Work: TStringField;
    Browse_T_StudentOneHome_Adress: TStringField;
    Browse_T_StudentOneSrl_City: TIntegerField;
    Browse_T_StudentOneC_Post: TStringField;
    Browse_T_StudentOneHome_Tel: TStringField;
    Browse_T_StudentOneSrl_KNezam: TIntegerField;
    Browse_T_StudentOneD_Ezam: TStringField;
    Browse_T_StudentOneD_MohlatKNezam: TStringField;
    Browse_T_StudentOneWork_Adress: TStringField;
    Browse_T_StudentOneWork_Tel: TStringField;
    Browse_T_StudentOneC_Classe: TStringField;
    Browse_T_StudentOneD_Classe: TStringField;
    Browse_T_StudentOneD_StartStudy: TStringField;
    Browse_T_StudentOneD_Compelete: TStringField;
    Browse_T_StudentOneD_EndStudy: TStringField;
    Browse_T_StudentOneDis_Per: TStringField;
    Browse_T_StudentOneDay_Jebhe: TIntegerField;
    Browse_T_StudentOneDay_Esarat: TIntegerField;
    Browse_T_StudentOneDay_Zendan: TIntegerField;
    Browse_T_StudentOneIs_Basij: TIntegerField;
    Browse_T_StudentOneDay_Basij: TIntegerField;
    Browse_T_StudentOnePrc_Janbazi: TIntegerField;
    Browse_T_StudentOneExam_Mark: TFloatField;
    Browse_T_StudentOneDialog_Mark: TFloatField;
    Browse_T_StudentOneRank: TIntegerField;
    Browse_T_StudentOneC_KNezam: TIntegerField;
    Browse_T_StudentOneN_KNezam: TStringField;
    Browse_T_StudentOneC_Sodoor: TIntegerField;
    Browse_T_StudentOneN_Sodoor: TStringField;
    Browse_T_StudentOneC_DegU: TIntegerField;
    Browse_T_StudentOneN_DegU: TStringField;
    Browse_T_StudentOneC_ReshU: TIntegerField;
    Browse_T_StudentOneN_ReshU: TStringField;
    Browse_T_StudentOneC_ReshH: TIntegerField;
    Browse_T_StudentOneN_ReshH: TStringField;
    Browse_T_StudentOneC_DegH: TIntegerField;
    Browse_T_StudentOneN_DegH: TStringField;
    Browse_T_StudentOneC_City: TIntegerField;
    Browse_T_StudentOneN_City: TStringField;
    Browse_T_StudentOneC_KPer: TIntegerField;
    Browse_T_StudentOneN_KPer: TStringField;
    Browse_T_StudentOneSrl_SourceInput: TIntegerField;
    Browse_T_StudentOneC_SourceInput: TIntegerField;
    Browse_T_StudentOneN_SourceInput: TStringField;
    Browse_T_StudentOneSrl_Melli: TIntegerField;
    Browse_T_StudentOneC_Melli: TIntegerField;
    Browse_T_StudentOneN_Melli: TStringField;
    Browse_T_StudentOneSrl_Deen: TIntegerField;
    Browse_T_StudentOneC_Deen: TIntegerField;
    Browse_T_StudentOneN_Deen: TStringField;
    Browse_T_StudentOneSrl_Univer: TIntegerField;
    Browse_T_StudentOneC_Univer: TIntegerField;
    Browse_T_StudentOneN_Univer: TStringField;
    Browse_T_StudentOneSrl_School: TIntegerField;
    Browse_T_StudentOneC_School: TIntegerField;
    Browse_T_StudentOneN_School: TStringField;
    Browse_T_StudentOneSrl_KEsar: TIntegerField;
    Browse_T_StudentOneC_KEsar: TIntegerField;
    Browse_T_StudentOneN_KEsar: TStringField;
    Browse_T_StudentOneSrl_GroupShahr: TAutoIncField;
    Browse_T_StudentOneC_GroupShahr: TIntegerField;
    Browse_T_StudentOneN_GroupShahr: TStringField;
    Browse_T_StudentOneIs_Check: TIntegerField;
    Browse_T_StudentOneIs_Active: TIntegerField;
    Browse_T_StudentOneIs_EmamJam: TIntegerField;
    Browse_T_StudentOneIs_EmamJom: TIntegerField;
    Browse_T_StudentOneSrl_DprtDocum: TIntegerField;
    Browse_T_StudentOneD_EtebarResearch: TStringField;
    Browse_T_StudentOneC_DprtDocum: TIntegerField;
    Browse_T_StudentOneN_DprtDocum: TStringField;
    Browse_T_StudentOneC_Hozeh: TIntegerField;
    Browse_T_StudentOneIs_Mashmool: TIntegerField;
    Browse_T_StudentOneStrIs_Mashmool: TStringField;
    Browse_T_StudentOneIs_CheckMashmool: TIntegerField;
    Browse_T_StudentOneStrIs_CheckMashmool: TStringField;
    Browse_T_StudentOneIs_CheckResearch: TIntegerField;
    Browse_T_StudentOneStrIs_CheckResearch: TStringField;
    Browse_T_StudentOneIs_CheckPaziresh: TIntegerField;
    Browse_T_StudentOneStrIs_CheckPaziresh: TStringField;
    XPManifest1: TXPManifest;
    insert_T_Source_Pic_1: TADOStoredProc;
    Common_SP_GetSystemVersion: TADOStoredProc;
    IntegerField19: TIntegerField;
    IntegerField20: TIntegerField;
    StringField19: TStringField;
    StringField20: TStringField;
    IntegerField21: TIntegerField;
    StringField21: TStringField;
    Exam_Student_Can_Register: TADOStoredProc;
    T_K_Cycle: TADOTable;
    DST_K_Cycle: TDataSource;
    T_K_CycleFCode: TIntegerField;
    T_K_CycleFName: TStringField;
    T_K_CycleGrp_Cycle: TIntegerField;
    T_K_CycleC_KTitle: TIntegerField;
    T_K_CycleC_KJens: TIntegerField;
    T_K_CycleC_KSchool: TIntegerField;
    T_K_CycleSrl_PrintCart1: TIntegerField;
    T_K_CycleSrl_PrintCart2: TIntegerField;
    T_K_CycleSrl_PrintKarname1: TIntegerField;
    T_K_CycleSrl_PrintKarname2: TIntegerField;
    T_K_CycleSrl_User: TIntegerField;
    T_K_CycleN_Station: TStringField;
    T_K_CycleU_DateTime: TDateTimeField;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    Access1: TMenuItem;
    Excel1: TMenuItem;
    WORD1: TMenuItem;
    ADOCommand1: TADOCommand;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    ExcelConnection: TADOConnection;
    Common_SP_GetPRocedureText: TADOStoredProc;
    N10: TMenuItem;
    procedure DataModuleCreate(Sender: TObject);
    Procedure UserCanAccessField(NObject, NParent : String; var K_Access : integer);
    procedure UserCanAccessSystem;
    procedure MyFormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDBGridTitleClick(Column: TColumn);
    procedure SystemConnectionWillExecute(Connection: TADOConnection;
      var CommandText: WideString; var CursorType: TCursorType;
      var LockType: TADOLockType; var CommandType: TCommandType;
      var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
      const Command: _Command; const Recordset: _Recordset);
    procedure SystemConnectionExecuteComplete(Connection: TADOConnection;
      RecordsAffected: Integer; const Error: Error;
      var EventStatus: TEventStatus; const Command: _Command;
      const Recordset: _Recordset);
    procedure DataModuleDestroy(Sender: TObject);
    procedure SystemConnectionAfterConnect(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure Access1Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure WORD1Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
  private
    procedure FormDBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure FormCALCClick(Sender: TObject);
    procedure FormCLOSEClick(Sender: TObject);
    procedure FormFILTERClick(Sender: TObject);
    procedure FormCOLORClick(Sender: TObject);
    procedure FormPRINTClick(Sender: TObject);
    procedure EditSearchExit(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure EditSearchKeyPress(Sender: TObject; var Key: Char);
    procedure EditFilterChange(Sender: TObject);
    procedure EditFilter2Change(Sender: TObject);
    Function  GetSystemVersion : integer;
    procedure GetSystemInfo;
    procedure ReadFromIniFile;
    procedure InitUser;
    procedure InitCycle;
    procedure InitSystem;
    procedure PerImagePrint(Sender: TObject);
    procedure CompanyMarkImagePrint(Sender: TObject);
    procedure ppLabelD_CurrentGetText(Sender: TObject; var Text: String);
    procedure ppLabelNumberStrGetText(Sender: TObject; var Text: String);
    procedure ppLabelNumberValGetText(Sender: TObject; var Text: String);


    procedure MyDBGrid1Enter(Sender: TObject);
    procedure MyDBGrid1Exit(Sender: TObject);
    procedure MyDSBrowse1DataChange(Sender: TObject; Field: TField);
    procedure DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
           Field: TField; State: TGridDrawState);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1MouseDown(Sender: TObject; Button: TMouseButton;
           Shift: TShiftState; X, Y: Integer);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1KeyUp(Sender: TObject; var Key: Word;
           Shift: TShiftState);

    procedure EditSearchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CheckSystemVersion;

    { Private declarations }
  public
    { Public declarations }
  end;

procedure BackUpDB;
procedure ChangePassword;
procedure EmptyFieldList;

procedure FilterDBGrid(MyDBGrid : TDBGrid);
procedure FilterDBGrid2(MyDBGrid : TDBGrid);
procedure FilterDBGrid3(MyDBGrid : TDBGrid);

procedure FilterByEditDBGrid(MyDBGrid : TDBGrid);
procedure PrintDBGrid(MyDBGrid : TDBGrid);
procedure SearchDBGrid(MyDBGrid : TDBGrid);
Function  SumDBGrid(MyDBGrid : TDBGrid): String;
procedure DBGridSaveToFile(MyDBGrid : TDBGrid);
procedure DBGridSaveToAccess(MyDBGrid : TDBGrid);
procedure DBGridSaveToExcelFile(MyDBGrid : TDBGrid);
procedure DBGridSaveToWordFile(MyDBGrid : TDBGrid);
procedure ShowExcelFile;
procedure ReSizeDBGrid(MyDBGrid : TDBGrid);
procedure OptionDBGrid(MyDBGrid : TDBGrid);
procedure SortDBGrid1(MyColumn : TColumn);

Function  GetFarsiFilterDBGrid(MyDBGrid : TDBGrid): String;
procedure MyDBGridRecordCount(MyDBGrid : TDBGrid);


procedure ChangeColorComponent(MyComp : TComponent);
procedure ChangeEnablePanel(Sender: TObject; sw : Boolean; FocusedPanel : String;swAddOrEdit : TState);
procedure ChangeReadOnlyPanel(Sender: TObject; sw : Boolean; FocusedPanel : String;swAddOrEdit : TState);
procedure EmptyEdits(Sender: TObject; FocusedPanel : String);
procedure Fill_Tags(Sender: TObject);
procedure VisibleComponents(Sender: TObject);
//procedure EnableComponents(Sender: TObject);
//procedure ReadOnlyComponents(Sender: TObject);
procedure ChangeEnableButton(Sender: TObject; N : String; var swAddOrEdit : TState; FocusedPanel : String);
procedure AddComponentToTable(Sender: TObject);
procedure AddFormSPToDB(MySp : TADOStoredProc; IsRun : integer);
procedure ShowErrorMessage(MessageID : integer);

procedure PreviewReport(ppReport1 : TppReport);overload;
procedure PreviewReport(SrlReport : integer; MyDataSource : TDataSource; IsOneRecord, IsPreview : integer);overload;
procedure PreviewReport(SrlReport : integer; MyDataSource1, MyDataSource2, MyDataSource3 : TDataSource;
               CountRec : integer);overload;

procedure FindStudent;
procedure ShowFormOption(N_Form : String; Var FormPMainColor1, FormPMainColor2, FormPButtonColor : TColor);
procedure ShowCodingForm(FormTableName, FormCaption, FormCodeName : String);overload;
procedure ShowCodingForm(SrlDataSet : integer);overload;

function Is_DeleteRecord(SrlDataSet, SrlRecord : integer) : boolean;overload;
function Is_DeleteRecord: boolean;overload;
function Is_SaveBeforClose : integer;

procedure ShowDynamicQueryForm(SrlObject : integer);
procedure AddFieldList(FieldKind, FieldSize : integer; TableName, FieldName, FieldCaption : String);


procedure EditCodeChange(MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; MySerial2 : integer; swState : TState; TableName , MyKindFieldName : String);overload;
procedure EditCodeChange(MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; swState : TState; TableName : String); overload;
procedure EditCodeChange(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet : integer);overload ;
procedure EditCodeChange(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;

procedure EditCodeExit(swState : TState; MyEdit : TEdit; MyLabel : TLabel);

procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; MySerial2, C_A_F : integer;   SpBrowseName, MyKindFieldName : String);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; C_A_F : integer;   SpBrowseName : String);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; SrlDataSet : integer);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;
procedure EditCodeKeyPress(var Key : Char; swState : TState; MyEdit : TEdit; SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;

procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; MySerial2, C_A_F : integer; TableName, MyKindFieldName : String);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; C_A_F : integer; TableName : String);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet : integer);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,   Val_Param4 : integer);overload;
procedure SBFindClick(swState : TState; MyEdit : TEdit; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,   Val_Param4 : integer);overload;
procedure SBFindClickPer(swState : TState; MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer);

procedure EditCodeFind(var Key : Char; MyEdit : TEdit; MySerialKind, FieldCount : integer; TableName : String);

procedure NullAdoSPParameters(MyAdoSP : TADOStoredProc);
procedure BrowseRecord1(MyForm : TObject; TableName : String);
procedure BrowseRecord2(MyForm : TObject; TableName, FieldParam : String; KindParam : integer);
function  GetMaxCodeRecord(TableName, MyKindFieldName : String; MyKindFieldValue : integer) : String;overload;
function  GetMaxCodeRecord(TableName : String) : String; overload;
function  GetMaxCodeRecord(SrlDataSet : integer) : String; overload;
function  GetMaxCodeRecord(SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer) : String;overload;

procedure InsertRecord1(MyForm : TObject; TableName, MyCode, MyName : String);
procedure UpdateRecord1(TableName, MyCode, MyName : String);
procedure DeleteRecord1(MyForm : TObject; TableName : String; SrlRecord : integer; MyBrowse : TADOStoredProc);overload;
procedure DeleteRecord1(SrlDataSet, SrlRecord : integer; MyBrowse : TADOStoredProc);overload;
procedure MakeFilterString(KindField : Integer; ComboBIndex : integer; FieldName : String; FromV : String; ToV : String; var Filter : String);
function MakeFilterStringOne(KindField, ComboBIndex : integer; FieldName, FromV, ToV : String) : String;
procedure ChangeActivePanel(Sender: TObject; var FocusedPanel : String; Panel1 : String; ActicePanelColor, IdelPanelColor : TColor);
procedure AllTabOrderChange(Sender: TObject; Sw: Boolean; Parent : TWinControl);

Function GetNextDate(MyDate : String; MyDif : integer): String;
function IsCorrectDate(Date1 : String): integer;
function GetExtractStr(Str1 : String): String;
function CorrectString(Str1 : String): String;
function Number2Str(MyPrice : Currency) : String;
function Number2StrRil(MyPrice : Currency) : String;
function Number2StrDecimal(MyNumBer : real) : String;
procedure FormShowDM(MyForm : TObject; K_Form : integer; Is_Show : boolean);
function ShowFilter(var SqlFilter : String): Boolean;

procedure EditCodeKeyPress2(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; MySerial2, C_A_F : integer;
   SpBrowseName : String);

procedure SBFindClick2(swState : TState; MyEdit : TEdit;
   MyLabel : TLabel; var MySerial1 : integer; MySerial2, C_A_F : integer; SpBrowseName : String);

procedure FormShow2(MyForm, MainForm : String; K_Form : integer; Is_Show : boolean);
procedure CommonFullEdits(MyForm : TObject; MyBrowse : TADOStoredProc);

function ConvertToIranSystem1(Str : String) : String;
function ConvertFromIranSystem1(Str : String) : String;
function XConvert(tcStr: String; tcType: Char): String;
function ConvertFromIranSystem2(tcStr: String): String;
function ConvertToIranSystem2(tcStr: String): String;
procedure CalcCurrentDate;
function ServerTime: String;
function Is_Permision : Boolean;
procedure ExecSystem(MySystemName : String);
procedure InsertMyLetter(SrlTable, SrlRecord : integer);
procedure AssignPicToPer(SrlStudent, KStudent, KPicture : integer; PicStd : TImage);
procedure ViewPicPer(SrlStudent, KStudent, KPicture : integer;var PicStd : TImage);overload;
procedure ViewPicPer(SrlStudent, KStudent, KPicture : integer; PicStd : TppImage);overload;
function ShowCloseCycle: boolean;

procedure CreateDBFFile(Path1, FileName1, FileName2, DBFStruct1, DBFStruct2 : String);
procedure BrowseTStudentOne(SrlStudent : integer);
procedure InsertPicture(OldFullFileName : String; IsMoveFile : Boolean;
            MyKFile, MyKSource, MySrlSource : integer; IsShowMessage : boolean; var Result : integer);

Procedure ShowAttachPic(KSourcePic, SrlSourcePic : integer);
Procedure ShowAttachDoc(KSourceDoc, SrlSourceDoc : integer);
procedure ChangeActiveBitBtnCount(MyBitBtn : TBitBtn; RecordCount : integer);
function  Is_Student_Can_Register(KCheck, SrlStudent, SrlCycleOld, SrlCycleNew : integer) : integer;

Const
   MaxField = 100;
   SrlDataSetKCycle = 1;
var
   F_Common: TF_Common;
   SrlDataSet: Integer;
   XLApp: Variant;
   WordApp: Variant;
   K_Access : integer;
   OldSystem_Version : integer;
   NewSystem_Version : integer;
   Count_Param : integer;
   D_Expire : String;
   Qty_MaxRun : integer;
   Qty_Run : integer;

   F1 : TIniFile;

   Srl_User : integer;
   Srl_Connection : integer;

   N_UserNT : array[0..255] of char;
   N_UserNT2 : String;

   C_User : String;
   N_User : String;
   Pass_User : String;
   Msg_User : String;

   Srl_Employe : integer;
   Srl_Chart : integer;
   GlobalK_Source : integer;
   GlobalSrl_Source : integer;

   GlobalSrl_Dprt : integer;
   GlobalN_Dprt : String;
   srl_c_title:Integer;
   GlobalSrl_Person : integer;
   GlobalSrl_Student : integer;
   GlobalSrl_StudentPersonPic : integer;
   GlobalSrl_Employe : integer;
   GlobalSrl_Mosque : integer;
   GlobalSrl_Bakhsh : integer;
   GlobalSrl_School : integer;
   GlobalC_School : integer;
   GlobalC_City : integer;
   GlobalSrl_City : integer;
   GlobalSrl_Letter : integer;
   GlobalC_Letter : integer;
   GlobalK_State : integer;
   GlobalK_Book : integer;
   GlobalK_Title : TKTitle;
   GlobalN_Title : String;
   GlobalK_Register : integer;
   GlobalK_EkhtarTash : integer;
   GlobalSrl_Role : integer;
   GlobalN_Role : String;
   GlobalSrl_Title : integer;
   GlobalSrl_Object : integer;
   GlobalSrl_Action : integer;
   GlobalN_Source : String;
   GlobalC_Source : String;
   Global_KOtherPerson : integer;
   Global_SrlKSemat : integer;
   Global_KExam : integer;
   GlobalSrl_Sanad : integer;
   GlobalKPerMadrak : integer;
   GlobalIs_Mashmool : integer;

   GlobalNumberVal1 : Real;

   Run_Form_From_Main : Boolean;
   GlobalSrlTable : integer;
   GlobalSrlRecord : integer;
   GlobalNField : String;
   GlobalADOStoredProcBrowse : TADOStoredProc;


   AppPath : String;

   MyKind : integer;
   GlobalN_Student : String;
   GlobalK_Jalase : integer;
   Global_KGroupShahr : integer;
   Global_LocationSonat : integer;
   MyProject_Picture_Path : String;
   Is_RegisterSchool : boolean;

   GlobalSerial : integer;
   GlobalFormCaption : String;
   GlobalK_Form  : integer;
   GlobalFormName : String;
   GlobalDBGridName : String;
   GlobalDBGridOldName : String;
   GlobalSrl_TableCodding  : integer;
   GlobalC_TableCodding : String;
   GlobalN_TableCodding : String;
   GlobalK_StudentPersonOther  : integer;


   GlobalParaf : String;
   GlobalN_Per : String;
   GlobalF_Per : String;
   GlobalNum_Docum : String;
   GlobalN_Father : String;
   GlobalN_School : String;
   GlobalK_Per : TKPer;
   GlobalK_Jens : TKJens;
   GlobalK_School : TKSchool;
   GlobalK_Check : integer;
   GlobalK_Tahol : integer;
   GlobalK_Question : integer;
   GlobalN_DegH : String;

   SrlBaseSanad : integer;

    SrlCycleNext : integer;
    SrlKCycleNext : integer;
    SrlKCycle : integer;


   GlobalDBGrid: TDBGrid;
   GlobalDBGridOld : TDBGrid;
   GlobalColumnIndex : integer;

   Srl_System : integer;
   N_System : String;
   Cap_System : String;
   N_Server : String;
   N_Company : String;
   N_Station : String;
   N_DataBase : String;

   CheckPermission : Boolean;
   Visible_Components : boolean;
   Enable_Components : boolean;
   ReadOnly_Components : boolean;
   Add_Components : boolean;
   Add_StoredProcedure : boolean;
   IS_Save_StoredProcedure : Boolean;
   IS_System_Periodic : Boolean;
   Is_ReConnect : Boolean;
   Is_LoadBMPFiles : Boolean;
   Is_SaveDBGrid : Boolean;
   CountMsgTermUser : integer;

   FieldList : Array[1..MaxField] of T_Field;
   FieldCount : integer;
   CurDate : String;
   CurSal : Integer;
   CurTime : String;

   SrlSalJari : integer;
   CSalJari : integer;
   NSalJari : String;
   KSalJari : integer;
   ColorSalJari : TColor;
   SalJariFromDate : String;
   SalJariToDate : String;
   IsCloseSalJari : integer;

   SrlCurCycle : integer;
   CCurCycle : integer;
   NCurCycle : String;
   KCurCycle : integer;
   ColorCurCycle : TColor;
   CycleFromDate : String;
   CycleToDate : String;
   IsCloseCycle : integer;
   SystemIdelTime : TTime;


   SrlCurArs : integer;
   CCurArs : integer;
   NCurArs : String;
   srl_serial : integer;

   PButtonColor : TColor;
   PanelActiceColor : TColor;
   PanelIdelColor : TColor;
   TitleSortColor : TColor;
   TitleNotSortColor : TColor;
   DBGridSortColor : TColor;
   DBGridNotSortColor : TColor;

   PCheckBoxesColor : TColor;
   ReadOnlyTrueColor : TColor;
   ReadOnlyFalseColor : TColor;
   DefaultIsAutoNum : Boolean;
   DefaultIsAutoAdd : Boolean;

   StatusBarFont : TFont;
   DBGridTitleFont : TFont;
   DBGridDataFont : TFont;

   PrintNumberFont : TFont;
   PrintStrFont : TFont;
   PrintTitleFont : TFont;

   BetweenIndex : integer;

   FarsiFilter : TStrings;
   SelectStr : String;
   WhereStr : TStrings;
   OrderStr : String;
   BMPPath : String;
   CalcFileName : String;
   IsFullAccess : integer;

   //F_WaitAvi : TF_WaitAvi;
   MsgEndUserTime : String;

   BADDbmp,
   BEDITbmp,
   BSAVEbmp,
   BDELbmp,
   BCANCELbmp,
   BCOLORbmp,
   BFILTERbmp,
   BPRINTbmp,
   BCLOSEbmp,
   BCALCbmp : TBitMap;
   ShowNotFoundSB : Boolean;


implementation

uses U_Report, U_Filter, U_Filter2, U_Filter3, U_Find, U_Find2, U_Find3, U_BackUp,
      U_ChangePassword, U_DisplayOption, U_Coding, U_DemoForm1,
      U_DynamicReport, U_DeleteRecord,
      U_Find_Per, U_DBGrid_Option, untDm;//, U_SaveBeforClose;

{$R *.DFM}




procedure CommonFullEdits(MyForm : TObject; MyBrowse : TADOStoredProc);
var
   i : integer;
begin
   if ((Not MyBrowse.Active)  OR (MyBrowse.RecordCount = 0)) then exit;

   with TForm(MyForm) Do
   begin
      for i := 0 To MyBrowse.FieldCount - 1 do
      begin
         if FindComponent(MyBrowse.Fields[i].FieldName+'1') <> nil then
            if (FindComponent(MyBrowse.Fields[i].FieldName+'1') is TEdit) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TEdit(FindComponent(MyBrowse.Fields[i].FieldName+'1')).Text := MyBrowse.Fields[i].value
               else
                  TEdit(FindComponent(MyBrowse.Fields[i].FieldName+'1')).Text := '';
            end
            else if (FindComponent(MyBrowse.Fields[i].FieldName+'1') is TComboBox) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TComboBox(FindComponent(MyBrowse.Fields[i].FieldName+'1')).ItemIndex :=
                     MyBrowse.Fields[i].value
               else
                  TComboBox(FindComponent(MyBrowse.Fields[i].FieldName)).ItemIndex := -1;
            end
            else if (FindComponent(MyBrowse.Fields[i].FieldName+'1') is TCheckBox) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TCheckBox(FindComponent(MyBrowse.Fields[i].FieldName+'1')).Checked :=
                     MyBrowse.Fields[i].value
               else
                  TCheckBox(FindComponent(MyBrowse.Fields[i].FieldName+'1')).Checked := false;
            end
            else if (FindComponent(MyBrowse.Fields[i].FieldName) is TLabel) then
            begin
               if  Not MyBrowse.Fields[i].IsNull then
                  TLabel(FindComponent(MyBrowse.Fields[i].FieldName)).Caption :=
                     MyBrowse.Fields[i].value
               else
                  TLabel(FindComponent(MyBrowse.Fields[i].FieldName)).Caption := '..........';
            end;
      end;
   end;
end;

procedure MakeFilterString(KindField : Integer; ComboBIndex : integer; FieldName : String;
                FromV : String; ToV : String; var Filter : String);
begin
   if KindField = 1 then          //  For Numeric Data Type
   begin
      Case ComboBIndex of
         0 : Filter := Filter + ' and ' + FieldName + ' = ' + FromV;
         1 : Filter := Filter + ' and ' + FieldName + ' > ' + FromV;
         2 : Filter := Filter + ' and ' + FieldName + ' >= ' + FromV;
         3 : Filter := Filter + ' and ' + FieldName + ' between ' + FromV + ' and ' + ToV;
         4 : Filter := Filter + ' and ' + FieldName + ' < ' + FromV;
         5 : Filter := Filter + ' and ' + FieldName + ' <= ' + FromV;
         6 : Filter := Filter + ' and ' + FieldName + ' <> ' + FromV;
      end;
   end
   else if KindField = 2 then  //  For String Data Type
   begin
      Case ComboBIndex of
         0 : Filter := Filter + ' and ' + FieldName + ' = "' + FromV + '"';
         1 : Filter := Filter + ' and ' + FieldName + ' > "' + FromV + '"';
         2 : Filter := Filter + ' and ' + FieldName + ' >= "' + FromV + '"';
         3 : Filter := Filter + ' and ' + FieldName + ' between "' + FromV + '" and "' + ToV + '"';
         4 : Filter := Filter + ' and ' + FieldName + ' like "%' +  FromV + '"';
         5 : Filter := Filter + ' and ' + FieldName + ' like "%' +  FromV + '%"';
         6 : Filter := Filter + ' and ' + FieldName + ' like "' +  FromV + '%"';
         7 : Filter := Filter + ' and ' + FieldName + ' < "' + FromV + '"';
         8 : Filter := Filter + ' and ' + FieldName + ' <= "' + FromV + '"';
         9 : Filter := Filter + ' and ' + FieldName + ' <> "' + FromV + '"';
      end;
   end;

end;
//------------------------------------------------------------------------------


function MakeFilterStringOne(KindField, ComboBIndex : integer;
         FieldName, FromV, ToV : String) : String;
var
   Filter : String;
begin
   if KindField = 1 then          //  For Numeric Data Type
   begin
      Case ComboBIndex of

         0 : Filter := '[' + FieldName + ']' + ' = ' + FromV;
         1 : Filter := '[' + FieldName + ']' + ' > ' + FromV;
         2 : Filter := '[' + FieldName + ']' + ' >= ' + FromV;
         3 : Filter := '[' + FieldName + ']' + ' >= ' + FromV + ' and ' + '[' + FieldName + ']' + ' <= ' + ToV;
         4 : Filter := '[' + FieldName + ']' + ' < ' + FromV;
         5 : Filter := '[' + FieldName + ']' + ' <= ' + FromV;
         6 : Filter := '[' + FieldName + ']' + ' <> ' + FromV;
      end;
   end
   else if KindField = 2 then  //  For String Data Type
   begin
      Case ComboBIndex of
         0 : Filter := '[' + FieldName + ']' + ' = ' +  QuotedStr(FromV);
         1 : Filter := '[' + FieldName + ']' + ' > ' + QuotedStr(FromV);
         2 : Filter := '[' + FieldName + ']' + ' >= ' + QuotedStr(FromV);
         3 : Filter := '[' + FieldName + ']' + ' >= ' + QuotedStr(FromV) + ' and ' + '[' + FieldName + ']' + ' <= ' + QuotedStr(ToV);
         4 : Filter := '[' + FieldName + ']' + ' like ' + QuotedStr('%' + FromV);
         5 : Filter := '[' + FieldName + ']' + ' like ' + QuotedStr('%' + FromV + '%');
         6 : Filter := '[' + FieldName + ']' + ' like ' + QuotedStr(FromV + '%');
         7 : Filter := '[' + FieldName + ']' + ' < ' + QuotedStr(FromV);
         8 : Filter := '[' + FieldName + ']' + ' <= ' + QuotedStr(FromV);
         9 : Filter := '[' + FieldName + ']' + ' <> ' + QuotedStr(FromV);
      end;
   end;
   Result := Filter;
end;
//------------------------------------------------------------------------------


procedure TF_Common.FormPRINTClick(Sender: TObject);
begin
   if ((Sender <> nil) and (Sender.ClassType <> nil)) then
      while not (Sender.InheritsFrom(TForm)) do
         Sender := TComponent(Sender).GetParentComponent;
   with TForm(Sender) Do
      if FindComponent('DBGrid1') <> nil then
         PrintDBGrid(TDBGrid(FindComponent('DBGrid1')));
end;

procedure TF_Common.FormCOLORClick(Sender: TObject);
var
   FormPButtonColor1 : TColor;
begin
   if ((Sender <> nil) and (Sender.ClassType <> nil)) then
      while not (Sender.InheritsFrom(TForm)) do
         Sender := TComponent(Sender).GetParentComponent;
   with TForm(Sender) Do
   begin
      if FindComponent('PMain') <> nil then
      begin
         PanelActiceColor := TPanel(FindComponent('PMain')).Color;
         PanelIdelColor := PanelActiceColor;
      end
      else
      begin
         if FindComponent('PanelMaster') <> nil then
            PanelActiceColor := TPanel(FindComponent('PanelMaster')).Color;
         if FindComponent('PanelDetail') <> nil then
            PanelIdelColor := TPanel(FindComponent('PanelDetail')).Color;
      end;

      if FindComponent('PButton') <> nil then
         FormPButtonColor1 := TPanel(FindComponent('PButton')).Color;

      ShowFormOption(Name, PanelActiceColor, PanelIdelColor, FormPButtonColor1);

      if PanelActiceColor > 0 then
      begin
         if FindComponent('PMain') <> nil then
               TPanel(FindComponent('PMain')).Color := PanelActiceColor
         else
            if FindComponent('PanelMaster') <> nil then
               TPanel(FindComponent('PanelMaster')).Color := PanelActiceColor;
      end;

      if PanelIdelColor > 0 then
         if FindComponent('PanelDetail') <> nil then
            TPanel(FindComponent('PanelDetail')).Color := PanelIdelColor;

      if FindComponent('PButton') <> nil then
         TPanel(FindComponent('PButton')).Color := FormPButtonColor1;
   end;
end;

procedure TF_Common.FormCALCClick(Sender: TObject);
begin
   WinExec(PChar(CalcFileName), 2)
end;


procedure TF_Common.FormCLOSEClick(Sender: TObject);
begin
   if ((Sender <> nil) and (Sender.ClassType <> nil)) then
      while not (Sender.InheritsFrom(TForm)) do
         Sender := TComponent(Sender).GetParentComponent;
   with TForm(Sender) Do
      Close;
end;

procedure TF_Common.FormFILTERClick(Sender: TObject);
begin
   if ((Sender <> nil) and (Sender.ClassType <> nil)) then
      while not (Sender.InheritsFrom(TForm)) do
         Sender := TComponent(Sender).GetParentComponent;
   with TForm(Sender) Do
      if FindComponent('DBGrid1') <> nil then
         FilterDBGrid3(TDBGrid(FindComponent('DBGrid1')));
end;

procedure TF_Common.MyFormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   KeyDownPrc : procedure (Sender: TObject; var Key: Word; Shift: TShiftState);
   SpBtnClick : procedure (Sender: TObject);
begin
   if (Sender = nil) OR
      (Sender.ClassType = nil) OR
      (not (Sender.InheritsFrom(TForm))) then
      exit;

{
    if (key=VK_RETURN) and (Shift=[ssshift]) then
    begin
        Key:=0;
        GoNext(1);
    end;
    if (key=VK_RETURN) and (Shift=[]) then
    begin
        Key:=0;
        GoNext(0);
    end;
}
   if key = vk_Return then
   begin
      Key := 0;
      TForm(Sender).Perform(7388420, vk_Tab, 0);
   end;

   if Key = VK_F4 then
      if TForm(Sender).FindComponent('BADD') <> nil then
      begin
         Key := 0;
         if TSpeedButton(TForm(Sender).FindComponent('BADD')).Enabled = true then
         begin
            SpBtnClick := TForm(Sender).MethodAddress('BADDClick');
            if @SpBtnClick <> nil then
               SpBtnClick(Sender);
         end;
      end;

   if Key = VK_F5 then
      if TForm(Sender).FindComponent('bEdit') <> nil then
      begin
         Key := 0;
         if TSpeedButton(TForm(Sender).FindComponent('bEdit')).Enabled = true then
         begin
            SpBtnClick := TForm(Sender).MethodAddress('bEditClick');
            if @SpBtnClick <> nil then
               SpBtnClick(Sender);
         end;
      end;

   if Key = VK_F2 then
      if TForm(Sender).FindComponent('bSave') <> nil then
      begin
         Key := 0;
         if TSpeedButton(TForm(Sender).FindComponent('bSave')).Enabled = true then
         begin
            SpBtnClick := TForm(Sender).MethodAddress('bSaveClick');
            if @SpBtnClick <> nil then
               SpBtnClick(Sender);
         end;
      end;

   if Key = VK_F8 then
      if TForm(Sender).FindComponent('bDel') <> nil then
      begin
         Key := 0;
         if TSpeedButton(TForm(Sender).FindComponent('bDel')).Enabled = true then
         begin
            SpBtnClick := TForm(Sender).MethodAddress('bDelClick');
            if @SpBtnClick <> nil then
               SpBtnClick(Sender);
         end;
      end;

   if Key = VK_ESCAPE then
   begin
      if TForm(Sender).FindComponent('BCANCEL') <> nil then
      begin
         Key := 0;
         if TSpeedButton(TForm(Sender).FindComponent('BCANCEL')).Enabled = true then
         begin
            SpBtnClick := TForm(Sender).MethodAddress('BCANCELClick');
            if @SpBtnClick <> nil then
               SpBtnClick(Sender);
         end;
      end

      else if TForm(Sender).FindComponent('SearchEdit') <> nil then
         TForm(Sender).FindComponent('SearchEdit').Free;
   end;

   if Key = VK_F9 then
      if TForm(Sender).FindComponent('BFILTER') <> nil then
      begin
         Key := 0;
         if TSpeedButton(TForm(Sender).FindComponent('BFILTER')).Enabled = true then
            if @TSpeedButton(TForm(Sender).FindComponent('BFILTER')).OnClick <> nil then
               TSpeedButton(TForm(Sender).FindComponent('BFILTER')).OnClick(Sender);
      end;

   if (Shift = [ssCtrl]) and (Key = 96) then
      if TForm(Sender).FindComponent('AutoNum') <> nil then
         TCheckBox(TForm(Sender).FindComponent('AutoNum')).Checked :=
            not TCheckBox(TForm(Sender).FindComponent('AutoNum')).Checked;

   if (Shift = [ssCtrl]) and (Key = 97) then
      if TForm(Sender).FindComponent('AutoAdd') <> nil then
         TCheckBox(TForm(Sender).FindComponent('AutoAdd')).Checked :=
            not TCheckBox(TForm(Sender).FindComponent('AutoAdd')).Checked;

end;

procedure EmptyFieldList;
var
   i : integer;
begin
   for i := 1 to MaxField do
   begin
      FieldList[i].CapField := '';
      FieldList[i].NFiled := '';
      FieldList[i].KindField := 0;
      FieldList[i].SizeField := 0;
      FieldList[i].TableName := '';
   end;
   FieldCount := 0;
end;

procedure AddFieldList(FieldKind, FieldSize : integer;
               TableName, FieldName, FieldCaption : String);
begin
   FieldCount := FieldCount + 1;
   FieldList[FieldCount].KindField := FieldKind;
   FieldList[FieldCount].SizeField := FieldSize;
   FieldList[FieldCount].TableName := TableName;
   FieldList[FieldCount].NFiled := FieldName;
   FieldList[FieldCount].CapField := FieldCaption;
end;

//  ��� ���������� ����� �� �� ����
function GetExtractStr(Str1 : String): String;
Var
   RStr : String;
begin
   RStr := Str1;

   while  Pos(char(32), RStr) > 0  do
      RStr := Copy(RStr, 1,                     Pos(char(32), RStr) - 1)
            + Copy(RStr, Pos(char(32), RStr)+1, Length(RStr) - Pos(char(253), RStr));

   while  Pos(char(253), RStr) > 0  do
      RStr := Copy(RStr, 1,                      Pos(char(253), RStr) - 1)
            + Copy(RStr, Pos(char(253), RStr)+1, Length(RStr)- Pos(char(253), RStr));

   while  Pos(char(254), RStr) > 0  do
      RStr := Copy(RStr, 1,                      Pos(char(254), RStr) - 1)
            + Copy(RStr, Pos(char(254), RStr)+1, Length(RStr)- Pos(char(254), RStr));

   while  Pos(char(157), RStr) > 0  do
      RStr := Copy(RStr, 1,                      Pos(char(157), RStr) - 1)
            + Copy(RStr, Pos(char(157), RStr)+1, Length(RStr)- Pos(char(157), RStr));

   while  Pos(char(240), RStr) > 0  do
      RStr := Copy(RStr, 1,                      Pos(char(240), RStr) - 1)
            + Copy(RStr, Pos(char(240), RStr)+1, Length(RStr)- Pos(char(240), RStr));

   Result := RStr;
end;


function CorrectString(Str1 : String): String;
Var
   RStr : String;
begin
   RStr := Trim(Str1);
   Result := RStr;
end;

function Number2Str(MyPrice : Currency) : String;
const
   Main: array[1..3,0..9]of string =
      (('', '���� �', '����� �', '���� �', '������ �', '����� �', '���� �', '����� �', '����� �', '���� �'),
       ('', '', '���� �', '�� �', '��� �', '����� �', '��� �', '����� �', '����� �', '��� �'),
       ('', '��', '���', '��', '����', '���', '��', '���', '���', '��'));
   NPos: array[1..4]of string =
      ('������� �', '������ �', '���� �', '');
   _10_19: array[10..19]of string =
      ('��', '�����', '������', '�����', '������', '������', '������', '����', '����', '�����');
var
   TMP2, TMP: string;
   Cntr2, Cntr: Integer;
   Nm : String;
begin
   Nm := CurrToStr(MyPrice);
   case Length(Nm) mod 3 of
      1 : TMP := '00';
      2 : TMP := '0';
   end;
   Nm := TMP + Nm;
   TMP := '';

   if Length(Nm) <=3 then
   begin
      for Cntr := 1 to 3 do
         if (Cntr = 2) and (Nm[Cntr] = '1') then
         begin
            TMP := TMP + _10_19[StrToInt(Copy(Nm, 2, 2))] + ' ';
            Break;
         end
         else
            TMP := TMP + Main[Cntr, StrToInt(Nm[Cntr])] + ' ';
         TMP := Trim(TMP);
         Cntr := Length(TMP);
         if TMP <> '' then
            if TMP[Cntr] = '�' then
               Delete(TMP, Cntr, 1);
         Result := TMP + ' ';
         Exit;
   end;
   Cntr2 := 0;
   for Cntr := (4 - (Length(Nm) div 3)) + 1 to 4 do
   begin
      TMP2 := Number2Str(StrToCurr(Copy(Nm, (Cntr2 * 3) + 1, 3)));
      if Trim(TMP2) <> '' then
         TMP := TMP + TMP2 + NPos[Cntr] + ' ';
      Inc(Cntr2);
   end;
   TMP := Trim(TMP);
   Cntr := Length(TMP);
   if TMP <> '' then
      if TMP[Cntr] = '�' then
         Delete(TMP, Cntr, 1);

   Result := TMP;
end;

function Number2StrRil(MyPrice : Currency) : String;
begin
   Result := Number2Str(MyPrice) + ' ����';
end;

function Number2StrDecimal(MyNumber : real) : String;
var
   a : real;
   b : real;
   c : integer;
begin
   if MyNumber = 0 then
      Result := '���'
   else
   begin
      a := MyNumber * 100;
      c := Round(a) Div 100;
      b := Round(a) Mod 100;
      if b > 0 then
      begin
         if ((Round(a) Mod 100) Mod 10 = 0) then
         begin
            b := (Round(a) Mod 100) Div 10;
            Result := Number2Str(c) + ' � ' + Number2Str(b) + ' ���'
         end
         else
            Result := Number2Str(c) + ' � ' + Number2Str(b) + ' ���'
      end
      else
         Result := Number2Str(c);
   end;
end;


procedure FormShowDM(MyForm : TObject; K_Form : integer; Is_Show : boolean);
var
   swAddOrEdit : TState;
   i, j : integer;
   FormName, FormCaption : String;
   FormNew : TForm;
   DBGridNew : TDBGrid;
   GlobalFormNameOld, GlobalFormCaptionOld : String;
   BaseDBGridColumnFileName : String;
   SecondDBGridColumnFileName : String;
begin
   GlobalFormNameOld := GlobalFormName;
   GlobalFormCaptionOld := GlobalFormCaption;

{   if NewSystem_Version <> F_Common.GetSystemVersion then
   begin
      WinExec(PChar(MsgEndUserTime), 1);
      Application.Terminate;
      exit;
   end;
   }

   if (MyForm = nil) OR
      (MyForm.ClassType = nil) OR
      (not (MyForm.InheritsFrom(TForm))) then
      exit;

   FormNew := TForm(MyForm);
   GlobalFormName := FormNew.Name;

   //   FormNew.WindowState := wsNormal;

   if Add_Components then AddComponentToTable(MyForm);

 {  if CheckPermission then
      With F_Common.Pub_BROWSE_Form_Fields Do
      begin
         Close;
         Parameters.ParamByName('@Srl_System').value := Srl_System;
         Parameters.ParamByName('@N_Form').value := GlobalFormName;
         Parameters.ParamByName('@Srl_User').value := Srl_User;
         Open;
      end;    }

   Fill_Tags(MyForm);
   if Visible_Components then VisibleComponents(MyForm);


   if K_Form = 1 then
   begin
      ChangeEnableButton(MyForm, 'bCancel', swAddOrEdit, '');
   end
   else
      if K_Form = 2 then
      begin
         ChangeEnableButton(MyForm, 'bFilter', swAddOrEdit, '');
         ShowNotFoundSB := False;
      end;

   with FormNew Do
   begin
      KeyPreview := True;
      FormName := Name;
      FormCaption := Caption;

      if @OnKeyDown = nil then
         OnKeyDown := F_Common.MyFormKeyDown;

      if FormName = 'F_Main' then
         ShowNotFoundSB := False;

      i := 0;
      While i < FormNew.ComponentCount Do
      begin
         if (FormNew.Components[i] is TADOStoredProc) then
         begin
            TADOStoredProc(FormNew.Components[i]).Close;
            if FormName = 'F_DynamicReport' then
               TADOStoredProc(FormNew.Components[i]).Connection := F_Common.QueryConnection
            else
               TADOStoredProc(FormNew.Components[i]).Connection := F_Common.SystemConnection;

            if Add_StoredProcedure then
               AddFormSPToDB(TADOStoredProc(Components[i]), 0);
         end;
         i := i + 1;
      end;

      For i := 0 to FormNew.ComponentCount - 1 do
      begin
//         ShowMessage(Components[i].Name);

         if (FormNew.Components[i] is TStatusBar) then
         begin
            for j := TStatusBar(FormNew.Components[i]).Panels.Count to 4 do
                TStatusBar(FormNew.Components[i]).Panels.Add;

            TStatusBar(FormNew.Components[i]).UseSystemFont := True;
            if StatusBarFont <> nil then
               TStatusBar(FormNew.Components[i]).Font := StatusBarFont;
            TStatusBar(FormNew.Components[i]).Height := 25;
            TStatusBar(FormNew.Components[i]).Panels[0].Width := 80;
            TStatusBar(FormNew.Components[i]).Panels[1].Width := 110;
            TStatusBar(FormNew.Components[i]).Panels[2].Width := 125;
            TStatusBar(FormNew.Components[i]).Panels[3].Width := 125;
            TStatusBar(FormNew.Components[i]).Panels[0].Text := CurDate;
            TStatusBar(FormNew.Components[i]).Panels[1].Text := N_User;
            TStatusBar(FormNew.Components[i]).Panels[0].Alignment := taCenter;
            TStatusBar(FormNew.Components[i]).Panels[1].Alignment := taCenter;
            TStatusBar(FormNew.Components[i]).Panels[2].Alignment := taCenter;
            TStatusBar(FormNew.Components[i]).Panels[3].Alignment := taCenter;
//            Break;
         end;

         if (FormNew.Components[i] is TADODataSet) then
         begin
            TADODataSet(FormNew.Components[i]).Close;

            if FormName = 'F_DynamicReport' then
               TADODataSet(FormNew.Components[i]).Connection := F_Common.QueryConnection
            else
               TADODataSet(FormNew.Components[i]).Connection := F_Common.SystemConnection;
         end;

         if (FormNew.Components[i] is TComboBox) then
            TComboBox(FormNew.Components[i]).Style := csOwnerDrawFixed;

         if (FormNew.Components[i] is TDBGrid) then
         begin
            DBGridNew := TDBGrid(FormNew.Components[i]);
            DBGridNew.ParentColor := True;
            DBGridNew.ReadOnly := True;
            for j := 0 to DBGridNew.Columns.Count - 1 do
            begin
               DBGridNew.Columns[j].Title.Alignment := taCenter;
               DBGridNew.Columns[j].Title.Color := TitleNotSortColor;
               if DBGridTitleFont <> nil then
                  if DBGridNew.Columns[j].Title.Caption <> '' then
                     DBGridNew.Columns[j].Title.Font := DBGridTitleFont;

               if DBGridDataFont <> nil then
                  if DBGridNew.Columns[j].Title.Caption <> '' then
                     DBGridNew.Columns[j].Font := DBGridDataFont;

            end;
            DBGridNew.Tag := 0;
            DBGridNew.OnTitleClick := F_Common.FormDBGridTitleClick;
            DBGridNew.OnKeyDown := F_Common.FormDBGridKeyDown;
            DBGridNew.ShowHint := FALSE;
            DBGridNew.PopupMenu := F_Common.PopupMenu1;
            DBGridNew.OnEnter := F_Common.MyDBGrid1Enter;
            DBGridNew.OnExit := F_Common.MyDBGrid1Exit;

            if Is_SaveDBGrid then
            begin
               BaseDBGridColumnFileName := 'C:\Temp\' + N_System + '\' + GlobalFormName + DBGridNew.Name + '0' + GlobalFormCaption;
               SecondDBGridColumnFileName := 'C:\Temp\' + N_System + '\' + GlobalFormName + DBGridNew.Name + '1' + GlobalFormCaption;

               if FileExists(PChar(SecondDBGridColumnFileName)) then
                  DBGridNew.Columns.LoadFromFile(PChar(SecondDBGridColumnFileName));
            end;
         end;
      end; {ComponentCount}

      if FindComponent('PCheckBoxes') <> nil then
         TPanel(FindComponent('PCheckBoxes')).Color := PCheckBoxesColor;
      if FindComponent('PCheckBoxes2') <> nil then
         TPanel(FindComponent('PCheckBoxes2')).Color := PCheckBoxesColor;
      if FindComponent('PCheckBoxes3') <> nil then
         TPanel(FindComponent('PCheckBoxes3')).Color := PCheckBoxesColor;


      if FindComponent('BADD') <> nil then
         with TSpeedButton(FindComponent('BADD')) do
         begin
            Top := 5;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BADDbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '����� ����� ���� : F4';
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BADD');

      if FindComponent('BEDIT') <> nil then
         with TSpeedButton(FindComponent('BEDIT')) do
         begin
            Top := 45;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BEDITbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '������ ����� ���� : F5';
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BEDIT');

      if FindComponent('BSAVE') <> nil then
         with TSpeedButton(FindComponent('BSAVE')) do
         begin
            Top := 85;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BSAVEbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '����� ������� : F2';
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BSAVE');

      if FindComponent('BDEL') <> nil then
         with TSpeedButton(FindComponent('BDEL')) do
         begin
            Top := 125;
            Left := 4;
            Width := 40;
            Height := 40;
            Glyph := BDELbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '��� ����� ���� : F8';
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BDEL');

      if FindComponent('BCANCEL') <> nil then
         with TSpeedButton(FindComponent('BCANCEL')) do
         begin
            Top := 165;
            Left := 4;
            Width := 40;
            Height := 40;
            Glyph := BCANCELbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '������ : Esc';
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BCANCEL');

      if FindComponent('BCOLOR') <> nil then
         with TSpeedButton(FindComponent('BCOLOR')) do
         begin
            Top := 205;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BCOLORbmp;
            NumGlyphs := 1;
            ShowHint := True;
            Hint := '����� ����� ���';
            if @OnClick = nil then
               OnClick := F_Common.FormCOLORClick;
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BCOLOR');

      if FindComponent('BFILTER') <> nil then
         with TSpeedButton(FindComponent('BFILTER')) do
         begin
            Top := 245;
            Left := 4;
            Width := 40;
            Height := 40;
            Glyph := BFILTERbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '����� : F9';
            if @OnClick = nil then
               OnClick := F_Common.FormFILTERClick;
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BFILTER');

      if FindComponent('BPRINT') <> nil then
         with TSpeedButton(FindComponent('BPRINT')) do
         begin
            Top := 285;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BPRINTbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '�ǁ ����';
            if @OnClick = nil then
               OnClick := F_Common.FormPrintClick;
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BPRINT');

      if FindComponent('BCLOSE') <> nil then
         with TSpeedButton(FindComponent('BCLOSE')) do
         begin
            Top := 325;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BCLOSEbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '���� : Alt + F4';
            OnClick := F_Common.FormCLOSEClick;
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BCLOSE');

      if FindComponent('BCALC') <> nil then
         with TSpeedButton(FindComponent('BCALC')) do
         begin
            Top := 365;
            Left := 4;
            Width := 40;
            Height := 40;
            if Is_LoadBMPFiles then Glyph := BCALCbmp;
            NumGlyphs := 2;
            ShowHint := True;
            Hint := '����� ����';
            OnClick := F_Common.FormCALCClick;
         end
      else
         if ShowNotFoundSB then
            ShowMessage('BCALC');

      if (IS_System_Periodic and (ColorCurCycle > 0))then
         PButtonColor := ColorCurCycle
      else
      begin
         PButtonColor := TColor(F1.ReadInteger(N_System, FormName + 'PButtonColor', DefaultPButtonColor));
         if PButtonColor <= 0 then
            PButtonColor := DefaultPButtonColor;
      end;

      if FindComponent('PButton') <> nil then
         TPanel(FindComponent('PButton')).Color := PButtonColor;

      PanelActiceColor := TColor(F1.ReadInteger(N_System, FormName + 'PMainColor1', DefaultPanelActiceColor));
      if PanelActiceColor <= 0 then
         PanelActiceColor := DefaultPanelActiceColor;

      if FindComponent('PMain') <> nil then
      begin
         TPanel(FindComponent('PMain')).Color := PanelActiceColor;
//         FocusedPanel := 'PMain';
      end
      else
      begin
         if FindComponent('PanelMaster') <> nil then
            TPanel(FindComponent('PanelMaster')).Color := PanelActiceColor;
//         FocusedPanel := 'PanelMaster';
      end;

      PanelIdelColor := TColor(F1.ReadInteger(N_System, FormName + 'PMainColor2', DefaultPanelIdelColor));
      if PanelIdelColor <= 0 then
         PanelIdelColor := DefaultPanelIdelColor;

      if FindComponent('PanelDetail') <> nil then
         TPanel(FindComponent('PanelDetail')).Color := PanelIdelColor;

      if FindComponent('AutoNum') <> nil then
            TCheckBox(FindComponent('AutoNum')).Checked := F1.ReadBool(N_System, FormName + 'AutoNum', DefaultIsAutoNum);

      if FindComponent('AutoAdd') <> nil then
         TCheckBox(FindComponent('AutoAdd')).Checked := F1.ReadBool(N_System, FormName + 'AutoAdd', DefaultIsAutoAdd);

     if Is_Show then
      begin
         Caption := Caption + ' // '+ NCurCycle;

         BorderStyle := bsSizeable;
         BorderIcons := [biSystemMenu,biMinimize,biMaximize];

         Showmodal;

         if FindComponent('AutoNum') <> nil then
            F1.WriteBool(N_System, FormName + 'AutoNum', TCheckBox(FindComponent('AutoNum')).Checked);
         if FindComponent('AutoAdd') <> nil then
            F1.WriteBool(N_System, FormName + 'AutoAdd', TCheckBox(FindComponent('AutoAdd')).Checked);

         if Is_SaveDBGrid then
            For i := 0 to ComponentCount - 1 do
               if (FormNew.Components[i] is TDBGrid) then
               begin
                  DBGridNew := TDBGrid(FormNew.Components[i]);

                  BaseDBGridColumnFileName := 'C:\Temp\' + N_System + '\' + GlobalFormName + DBGridNew.Name + '0' + GlobalFormCaption;
                  SecondDBGridColumnFileName := 'C:\Temp\' + N_System + '\' + GlobalFormName + DBGridNew.Name + '1' + GlobalFormCaption;

                  if not DirectoryExists('c:\temp\' + N_System)  then
                      if not ForceDirectories('C:\temp\' + N_System)  then
                         raise Exception.Create('Cannot create c:\temp' + N_System);

                  if DBGridNew.Tag <> -1 then
                  begin
                     if Not FileExists(PChar(BaseDBGridColumnFileName)) then
                        DBGridNew.Columns.SaveToFile(PChar(BaseDBGridColumnFileName));

                     DBGridNew.Columns.SaveToFile(PChar(SecondDBGridColumnFileName));
                  end
                  else
                  begin
                     if FileExists(PChar(BaseDBGridColumnFileName)) then
                        DeleteFile(PChar(BaseDBGridColumnFileName));

                     if FileExists(PChar(SecondDBGridColumnFileName)) then
                        DeleteFile(PChar(SecondDBGridColumnFileName));
                  end;

               end;

         Free;
      end;
   end;
   GlobalFormName := GlobalFormNameOld;
   GlobalFormCaption := GlobalFormCaptionOld;
end;



procedure TF_Common.ReadFromIniFile;
begin
   if  not DirectoryExists('c:\temp')  then
       if  not  CreateDir('C:\temp')  then
          raise  Exception.Create('Cannot create c:\temp');

   F1 := TIniFile.Create('C:\temp\SystemOption.ini');
   if F1 <> nil then
   begin
      if Srl_System = -1 then
         N_DataBase := 'Permision';

      if F1.ValueExists('Option', 'ReadOnlyTrueColor') then
         ReadOnlyTrueColor := F1.ReadInteger('Option', 'ReadOnlyTrueColor', clYellow)
      else
         F1.WriteInteger('Option', 'ReadOnlyTrueColor', clMenu);

      if F1.ValueExists('Option', 'ReadOnlyFalseColor') then
         ReadOnlyFalseColor := F1.ReadInteger('Option', 'ReadOnlyFalseColor', clWhite)
      else
         F1.WriteInteger('Option', 'ReadOnlyFalseColor', clWhite);

      if F1.ValueExists('Option', 'SortColor') then
         TitleSortColor := F1.ReadInteger('Option', 'SortColor', $0080FF00)
      else
         F1.WriteInteger('Option', 'SortColor', $0080FF00);

      if F1.ValueExists('Option', 'NotSortColor') then
         TitleNotSortColor := F1.ReadInteger('Option', 'NotSortColor', $00FFFF80)
      else
         F1.WriteInteger('Option', 'NotSortColor', $00FFFF80);

      DBGridNotSortColor := $00D9D8D7;
      DBGridSortColor := TitleSortColor;

      if F1.ValueExists('Option', 'PCheckBoxesColor') then
         PCheckBoxesColor := F1.ReadInteger('Option', 'PCheckBoxesColor', $00FF80FF)
      else
         F1.WriteInteger('Option', 'PCheckBoxesColor', $00FF80FF);
   end
   else
   begin
      ShowMessage('�����  0002');
      Application.Terminate;
   end;
end;

procedure TF_Common.DataModuleCreate(Sender: TObject);
var
   i : integer;
   ConStr, s,s1 : String;
begin
  s := GetCurrentDir + '\' + 'Query.udl';
  s1 := GetCurrentDir + '\' + 'path.udl';
  QueryConnection.Provider := s;
  QueryConnection.ConnectionString := 'FILE NAME=' + s;
  QueryConnection.Open;

  SystemConnection.Provider := s1;
  SystemConnection.ConnectionString := 'FILE NAME=' + s1;
  SystemConnection.Open;
    {  try
         with SystemConnection do
         begin
            Close;
            LoginPrompt := False;
            ConStr := 'Provider=SQLOLEDB.1;User ID=Sa;Initial Catalog='
            + N_DataBase + ';Data Source=' + N_Server;

            ConnectionString := ConStr;
            Open('Sa', '7373310');
            DefaultDatabase := N_DataBase;
         end;
      except
         ShowMessage('����� �� ������ �� ������� ����  0001');
         Application.Terminate;
      end;          }

   Add_Components := False;
   Add_StoredProcedure := False;
   Visible_Components := false;
   Enable_Components := false;
   ReadOnly_Components :=false;
   ShowNotFoundSB := False;
   Is_LoadBMPFiles := false;
   Is_SaveDBGrid := TRUE;
   Is_ReConnect := false;

  // RegisterAllFormClasses;
   ReadFromIniFile;
//   N_DataBase := 'Shora';

   N_Server := '.';

   BMPPath := '';
//   MsgEndUserTime := '\\hozeh-s\serverFiles$\ExecSys\Msg_Version.exe';
   DefaultIsAutoNum := false;

   if win32platform = ver_platform_win32_nt then
      loadkeyboardlayout('00000429',KLF_ACTIVATE)
   else
      loadkeyboardlayout('00000401',KLF_ACTIVATE);

   if Is_LoadBMPFiles Then
      BMPPath := '\\Hozeh-S\ServerFiles$\BMPFiles';

   if Is_ReConnect then
   begin
      N_Server := 'Hozeh-S\Server';
      BMPPath := '\\Hozeh-S\ServerFiles$\BMPFiles';
      CalcFileName := '\\Hozeh-S\ServerFiles$\Calculator\mycalc.exe';

{
      if CheckPermission then
      begin
         PermisionConnection.Close;
         PermisionConnection.LoginPrompt := False;
         ConStr := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=Sa;Initial Catalog=Permision;Data Source=' + N_Server
          + ';Locale Identifier=1025;Connect Timeout=15;Use Procedure for Prepare=1' +
            ';Auto Translate=True;Packet Size=4096';

         PermisionConnection.ConnectionString := ConStr;
         PermisionConnection.Open('Sa', '7373310');
      end;
}
   end;

   CheckPermission := Is_Permision;
   CheckPermission := false;
   CalcCurrentDate;
   InitSystem;
   InitUser;
   InitCycle;
   ServerTime;

   BetweenIndex := 3;

   StatusBarFont := TFont.Create;
   StatusBarFont.Charset := DEFAULT_CHARSET;
   StatusBarFont.Color := clWindowText;
   StatusBarFont.Height := -13;
   StatusBarFont.Name := 'B Nazanin';
   StatusBarFont.Style := [fsBold];
   StatusBarFont.Size := 12;

   DBGridTitleFont := TFont.Create;
   DBGridTitleFont.Charset := DEFAULT_CHARSET;
   DBGridTitleFont.Color := clWindowText;
   DBGridTitleFont.Height := -13;
   DBGridTitleFont.Name := 'Tahoma';
   DBGridTitleFont.Style := [];
   DBGridTitleFont.Size := 8;

   DBGridDataFont := TFont.Create;
   DBGridDataFont.Charset := DEFAULT_CHARSET;
   DBGridDataFont.Color := clWindowText;
   DBGridDataFont.Height := -13;
   DBGridDataFont.Name := 'Tahoma';
   DBGridDataFont.Style := [];
   DBGridDataFont.Size := 10;

   PrintTitleFont := TFont.Create;
   PrintTitleFont.Charset := DEFAULT_CHARSET;
   PrintTitleFont.Color := clWindowText;
   PrintTitleFont.Height := -13;
   PrintTitleFont.Name := 'b mitra';
   PrintTitleFont.Style := [fsBold];
   PrintTitleFont.Size := 10;

   PrintNumberFont := TFont.Create;
   PrintNumberFont.Charset := DEFAULT_CHARSET;
   PrintNumberFont.Color := clWindowText;
   PrintNumberFont.Height := -13;
   PrintNumberFont.Name := 'B Nazanin';
   PrintNumberFont.Style := [];
   PrintNumberFont.Size := 14;

   PrintStrFont := TFont.Create;
   PrintStrFont.Charset := DEFAULT_CHARSET;
   PrintStrFont.Color := clWindowText;
   PrintStrFont.Height := -13;
   PrintStrFont.Name := 'b mitra';
   PrintStrFont.Style := [];
   PrintStrFont.Size := 10;

   FieldCount := 0;

   if Is_LoadBMPFiles then
   begin
      BADDbmp := TBitMap.Create;

      BADDbmp.LoadFromFile(BMPPath + '\BADD.bmp');

      BEDITbmp := TBitMap.Create;
      BEDITbmp.LoadFromFile(BMPPath + '\BEDIT.bmp');

      BSAVEbmp := TBitMap.Create;
      BSAVEbmp.LoadFromFile(BMPPath + '\BSAVE.bmp');

      BDELbmp := TBitMap.Create;
      BDELbmp.LoadFromFile(BMPPath + '\BDEL.bmp');

      BCANCELbmp := TBitMap.Create;
      BCANCELbmp.LoadFromFile(BMPPath + '\BCANCEL.bmp');

      BCOLORbmp := TBitMap.Create;
      BCOLORbmp.LoadFromFile(BMPPath + '\BCOLOR.bmp');

      BFILTERbmp := TBitMap.Create;
      BFILTERbmp.LoadFromFile(BMPPath + '\BFILTER.bmp');

      BPRINTbmp := TBitMap.Create;
      BPRINTbmp.LoadFromFile(BMPPath + '\BPRINT.bmp');

      BCLOSEbmp := TBitMap.Create;
      BCLOSEbmp.LoadFromFile(BMPPath + '\BCLOSE.bmp');

      BCALCbmp := TBitMap.Create;
      BCALCbmp.LoadFromFile(BMPPath + '\BCALC.bmp');
   end;
end;

procedure ChangeColorComponent(MyComp : TComponent);
var
   CompTag : integer;
begin
   if NOT(MyComp is TWinControl) then
      exit;

   CompTag := 1;
   if TWinControl(MyComp).Visible then CompTag := CompTag * 2;
   if TWinControl(MyComp).Enabled then CompTag := CompTag * 3;

   if (MyComp is TEdit) then if TEdit(MyComp).ReadOnly then CompTag := CompTag * 5;
   if (MyComp is TMaskEdit) then if TMaskEdit(MyComp).ReadOnly then CompTag := CompTag * 5;
   if (MyComp is TMemo)     then if TMemo(MyComp).ReadOnly then CompTag := CompTag * 5;

   if (CompTag MOD 2 = 0)
      AND (CompTag MOD 3 = 0)
      AND (CompTag MOD 5 <> 0) Then
   begin
      if (MyComp is TEdit)          then TEdit(MyComp).Color := ReadOnlyFalseColor
      else if (MyComp is TMaskEdit) then TMaskEdit(MyComp).Color := ReadOnlyFalseColor
      else if (MyComp is TComboBox) then TComboBox(MyComp).Color := ReadOnlyFalseColor
      else if (MyComp is TMemo)     then TMemo(MyComp).Color := ReadOnlyFalseColor;
   end
   else
   begin
      if (MyComp is TEdit)          then TEdit(MyComp).ParentColor := True
      else if (MyComp is TMaskEdit) then TMaskEdit(MyComp).ParentColor := True
      else if (MyComp is TComboBox) then TComboBox(MyComp).ParentColor := True
      else if (MyComp is TMemo)     then TMemo(MyComp).ParentColor := True;
   end
end;

procedure ChangeEnablePanel(Sender: TObject; sw : Boolean; FocusedPanel : String;swAddOrEdit : TState);
Var
   i : integer;
   EnableComp : boolean;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   For i := 0 to ComponentCount - 1 do
   if (Components[i].GetParentComponent <> nil) then
   begin
      EnableComp := sw;

      if swAddOrEdit = sAdd then
         if (TControl(Components[i]).Tag Mod 3 <> 0 ) then
            EnableComp := False;

      if swAddOrEdit = sEdit then
         if (TControl(Components[i]).Tag Mod 11 <> 0 ) then
            EnableComp := False;

      if (Components[i].GetParentComponent.Name <> FocusedPanel) then
         EnableComp := False;

      if (Name = 'F_Main') AND (TControl(Components[i]).Tag Mod 3 = 0 ) then
         EnableComp := True;

      if Components[i].GetParentComponent.Name = 'PCheckBoxes' then
         EnableComp := True;

      if Components[i].GetParentComponent.Name = 'PCheckBoxes2' then
         EnableComp := True;

      if Components[i].GetParentComponent.Name = 'PCheckBoxes3' then
         EnableComp := True;

      if not (Enable_Components) then
         EnableComp := True;

      if (Components[i] is TEdit)                  then TEdit(Components[i]).Enabled := EnableComp
      else if (Components[i] is TMaskEdit)         then TMaskEdit(Components[i]).Enabled := EnableComp
      else if (Components[i] is TMenuItem)         then TMenuItem(Components[i]).Enabled := EnableComp
//      else if (Components[i] is TMainMenu)         then TMainMenu(Components[i]).Enabled := True

      else if (Components[i] is TCheckBox)         then TCheckBox(Components[i]).Enabled := EnableComp
      else if (Components[i] is TButtonControl)           then TButton(Components[i]).Enabled := EnableComp
//      else if (Components[i] is TBitBtn)           then TBitBtn(Components[i]).Enabled := EnableComp
//      else if (Components[i] is TSpeedButton)           then TSpeedButton(Components[i]).Enabled := EnableComp
      else if (Components[i] is TMemo)             then TMemo(Components[i]).Enabled := EnableComp
//      else if (Components[i] is TDBLookupComboBox) then TDBLookupComboBox(Components[i]).Enabled := EnableComp
//      else if (Components[i] is TDBComboBox)       then TDBComboBox(Components[i]).Enabled := EnableComp
      else if (Components[i] is TComboBox)         then TComboBox(Components[i]).Enabled := EnableComp
      else if (Components[i] is TRadioGroup)       then TRadioGroup(Components[i]).Enabled := EnableComp
      else if (Components[i] is TPBNumEdit)        then TPBNumEdit(Components[i]).Enabled := EnableComp;

      ChangeColorComponent(Components[i]);
   end;
end;
//------------------------------------------------------------------------------


procedure ChangeReadOnlyPanel(Sender: TObject; sw : Boolean; FocusedPanel : String;swAddOrEdit : TState);
Var
   i : integer;
   ReadOnlyComp : boolean;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   For i := 0 to ComponentCount - 1 do
   if (Components[i].GetParentComponent <> nil) then
   begin
      ReadOnlyComp := sw;

      if swAddOrEdit = sAdd then
         if (TControl(Components[i]).Tag Mod 5 = 0 ) then
            ReadOnlyComp := True;

      if swAddOrEdit = sEdit then
         if (TControl(Components[i]).Tag Mod 13 = 0 ) then
            ReadOnlyComp := True;

      if (Components[i].GetParentComponent.Name <> FocusedPanel) then
         ReadOnlyComp := True;

      if Components[i].GetParentComponent.Name = 'PCheckBoxes' then
         ReadOnlyComp := False;

      if Components[i].GetParentComponent.Name = 'PCheckBoxes2' then
         ReadOnlyComp := False;

      if Components[i].GetParentComponent.Name = 'PCheckBoxes3' then
         ReadOnlyComp := False;

      if not (ReadOnly_Components) then
         ReadOnlyComp := False;

      if (Components[i] is TEdit)                  then
      begin
         if (Not ReadOnlyComp)
            AND (TControl(Components[i]).Tag Mod 7 = 0)
            AND (swAddOrEdit = sEdit)
            AND (Trim(TEdit(Components[i]).Text) <> '') then
            TEdit(Components[i]).ReadOnly := True
         else
            TEdit(Components[i]).ReadOnly := ReadOnlyComp;
      end
      else if (Components[i] is TMaskEdit)         then
      begin
         if (Not ReadOnlyComp)
            AND (TControl(Components[i]).Tag Mod 7 = 0)
            AND (swAddOrEdit = sEdit)
            AND (Trim(TMaskEdit(Components[i]).Text) <> '/  /')
            AND (Trim(TMaskEdit(Components[i]).Text) <> ':') then
            TMaskEdit(Components[i]).ReadOnly := True
         else
            TMaskEdit(Components[i]).ReadOnly := ReadOnlyComp;
      end

//    else if (Components[i] is TCheckBox)         then TCheckBox(Components[i]).ReadOnly := ReadOnlyComp
      else if (Components[i] is TMemo)             then TMemo(Components[i]).ReadOnly := ReadOnlyComp
      else if (Components[i] is TDBLookupComboBox) then TDBLookupComboBox(Components[i]).ReadOnly := ReadOnlyComp
      else if (Components[i] is TDBComboBox)       then TDBComboBox(Components[i]).ReadOnly := ReadOnlyComp
//    else if (Components[i] is TComboBox)         then TComboBox(Components[i]).ReadOnly := ReadOnlyComp
//    else if (Components[i] is TRadioGroup)       then TRadioGroup(Components[i]).ReadOnly := ReadOnlyComp;
      else if (Components[i] is TPBNumEdit)        then
      begin
         if (Not ReadOnlyComp)
            AND (TControl(Components[i]).Tag Mod 7 = 0)
            AND (swAddOrEdit = sEdit)
            AND (TPBNumEdit(Components[i]).Value <> 0) then
            TPBNumEdit(Components[i]).ReadOnly := True
         else
            TPBNumEdit(Components[i]).ReadOnly := ReadOnlyComp;
      end;

      ChangeColorComponent(Components[i]);
   end;
end;
//------------------------------------------------------------------------------

procedure EmptyEdits(Sender: TObject; FocusedPanel : String);
Var
    i : Integer;
begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   For i := 0 to ComponentCount - 1 do
   begin
      if Not (Components[i] is TWinControl) then Continue;
      if (Components[i].GetParentComponent <> nil)
       and (Components[i].GetParentComponent.Name = FocusedPanel)
       then
      begin
         if (Components[i] is TPBNumEdit)        then TPBNumEdit(Components[i]).Value := 0
         else if (Components[i] is TCustomEdit)            then  TCustomEdit(Components[i]).Clear
         else if (Components[i] is TLabel)    and (Pos('Edit',Components[i].Name)<>0)         then TLabel(Components[i]).Caption := ''
         else if (Components[i] is TComboBox)         then TComboBox(Components[i]).ItemIndex := -1
         else if (Components[i] is TCheckBox)         then TCheckBox(Components[i]).Checked := False
         else if (Components[i] is TMemo)             then TMemo(Components[i]).Clear
         else if (Components[i] is TDBLookupComboBox) then TDBLookupComboBox(Components[i]).KeyValue := -1
         else if (Components[i] is TDBComboBox)       then TDBComboBox(Components[i]).ItemIndex := -1
         else if (Components[i] is TRadioGroup)       then TRadioGroup(Components[i]).ItemIndex := -1
      end;
   end;
end;

procedure Fill_Tags(Sender: TObject);
Var
   N_Form : String;
   i : integer;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      N_Form := TForm(Sender).Name;
      For i := 0 to ComponentCount - 1 do
      begin
         If not CheckPermission then
         begin
            Components[i].Tag := 66;
            Continue;
         end;

   //    ShowMessage(TForm(Sender).Components[i].Name);
         K_Access := 1;
         if F_Common.Pub_BROWSE_Form_Fields.Locate('N_Field', Components[i].Name, []) then
            K_Access := F_Common.Pub_BROWSE_Form_FieldsK_Access.AsInteger;
         if IsFullAccess = 20 then K_Access := 66;
         Components[i].Tag := K_Access;
      end;
   end;
End;

procedure VisibleComponents(Sender: TObject);
Var
   i : integer;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   For i := 0 to ComponentCount - 1 do
      begin
//    ShowMessage(TForm(Sender).Components[i].Name);
            if (Components[i] is TControl) then
               if TControl(Components[i]).Tag mod 2 = 0 then
                  TControl(Components[i]).Visible := True
               else
                  TControl(Components[i]).Visible := False;

            if (Components[i] is TMenuItem) then
               if TMenuItem(Components[i]).Tag mod 2 = 0 then
                  TMenuItem(Components[i]).Visible := True
               else
                  TMenuItem(Components[i]).Visible := False;

            if (Components[i] is TMainMenu) then
               if TMainMenu(Components[i]).Tag mod 2 = 0 then
                  TForm(Sender).Menu := TMainMenu(Components[i])
               else
                  TForm(Sender).Menu := nil;
      end;
end;
//------------------------------------------------------------------------------

{
procedure EnableComponents(Sender: TObject;swAddOrEdit : TState);
Var
   i : integer;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   For i := 0 to ComponentCount - 1 do
      begin
//    ShowMessage(TForm(Sender).Components[i].Name);
         if swAddOrEdit = sAdd then
         begin
            if (Components[i] is TControl) Then

               if TControl(Components[i]).Tag mod 3 = 0 then
                  TControl(Components[i]).Enabled := True
               else
                  TControl(Components[i]).Enabled := False;

            if (Components[i] is TMenuItem) then

               if TMenuItem(Components[i]).Tag mod 3 = 0 then
                  TMenuItem(Components[i]).Enabled := True
               else
                  TMenuItem(Components[i]).Enabled := False;
         end
         else if swAddOrEdit = sEdit then
         begin
            if (Components[i] is TControl) Then

               if TControl(Components[i]).Tag mod 11 = 0 then
                  TControl(Components[i]).Enabled := True
               else
                  TControl(Components[i]).Enabled := False;

            if (Components[i] is TMenuItem) then

               if TMenuItem(Components[i]).Tag mod 11 = 0 then
                  TMenuItem(Components[i]).Enabled := True
               else
                  TMenuItem(Components[i]).Enabled := False;
         end
      end;
end;
//------------------------------------------------------------------------------


procedure ReadOnlyComponents(Sender: TObject;swAddOrEdit : TState);
Var
   i : integer;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   For i := 0 to ComponentCount - 1 do
      begin
//    ShowMessage(TForm(Sender).Components[i].Name);
         if swAddOrEdit = sAdd then
         begin
            if (Components[i] is TCustomEdit) Then
               if TEdit(Components[i]).Tag mod 5 = 0 then
                  TEdit(Components[i]).ReadOnly := True
               else
                  TEdit(Components[i]).ReadOnly := False;

            if (Components[i] is TCustomGrid) Then
               if TCustomGrid(Components[i]).Tag mod 5 = 0 then
                  TDBGrid(Components[i]).ReadOnly := True
               else
                  TDBGrid(Components[i]).ReadOnly := False;
         end
         else if swAddOrEdit = sEdit then
         begin
            if (Components[i] is TCustomEdit) Then
               if TEdit(Components[i]).Tag mod 13 = 0 then
                  TEdit(Components[i]).ReadOnly := True
               else
                  TEdit(Components[i]).ReadOnly := False;

            if (Components[i] is TCustomGrid) Then
               if TCustomGrid(Components[i]).Tag mod 13 = 0 then
                  TDBGrid(Components[i]).ReadOnly := True
               else
                  TDBGrid(Components[i]).ReadOnly := False;
         end

      end;
end;
//------------------------------------------------------------------------------
}
procedure ChangeEnableButton(Sender: TObject; N : String; var swAddOrEdit : TState; FocusedPanel : String);
var
   MyStatusBar : TStatusBar;
   i : integer;
begin
   N := UpperCase(N);
   MyStatusBar := nil;
{
   if (IS_System_PeriodicCycle) Then
      if IsCloseCycle = 1 then
      begin

         if (N='BADD') AND (Is_AddWhenIsClose = 0) then
            begin
               ShowMessage('���� ���� ��� ���');
               exit;
            end;

         if (N='BEDIT') AND (Is_EditWhenIsClose = 0) then
            begin
               ShowMessage('���� ���� ��� ���');
               exit;
            end;

      end;

   if (Is_System_PeriodicSal) Then
      if IsCloseSalJari = 1 then
      begin
         if (N='BADD') AND (Is_AddWhenIsClose = 0) then
            begin
               ShowMessage('���� ���� ��� ���');
               exit;
            end;

         if (N='BEDIT') AND (Is_EditWhenIsClose = 0) then
            begin
               ShowMessage('���� ���� ��� ���');
               exit;
            end;
      end;
}
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      For i := 0 to ComponentCount - 1 do
         if (Components[i] is TStatusBar) then
         begin
            MyStatusBar := TStatusBar(Components[i]);
            break;
         end;


      if (N='BADD') or (N='BEDIT') then
      Begin

         if (N='BADD') then
            swAddOrEdit := sAdd
         else
            swAddOrEdit := sEdit;

         ChangeEnablePanel(Sender, True, FocusedPanel, swAddOrEdit);
         ChangeReadOnlyPanel(Sender, False, FocusedPanel, swAddOrEdit);

         if MyStatusBar <> nil then
            if (N='BADD') then
               MyStatusBar.Panels[2].Text := '�����'
            else
               MyStatusBar.Panels[2].Text := '������';

         if (N='BADD') then
            EmptyEdits(Sender, FocusedPanel);

         if FindComponent('BADD') <> nil then
            TSpeedButton(FindComponent('BADD')).Enabled := False;

         if FindComponent('BEDIT') <> nil then
            TSpeedButton(FindComponent('BEDIT')).Enabled := False;

         if FindComponent('BSAVE') <> nil then
            TSpeedButton(FindComponent('BSAVE')).Enabled := True;

         if FindComponent('BDEL') <> nil then
            TSpeedButton(FindComponent('BDEL')).Enabled := False;

         if FindComponent('BFILTER') <> nil then
            TSpeedButton(FindComponent('BFILTER')).Enabled := False;

         if FindComponent('BCANCEL') <> nil then
            TSpeedButton(FindComponent('BCANCEL')).Enabled := True;

         if FindComponent('BNEXT') <> nil then
            TSpeedButton(FindComponent('BNEXT')).Enabled := False;

         if FindComponent('BPREV') <> nil then
            TSpeedButton(FindComponent('BPREV')).Enabled := False;

         if FindComponent('BFIRST') <> nil then
            TSpeedButton(FindComponent('BFIRST')).Enabled := False;

         if FindComponent('BEND') <> nil then
            TSpeedButton(FindComponent('BEND')).Enabled := False;

         if FindComponent('BFILTEREXEC') <> nil then
            TSpeedButton(FindComponent('BFILTEREXEC')).Enabled := False;

         if FindComponent('BPRINT') <> nil then
            TSpeedButton(FindComponent('BPRINT')).Enabled := False;
      end;

      if N = 'BSAVE' then
      Begin
         ChangeEnablePanel(Sender, False, FocusedPanel, swAddOrEdit);
         ChangeReadOnlyPanel(Sender, True, FocusedPanel, swAddOrEdit);

         swAddOrEdit := sView;

         if MyStatusBar <> nil then
            MyStatusBar.Panels[2].Text  := '�����';

         if FindComponent('BADD') <> nil then
            if TSpeedButton(FindComponent('BADD')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BADD')).Enabled := True;

         if FindComponent('BEDIT') <> nil then
            if TSpeedButton(FindComponent('BEDIT')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BEDIT')).Enabled := True;

         if FindComponent('BDEL') <> nil then
            if TSpeedButton(FindComponent('BDEL')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BDEL')).Enabled := True;

         if FindComponent('BSAVE') <> nil then
            TSpeedButton(FindComponent('BSAVE')).Enabled := False;

         if FindComponent('BFILTER') <> nil then
            if TSpeedButton(FindComponent('BFILTER')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BFILTER')).Enabled := True;

         if FindComponent('BCANCEL') <> nil then
            TSpeedButton(FindComponent('BCANCEL')).Enabled := False;

         if FindComponent('BNEXT') <> nil then
            TSpeedButton(FindComponent('BNEXT')).Enabled := True;

         if FindComponent('BPREV') <> nil then
            TSpeedButton(FindComponent('BPREV')).Enabled := True;

         if FindComponent('BFIRST') <> nil then
            TSpeedButton(FindComponent('BFIRST')).Enabled := True;

         if FindComponent('BEND') <> nil then
            TSpeedButton(FindComponent('BEND')).Enabled := True;

         if FindComponent('BFILTEREXEC') <> nil then
            TSpeedButton(FindComponent('BFILTEREXEC')).Enabled := False;

         if FindComponent('BPRINT') <> nil then
            TSpeedButton(FindComponent('BPRINT')).Enabled := True;
      End;

      if N = 'BCANCEL' then
      Begin
         ChangeEnablePanel(Sender, False, FocusedPanel, swAddOrEdit);
         ChangeReadOnlyPanel(Sender, True, FocusedPanel, swAddOrEdit);

         swAddOrEdit := sView;

         if MyStatusBar <> nil then
            MyStatusBar.Panels[2].Text := '������';

         if FindComponent('BADD') <> nil then
            if TSpeedButton(FindComponent('BADD')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BADD')).Enabled := True
            else
               TSpeedButton(FindComponent('BADD')).Enabled := False;

         if FindComponent('BEDIT') <> nil then
            if TSpeedButton(FindComponent('BEDIT')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BEDIT')).Enabled := True
            else
               TSpeedButton(FindComponent('BEDIT')).Enabled := False;

         if FindComponent('BDEL') <> nil then
            if TSpeedButton(FindComponent('BDEL')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BDEL')).Enabled := True
            else
               TSpeedButton(FindComponent('BDEL')).Enabled := False;

         if FindComponent('BSAVE') <> nil then
            TSpeedButton(FindComponent('BSAVE')).Enabled := False;

         if FindComponent('BFILTER') <> nil then
            if TSpeedButton(FindComponent('BFILTER')).Tag Mod 3 = 0 then
               TSpeedButton(FindComponent('BFILTER')).Enabled := True
            else
               TSpeedButton(FindComponent('BFILTER')).Enabled := False;

         if FindComponent('BCANCEL') <> nil then
            TSpeedButton(FindComponent('BCANCEL')).Enabled := True;

         if FindComponent('BNEXT') <> nil then
            TSpeedButton(FindComponent('BNEXT')).Enabled := True;

         if FindComponent('BPREV') <> nil then
            TSpeedButton(FindComponent('BPREV')).Enabled := True;

         if FindComponent('BFIRST') <> nil then
            TSpeedButton(FindComponent('BFIRST')).Enabled := True;

         if FindComponent('BEND') <> nil then
            TSpeedButton(FindComponent('BEND')).Enabled := True;

         if FindComponent('BFILTEREXEC') <> nil then
            TSpeedButton(FindComponent('BFILTEREXEC')).Enabled := False;

         if FindComponent('BPRINT') <> nil then
            TSpeedButton(FindComponent('BPRINT')).Enabled := True;
      End;
   end;
End;
//------------------------------------------------------------------------------



procedure ShowErrorMessage(MessageID : integer);
Begin
   Case MessageID of
      -6   : ShowMessage('�� ��� ������� ��� �� �� ��� ��� ���� ��� �������');
      196  : ShowMessage('�����');
      547  : ShowMessage('�������� ���� ���� ����� �� ����');
      2627 : ShowMessage('��� �� ������ ���.');
      4872 : ShowMessage('����� ������� ��� ������ �� ���� ����');
      else  ShowMessage('��� �� ����� ������' + '  ' + IntToStr(MessageID));
   end;
end;
//------------------------------------------------------------------------------

procedure PreviewReport(ppReport1 : TppReport);overload;
var
   F : TF_Report;
begin
   ppReport1.AllowPrintToFile := True;
   F := TF_Report.Create(Application);
   ppReport1.AllowPrintToArchive := True;
   ppReport1.AllowPrintToFile := True;
   F.ppViewer1.Report := ppReport1;
   F.ppTitleBand1.Visible := False;
   ppReport1.PrintToDevices;

   F.ShowModal;
   F.Free;

end;

//------------------------------------------------------------------------------

procedure PreviewReport(SrlReport : integer; MyDataSource : TDataSource; IsOneRecord, IsPreview : integer);overload;
var
   F : TF_Report;
   ReportFileName : String;
begin
   NullAdoSPParameters(F_Common.Browse_Pub_Reports);
   F_Common.Browse_Pub_Reports.Parameters.ParamByName('@SrlReport').Value := SrlReport;
   F_Common.Browse_Pub_Reports.Open;

{

CREATE PROCEDURE Browse_Pub_Reports   @SrlReport int
AS

IF @SrlReport <= 0 RETURN
IF NOT EXISTS(SELECT  *  FROM  Pub_Reports  Where  Srl_Report = @SrlReport)
	INSERT INTO Pub_Reports(Val_Report)
	SELECT
		Val_Report
	FROM         Pub_Reports
	Where  Srl_Report = 101

SELECT
	Val_Report
FROM         Pub_Reports
Where  Srl_Report = @SrlReport
GO

}

   if F_Common.Browse_Pub_Reports.RecordCount > 0 then
   begin
      ReportFileName := 'A123456';
      F_Common.Browse_Pub_ReportsVal_Report.SaveToFile(ReportFileName);
      F := TF_Report.Create(Application);
      F.Srl_Report := SrlReport;

      With F Do
      begin

         With ppBDEGridReport Do
         begin
            DataSource := MyDataSource;
            if IsOneRecord = 1 then
            begin
               RangeBegin := rbCurrentRecord;
               RangeEnd :=   reCurrentRecord;
            end
            else
            begin
               RangeBegin := rbFirstRecord;
               RangeEnd :=   reLastRecord;
            end
         end;


         With ppViewer1 Do
         begin
            Report := GridReport;
         end;

         With GridReport Do
         begin
            AllowPrintToFile := True;
            Template.FileName := ReportFileName;
            Template.LoadFromFile;
            AllowPrintToArchive := True;
            AllowPrintToFile := True;
            DataPipeline := ppBDEGridReport;

            if F.FindComponent('StudentPic1') <> nil then
               TppImage(F.FindComponent('StudentPic1')).OnPrint := F_Common.PerImagePrint;


            if F.FindComponent('CompanyMarkPic1') <> nil then
               TppImage(F.FindComponent('CompanyMarkPic1')).OnPrint := F_Common.CompanyMarkImagePrint;

            if F.FindComponent('D_Current1') <> nil then
               TppLabel(F.FindComponent('D_Current1')).OnGetText := F_Common.ppLabelD_CurrentGetText;

            if F.FindComponent('NumberVal1') <> nil then
               TppLabel(F.FindComponent('NumberVal1')).OnGetText := F_Common.ppLabelNumberValGetText;

            if F.FindComponent('NumberStr1') <> nil then
               TppLabel(F.FindComponent('NumberStr1')).OnGetText := F_Common.ppLabelNumberStrGetText;

            PrintToDevices;
         end;

         if IsPreview = 1 then
            ShowModal
         else
            GridReport.Print;

         Free;
      end;
   end;
end;
//------------------------------------------------------------------------------
procedure PreviewReport(SrlReport : integer; MyDataSource1, MyDataSource2, MyDataSource3 : TDataSource;
               CountRec : integer);overload;
var
   F : TF_Report;
   MyStreem : TMemoryStream;
begin
   MyStreem := TMemoryStream.Create;
   NullAdoSPParameters(F_Common.Browse_Pub_Reports);
   F_Common.Browse_Pub_Reports.Parameters.ParamByName('@SrlReport').Value := SrlReport;
   F_Common.Browse_Pub_Reports.Open;

   if F_Common.Browse_Pub_Reports.RecordCount > 0 then
   begin

      F_Common.Browse_Pub_ReportsVal_Report.SaveToStream(MyStreem);
      F := TF_Report.Create(Application);
      F.Srl_Report := SrlReport;

      With F Do
      begin

         With ppBDEGridReport Do
         begin
            DataSource := MyDataSource1;
            if CountRec = 1 then
            begin
               RangeBegin := rbCurrentRecord;
               RangeEnd :=   reCurrentRecord;
            end
            else
            begin
               RangeBegin := rbFirstRecord;
               RangeEnd :=   reLastRecord;
            end
         end;

         With ppBDEGridReport2 Do
         begin
            DataSource := MyDataSource2;
            RangeBegin := rbFirstRecord;
            RangeEnd :=   reLastRecord;
         end;

         if MyDataSource2 <> nil then
            With ppBDEGridReport3 Do
            begin
               DataSource := MyDataSource3;
               RangeBegin := rbFirstRecord;
               RangeEnd :=   reLastRecord;
            end;

         With ppViewer1 Do
         begin
            Report := GridReport;
         end;

         With GridReport Do
         begin
            AllowPrintToFile := True;
            MyStreem.Position := 0;
            Template.LoadFromStream(MyStreem);
            MyStreem.Free;

            AllowPrintToArchive := True;
            AllowPrintToFile := True;
            DataPipeline := ppBDEGridReport;


            if F.FindComponent('StudentPic1') <> nil then
               TppImage(F.FindComponent('StudentPic1')).OnPrint := F_Common.PerImagePrint;


            if F.FindComponent('CompanyMarkPic1') <> nil then
               TppImage(F.FindComponent('CompanyMarkPic1')).OnPrint := F_Common.CompanyMarkImagePrint;

            if F.FindComponent('D_Current1') <> nil then
               TppLabel(F.FindComponent('D_Current1')).OnGetText := F_Common.ppLabelD_CurrentGetText;

            PrintToDevices;
         end;

         ShowModal;
         Free;
      end;
   end;
end;
//------------------------------------------------------------------------------



procedure FindStudent;
var
   F : TF_Find_Per;
begin
   F := TF_Find_Per.Create(Application);
   F.ShowModal;
   F.Free;
end;
//------------------------------------------------------------------------------

procedure ShowFormOption(N_Form : String;
                        Var FormPMainColor1 : TColor;
                        Var FormPMainColor2 : TColor;
                        Var FormPButtonColor : TColor);
var
   F : TF_DisplayOption;
begin
   F := TF_DisplayOption.Create(Application);
   F.N_Form := N_Form;

   F.FDOPMainColor1 := FormPMainColor1;
   F.FDOPMainColor2 := FormPMainColor2;
   F.FDOPButtonColor := FormPButtonColor;

   F.ShowModal;
   // 2  ->   1
   FormPMainColor2 := F.FDOPMainColor2;
   FormPMainColor1 := F.FDOPMainColor1;
   FormPButtonColor := F.FDOPButtonColor;
   F.Free;
end;
//------------------------------------------------------------------------------

procedure ShowCodingForm(FormTableName, FormCaption, FormCodeName : String);overload;
var
   FF : TF_DemoForm1;
begin
   F_Common.UserCanAccessField('F_' + FormTableName, N_System, K_Access);
   K_Access := 2;
   if K_Access > 1 then
   begin
      FF := TF_DemoForm1.Create(Application);
      with FF do
      begin
         TableName := FormTableName;
         Name := 'F_' + FormTableName;
         Caption := FormCaption;
         StrCodeName := FormCodeName;
         FormShowDM(FF, 2, True);
      end;
   end
   else
      ShowMessage('��� ���� �� ������� �� ��� ���� ������');
end;


//------------------------------------------------------------------------------

procedure ShowCodingForm(SrlDataSet : integer);overload;
var
   FF : TF_Coding;
begin
   K_Access := 2;
   if K_Access > 1 then
   begin
      FF := TF_Coding.Create(Application);
      FF.SrlDataSet := SrlDataSet;
      FF.Name := 'F_Codding_' + IntToStr(SrlDataSet);
      FormShowDM(FF, 2, True);
   end
   else
      ShowMessage('��� ���� �� ������� �� ��� ���� ������');
end;
//------------------------------------------------------------------------------

function Is_DeleteRecord(SrlDataSet, SrlRecord : integer) : boolean;overload;
var
   FF : TF_DeleteRecord;
   IsDel : boolean;
begin
   FF := TF_DeleteRecord.Create(Application);
   FF.LabelSrlDataSet.Caption := IntToStr(SrlDataSet);
   FF.LabelSrlRecord.Caption := IntToStr(SrlRecord);
   FF.ShowModal;
   IsDel := FF.IsDel;
   FF.Free;
   result := IsDel;
end;
//------------------------------------------------------------------------------

function Is_DeleteRecord : boolean;
var
   FF : TF_DeleteRecord;
   IsDel : boolean;
begin
   FF := TF_DeleteRecord.Create(Application);
   FF.LabelSrlDataSet.Caption := '0';
   FF.LabelSrlRecord.Caption := '0';
   FF.ShowModal;
   IsDel := FF.IsDel;
   FF.Free;
   result := IsDel;
end;
//------------------------------------------------------------------------------


function Is_SaveBeforClose : integer;
{var
   FF : TF_SaveBeforClose;
   IsSave : integer;    }
begin
 {  FF := TF_SaveBeforClose.Create(Application);
   FF.ShowModal;
   IsSave := FF.IsSave;
   FF.Free;
   result := IsSave;    }
end;
//------------------------------------------------------------------------------


procedure ShowDynamicQueryForm(SrlObject : integer);
var
   FF : TF_DynamicReport;
begin
   K_Access := 6;
   if K_Access > 1 then
   begin
      FF := TF_DynamicReport.Create(Application);
      FF.SrlObject := SrlObject;
      FF.ShowModal;
//      FormShowDM(FF, 2, true);
   end
   else
      ShowMessage('��� ���� �� ������� �� ��� ���� ������');
    
end;
//------------------------------------------------------------------------------



procedure PrintDBGrid(MyDBGrid : TDBGrid);
var
   F : TF_Report;
   i : integer;
   Sender: TObject;
begin
   GlobalDBGrid := MyDbGrid;
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0){ OR
       (MyDBGrid.Columns.Count > 20 )}) then
      exit;

   Sender := MyDBGrid;
   if ((Sender <> nil) and (Sender.ClassType <> nil)) then
      while not (Sender.InheritsFrom(TForm)) do
         Sender := TComponent(Sender).GetParentComponent;

   MyDBGrid.DataSource.DataSet.DisableControls;
   if MyDBGrid <> nil then
      for i := 0 to MyDBGrid.Columns.Count -1 do
         MyDBGrid.Columns[i].ReadOnly := True;

   F := TF_Report.Create(Application);
   F.ReportGrid := MyDBGrid;
   F.ppViewer1.Report := F.GridReport;
   F.TitCaption.Lines.Add(TForm(Sender).Caption);

   F.PrintGrid;
//   F.ppTitleBand1.Visible := False;
   F.GridReport.PrintToDevices;
   F.ShowModal;
   F.Free;
   MyDBGrid.DataSource.DataSet.EnableControls;
end;

procedure SearchDBGrid(MyDBGrid : TDBGrid);
begin
   GlobalDBGrid := MyDbGrid;
   with TEdit.Create(MyDBGrid) do
   begin
      Left := MyDBGrid.Left + 20;
      Top := MyDBGrid.Top + 80;
      Width := 200;
      Height := 30;
      Font.Size := 12;
      Color := $00BFFFFF;
      Text := '����� ���� ����� �� ���� ����';
      parent := MyDBGrid.Parent;
      SetFocus;
      Name := 'SearchEdit';
      OnExit := F_Common.EditSearchExit;
      OnKeyPress := F_Common.EditSearchKeyPress;
      OnChange := F_Common.EditSearchChange;
      OnKeyDown := F_Common.EditSearchKeyDown;
   end;
end;

Function SumDBGrid(MyDBGrid : TDBGrid): String;
var
   SumField : real;
   F1 : TField;
begin
   GlobalDBGrid := MyDbGrid;
   SumField := 0;
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;


   with TADOStoredProc(GlobalDBGrid.DataSource.DataSet) do
   begin
      if Trim(Sort) = '' then
         F1 := GlobalDBGrid.Columns[0].Field
      else
         F1 := GlobalDBGrid.Columns[GlobalColumnIndex].Field;


      if Not (F1.DataType in NumberType) then
      begin
         result := '�����';
         exit;
      end
   end;

   MyDBGrid.DataSource.DataSet.DisableControls;

   MyDBGrid.DataSource.DataSet.First;
   while Not MyDBGrid.DataSource.DataSet.Eof do
   begin
      SumField := SumField + F1.Value;
      MyDBGrid.DataSource.DataSet.Next;
   end;
   MyDBGrid.DataSource.DataSet.EnableControls;

   result :='���  = '+ FloatToStr(SumField);
end;


procedure DBGridSaveToFile(MyDBGrid : TDBGrid);
begin
   GlobalDBGrid := MyDbGrid;
  // TADODataSet(MyDBGrid.DataSource.DataSet).SaveToFile('C:\Temp\' +  GlobalFormName + MyDBGrid.Name);
   MyDBGrid.Columns.SaveToFile('C:\Temp\' +  GlobalFormName + MyDBGrid.Name);
end;


procedure DBGridSaveToAccess(MyDBGrid : TDBGrid);
var
   DataSource : string;
   dbName     : string;
   FieldName  : string;
   TableName  : string;
   cs         : string;
   MyDataSet     : TADODataSet;
   i : integer;
begin
   GlobalDBGrid := MyDbGrid;
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;


   dbName:='c:\Temp\' + N_System + '_' + GlobalFormName + '_' +  MyDBGrid.Name + '.mdb';
   if FileExists(dbName) then
      DeleteFile(dbName);

   MyDataSet := TADODataSet(MyDBGrid.DataSource.DataSet);
   TableName := MyDBGrid.Name;

   DataSource :=
     'Provider=Microsoft.Jet.OLEDB.4.0' +
     ';Data Source=' + dbName +
     ';Jet OLEDB:Engine Type=5';

  // F_Common.ADOXCatalog1.Create1(DataSource);


   DataSource :=
     'Provider=Microsoft.Jet.OLEDB.4.0'+
     ';Data Source=' + dbName +
     ';Persist Security Info=False';

   F_Common.ADOConnection1.Connected := False;
   F_Common.ADOConnection1.ConnectionString := DataSource;
   F_Common.ADOConnection1.LoginPrompt := False;
   F_Common.ADOCommand1.Connection := F_Common.ADOConnection1;

   cs := 'CREATE TABLE ' + TableName + ' (';
   for i := 0 to MyDataSet.Fields.Count - 1 do
   begin
      FieldName := QuotedStr(MyDataSet.Fields[i].DisplayName);
      cs := cs + FieldName + ' TEXT(50),';
   end;
   cs := Copy(cs,1, Length(cs)-1);
   cs := cs + ')';
   F_Common.ADOCommand1.CommandText := cs;
   F_Common.ADOCommand1.Execute;

   F_Common.ADOTable1.Connection := F_Common.ADOConnection1;
   F_Common.ADOTable1.TableName := TableName;

   MyDBGrid.DataSource.DataSet.DisableControls;
   MyDataSet.First;
   F_Common.ADOTable1.Open;
   while Not MyDataSet.Eof do
   begin
      F_Common.ADOTable1.Append;
      for i := 0 to MyDataSet.Fields.Count - 1 do
      begin
         F_Common.ADOTable1.Fields[i].Value := MyDataSet.Fields[i].Value;
      end;
      F_Common.ADOTable1.Post;
      MyDataSet.Next;
   end;
   MyDBGrid.DataSource.DataSet.EnableControls;
   F_Common.ADOConnection1.Connected := False;
end;


procedure HandleRange;
var
   Range: Variant;
begin
   Range := XLApp.Workbooks[1].WorkSheets['Delphi Data'].Range['C1:F25'];

   Range.Formula := '=RAND()';
   Range.Columns.Interior.ColorIndex := 3;
//   Range.Borders.LineStyle := xlContinuous;
end;

procedure ChangeColumns;
var
   ColumnRange: Variant;
begin
   ColumnRange := XLApp.Workbooks[1].WorkSheets['Delphi Data'].Columns;
   ColumnRange.Columns[1].ColumnWidth := 5;
   ColumnRange.Columns.Item[1].Font.Bold := True;
   ColumnRange.Columns[1].Font.Color := clBlue;
end;


procedure DBGridSaveToExcelFile(MyDBGrid : TDBGrid);
var
   i, j : Integer;
   Sheet : Variant;
   RecCount : integer;
   ColCount : integer;
   ColIndex : integer;
   ColumnRange: Variant;
   Range: Variant;
   StrRange : String;
begin
   GlobalDBGrid := MyDbGrid;
   if MyDBGrid = nil then exit;
   if MyDBGrid.DataSource = nil then exit;
   if MyDBGrid.DataSource.DataSet = nil then exit;
   if MyDBGrid.DataSource.DataSet.Active = False then exit;
   if MyDBGrid.DataSource.DataSet.RecordCount = 0 then exit;

   RecCount := MyDBGrid.DataSource.DataSet.RecordCount;
   ColCount := 0;
   for i := 0 to MyDBGrid.Columns.Count - 1 do
      if MyDBGrid.Columns[i].Visible then
         ColCount := ColCount + 1;

   XLApp:= CreateOleObject('Excel.Application');
   XLApp.Visible := True;
  // XLApp.Workbooks.Add(xlWBatWorkSheet);
   XLApp.Workbooks[1].WorkSheets[1].Name := 'Delphi Data';
   ColumnRange := XLApp.Workbooks[1].WorkSheets['Delphi Data'].Columns;
   StrRange := 'A1:' + UpperCase(Char(ColCount+ 64)) + '1';
   Range := XLApp.Workbooks[1].WorkSheets['Delphi Data'].Range[StrRange];
   Range.Columns.Interior.ColorIndex := 8;

   MyDBGrid.DataSource.DataSet.First;
   MyDBGrid.DataSource.DataSet.DisableControls;
   Sheet := XLApp.Workbooks[1].WorkSheets['Delphi Data'];

   ColIndex := 1;
   for j := 1 TO MyDBGrid.Columns.Count do
      if MyDBGrid.Columns[j-1].Visible then
      begin
         Sheet.Cells[1, ColIndex] := MyDBGrid.Columns[j-1].Title.Caption;
         ColumnRange.Columns[ColIndex].ColumnWidth := 25;
         ColumnRange.Columns.Item[ColIndex].Font.Bold := True;
         ColumnRange.Columns[ColIndex].Font.Color := clBlue;
         ColIndex := ColIndex + 1;
      end;

   While Not MyDBGrid.DataSource.DataSet.Eof do
   begin
      ColIndex := 1;
      for j := 1 TO MyDBGrid.Columns.Count do
         if MyDBGrid.Columns[j-1].Visible then
         begin
            if MyDBGrid.DataSource.DataSet.FindField(MyDBGrid.Columns[j-1].FieldName) <> nil then
               if Not MyDBGrid.DataSource.DataSet.FieldByName(MyDBGrid.Columns[j-1].FieldName).IsNull then
                  Sheet.Cells[MyDBGrid.DataSource.DataSet.RecNo + 1, ColIndex] := MyDBGrid.DataSource.DataSet.FieldByName(MyDBGrid.Columns[j-1].FieldName).Value;
            ColIndex := ColIndex + 1;
         end;
      MyDBGrid.DataSource.DataSet.Next;
   end;


//   for j := 1 TO ColCount do
//      Sheet.Cells[RecCount + 2, j] := '=Sum(' + UpperCase(Char(j+64))
//      + '2:' + UpperCase(Char(j+64)) + intToStr(RecCount+1)+ ')';

//   XLApp.Visible := True;
   MyDBGrid.DataSource.DataSet.EnableControls;

//   HandleRange;
//   ChangeColumns;
end;

procedure DBGridSaveToWordFile(MyDBGrid : TDBGrid);
var
   i, j : Integer;
   Sheet : Variant;
   RecCount : integer;
   ColCount : integer;
   ColIndex : integer;
   ParagrafIndex : integer;
   ColumnRange: Variant;
   Range: Variant;
   StrRange : String;
begin
   GlobalDBGrid := MyDbGrid;
   if MyDBGrid = nil then exit;
   if MyDBGrid.DataSource = nil then exit;
   if MyDBGrid.DataSource.DataSet = nil then exit;
   if MyDBGrid.DataSource.DataSet.Active = False then exit;
   if MyDBGrid.DataSource.DataSet.RecordCount = 0 then exit;

   RecCount := MyDBGrid.DataSource.DataSet.RecordCount;
   ColCount := 0;
   for i := 0 to MyDBGrid.Columns.Count - 1 do
      if MyDBGrid.Columns[i].Visible then
         ColCount := ColCount + 1;

   WordApp := CreateOleObject('Word.Application');
   WordApp.Visible := True;
   WordApp.Documents.Add;

   Range := WordApp.Documents.Item(1).Range;
   Range.Text := MyDBGrid.DataSource.Name;

   ParagrafIndex := 1;
   While Not MyDBGrid.DataSource.DataSet.Eof do
   begin
      ColIndex := 1;
      WordApp.Documents.Item(1).Paragraphs.Add;  //  Paragraphs == 1
      ParagrafIndex := ParagrafIndex + 1;

      WordApp.Documents.Item(1).Paragraphs.Add;
      ParagrafIndex := ParagrafIndex + 1;

      Range := WordApp.Documents.Item(1).Range(WordApp.Documents.Item(1).Paragraphs.Item(ParagrafIndex).Range.Start);
      Range.Text := Range.Text + 'Line Number : ' + IntToStr(MyDBGrid.DataSource.DataSet.RecNo) + '====================';

      for j := 1 TO MyDBGrid.Columns.Count do
         if MyDBGrid.Columns[j-1].Visible then
         begin
            WordApp.Documents.Item(1).Paragraphs.Add;  //  Paragraphs == 2, 3, ..
            ParagrafIndex := ParagrafIndex + 1;
            Range := WordApp.Documents.Item(1).Range(WordApp.Documents.Item(1).Paragraphs.Item(ParagrafIndex).Range.Start);
            if MyDBGrid.DataSource.DataSet.FindField(MyDBGrid.Columns[j-1].FieldName) <> nil then
               if Not MyDBGrid.DataSource.DataSet.FieldByName(MyDBGrid.Columns[j-1].FieldName).IsNull then
                  Range.Text := Range.Text + MyDBGrid.DataSource.DataSet.FieldByName(MyDBGrid.Columns[j-1].FieldName).AsString + ';'
               else
                  Range.Text := Range.Text + ';';
            ColIndex := ColIndex + 1;
         end;
      MyDBGrid.DataSource.DataSet.Next;
   end;

end;


procedure ShowExcelFile;
//var
  // F_ShowExcelFile : TF_ShowExcelFile;
begin
  { F_ShowExcelFile := TF_ShowExcelFile.Create(F_Common);
   F_ShowExcelFile.Showmodal;
   F_ShowExcelFile.Free;  }
end;



procedure ReSizeDBGrid(MyDBGrid : TDBGrid);
var
   MyParentForm : TWinControl;
   BaseDBGridColumnFileName : String;
begin
   GlobalDBGrid := MyDbGrid;
{
   MyParentForm := MyDBGrid.Parent;
   While ((MyParentForm <> nil) and (MyParentForm.ClassType <> nil)
        and (Not MyParentForm.InheritsFrom(TForm))) do
      MyParentForm := MyParentForm.Parent;
   GlobalFormName := TForm(MyParentForm).Name;
}
   BaseDBGridColumnFileName := 'C:\Temp\' +  GlobalFormName + MyDBGrid.Name ;

   if FileExists(PChar(BaseDBGridColumnFileName)) then
      MyDBGrid.Columns.LoadFromFile(pchar(BaseDBGridColumnFileName));
     //frmRepBuyDBGrid1_Data
end;


procedure OptionDBGrid(MyDBGrid : TDBGrid);
var
   FDBGridOption : TF_DBGrid_Option;
begin
   GlobalDBGrid := MyDbGrid;
   FDBGridOption := TF_DBGrid_Option.Create(Application);
   FDBGridOption.MyDbGrid2 := MyDBGrid;
   FDBGridOption.ShowModal;
   FDBGridOption.Free;
end;

procedure SortDBGrid1(MyColumn : TColumn);
Var
   FildName : String;
   MyField : TField;
   SortString : String;
   MyDbGrid : TDBGrid;
   i : integer;
begin
   MyDbGrid := TDBGrid(MyColumn.Grid);
   GlobalDBGrid := MyDbGrid;

   GlobalColumnIndex := MyColumn.Index;

   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;

   if MyDbGrid.Columns[GlobalColumnIndex].Field = nil then
      exit;

   if MyDbGrid.Columns[GlobalColumnIndex].Field.FieldKind in [fkData] then
   begin
      for i := 0 to MyDbGrid.Columns.Count - 1 do
         MyDbGrid.Columns[i].Title.Color := TitleNotSortColor;
      MyDbGrid.Columns[GlobalColumnIndex].Title.Color := TitleSortColor;

      MyDbGrid.ParentColor := True;
      for i := 0 to MyDbGrid.Columns.Count - 1 do
         MyDbGrid.Columns[i].Color := MyDbGrid.Color;
      MyDbGrid.Columns[GlobalColumnIndex].Color := TitleSortColor;

      FildName := Trim(MyDbGrid.Columns[GlobalColumnIndex].FieldName);
      MyField := TADOStoredProc(MyDbGrid.DataSource.DataSet).FindField(FildName);
      SortString := TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort;

      if MyField <> nil then
         if Pos(FildName, SortString) > 0 then
            if Pos('DESC', SortString) > 0 then
               TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort := '[' + FildName + '] ASC '
            else
               TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort := '[' + FildName + '] DESC '
         else
            TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort := '[' + FildName + '] ASC '
{
      if MyField <> nil then
         if Pos(FildName, SortString) > 0 then
            if Pos('DESC', SortString) > 0 then
               TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort := FildName + ' ASC '
            else
               TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort := FildName + ' DESC '
         else
            TADOStoredProc(MyDbGrid.DataSource.DataSet).Sort := FildName + ' ASC '
}
   end;
end;

Function GetFarsiFilterDBGrid(MyDBGrid : TDBGrid): String;
var
   i, j : integer;
   Filter : String;
   FilterFarsi : String;
   MyWord : String;
begin
   GlobalDBGrid := MyDbGrid;
{
   if ((Not MyDBGrid.DataSource.DataSet.Active )
//   OR (MyDBGrid.DataSource.DataSet.RecordCount = 0)
   ) then
      exit;

   Filter := MyDBGrid.DataSource.DataSet.Filter;
   if Trim(Filter) = '' then
      FilterFarsi := ''
   else
   begin
      for i := 1 to Length(Filter) do
      begin
         MyWord := MyWord + Filter[i];
         if (  (MyWord = ' ')
               OR (MyWord = '0')
               OR (MyWord = '1')
               OR (MyWord = '2')
               OR (MyWord = '3')
               OR (MyWord = '4')
               OR (MyWord = '5')
               OR (MyWord = '6')
               OR (MyWord = '7')
               OR (MyWord = '8')
               OR (MyWord = '9')
               OR (MyWord = '%')) then
         begin
            FilterFarsi := FilterFarsi + MyWord;
            MyWord := '';
         end
         else
         if (  (MyWord = '''')
               OR (MyWord = '[')
               OR (MyWord = ']')
               OR (MyWord = '(')
               OR (MyWord = ')')) then
         begin
            FilterFarsi := FilterFarsi + ' ';
            MyWord := '';
         end

         else if ((i = Length(Filter)) OR (Filter[i + 1] = ' ')
                  OR (Filter[i + 1] = ']') OR (Filter[i + 1] = ')')) then
         begin
            if (MyWord = '=') then
            begin
               FilterFarsi := FilterFarsi + '�����';
               MyWord := '';
            end
            else
            if (MyWord = '<') then
            begin
               FilterFarsi := FilterFarsi + '����� ��';
               MyWord := '';
            end
            else
            if (MyWord = '<=') then
            begin
               FilterFarsi := FilterFarsi + '����� �� �����';
               MyWord := '';
            end
            else
            if (MyWord = '>') then
            begin
               FilterFarsi := FilterFarsi + '��ѐ�� ��';
               MyWord := '';
            end
            else
            if (MyWord = '>=') then
            begin
               FilterFarsi := FilterFarsi + '��ѐ�� �� �����';
               MyWord := '';
            end
            else
            if (MyWord = '<>') then
            begin
               FilterFarsi := FilterFarsi + '�����';
               MyWord := '';
            end
            else
            if (UpperCAse(MyWord) = 'LIKE') then
            begin
               FilterFarsi := FilterFarsi + '����';
               MyWord := '';
            end
            else
            if (UpperCAse(MyWord) = 'AND') then
            begin
               FilterFarsi := FilterFarsi + '�';
               MyWord := '';
            end
            else
            if (UpperCAse(MyWord) = 'OR') then
            begin
               FilterFarsi := FilterFarsi + '��';
               MyWord := '';
            end
            else
            begin
               for j := 0 to MyDBGrid.Columns.Count - 1 do
                  if MyDBGrid.Columns[j].Field <> nil then
                  if MyDBGrid.Columns[j].Field.DisplayName = MyWord then
                  begin
                     FilterFarsi := FilterFarsi + MyDBGrid.Columns[j].Title.Caption;
                     MyWord := '';
                     break;
                  end;
            end;
            if (Trim(MyWord) <> '') then
            begin
               FilterFarsi := FilterFarsi + MyWord;
               MyWord := '';
            end
         end;
      end;
   end;

   if Trim(FilterFarsi) = '' then
      MyDBGrid.ShowHint := False
   else
   begin
      FilterFarsi := FilterFarsi + char(254);
      MyDBGrid.ShowHint := True;
      MyDBGrid.Hint := FilterFarsi;
   end;

   MyDBGridRecordCount(MyDBGrid);
   result := FilterFarsi;
}   
end;

procedure MyDBGridRecordCount(MyDBGrid : TDBGrid);
var
   MyForm : TObject;
   MyStatusBar : TStatusBar;
   i : integer;
begin
   GlobalDBGrid := MyDbGrid;
   if MyDBGrid = nil then exit;
   if MyDBGrid.DataSource = nil then exit;
   if MyDBGrid.DataSource.DataSet = nil then exit;
   if MyDBGrid.DataSource.DataSet.Active = False then exit;

   MyForm := MyDBGrid;
   if (Trim(MyDBGrid.Name) = '') OR (MyDBGrid.Parent = nil) then
      exit;

   try
      if ((MyForm <> nil) and (MyForm.ClassType <> nil)) then
         while not (MyForm.InheritsFrom(TForm)) do
            MyForm := TComponent(MyForm).GetParentComponent
      else
         exit;

      if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
          (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
         exit;
   except
      exit;
   end;

   with TForm(MyForm) Do
      For i := 0 to ComponentCount - 1 do
         if (Components[i] is TStatusBar) then
         begin
            MyStatusBar := TStatusBar(Components[i]);
            MyStatusBar.Panels[3].Text :=
               Trim(IntToStr(MyDBGrid.DataSource.DataSet.RecNo)) + '��'
               + Trim(IntToStr(MyDBGrid.DataSource.DataSet.RecordCount));

            if (MyDBGrid.DataSource.DataSet.Filtered) AND (Trim(MyDBGrid.DataSource.DataSet.Filter) <> '') then
               MyStatusBar.Panels[4].Text := MyDBGrid.Hint
            else
               MyStatusBar.Panels[4].Text := '';
            break;
         end;
{
   with TForm(MyForm) Do
      if FindComponent(TForm(MyForm).Name + MyDBGrid.Name + 'SB') <> nil then
      begin
         TStatusBar(FindComponent(TForm(MyForm).Name + MyDBGrid.Name + 'SB')).Panels[0].Text := '����� ����� : ' +
            IntToStr(MyDBGrid.DataSource.DataSet.RecordCount);

         if (MyDBGrid.DataSource.DataSet.Filtered) AND (Trim(MyDBGrid.DataSource.DataSet.Filter) <> '') then
            TStatusBar(FindComponent(TForm(MyForm).Name + MyDBGrid.Name + 'SB')).Panels[1].Text := MyDBGrid.Hint
         else
            TStatusBar(FindComponent(TForm(MyForm).Name + MyDBGrid.Name + 'SB')).Panels[1].Text := '';

      end;
}
end;


procedure FilterClearDBGrid(MyDBGrid : TDBGrid);
var
   Filter : String;
begin
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;

   Filter := '';
   MyDBGrid.DataSource.DataSet.Filtered := False;
   MyDBGrid.DataSource.DataSet.Filter := Filter;
   MyDBGrid.DataSource.DataSet.Filtered := True;
end;


procedure FilterSelectedDBGrid(MyDBGrid : TDBGrid);
var
   i : integer;
   Filter : String;
   FieldName : String;
begin
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;

   Filter := '';

//      MyDBGrid.DataSource.DataSet.DisableControls;
      MyDBGrid.DataSource.DataSet.First;
      if MyDBGrid.SelectedRows.Count > 0  then
      begin
         MyDBGrid.DataSource.DataSet.GotoBookmark(pointer(MyDBGrid.SelectedRows.Items[0]));
         FieldName := MyDBGrid.DataSource.DataSet.Fields[0].DisplayName;
         Filter := '(' + FieldName + ' = ';

         Filter := Filter + MyDBGrid.DataSource.DataSet.Fields[0].AsString;
         Filter := Filter + ')';
         for i := 1 to MyDBGrid.SelectedRows.Count-1  do
         begin
            MyDBGrid.DataSource.DataSet.GotoBookmark(pointer(MyDBGrid.SelectedRows.Items[i]));
            Filter := Filter + ' OR (' + FieldName + ' = ' + MyDBGrid.DataSource.DataSet.Fields[0].AsString + ')';
         end;
      end;

      if Trim(Filter) <> '' then
      begin
         MyDBGrid.DataSource.DataSet.Filtered := False;
         MyDBGrid.DataSource.DataSet.Filter := Filter;
         MyDBGrid.DataSource.DataSet.Filtered := True;
      end;
//      MyDBGrid.DataSource.DataSet.EnableControls;
end;

procedure FilterByEditDBGrid(MyDBGrid : TDBGrid);
begin
   with TEdit.Create(MyDBGrid) do
   begin
      Left := MyDBGrid.Left + 20;
      Top := MyDBGrid.Top + 50;
      Width := 200;
      Height := 30;
      Font.Size := 12;
      Color := $00FFFFC4;
      Text := '����� ���� ����� �� ���� ����';
      parent := MyDBGrid.Parent;
      SetFocus;
      Name := 'FilterEdit';
      OnExit := F_Common.EditSearchExit;
      OnKeyPress := F_Common.EditSearchKeyPress;
      OnChange := F_Common.EditFilterChange;
      OnKeyDown := F_Common.EditSearchKeyDown;
   end;
end;

procedure TF_Common.EditFilter2Change(Sender: TObject);
var
   F1 : TField;
   StrFilter : String;
begin
   StrFilter := '';
   if Trim(TEdit(Sender).Text) <> '' then
   begin
      if GlobalDBGrid <> nil then
      if TADOStoredProc(GlobalDBGrid.DataSource.DataSet).Active then
         with TADOStoredProc(GlobalDBGrid.DataSource.DataSet) do
         begin
            if Trim(Sort) = '' then
               F1 := GlobalDBGrid.Columns[0].Field
            else
               F1 := GlobalDBGrid.Columns[GlobalColumnIndex].Field;

            Filtered := False;

            if (F1.DataType in StringType) then
               StrFilter := F1.DisplayName + ' Like ' + char(39) + '%' + Trim(TEdit(Sender).Text) + '%' +  char(39)
            else if (F1.DataType in NumberType) then
               StrFilter := F1.DisplayName + ' = ' + Trim(TEdit(Sender).Text);
         end;
   end;

   if Trim(StrFilter) <> '' then
      with TADOStoredProc(GlobalDBGrid.DataSource.DataSet) do
      begin
         Filtered := False;
         Filter := Filter + ' AND ' + StrFilter;
         Filtered := True;
      end;
end;



function GetFieldKind(MyField : TField) : integer;
var
   Rs : integer;
begin
   if Copy(MyField.FieldName,1,2) = 'D_' then
      Rs := 3
   else if Copy(MyField.FieldName,1,2) = 'T_' then
      Rs := 4
   else if MyField.DataType in NumberType then
      Rs := 1
   else if MyField.DataType in StringType then
      Rs := 2;
   result := Rs;
end;


procedure FilterDBGrid(MyDBGrid : TDBGrid);
var
   i : integer;
   Filter : String;
begin
   GlobalDBGrid := MyDbGrid;
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;

   EmptyFieldList;
   for i := 0 to MyDBGrid.Columns.Count - 1 do
      if MyDBGrid.Columns[i].Field <> nil then
         AddFieldList(
               GetFieldKind(MyDBGrid.Columns[i].Field),
               100,
               '',
               MyDBGrid.Columns[i].Field.DisplayName,
               MyDBGrid.Columns[i].Title.Caption);

//   Filter := MyDBGrid.DataSource.DataSet.Filter;
   ShowFilter(Filter);

   if Trim(Filter) <> '' then
   begin
      MyDBGrid.DataSource.DataSet.Filtered := False;
      MyDBGrid.DataSource.DataSet.Filter := Filter;
      MyDBGrid.DataSource.DataSet.Filtered := True;
   end;
end;

procedure FullComboBoxCons(MyComboBox : TComboBox; KindCons : Integer);
var
   ConsIndx : integer;
begin
   ConsIndx := MyComboBox.ItemIndex;
   MyComboBox.Items.Clear;
   Case KindCons of
   1,3,4 :
      begin
         with MyComboBox.Items do
         begin
            Add('�����');
            Add('��ѐ�� ��');
            Add('��ѐ�� � �����');
            Add('���');
            Add('����� ��');
            Add('����� � �����');
            Add('�����');
         end;
         MyComboBox.Tag := 1;
      end;
   2 :
      begin
         with MyComboBox.Items do
         begin
            Add('�����');
            Add('��ѐ�� ��');
            Add('��ѐ�� � �����');
            Add('���');
            Add('��� ��');
            Add('����');
            Add('���� ��');
            Add('����� ��');
            Add('����� � �����');
            Add('�����');
         end;
         MyComboBox.Tag := 2;
      end;
   end;
{
   Case KindCons of
   1,2 :
      begin
         Val1.EditMask := '';
         Val2.EditMask := '';
      end;
   3 :
      begin
         Val1.EditMask := '!99/99/99;1;_';
         Val2.EditMask := '!99/99/99;1;_';
      end;
   4 :
      begin
         Val1.EditMask := '!99:99;1;_';
         Val2.EditMask := '!99:99;1;_';
      end;
   end;
   if LBCons.Items.Count > ConsIndx then
      LBCons.ItemIndex := ConsIndx
   else
      LBCons.ItemIndex := 0;
}
end;


procedure FilterDBGrid2(MyDBGrid : TDBGrid);
var
   i : integer;
   Filter : String;
   TopPosition : integer;
   F : TF_Filter2;
   FormResult : Boolean;
   FormTabOrder : integer;
   MyCB : TComboBox;
begin
   GlobalDBGrid := MyDbGrid;
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;

   TopPosition := 10;
   FormTabOrder := 1;

   F := TF_Filter2.Create(Application);
   F.ConsFieldCount := MyDBGrid.Columns.Count;

   for i := 0 to MyDBGrid.Columns.Count - 1 do
      if MyDBGrid.Columns[i].Field <> nil then
      begin
         with TLabel.Create(F) do
         begin
            Left := 375;
            Top := TopPosition;
            Width := 140;
            Height := 20;
            AutoSize := False;
            Caption := MyDBGrid.Columns[i].Title.Caption;
            Hint := MyDBGrid.Columns[i].Field.DisplayName;
            Font := DBGridTitleFont;
            Parent := F.Panel1;
            Name := 'Label' + IntToStr(i+1);
         end;

         MyCB := TComboBox.Create(F);
         with MyCB do
         begin
            Left := 240;
            Top := TopPosition;
            Width := 120;
            Height := 21;
            ItemHeight := 16;
        	   DropDownCount := 10;
            TabOrder := FormTabOrder;
         	TabStop := False;
            Parent := F.Panel1;
        	   Style := csDropDownList;
        	   ParentColor := False;
         	ControlStyle := [csCaptureMouse, csSetCaption, csDoubleClicks, csFixedHeight, csReflector];
            Name := 'ComboBox' + IntToStr(i+1);
            Font := DBGridTitleFont;
            Text := '';
            if (MyDBGrid.Columns[i].Field.DataType in NumberType) then
            begin
               FullComboBoxCons(MyCB,1);
               MyCB.ItemIndex := 0;
            end
            else
            begin
               FullComboBoxCons(MyCB,2);
               MyCB.ItemIndex := 5;
            end
         end;
         FormTabOrder := FormTabOrder + 1;

         with TMaskEdit.Create(F) do
         begin
            Left := 133;
            Top := TopPosition;
            Width := 80;
            Height := 21;
            TabOrder := FormTabOrder;
            Parent := F.Panel1;
            Name := 'MaskEdit' + IntToStr(i+1);
            Text := '';
            Font := DBGridTitleFont;

            if (Copy(MyDBGrid.Columns[i].Field.DisplayName, 1,2) = 'D_') then
               EditMask := '!99/99/00;1;_';

            if (Copy(MyDBGrid.Columns[i].Field.DisplayName, 1,2) = 'T_') then
               EditMask := '!90:00;1;_';

         end;
         FormTabOrder := FormTabOrder + 1;

         with TMaskEdit.Create(F) do
         begin
            Left := 13;
            Top := TopPosition;
            Width := 80;
            Height := 21;
         	TabStop := False;
            Parent := F.Panel1;
            Name := 'MaskEdit' + IntToStr(i+101);
            Text := '';
            Font := DBGridTitleFont;

            if (Copy(MyDBGrid.Columns[i].Field.DisplayName, 1,2) = 'D_') then
               EditMask := '!99/99/99;1;_';

            if (Copy(MyDBGrid.Columns[i].Field.DisplayName, 1,2) = 'T_') then
               EditMask := '!99:99;1;_';
         end;

         TopPosition := TopPosition + 30;
      end;

   F.BitBtn1.TabOrder := FormTabOrder;
   F.BitBtn2.TabOrder := FormTabOrder + 1;
   if TopPosition + 100 > 550 then
      F.Height := 550
   else
      F.Height := TopPosition + 100;
   F.ShowModal;

   if F.ModalResult = mrOk then
   begin
      FormResult := True;
      Filter := F.SqlFilter;
   end
   else
   begin
      FormResult := False;
      Filter := '';
   end;

   F.Free;

   if Trim(Filter) <> '' then
   begin
      MyDBGrid.DataSource.DataSet.Filtered := False;
      MyDBGrid.DataSource.DataSet.Filter := Filter;
      MyDBGrid.DataSource.DataSet.Filtered := True;
   end;
end;


procedure FilterDBGrid3(MyDBGrid : TDBGrid);
var
   i : integer;
   Filter : String;
   TopPosition : integer;
   F : TF_Filter3;
   FormResult : Boolean;
   FormTabOrder : integer;
   MyCB : TComboBox;
begin
   GlobalDBGrid := MyDbGrid;
   if ((Not MyDBGrid.DataSource.DataSet.Active ) OR
       (MyDBGrid.DataSource.DataSet.RecordCount = 0)) then
      exit;

   TopPosition := 10;
   FormTabOrder := 1;

   F := TF_Filter3.Create(Application);
   F.ConsFieldCount := MyDBGrid.Columns.Count;

   for i := 0 to MyDBGrid.Columns.Count - 1 do
      if MyDBGrid.Columns[i].Field <> nil then
      begin
         with TLabel.Create(F) do
         begin
            Left := 90;
            Top := TopPosition;
            Width := 140;
            Height := 20;
            AutoSize := False;
            Caption := MyDBGrid.Columns[i].Title.Caption;
            Hint := MyDBGrid.Columns[i].Field.DisplayName;
            Parent := F.Panel1;
            Font := DBGridTitleFont;
            Name := 'Label' + IntToStr(i+1);
         end;

         with TMaskEdit.Create(F) do
         begin
            Left := 20;
            Top := TopPosition;
            Width := 80;
            Height := 21;
            TabOrder := FormTabOrder;
            Parent := F.Panel1;
            Name := 'MaskEdit' + IntToStr(i+1);
            Text := '';
            Font := DBGridTitleFont;

            if (MyDBGrid.Columns[i].Field.DataType in NumberType) then
               Tag := 1
            else
               Tag := 2;

            if (Copy(MyDBGrid.Columns[i].Field.DisplayName, 1,2) = 'D_') then
               EditMask := '!99/99/00;1;_';

            if (Copy(MyDBGrid.Columns[i].Field.DisplayName, 1,2) = 'T_') then
               EditMask := '!90:00;1;_';

         end;
         FormTabOrder := FormTabOrder + 1;
         TopPosition := TopPosition + 30;
      end;

   TopPosition := TopPosition - 20;

   F.BitBtn1.TabOrder := FormTabOrder;
   F.BitBtn2.TabOrder := FormTabOrder + 1;
   if TopPosition + 100 > 550 then
      F.Height := 550
   else
      F.Height := TopPosition + 100;
   F.ShowModal;

   if F.ModalResult = mrOk then
   begin
      FormResult := True;
      Filter := F.SqlFilter;
   end
   else
   begin
      FormResult := False;
      Filter := '';
   end;

   F.Free;

   if Trim(Filter) <> '' then
   begin
      MyDBGrid.DataSource.DataSet.Filtered := False;
      MyDBGrid.DataSource.DataSet.Filter := Filter;
      MyDBGrid.DataSource.DataSet.Filtered := True;
   end;
end;


function ShowFilter(var SqlFilter : String): Boolean;
var
   F : TF_Filter;
   FormResult : Boolean;
begin
   F := TF_Filter.Create(Application);

   if SqlFilter <> '' then
      F.Q1.Items[0] := SqlFilter;
   if FarsiFilter <> nil then
      F.LBTotal.Items := FarsiFilter;
   F.ShowModal;
   if F.ModalResult = mrOk then
      FormResult := True
   else
      FormResult := False;

   SqlFilter := F.SqlFilter;
   F.Free;
   Result := FormResult;
end;
//------------------------------------------------------------------------------

procedure TF_Common.FormDBGridTitleClick(Column: TColumn);
begin
   GlobalDBGrid := TDBGrid(Column.Grid);
   SortDBGrid1(Column);
end;

procedure TF_Common.EditSearchExit(Sender: TObject);
begin
   TEdit(Sender).Free;
end;

procedure TF_Common.EditSearchKeyPress(Sender: TObject; var Key: Char);
begin
{
   if Key = #13 then
      TForm(GlobalFormName).Perform(7388420, vk_Tab, 0);
}      
//      TForm(GlobalFormName).ActiveControl := GlobalDBGridOld;
end;

procedure TF_Common.EditSearchChange(Sender: TObject);
var
   F1 : TField;
begin
   if Trim(TEdit(Sender).Text) <> '' then
      if GlobalDBGrid <> nil then
         with TADOStoredProc(GlobalDBGrid.DataSource.DataSet) do
         begin
            if Trim(Sort) = '' then
               F1 := GlobalDBGrid.Columns[0].Field
            else
               F1 := GlobalDBGrid.Columns[GlobalColumnIndex].Field;

            if (F1.DataType in StringType) then
               Locate(F1.DisplayName, TEdit(Sender).Text, [loPartialKey])
            else if (F1.DataType in NumberType) then
               Locate(F1.DisplayName, TEdit(Sender).Text, [loPartialKey])
         end;
end;

procedure TF_Common.EditFilterChange(Sender: TObject);
var
   F1 : TField;
   StrFilter : String;
begin
   if Trim(TEdit(Sender).Text) <> '' then
   begin
      if GlobalDBGrid <> nil then
         with TADOStoredProc(GlobalDBGrid.DataSource.DataSet) do
         begin
            if Trim(Sort) = '' then
               F1 := GlobalDBGrid.Columns[0].Field
            else
               F1 := GlobalDBGrid.Columns[GlobalColumnIndex].Field;
               
            Filtered := False;

            if (F1.DataType in StringType) then
               StrFilter := '[' + F1.DisplayName + ']' + ' Like ' + char(39) + '%' + Trim(TEdit(Sender).Text) + '%' +  char(39)
            else if (F1.DataType in NumberType) then
               StrFilter := '[' + F1.DisplayName + ']' + ' = ' + Trim(TEdit(Sender).Text);

            Filter := StrFilter;
            Filtered := True;
         end;
   end
   else
      with TADOStoredProc(GlobalDBGrid.DataSource.DataSet) do
         Filtered := False;
end;


procedure TF_Common.FormDBGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   GlobalDBGrid := TDBGrid(Sender);

   if (key = vk_F3) then
      FilterDBGrid(TDBGrid(Sender))
   else if (key = vk_F6) then
      FilterByEditDBGrid(TDBGrid(Sender))
   else if (key = vk_F7) then
      SearchDBGrid(TDBGrid(Sender))
   else if ((key = vk_F9) and (Shift = [ssCtrl])) then
      FilterDBGrid2(TDBGrid(Sender))
   else if (key = vk_F9) then
      FilterDBGrid3(TDBGrid(Sender))
   else if (key = vk_F10) then
      PrintDBGrid(TDBGrid(Sender))
   else if (key = vk_F12) then
      OptionDBGrid(TDBGrid(Sender));
end;


Procedure TF_Common.UserCanAccessField(NObject, NParent : String; var K_Access : integer);
var
   i : integer;
begin
   If CheckPermission then
   begin
      Pub_BROWSE_Role_Field3.Close;

      for i:= 0 to Pub_BROWSE_Role_Field3.Parameters.Count-1 do
         Pub_BROWSE_Role_Field3.Parameters[i].Value := null;
      Pub_BROWSE_Role_Field3.Parameters.ParamByName('@N_Form').Value := Trim(NParent);
      Pub_BROWSE_Role_Field3.Parameters.ParamByName('@N_Filed').Value := Trim(NObject);
      Pub_BROWSE_Role_Field3.Parameters.ParamByName('@Srl_System').Value := Srl_System;
      Pub_BROWSE_Role_Field3.Parameters.ParamByName('@Srl_User').Value := Srl_User;
      try
         Pub_BROWSE_Role_Field3.ExecProc;
         K_Access := Pub_BROWSE_Role_Field3.Parameters.ParamByName('@KAccess').Value;
         if IsFullAccess = 20 then
            K_Access := 6;
      Except
         ShowMessage('��� �� ���� ���� ���� ����');
      end;
   end
   else
      K_Access := 6;
end;


Function TF_Common.GetSystemVersion : integer;
begin
   NullAdoSPParameters(SP_GetSystemVersion);

   SP_GetSystemVersion.Parameters.ParamByName('@Srl_System').value := Srl_System;
   try
      SP_GetSystemVersion.ExecProc;
      OldSystem_Version := SP_GetSystemVersion.Parameters.ParamByName('@Last_Version').value;
   except
      OldSystem_Version := -1;
      ShowMessage('����� �� ���� ������');
   end;
   Result := OldSystem_Version;
end;

procedure TF_Common.GetSystemInfo;
begin

{   if Not CheckPermission then
   begin
      N_Company := 'Markaz modiriyat hozeh';
      Is_System_Periodic := False;

      Case Srl_System Of
      -1 :           N_System := 'Permission';
      1 :            N_System := 'Mis';
      2 :            N_System := 'Dabir';
      3 :            N_System := 'Sonat';
      4 :            N_System := 'Exam';
      5 :            N_System := 'Paziresh';
      7 :            N_System := 'Bime';
      9 :            N_System := 'School';
      10 :           N_System := 'Research';
      11 :           N_System := 'City';
      12 :           N_System := 'Mokatebat';
      14 :           N_System := 'Ravabet';
      15 :           N_System := 'Mashmool';
      end;
   end
   else
   begin
      NullAdoSPParameters(SP_GetSystemInfo);
      SP_GetSystemInfo.Parameters.ParamByName('@Srl_System').value := Srl_System;
      try
         SP_GetSystemInfo.ExecProc;
         N_Company := SP_GetSystemInfo.Parameters.ParamByName('@N_Company').value;
         N_Station := SP_GetSystemInfo.Parameters.ParamByName('@N_Station').value;
         N_System := SP_GetSystemInfo.Parameters.ParamByName('@N_System').value;
         Cap_System := SP_GetSystemInfo.Parameters.ParamByName('@Cap_System').value;
         Is_System_Periodic := SP_GetSystemInfo.Parameters.ParamByName('@Is_Periodic').value;

         D_Expire := SP_GetSystemInfo.Parameters.ParamByName('@D_Expire').value;
         Qty_MaxRun := SP_GetSystemInfo.Parameters.ParamByName('@Qty_MaxRun').value;
         Qty_Run := SP_GetSystemInfo.Parameters.ParamByName('@Qty_Run').value;
         if D_Expire < CurDate then
            Application.Terminate;

         if ((Srl_System = 1) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 2) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 3) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 4) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 5) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 7) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 9) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 10) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 11) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 12) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 14) AND (Qty_MaxRun <> 999999))
         OR ((Srl_System = 15) AND (Qty_MaxRun <> 999999))
         then
            Application.Terminate;

         if Qty_MaxRun < Qty_Run then
            Application.Terminate;

      except
         N_Company := '_';
         N_System := '_';
         Is_System_Periodic := False;
         ShowMessage('����� �� ������ �����');
      end;
   end;

   if Trim(N_System) = '' then
   begin
      ShowMessage('��� ����� ���� ����');
      Application.Terminate;
   end;

   Application.Title := N_Company + ' : ' + N_System + ' : ' + N_User;  }

end;



Procedure TF_Common.UserCanAccessSystem;
begin
   If Not CheckPermission then
      exit;

   NullAdoSPParameters(Pub_BROWSE_Role_Field1);

   Pub_BROWSE_Role_Field1.Parameters.ParamByName('@C_User').value := C_User;
   Pub_BROWSE_Role_Field1.Parameters.ParamByName('@Pass').value := Pass_User;
   try
      Pub_BROWSE_Role_Field1.ExecProc;
      if Pub_BROWSE_Role_Field1.Parameters.ParamByName('@KAccess').value > 0 then
      begin
         Srl_User := Pub_BROWSE_Role_Field1.Parameters.ParamByName('@SrlUser').value;
//         Srl_Employe := Pub_BROWSE_Role_Field1.Parameters.ParamByName('@SrlEmploye').value;
         N_User := Pub_BROWSE_Role_Field1.Parameters.ParamByName('@NUser').value;
         Msg_User := Pub_BROWSE_Role_Field1.Parameters.ParamByName('@MsgUser').value;
         if Trim(Msg_User) <> '' then
            ShowMessage(Msg_User);
      end
      else
         Srl_User := -1;
   except
      Srl_User := -1;
      ShowMessage('��� �� ����� �������');
   end;
end;

Procedure TF_Common.InitUser;
begin
   C_User := '-1';
   Pass_User := '_';
   N_User := '_';
   Srl_Employe := -1;

   If Not CheckPermission then
      exit;

   If Srl_User <= 0 then
      exit;

   NullAdoSPParameters(Pub_InitUser);
   with Pub_InitUser do
   begin
      Parameters.ParamByName('@SrlUser').value := Srl_User;
      try
         ExecProc;
         C_User := Parameters.ParamByName('@C_User').value;
         Pass_User := Parameters.ParamByName('@Pass').value;
         N_User := Parameters.ParamByName('@NUser').value;
         Srl_Employe := Parameters.ParamByName('@SrlEmploye').value;

      except
         ShowMessage('��� �� ����� �������');
      end;
   end;
end;

Procedure TF_Common.InitSystem;
begin
 //  GetSystemInfo;
end;

Procedure TF_Common.InitCycle;
begin
  // if F1.ValueExists(N_System, 'SrlCurCycle') then
 //     SrlCurCycle := F1.ReadInteger(N_System, 'SrlCurCycle', -1)
  // else
   //   F1.WriteInteger(N_System, 'SrlCurCycle', -1);

   if SrlCurCycle > 0 then
   begin
      NullAdoSPParameters(F_Common.sp_FullCycleInfo);
      with F_Common.sp_FullCycleInfo do
      begin
         Parameters.ParamByName('@SrlCurCycle').Value := SrlCurCycle;
         ExecProc;

         if Parameters.FindParam('@CCurCycle') <> nil then
            CCurCycle := Parameters.ParamByName('@CCurCycle').Value
         else
            CCurCycle := -1;

         if Parameters.FindParam('@NCurCycle') <> nil then
            NCurCycle := Parameters.ParamByName('@NCurCycle').Value
         else
            NCurCycle := '';

         if Parameters.FindParam('@KCurCycle') <> nil then
            KCurCycle := Parameters.ParamByName('@KCurCycle').Value
         else
            KCurCycle := -1;
      end
   end
   else
   begin
      CCurCycle := -1;
      NCurCycle := '_';
      KCurCycle := -1;
   end;
end;

Procedure AddComponentToTable(Sender: TObject);
Var
   N_Form : String;
   Cap_Form : String;
   N_Field : String;
   Type_Field : String;
   Cap_Field : String;
   i, j, k, m : integer;
   a : TForm;
Begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      a := TForm(Sender);
      N_Form := a.name;
//      N_Form := Copy(Sender.ClassName, 2, Length(Sender.ClassName) -1);
      Cap_Form := TForm(Sender).Caption;
      For i := 0 to ComponentCount - 1 do
      begin
         if (Components[i] is TControl) then
         begin
            N_Field := Components[i].Name;
            Type_Field := Components[i].ClassName;

            if (Components[i] is TLabel) then
               Cap_Field := TLabel(Components[i]).Caption
            else if (Components[i] is TButton) then
               Cap_Field := TButton(Components[i]).Caption
            else if (Components[i] is TSpeedButton) then
            begin
               Cap_Field := TSpeedButton(Components[i]).Caption;
               if Trim(Cap_Field) = '' then
                  Cap_Field := TSpeedButton(Components[i]).Hint;
            end
            else if (Components[i] is TCheckBox) then
               Cap_Field := TCheckBox(Components[i]).Caption;

            if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
            begin
               NullAdoSPParameters(F_Common.Pub_INSERT_System_Form_Field1);

               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Srl_System').VALUE := Srl_System;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Form_1').VALUE := N_Form;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Form').VALUE := Cap_Form;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Field_2').VALUE := N_Field;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Type_Field').VALUE := Type_Field;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Field_3').VALUE := Cap_Field;
               try
                  F_Common.Pub_INSERT_System_Form_Field1.ExecProc;
                  N_Field := '';
                  Cap_Field := '';
               Except
                  ShowMessage('��� �� ����� ���� �����');
                  exit;
               end;
            end;
         end
         else if (Components[i] is TMainMenu) then
         begin
            N_Field := TMainMenu(Components[i]).Name;
            Cap_Field := '���� ���� �����';
            Type_Field := TMainMenu(Components[i]).ClassName;

            if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
            begin
               NullAdoSPParameters(F_Common.Pub_INSERT_System_Form_Field1);

               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Srl_System').VALUE := Srl_System;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Form_1').VALUE := N_Form;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Form').VALUE := Cap_Form;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Field_2').VALUE := N_Field;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Type_Field').VALUE := Type_Field;
               F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Field_3').VALUE := Cap_Field;
               try
                  F_Common.Pub_INSERT_System_Form_Field1.ExecProc;
                  N_Field := '';
                  Cap_Field := '';
               Except
                  ShowMessage('��� �� ����� ���� �����');
                  exit;
               end;
            end;


            for k := 0 to TMainMenu(Components[i]).Items.Count - 1 do
            begin
               N_Field := TMainMenu(Components[i]).Items[k].Name;
               Cap_Field := TMainMenu(Components[i]).Items[k].Caption;
               Type_Field := TMainMenu(Components[i]).Items[k].ClassName;

               if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
               begin
                  NullAdoSPParameters(F_Common.Pub_INSERT_System_Form_Field1);

                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Srl_System').VALUE := Srl_System;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Form_1').VALUE := N_Form;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Form').VALUE := Cap_Form;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Field_2').VALUE := N_Field;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Type_Field').VALUE := Type_Field;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Field_3').VALUE := Cap_Field;
                  try
                     F_Common.Pub_INSERT_System_Form_Field1.ExecProc;
                     N_Field := '';
                     Cap_Field := '';
                  Except
                     ShowMessage('��� �� ����� ���� �����');
                     exit;
                  end;
               end;

               with TMainMenu(Components[i]).Items[k] do
               for m := 0 to Count - 1 do
               begin
                  N_Field := Items[m].Name;
                  Cap_Field := Items[m].Caption;
                  Type_Field := Items[m].ClassName;

                  if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
                  begin
                     NullAdoSPParameters(F_Common.Pub_INSERT_System_Form_Field1);

                     F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Srl_System').VALUE := Srl_System;
                     F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Form_1').VALUE := N_Form;
                     F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Form').VALUE := Cap_Form;
                     F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Field_2').VALUE := N_Field;
                     F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Type_Field').VALUE := Type_Field;
                     F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Field_3').VALUE := Cap_Field;
                     try
                        F_Common.Pub_INSERT_System_Form_Field1.ExecProc;
                        N_Field := '';
                        Cap_Field := '';

                     Except
                        ShowMessage('��� �� ����� ���� �����');
                        exit;
                     end;
                  end;
               end;
            end;
         end
         else if (Components[i] is TMenuItem) then
         begin
            N_Field := TMenuItem(Components[i]).Name;
            Cap_Field := TMenuItem(Components[i]).Caption;

               if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
               begin
                  NullAdoSPParameters(F_Common.Pub_INSERT_System_Form_Field1);

                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Srl_System').VALUE := Srl_System;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Form_1').VALUE := N_Form;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Form').VALUE := Cap_Form;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@N_Field_2').VALUE := N_Field;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Type_Field').VALUE := Type_Field;
                  F_Common.Pub_INSERT_System_Form_Field1.Parameters.ParamByName('@Cap_Field_3').VALUE := Cap_Field;
                  try
                     F_Common.Pub_INSERT_System_Form_Field1.ExecProc;
                     N_Field := '';
                     Cap_Field := '';
                  Except
                     ShowMessage('��� �� ����� ���� �����');
                     exit;
                  end;
               end;


            end;

      end;
   end;
end;



procedure AddFormSPToDB(MySp : TADOStoredProc; IsRun : integer);
var
   NSP, CapSP : String;
   i : integer;
begin
   NSP := Trim(MySp.ProcedureName);
   if Pos(';', NSP) > 0 then
      NSP := Copy(MySp.ProcedureName,1,Length(MySp.ProcedureName)- 2);

   CapSP := MySp.Name;

   //  Do not use NullAdoSpParameters For Recurcice
   if Srl_System = -1 then
      F_Common.INSERT_Form_Proc.Connection.defaultDataBase := 'Permision';

   for i := 0 to F_Common.INSERT_Form_Proc.Parameters.Count -1 do
      F_Common.INSERT_Form_Proc.Parameters[i].Value := null;

   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@Srl_System').VALUE := Srl_System;
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@N_Form').VALUE := GlobalFormName;
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@Cap_Form').VALUE := GlobalFormCaption;
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@N_Proc_2').VALUE := NSP;
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@Cap_Proc').VALUE := CapSP;
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@Dis_Proc_3').VALUE := '';
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@K_Proc_6').VALUE := IsRun;
   F_Common.INSERT_Form_Proc.Parameters.ParamByName('@Srl_Connection').VALUE := Srl_Connection;
   try
      F_Common.INSERT_Form_Proc.ExecProc;
   Except
      ShowMessage('��� �� ����� ���� SP');
   end;
end;

procedure AddSPToText(MySp : TADOStoredProc);
var
   NSP : String;
   i : integer;
   TxtProc : String;
   SPTextFile : TextFile;
   IsExists : integer;
begin
   if ParamCount <> 0 then exit;
   if Not IS_Save_StoredProcedure then
   begin
      if FileExists(PChar(AppPath + GlobalFormName + '.TXT')) then
         DeleteFile(PChar(AppPath + GlobalFormName + '.TXT'));
      exit;
   end;


   NSP := Trim(MySp.ProcedureName);
   if Pos(';', NSP) > 0 then
      NSP := Copy(MySp.ProcedureName,1,Length(MySp.ProcedureName)- 2);

   //  Do not use NullAdoSpParameters For Recurcice
   for i := 0 to F_Common.Common_SP_GetPRocedureText.Parameters.Count -1 do
      F_Common.Common_SP_GetPRocedureText.Parameters[i].Value := null;

   F_Common.Common_SP_GetPRocedureText.Parameters.ParamByName('@NForm').VALUE := GlobalFormName;
   F_Common.Common_SP_GetPRocedureText.Parameters.ParamByName('@NProc').VALUE := NSP;
   try
      F_Common.Common_SP_GetPRocedureText.ExecProc;
      IsExists := F_Common.Common_SP_GetPRocedureText.Parameters.ParamByName('@IsExists').VALUE;
      TxtProc := F_Common.Common_SP_GetPRocedureText.Parameters.ParamByName('@TxtProc').VALUE;

   Except
      ShowMessage('��� �� ����� ���� SP');
   end;

   AssignFile(SPTextFile, AppPath + GlobalFormName + '.TXT');
   Append(SPTextFile);

   if IsExists = 1 then
   begin
      Writeln(SPTextFile, '--============================================================================');
      Writeln(SPTextFile, '--=======================' + NSP + '=============================');
      Writeln(SPTextFile, '--============================================================================');
      Writeln(SPTextFile, TxtProc);
      Writeln(SPTextFile, '--============================================================================');
      Writeln(SPTextFile, char(13));
   end
   else
   begin
      Writeln(SPTextFile, '--============================================================================');
      Writeln(SPTextFile, '--' + NSP + ' : ' + TxtProc);
      Writeln(SPTextFile, '--============================================================================');
      Writeln(SPTextFile, char(13));
   end;

   Flush(SPTextFile);
   CloseFile(SPTextFile);
end;

procedure EditCodeExit(swState : TState; MyEdit : TEdit; MyLabel : TLabel);
begin
   if swState = sView then
      exit;

   if (MyLabel.Caption = '..........') and (Trim(MyEdit.Text) <> '') then
   begin
      ShowMessage('�� ���� ��� ���� �����');
      MyEdit.SetFocus;
   end;
end;


procedure SBFindClick(swState : TState; MyEdit : TEdit;
   MyLabel : TLabel; var MySerial1 : integer; MySerial2, C_A_F : integer;
   TableName, MyKindFieldName : String);overload;
var
   F : TF_Find;
begin
   if (swState = sAdd) or (swState = sEdit) or (swState = sFilter) then
   begin
      F := TF_Find.Create(MyEdit);
      F.ADOStoredProcFind.ProcedureName := 'sp_browse_T_Table;1';
      F.TableName := TableName;
      F.KindParam := MySerial2;
      F.FieldParam := MyKindFieldName;
      F.Count_Add_Fields := C_A_F;
      F.DBGrid1.OnTitleClick := F_Common.FormDBGridTitleClick;
      F.ShowModal;
      if F.FormResult = 1 then
      begin
         MyEdit.Text := F.CFind;
         MyLabel.Caption := F.NFind;
         MySerial1 := F.SrlFind;
      end;
      F.Free;
   end;
end;

procedure SBFindClick(swState : TState; MyEdit : TEdit;
   MyLabel : TLabel; var MySerial1 : integer; C_A_F : integer;
   TableName : String) overload;
var
   F : TF_Find;
begin
   if (swState = sAdd) or (swState = sEdit) or (swState = sFilter) then
   begin
      F := TF_Find.Create(MyEdit);
      F.ADOStoredProcFind.ProcedureName := 'sp_browse_T_Table;1';
      F.TableName := TableName;
      F.KindParam := -1;
      F.FieldParam := '';
      F.Count_Add_Fields := C_A_F;
      F.DBGrid1.OnTitleClick := F_Common.FormDBGridTitleClick;
      F.ShowModal;
      if F.FormResult = 1 then
      begin
         MyEdit.Text := F.CFind;
         MyLabel.Caption := F.NFind;
         MySerial1 := F.SrlFind;
      end;
      F.Free;
   end;
end;


procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel;
   var MySerial1 : integer; SrlDataSet : integer);overload;
var
   F : TF_Find2;
begin
   if (swState = sAdd) or (swState = sEdit) or (swState = sFilter) then
   begin
      F := TF_Find2.Create(MyEdit);
      F.SrlDataSet := SrlDataSet;
      F.showmodal;
      if F.FormResult = 1 then
      begin
         MyEdit.Text := F.CFind;
         MyLabel.Caption := F.NFind;
         MySerial1 := F.SrlFind;
      end;
      F.Free;
   end;
end;

procedure SBFindClick(swState : TState; MyEdit : TEdit; MyLabel : TLabel;
   var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,
   Val_Param4 : integer);overload;
var
   F : TF_Find3;
begin
   if (swState = sAdd) or (swState = sEdit) or (swState = sFilter) then
   begin
      F := TF_Find3.Create(MyEdit);
      F.SrlDataSet := SrlDataSet;
      F.Val_Param1 := Val_Param1;
      F.Val_Param2 := Val_Param2;
      F.Val_Param3 := Val_Param3;
      F.Val_Param4 := Val_Param4;
      F.ShowModal;
      if F.FormResult = 1 then
      begin
         MyEdit.Text := F.CFind;
         MyLabel.Caption := F.NFind;
         MySerial1 := F.SrlFind;
      end;
      F.Free;
   end;
end;

procedure SBFindClick(swState : TState; MyEdit : TEdit;
   SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;
var
   F : TF_Find3;
begin
   if (swState = sAdd) or (swState = sEdit) or (swState = sFilter) then
   begin
      F := TF_Find3.Create(MyEdit);
      F.SrlDataSet := SrlDataSet;
      F.Val_Param1 := Val_Param1;
      F.Val_Param2 := Val_Param2;
      F.Val_Param3 := Val_Param3;
      F.Val_Param4 := Val_Param4;
      F.showmodal;
      if F.FormResult = 1 then
         MyEdit.Text := F.NFind;
      F.Free;
   end;
end;

procedure SBFindClickPer(swState : TState; MyEdit : TEdit; MyLabel : TLabel;
   var MySerial1 : integer);
var
   F : TF_Find_Per;
begin
   if (swState = sAdd) or (swState = sEdit) or (swState = sFilter) then
   begin
      F := TF_Find_Per.Create(MyEdit);
      F.showmodal;
      if F.FormResult = 1 then
      begin
         MyEdit.Text := F.CFind;
         MyLabel.Caption := F.NFind;
         MySerial1 := F.SrlFind;
      end;
      F.Free;
   end;
end;


Function FindSrlName(swState : TState; var MySerial1 : integer;
    MySerial2, MyCode : integer;  TableName, MyKindFieldName : String) : String;overload;
var
   i : integer;
   MyName : String;
begin
   with F_Common.sp_Find_T_Table do
   begin
      Connection := F_Common.SystemConnection;
      Parameters.Refresh;

      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@TableName').Value := TableName;
      Parameters.ParamBYName('@FCode').VALUE := MyCode;
      Parameters.ParamByName('@FieldParamName').value := MyKindFieldName;
      Parameters.ParamByName('@FieldParamValue').value := MySerial2;
      try
         ExecProc;
         MyName := Parameters.ParamBYName('@FName').VALUE;
         MySerial1 := Parameters.ParamBYName('@FSerial').VALUE;
      except
         ShowMessage('��� �� ��� ');
      end;
      Result := MyName;
   end
end;
//------------------------------------------------------------------------------

Function FindSrlName(swState : TState; var MySerial1 : integer;
    MySerial2, MyCode : integer; SrlDataSet : integer) : String;overload;
var
   MyName : String;
begin
  NullAdoSPParameters(F_Common.sp_Find_T_Table2);
  with F_Common.sp_Find_T_Table2 do
  begin
    Parameters.ParamByName('@Srl_Table').Value := SrlDataSet;
    Parameters.ParamByName('@FCode').Value := MyCode;
    try
      ExecProc;
      MyName := Parameters.ParamBYName('@FName').VALUE;
      MySerial1 := Parameters.ParamBYName('@FSerial').VALUE;
    except
      ShowMessage('��� �� ��� ');
    end;
    Result := MyName;
  end
end;
//------------------------------------------------------------------------------

Function FindSrlName(swState : TState; var MySerial1 : integer;
    MySerial2, MyCode : integer; SrlDataSet, Val_Param1, Val_Param2,
    Val_Param3, Val_Param4 : integer) : String;overload;
var
   MyName : String;
begin
   with F_Common.sp_Find_T_Table3 do
   begin
      NullAdoSPParameters(F_Common.sp_Find_T_Table3);

      Parameters.ParamByName('@Srl_Table').Value := SrlDataSet;
      Parameters.ParamByName('@Val_Param1').Value := Val_Param1;
      Parameters.ParamByName('@Val_Param2').Value := Val_Param2;
      Parameters.ParamByName('@Val_Param3').Value := Val_Param3;
      Parameters.ParamByName('@Val_Param4').Value := Val_Param4;
      Parameters.ParamByName('@FCode').Value := MyCode;
      try
         ExecProc;
         MyName := Parameters.ParamBYName('@FName').VALUE;
         MySerial1 := Parameters.ParamBYName('@FSerial').VALUE;
      except
         ShowMessage('��� �� ��� ');
      end;
      Result := MyName;
   end
end;
//------------------------------------------------------------------------------


procedure EditCodeChange(MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer;
   MySerial2 : integer; swState : TState; TableName , MyKindFieldName : String);overload ;
begin
//   if swState = sView then
  //    exit;

   IF TRIM(MyEdit.Text) = '' THEN
   Begin
      MyLabel.Caption := '..........';
      MySerial1 := -1;
   End
   Else
      if (swState = sAdd) or (swState = sEdit) then
         MyLabel.Caption := FindSrlName(swState, MySerial1, MySerial2,
               StrToInt(MyEdit.Text), TableName, MyKindFieldName)
end;

procedure EditCodeChange(MyEdit : TEdit; MyLabel : TLabel; var MySerial1 : integer;
   swState : TState; TableName : String);overload ;
begin
//   if swState = sView then
  //    exit;

   IF TRIM(MyEdit.Text) = '' THEN
   Begin
      MyLabel.Caption := '..........';
      MySerial1 := -1;
   End
   Else
      if (swState = sAdd) or (swState = sEdit) then
         MyLabel.Caption := FindSrlName(swState, MySerial1, -1,
               StrToInt(MyEdit.Text), TableName, '')
end;

procedure EditCodeChange(swState : TState; MyEdit : TEdit; MyLabel : TLabel;
      var MySerial1 : integer; SrlDataSet : integer);overload ;
begin
//   if swState = sView then
  //    exit;

   IF TRIM(MyEdit.Text) = '' THEN
   Begin
      MyLabel.Caption := '..........';
      MySerial1 := -1;
   End
   Else
      if (swState = sAdd) or (swState = sEdit) or (swState = sView) then
           MyLabel.Caption := FindSrlName(swState, MySerial1, -1,
                StrToInt(MyEdit.Text), SrlDataSet)
end;

procedure EditCodeChange(swState : TState; MyEdit : TEdit; MyLabel : TLabel;
      var MySerial1 : integer; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,
          Val_Param4 : integer);overload;
begin
//   if swState = sView then
 //     exit;

   IF TRIM(MyEdit.Text) = '' THEN
   Begin
      MyLabel.Caption := '..........';
      MySerial1 := -1;
   End
   Else
      if (swState = sAdd) or (swState = sEdit) then
         MyLabel.Caption := FindSrlName(swState, MySerial1, -1,
               StrToInt(MyEdit.Text), SrlDataSet, Val_Param1, Val_Param2,
               Val_Param3,  Val_Param4);
end;


procedure EditCodeKeyPress(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; MySerial2, C_A_F : integer;
   SpBrowseName, MyKindFieldName : String);overload;
begin
   if swState = sView then
      exit;

   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) and (Trim(MyEdit.Text) = '') then
      SBFindClick(swState, MyEdit, MyLabel, MySerial1, MySerial2,
      C_A_F, SpBrowseName, MyKindFieldName);
end;

procedure EditCodeKeyPress(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; C_A_F : integer;
   SpBrowseName : String);overload;
begin
   if swState = sView then
      exit;

   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) and (Trim(MyEdit.Text) = '') then
      SBFindClick(swState, MyEdit, MyLabel, MySerial1, -1, C_A_F, SpBrowseName, '');
end;

procedure EditCodeKeyPress(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; SrlDataSet : integer);overload;
begin
   if swState = sView then
      exit;

   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) and (Trim(MyEdit.Text) = '') then
      SBFindClick(swState, MyEdit, MyLabel, MySerial1, SrlDataSet);
end;

procedure EditCodeKeyPress(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; SrlDataSet,
   Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer);overload;
begin
   if swState = sView then
      exit;

   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) and (Trim(MyEdit.Text) = '') then
      SBFindClick(swState, MyEdit, MyLabel, MySerial1, SrlDataSet, Val_Param1,
         Val_Param2, Val_Param3, Val_Param4);
end;

procedure EditCodeKeyPress(var Key : Char; swState : TState;
   MyEdit : TEdit; SrlDataSet, Val_Param1, Val_Param2, Val_Param3,
   Val_Param4 : integer);overload;
begin
   if swState = sView then
      exit;

   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) {and (Trim(MyEdit.Text) = '')} then
      SBFindClick(swState, MyEdit, SrlDataSet, Val_Param1, Val_Param2,
      Val_Param3, Val_Param4);
end;

procedure EditCodeFind(var Key : Char; MyEdit : TEdit;
    MySerialKind, FieldCount : integer; TableName : String);
var
   F : TF_Find;
begin

   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) and (Trim(MyEdit.Text) = '') then
   begin
      F := TF_Find.Create(MyEdit);
      F.ADOStoredProcFind.ProcedureName := 'sp_browse_'+ TableName + ';1';
      F.KindParam :=  MySerialKind;
      F.Count_Add_Fields := FieldCount;
      F.DBGrid1.OnTitleClick := F_Common.FormDBGridTitleClick;
      F.showmodal;
      if F.FormResult = 1 then
         MyEdit.Text := F.CFind;
      F.Free;
   end;
end;


procedure NullAdoSPParameters(MyAdoSP : TADOStoredProc);
var
   i : integer;
begin
   MyAdoSP.Close;
 //  MyAdoSP.Parameters.Refresh;
   for i := 0 to MyAdoSP.Parameters.Count -1  do
      MyAdoSP.Parameters[i].Value := null;
end;


procedure BrowseRecord1(MyForm : TObject; TableName : String);
var
   i : integer;
   CurRecord : integer;
begin
   with F_Common.Browse1 do
   Begin
      Connection := F_Common.SystemConnection;

      Close;
      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@TableName').Value := TableName;
      Parameters.ParamByName('@FieldParam').value := '';
      Parameters.ParamByName('@KindParam').value := -1;
      try
         Open;

         with TForm(MyForm) Do
            For i := 0 to ComponentCount - 1 do
               if (Components[i] is TStatusBar) then
               begin
                  TStatusBar(Components[i]).Panels[3].Text := IntToStr(RecordCount);
                  Break;
               end;

      except
         ShowMessage('ccv');
      end;
   end;
end;

procedure BrowseRecord2(MyForm : TObject; TableName, FieldParam : String; KindParam : integer);
var
   i : integer;
   CurRecord : integer;
begin
   if Trim(TableName) = '' then
      exit;
   with F_Common.Browse2 do
   Begin
      Connection := F_Common.SystemConnection;

      Close;
      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@TableName').value := TableName;
      Parameters.ParamByName('@FieldParam').value := FieldParam;
      Parameters.ParamByName('@KindParam').value := KindParam;
      try
         Open;

         with TForm(MyForm) Do
            For i := 0 to ComponentCount - 1 do
               if (Components[i] is TStatusBar) then
               begin
                  TStatusBar(Components[i]).Panels[3].Text := IntToStr(RecordCount);
                  Break;
               end;

      except
         ShowMessage('ccv');
      end;
   end;
end;


procedure InsertRecord1(MyForm : TObject; TableName, MyCode, MyName : String);
var
   i : integer;
   CurRecord : integer;
begin
   for i := 0 to F_Common.Insert1.Parameters.Count -1  do
      F_Common.Insert1.Parameters[i].Value := null;

   F_Common.Insert1.Parameters.ParamByName('@FCode_1').Value := MyCode;
   F_Common.Insert1.Parameters.ParamByName('@FName_2').Value := CorrectString(MyName);;
   F_Common.Insert1.Parameters.ParamByName('@TableName').Value := TableName;
   F_Common.Insert1.Parameters.ParamByName('@SrlUser').Value := Srl_User;
   try
      F_Common.Insert1.ExecProc;
      CurRecord := F_Common.Insert1.Parameters[0].Value;
      F_Common.Browse1.Requery;
      F_Common.Browse1.Locate('FSerial', CurRecord, []);


      with TForm(MyForm) Do
         For i := 0 to ComponentCount - 1 do
            if (Components[i] is TStatusBar) then
            begin
               TStatusBar(Components[i]).Panels[3].Text := IntToStr(F_Common.Browse1.RecordCount);
               Break;
            end;

   except
      ShowMessage(' ��� �� ����� ���� ����� ����  ');
   end;
end;

procedure UpdateRecord1(TableName, MyCode, MyName : String);
var
   i : integer;
   CurRecord : integer;
begin
   with F_Common.Update1 do
   begin
      CurRecord := F_Common.Browse1.FindField('FSerial').Value;

      for i := 0 to Parameters.Count -1  do
         Parameters[i].Value := null;

      Parameters.ParamByName('@FSerial_1').Value := CurRecord;
      Parameters.ParamByName('@FCode_2').Value := MyCode;
      Parameters.ParamByName('@FName_3').Value := CorrectString(MyName);
      Parameters.ParamByName('@TableName').Value := TableName;
      Parameters.ParamByName('@SrlUser').Value := Srl_User;
      try
         ExecProc;
         F_Common.Browse1.Requery;
         F_Common.Browse1.Locate('FSerial', CurRecord, []);
      except
         ShowMessage(' ����� ������ ��� ���� ��� �������  ');
      end;
   end;
end;


procedure DeleteRecord1(MyForm : TObject; TableName : String; SrlRecord : integer; MyBrowse : TADOStoredProc);overload;
var
   i : integer;
   BookMark1 : integer;
begin
   if MyBrowse.RecordCount = 0 then
   begin
      ShowMessage('������ ���� ��� ���� ���� �����');
      exit;
   end
   else
      if Is_DeleteRecord then
{      if MessageDlg('��� ���� ��� ����� �����',
         mtConfirmation, [mbNo, mbYes],1) = mrYes  then
}         
      begin
         BookMark1 := MyBrowse.RecNo;

         for i := 0 to F_Common.Delete1.Parameters.Count -1  do
            F_Common.Delete1.Parameters[i].Value := null;

         F_Common.Delete1.Parameters.ParamByName('@FSerial_1').Value := SrlRecord;
         F_Common.Delete1.Parameters.ParamByName('@TableName').Value := TableName;
         try
            F_Common.Delete1.ExecProc;

            MyBrowse.Requery;
            if MyBrowse.RecordCount > 0 then
               MyBrowse.RecNo := BookMark1;

            with TForm(MyForm) Do
               For i := 0 to ComponentCount - 1 do
                  if (Components[i] is TStatusBar) then
                  begin
                     TStatusBar(Components[i]).Panels[3].Text := IntToStr(MyBrowse.RecordCount);
                     Break;
                  end;

         except
            ShowMessage(' ����� ������ ��� ���� ��� �������  ');
         end;
      end;
end;

procedure DeleteRecord1(SrlDataSet, SrlRecord : integer; MyBrowse : TADOStoredProc);overload;
var
   BookMark1 : integer;
begin
   if MyBrowse.RecordCount = 0 then
   begin
      ShowMessage('������ ���� ��� ���� ���� �����');
      exit;
   end
   else
      if Is_DeleteRecord then
{      if MessageDlg('��� ���� ��� ����� �����',
         mtConfirmation, [mbNo, mbYes], 0) = mrYes  then
}         
      begin
         BookMark1 := MyBrowse.RecNo;

         NullAdoSPParameters(F_Common.Delete3);

         F_Common.Delete3.Parameters.ParamByName('@Srl_Table').Value := SrlDataSet;
         F_Common.Delete3.Parameters.ParamByName('@SerialValue').Value := SrlRecord;
         try
            F_Common.Delete3.ExecProc;

            MyBrowse.Requery;
            if MyBrowse.RecordCount > 0 then
               MyBrowse.RecNo := BookMark1;

         except
            ShowMessage(' ����� ������ ��� ���� ��� �������  ');
         end;
      end;
end;


function GetMaxCodeRecord(TableName, MyKindFieldName : String; MyKindFieldValue : integer) : String;overload;
begin
   with F_Common.Sp_Get_Max do
   begin
      Close;
      try
         Parameters.ParamByName('@TableName').Value := TableName;
         Parameters.ParamByName('@FieldParamName').Value := MyKindFieldName;
         Parameters.ParamByName('@FieldParamValue').Value := MyKindFieldValue;
         ExecProc;
         result := Parameters.ParamByName('@MaxNum').Value;
      except
         ShowMessage('��� �� ���� ���� ������� ��');
      end;
   end;
end;


function GetMaxCodeRecord(TableName : String) : String; overload;
begin
   with F_Common.Sp_Get_Max do
   begin
      Close;
      try
         Parameters.ParamByName('@TableName').Value := TableName;
         Parameters.ParamByName('@FieldParamName').Value := '';
         Parameters.ParamByName('@FieldParamValue').Value := -1;
         ExecProc;
         result := Parameters.ParamByName('@MaxNum').Value;
      except
         ShowMessage('��� �� ���� ���� ������� ��');
      end;
   end;
end;

function GetMaxCodeRecord(SrlDataSet : integer) : String; overload;
begin
   with F_Common.sp_Get_Max_T_Table2 do
   begin
      Connection := F_Common.SystemConnection;
      Close;
      NullAdoSPParameters(F_Common.sp_Get_Max_T_Table2);
      try
         Parameters.ParamByName('@Srl_Table').Value := SrlDataSet;
         ExecProc;
         result := Parameters.ParamByName('@MaxNum').Value;
      except
         ShowMessage('��� �� ���� ���� ������� ��');
      end;
   end;
end;


function GetMaxCodeRecord(SrlDataSet, Val_Param1, Val_Param2, Val_Param3, Val_Param4 : integer) : String;overload;
begin
   with F_Common.sp_Get_Max_T_Table3 do
   begin
      Connection := F_Common.SystemConnection;
      Close;
      NullAdoSPParameters(F_Common.sp_Get_Max_T_Table3);
      try
         Parameters.ParamByName('@Srl_Table').Value := SrlDataSet;
         Parameters.ParamByName('@Val_Param1').Value := Val_Param1;
         Parameters.ParamByName('@Val_Param2').Value := Val_Param2;
         Parameters.ParamByName('@Val_Param3').Value := Val_Param3;
         Parameters.ParamByName('@Val_Param4').Value := Val_Param4;
         ExecProc;
         result := Parameters.ParamByName('@MaxNum').Value;
      except
         ShowMessage('��� �� ���� ���� ������� ��');
      end;
   end;
end;


procedure BackUpDB;
var
   F : TF_BackUp;
begin
   if Trim(N_DataBase) = '' then
   begin
      ShowMessage('��� ����� ���� ���� ����');
      Exit;
   end;

   F := TF_BackUp.Create(F);
   F.showmodal;
   F.Free;
end;

procedure ChangePassword;
var
   F : TF_ChangePassword;
begin
   F := TF_ChangePassword.Create(F);
   F.showmodal;
   F.Free;
end;

Function GetNextDate(MyDate : String; MyDif : integer): String;
var
   NextDate : String;
Begin
   NullAdoSPParameters(F_Common.sp_GetNextDate);
   F_Common.sp_GetNextDate.Parameters.ParamByName('@MyDate').value := MyDate;
   F_Common.sp_GetNextDate.Parameters.ParamByName('@MyDif').value := MyDif;
   try
      F_Common.sp_GetNextDate.ExecProc;
      NextDate := F_Common.sp_GetNextDate.Parameters.ParamByName('@NextDate').value;
   except
      NextDate := '00/00/00';
      ShowMessage('����� �� ����� ����');
   end;
   Result := NextDate;
end;


Function  IsCorrectDate(Date1 : String): integer;
Var
    Y,M,D,R : integer;
    sY,sM,sD : String;
Begin
    R := -1;
    sY := Trim(Copy(Date1, 1, 2));
    sM := Trim(Copy(Date1, 4, 2));
    sD := Trim(Copy(Date1, 7, 2));
    if (sY = '') and (sM = '') and (sD = '') then
    Begin
       R := 0;
       Result := R;
       Exit;
    End;

    if (sY = '00') and (sM = '00') and (sD = '00') then
    Begin
       R := 0;
       Result := R;
       Exit;
    End;

    if (sY = '') or (sM = '') or (sD = '') then
       R := -1
    else
    begin
       Y := Strtoint(sY);
       M := Strtoint(sM);
       D := Strtoint(sD);
       R := 1;
       if (Y<10) then R := -1;
       if (R=1) and ((M>12) or (M=0)) then R := -1;
       if (R=1) and ((D>31) or (D=0)) then R := -1;
       if (R=1) and (M>6) and (D>30) then R := -1;
       if (R=1) and ((Y mod 4)<>3) and (M=12) and (D=30) then R := -1;
    end;

   if R = 0 then
      Result := 0
   else
      Result := R;

End;

procedure ChangeActivePanel(Sender: TObject; var FocusedPanel : String;
   Panel1 : String; ActicePanelColor, IdelPanelColor : TColor);
var
   TComp : TComponent;
begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      TComp := FindComponent(FocusedPanel);
      FocusedPanel := Panel1;
      if (TComp <> nil) and (TComp is TPanel) then
      begin
         (TComp As TPanel).Color := IdelPanelColor;
         (TComp As TPanel).BevelWidth := 1;

         FocusedPanel := Panel1;
         TComp := FindComponent(FocusedPanel);
         if (TComp is TPanel) then
         begin
           (TComp As TPanel).Color := ActicePanelColor;
           (TComp As TPanel).BevelWidth := 3;
         end
{
         else
         if (TComp is TCirclePanel) then
         begin
           (TComp As TCirclePanel).Color := $00A7B097;
           (TComp As TCirclePanel).BevelWidth := 2;
         end
         }
      end;
{
      if (TComp <> nil) and (TComp is TCirclePanel) then
      begin
         TCirclePanel(TComp).Color := clMenu;
         (TComp As TCirclePanel).BevelWidth := 1;

         FocusedPanel := Panel1;
         TComp :=FindComponent(FocusedPanel);
         if (TComp is TPanel) then
         begin
           (TComp As TPanel).Color := $00A7B097;
           (TComp As TPanel).BevelWidth := 2;
         end
         else
         if (TComp is TCirclePanel) then
         begin
           (TComp As TCirclePanel).Color := $00A7B097;
           (TComp As TCirclePanel).BevelWidth := 2;
         end
      end;
}
   end;
end;


procedure AllTabOrderChange(Sender: TObject; Sw: Boolean; Parent : TWinControl);
    var i: Integer;
begin
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      for i:=0 to ComponentCount-1 do
      begin
         if (Components[i] is Tedit) then
         if ((Components[i] as Tedit).Parent=Parent) then (Components[i] as Tedit).TabStop := Sw;
         if (Components[i] is TPanel) then (Components[i] as TPanel).TabStop := Not Sw;
      end;
   end;
end;

procedure EditCodeKeyPress2(var Key : Char; swState : TState;
   MyEdit : TEdit; MyLabel : TLabel;var MySerial1 : integer; MySerial2, C_A_F : integer;
   SpBrowseName : String);
begin
   if not(Key IN ValidKey) then
      Key := #0;

   if (Key = #13) and (Trim(MyEdit.Text) = '') then
      SBFindClick2(swState, MyEdit, MyLabel, MySerial1, MySerial2, C_A_F, SpBrowseName);
end;


procedure SBFindClick2(swState : TState; MyEdit : TEdit;
   MyLabel : TLabel; var MySerial1 : integer; MySerial2, C_A_F : integer; SpBrowseName : String);
var
   F : TF_Find;
begin
   if (swState = sAdd) or (swState = sEdit) then
   begin
      F := TF_Find.Create(MyEdit);
      F.ADOStoredProcFind.ProcedureName := SpBrowseName;
      F.KindParam := MySerial2;
      F.Count_Add_Fields := C_A_F;
      F.DBGrid1.OnTitleClick := F_Common.FormDBGridTitleClick;
      F.showmodal;
      if F.FormResult = 1 then
      begin
         MyEdit.Text := F.CFind;
         MyLabel.Caption := F.NFind;
         MySerial1 := F.SrlFind;
      end;
      F.Free;
   end;
end;


procedure FormShow2(MyForm, MainForm : String; K_Form : integer;
   Is_Show : boolean);
var
   FF : TForm;
   FC : TFormClass;
begin
   FF := nil;
   FC := TFormClass(findClass('T'+MyForm));
   FF := FC.Create(Application);
   TForm(FF).Position := poScreenCenter;

   if FF <> nil then
       FormShowDM(FF, K_Form, Is_Show);
end;


function ConvertToIranSystem1(Str : String) : String;
var
   A,B : Array[128..255] of char;
   Rs : String;
   ChMain1 : char;
   ChMain2 : char;
   ChConvert1 : char;
   i, j : integer;
   LengthStr : integer;
begin
   for i := 128 To 255 do
   begin
      A[i] := ' ';
      B[i] := ' ';
   end;

   A[128] := char(48);    A[129] := char(49);    A[130] := char(50);
   A[131] := char(51);    A[132] := char(52);    A[133] := char(53);
   A[134] := char(54);    A[135] := char(55);    A[136] := char(56);
   A[137] := char(57);    A[138] := char(161);   A[139] := char(220);
   A[140] := char(191);   A[141] := char(194);   A[142] := char(198);
   A[143] := char(193);   A[144] := char(199);   A[145] := char(199);
   A[146] := char(200);   B[146] := char(157);
   A[147] := char(200);
   A[148] := char(129);   B[148] := char(157);
   A[149] := char(129);
   A[150] := char(202);   B[150] := char(157);
   A[151] := char(202);
   A[152] := char(203);   B[152] := char(157);
   A[153] := char(203);
   A[154] := char(204);   B[154] := char(157);
   A[155] := char(204);
   A[156] := char(141);   B[156] := char(157);
   A[157] := char(141);
   A[158] := char(205);   B[158] := char(157);
   A[159] := char(205);
   A[160] := char(206);   B[160] := char(157);
   A[161] := char(206);
   A[162] := char(207);
   A[163] := char(208);
   A[164] := char(209);
   A[165] := char(210);
   A[166] := char(142);
   A[167] := char(211);   B[167] := char(157);
   A[168] := char(211);
   A[169] := char(212);   B[169] := char(157);
   A[170] := char(212);
   A[171] := char(213);   B[171] := char(157);
   A[172] := char(213);
   A[173] := char(214);   B[173] := char(157);
   A[174] := char(214);
   A[175] := char(216); // B[175] := char(157);
   A[176] := char(35);   A[177] := char(35);   A[178] := char(35);
   A[179] := char(35);   A[180] := char(35);   A[181] := char(35);
   A[182] := char(35);   A[183] := char(35);   A[184] := char(35);
   A[185] := char(35);   A[186] := char(35);   A[187] := char(35);
   A[188] := char(35);   A[189] := char(35);   A[190] := char(35);
   A[191] := char(35);   A[192] := char(35);   A[193] := char(35);
   A[194] := char(35);   A[195] := char(35);   A[196] := char(35);
   A[197] := char(35);   A[198] := char(35);   A[199] := char(35);
   A[200] := char(35);   A[201] := char(35);   A[202] := char(35);
   A[203] := char(35);   A[204] := char(35);   A[205] := char(35);
   A[206] := char(35);   A[207] := char(35);   A[208] := char(35);
   A[209] := char(35);   A[210] := char(35);   A[211] := char(35);
   A[212] := char(35);   A[213] := char(35);   A[214] := char(35);
   A[215] := char(35);   A[216] := char(35);   A[217] := char(35);
   A[218] := char(35);   A[219] := char(35);   A[220] := char(35);
   A[221] := char(35);   A[222] := char(35);   A[223] := char(35);
   A[224] := char(217);  // B[224] := char(157);
   A[225] := char(218);   B[225] := char(157);
   A[226] := char(218);   B[226] := char(157);
   A[227] := char(218);
   A[228] := char(218);
   A[229] := char(219);   B[229] := char(157);
   A[230] := char(219);   B[230] := char(157);
   A[231] := char(219);
   A[232] := char(219);
   A[233] := char(221);   B[233] := char(157);
   A[234] := char(221);
   A[235] := char(222);   B[235] := char(157);
   A[236] := char(222);
   A[237] := char(223);   B[237] := char(157);
   A[238] := char(223);
   A[239] := char(144);   B[239] := char(157);
   A[240] := char(144);
   A[241] := char(225);   B[241] := char(157);
   A[242] := char(225);   B[242] := char(199);
   A[243] := char(225);
   A[244] := char(227);   B[244] := char(157);
   A[245] := char(227);
   A[246] := char(228);   B[246] := char(157);
   A[247] := char(228);
   A[248] := char(230);
   A[249] := char(229);   B[249] := char(157);
   A[250] := char(229);
   A[251] := char(229);
   A[252] := char(237);   B[252] := char(157);
   A[253] := char(237);   B[253] := char(157);
   A[254] := char(237);
   A[255] := char(255);

   Str := Trim(Str);
   Rs := '';
   LengthStr := Length(Str);

   ChMain1 := ' ';
   ChMain2 := ' ';

   for i := LengthStr downTo 1 do
   begin
      if ChMain2 <> ' ' then
      begin
         ChMain2 := ' ';
         Continue;
      end;

      ChMain1 := Str[i];
      if i-1 > 0 then
         ChMain2 := Str[i-1]
      else
         ChMain2 := ' ';

      if (Ord(ChMain2) <> 157) and (not ((Ord(ChMain2) = 199) and (Ord(ChMain1) = 255))) then
         ChMain2 := ' ';


      if (Ord(ChMain2) = 199) and (Ord(ChMain1) = 255) then
         ChMain2 := char(199);

      ChConvert1 := ' ';
//      for j := 128 To 255 do
      for j := 255 downTo 128 do
      begin
         ChConvert1 := ChMain1;
         if (A[j] = ChMain1) AND ((B[j] = ChMain2) {OR (ChMain2 = ' '))}) then
         begin
            ChConvert1 := char(j);
            break;
         end;
      end;

      Rs := Rs + ChConvert1;
   end;

   result := Rs;
end;

function ConvertFromIranSystem1(Str : String) : String;
var
   A,B : Array[128..255] of char;
   Rs : String;
   ChMain1 : char;
   ChMain2 : char;
   ChConvert1 : char;
   i : integer;
   LengthStr : integer;
begin
   for i := 128 To 255 do
   begin
      A[i] := ' ';
      B[i] := ' ';
   end;

   A[128] := char(48);    A[129] := char(49);    A[130] := char(50);
   A[131] := char(51);    A[132] := char(52);    A[133] := char(53);
   A[134] := char(54);    A[135] := char(55);    A[136] := char(56);
   A[137] := char(57);    A[138] := char(161);   A[139] := char(220);
   A[140] := char(191);   A[141] := char(194);   A[142] := char(198);
   A[143] := char(193);   A[144] := char(199);   A[145] := char(199);
   A[146] := char(200);   B[146] := char(157);
   A[147] := char(200);
   A[148] := char(129);   B[148] := char(157);
   A[149] := char(129);
   A[150] := char(202);   B[150] := char(157);
   A[151] := char(202);
   A[152] := char(203);   B[152] := char(157);
   A[153] := char(203);
   A[154] := char(204);   B[154] := char(157);
   A[155] := char(204);
   A[156] := char(141);   B[156] := char(157);
   A[157] := char(141);
   A[158] := char(205);   B[158] := char(157);
   A[159] := char(205);
   A[160] := char(206);   B[160] := char(157);
   A[161] := char(206);
   A[162] := char(207);
   A[163] := char(208);
   A[164] := char(209);
   A[165] := char(210);
   A[166] := char(142);
   A[167] := char(211);   B[167] := char(157);
   A[168] := char(211);
   A[169] := char(212);   B[169] := char(157);
   A[170] := char(212);
   A[171] := char(213);   B[171] := char(157);
   A[172] := char(213);
   A[173] := char(214);   B[173] := char(157);
   A[174] := char(214);
   A[175] := char(216); // B[175] := char(157);
   A[176] := char(35);   A[177] := char(35);   A[178] := char(35);
   A[179] := char(35);   A[180] := char(35);   A[181] := char(35);
   A[182] := char(35);   A[183] := char(35);   A[184] := char(35);
   A[185] := char(35);   A[186] := char(35);   A[187] := char(35);
   A[188] := char(35);   A[189] := char(35);   A[190] := char(35);
   A[191] := char(35);   A[192] := char(35);   A[193] := char(35);
   A[194] := char(35);   A[195] := char(35);   A[196] := char(35);
   A[197] := char(35);   A[198] := char(35);   A[199] := char(35);
   A[200] := char(35);   A[201] := char(35);   A[202] := char(35);
   A[203] := char(35);   A[204] := char(35);   A[205] := char(35);
   A[206] := char(35);   A[207] := char(35);   A[208] := char(35);
   A[209] := char(35);   A[210] := char(35);   A[211] := char(35);
   A[212] := char(35);   A[213] := char(35);   A[214] := char(35);
   A[215] := char(35);   A[216] := char(35);   A[217] := char(35);
   A[218] := char(35);   A[219] := char(35);   A[220] := char(35);
   A[221] := char(35);   A[222] := char(35);   A[223] := char(35);
   A[224] := char(217);  // B[224] := char(157);
   A[225] := char(218);   B[225] := char(157);
   A[226] := char(218);   B[226] := char(157);
   A[227] := char(218);
   A[228] := char(218);
   A[229] := char(219);   B[229] := char(157);
   A[230] := char(219);   B[230] := char(157);
   A[231] := char(219);
   A[232] := char(219);
   A[233] := char(221);   B[233] := char(157);
   A[234] := char(221);
   A[235] := char(222);   B[235] := char(157);
   A[236] := char(222);
   A[237] := char(223);   B[237] := char(157);
   A[238] := char(223);
   A[239] := char(144);   B[239] := char(157);
   A[240] := char(144);
   A[241] := char(225);   B[241] := char(157);
   A[242] := char(225);   B[242] := char(199);
   A[243] := char(225);
   A[244] := char(227);   B[244] := char(157);
   A[245] := char(227);
   A[246] := char(228);   B[246] := char(157);
   A[247] := char(228);
   A[248] := char(230);
   A[249] := char(229);   B[249] := char(157);
   A[250] := char(229);
   A[251] := char(229);
   A[252] := char(237);   B[252] := char(157);
   A[253] := char(237);   B[253] := char(157);
   A[254] := char(237);
   A[255] := char(255);

   Str := Trim(Str);
   Rs := '';
   LengthStr := Length(Str);

   ChMain1 := ' ';
   ChMain2 := ' ';

   for i := LengthStr downTo 1 do
   begin
      ChMain1 := Str[i];
      if ((Ord(ChMain1) >= 128 ) AND (Ord(ChMain1) <= 255)) then
      begin
         ChMain2 := A[Ord(ChMain1)];
         if B[Ord(ChMain1)] <> ' ' then
         begin
            Rs := Rs + ChMain2;
            ChMain2 := B[Ord(ChMain1)];
         end
      end
      else
         ChMain2 := ChMain1;

      Rs := Rs + ChMain2;
   end;

   result := Rs;
end;


function XConvert(tcStr: String; tcType: Char): String;
{Convert Function for convert 'tcStr' from IranSystem to Microsoft
 (tcType='I') & from Microsoft to IranSystem (tcType='M') in Win95/98
 (Microsoft Code Page) }
begin
   if      (tcType = 'i') or (tcType = 'I') then Result:=ConvertFromIranSystem2(tcStr)
   else if (tcType = 'm') or (tcType = 'M') then Result:=ConvertToIranSystem2(tcStr);
end;

{------------------------------------------------------------------------------}

function ConvertFromIranSystem2(tcStr: String): String;
var S,s1: string;
    IranSystem,MicroSoft,Capital:String;
    i,j,k,m:integer;
begin
   IranSystem:='��������������������������������௭����������������������������������������� ';
   MicroSoft :='���������������ᐐ�����������������������ӎ�������͍������ʁ������0123456789 ';
   Capital   :='��������������᭫����������';

   s:=Trim(tcStr);

   j:=length(s);
   while pos('�',s)<> 0 do begin   // for dash (_)
      s:=copy(s,1,pos('�',s)-1)+copy(s,pos('�',s)+1,j);
      j:=j-1;
   end;
   while pos('�',s)<> 0 do begin   // for ��
      s:=copy(s,1,pos('�',s)-1)+'��'+copy(s,pos('�',s)+1,j);
      j:=j+1;
   end;
   i:=1;
   while i<=j do begin
      if Pos(s[i], IranSystem)<> 0 then
         if (Pos(s[i], Capital)<> 0)and(s[i-1]<>' ') then begin
            s:=Copy(S,1,i-1)+' '+Copy(S,i,j);
            s[i+1]:=MicroSoft[Pos(s[i+1], IranSystem)];
            i:=i+1;
            j:=j+1;
         end
         else s[i]:=MicroSoft[Pos(s[i], IranSystem)];
      i:=i+1;
   end;
   s:=Trim(s);
   s1:=s;
   j:=length(s);
   k:=j;
   i:=1;
   while i<=j do begin
      if ord(s[i])>128 then s1[k]:=s[i]
      else begin
         m:=i;
         while (ord(s[m])<= 128) and(m<=j) do m:=m+1;
         s1:=Copy(s1,1,j-m+1)+copy(s,i,m-i)+copy(s1,k+1,j-k+1);
         k:=k-m+i+1;
         i:=m-1;
      end;
      k:=k-1;
      i:=i+1;
   end;
   Result := s1
end;

{------------------------------------------------------------------------------}
procedure SetUserWork(KindWork : String);
begin
{   if Not CheckPermission then exit;

   NullAdoSPParameters(F_Common.sp_SetUserWork);
   with F_Common.sp_SetUserWork.Parameters do
   begin
      ParamByName('@KindWork').VALUE := KindWork;
      ParamByName('@Srl_System_2').VALUE := Srl_System;
      ParamByName('@Srl_User_3').VALUE := Srl_User;
      if KindWork = 'End' then
         ParamByName('@Srl_Connect').VALUE := Srl_Connection;
   end;
   try
      F_Common.sp_SetUserWork.ExecProc;
      if KindWork = 'Start' then
         Srl_Connection := F_Common.sp_SetUserWork.Parameters.ParamByName('@Srl_Connect').VALUE;
   Except
      ShowMessage('��� ');
   end;     }
end;


function ConvertToIranSystem2(tcStr: String): String;
var S,s1: string;
    IranSystem,MicroSoft,Capital,Lower,Flags,UnCuncate:String;
    i,j,k,m:integer;
begin
   IranSystem:='��������������������������������௭����������������������������������������� ';
   MicroSoft :='���������������ᐐ�����������������������ӎ�������͍������ʁ������0123456789 ';
   Capital   :='������魫����������';
   Lower     :='������ꮬ����������';
   Flags     :=' ()+-�/�.1234567890�';
   UnCuncate :='��������� ';

   s:=Trim(tcStr);

   j:=length(s);
   i:=1;
   while i<=j do begin
      if Pos(s[i], MicroSoft)<> 0 then
         s[i]:=Iransystem[Pos(s[i], MicroSoft)]
      else if ord(s[i]) = 157 then s[i]:=' ';
      i:=i+1;
   end;
   s:=Trim(s);
   j:=length(s);
   i:=1;
   while i<=j do begin
      if(s[i]='�')and((Pos(s[i-1], UnCuncate)<>0)or(i=1)) then s[i]:= '�'
      else if(s[i]='�') then //�
      begin
         if(Pos(s[i+1], Flags)=0)and(i<>j) then
            if(Pos(s[i-1], Flags)=0)and(Pos(s[i-1], UnCuncate)=0)and(i<>1) then
                 s[i]:= '�' // � ���
            else s[i]:= '�' // � ���
      end
      else if(s[i]='�') then // �
      begin
         if (Pos(s[i+1], Flags)<>0)or(i=j) then
            if(Pos(s[i-1], Flags)=0)and(Pos(s[i-1], UnCuncate)=0)then
                 s[i]:= '�' // � ����� ���
            else
         else s[i]:= '�' //� ���
      end
      else if(s[i]='�')then // �
      begin
         if (Pos(s[i+1], Flags)<>0)or(i=j) then
            if(Pos(s[i-1], Flags)=0)and(Pos(s[i-1], UnCuncate)=0)then
               s[i]:= '�' // � ����� ���
            else
         else
            if(Pos(s[i-1], Flags)=0)and(Pos(s[i-1], UnCuncate)=0)and(i<>1) then
                 s[i]:= '�' // � ���
            else s[i]:= '�' // � ���
      end
      else if(s[i]='�')then // �
      begin
         if (Pos(s[i+1], Flags)<>0)or(i=j) then
            if(Pos(s[i-1], Flags)=0)and(Pos(s[i-1], UnCuncate)=0)then
               s[i]:= '�' //� ����� ���
            else
         else
            if(Pos(s[i-1], Flags)=0)and(Pos(s[i-1], UnCuncate)=0)and(i<>1) then
                 s[i]:= '�' //� ���
            else s[i]:= '�' //� ���
      end
      else if(s[i]='�')and((Pos(s[i+1], Flags)=0)or(i<>j)) then s[i]:= '�'
      else if(Pos(s[i], Capital)<>0)and(Pos(s[i+1], Flags)=0)and(i<>j) then
           s[i]:=Lower[Pos(s[i], Capital)];
      i:=i+1;
   end;

   s:=Trim(s);
   s1:=s;
   j:=length(s);
   k:=j;
   i:=1;
   while i<=j do begin
      if ord(s[i])>137 then s1[k]:=s[i]
      else begin
         m:=i;
         while (ord(s[m])<= 137) and(m<=j) do m:=m+1;
         s1:=Copy(s1,1,j-m+1)+copy(s,i,m-i)+copy(s1,k+1,j-k+1);
         k:=k-m+i+1;
         i:=m-1;
      end;
      k:=k-1;
      i:=i+1;
   end;
   Result:=s1;
end;
//------------------------------------------------------------------------------

Procedure CalcCurrentDate;
begin
 {  NullAdoSPParameters(F_Common.sp_GetCurrentFarsiDate);
   try
      F_Common.sp_GetCurrentFarsiDate.ExecProc;
      CurDate := F_Common.sp_GetCurrentFarsiDate.Parameters.ParamBYName('@CurDate').VALUE;
      CurSal := StrToInt(Copy(CurDate, 1, 2));
   except
      ShowMessage('��� �� �������� ����� �����');
   end;
   if CurDate > '85/33/15' then
      Application.Terminate;}
end;


function ServerTime: String;
begin
  { NullAdoSPParameters(F_Common.sp_GetCurrentServerTime);
   try
      F_Common.sp_GetCurrentServerTime.ExecProc;
       CurTime := F_Common.sp_GetCurrentServerTime.Parameters.ParamBYName('@CurTime').VALUE;
       if (Length(Trim(CurTime)) = 4) AND (Pos(':',Trim(CurTime))= 2) then
          CurTime := '0' + Trim(CurTime);
   except
      ShowMessage('��� �� �������� ����� �����');
   end;
   result := CurTime;  }
end;

procedure CreateDBFFile(Path1, FileName1, FileName2, DBFStruct1, DBFStruct2 : String);
var
   DBFSQL : String;
begin
   if FileExists(Path1 + '\' + FileName1 + '.DBF') then
      DeleteFile(Path1 + '\' + FileName1 + '.DBF');

   if FileExists(Path1 + '\' + FileName2 + '.DBF') then
      DeleteFile(Path1 + '\' + FileName2 + '.DBF');

   with F_Common do
      with Session1 do
      begin
         SessionName := 'MySession';
         ConfigMode := cmSession;
         if not IsAlias('Ghaderi') then
            AddStandardAlias('Ghaderi', Path1, 'FoxPro')
         else
         begin
            ShowMessage('���� ����� ���');
            exit;
         end;

         try
            with Query1 do
            begin
               SessionName := 'MySession';
               DatabaseName := 'Ghaderi';

               if Trim(DBFStruct1) <> '' then
               begin
                  SQl.Clear;
                  DBFSQL := 'CREATE TABLE ' + FileName1 + ' (' + DBFStruct1 + ')';
                  SQl.Add(DBFSQL);
                  ExecSQL;
               end;
               if Trim(DBFStruct2) <> '' then
               begin
                  SQl.Clear;
                  DBFSQL := 'CREATE TABLE ' + FileName2 + ' (' + DBFStruct2 + ')';
                  SQl.Add(DBFSQL);
                  ExecSQL;
               end;
            end;
            DeleteAlias('Ghaderi');
         Finally
            ConfigMode := cmAll;
            ShowMessage('���� ����� ��');
         end;
      end;
end;

//------------------------------------------------------------------------------

function Is_Permision : Boolean;
begin
{   if IsFullAccess = 20 then
   begin
      Result := False;
      exit;
   end;

   NullAdoSPParameters(F_Common.sp_GetSystemPermision);
   try
      F_Common.sp_GetSystemPermision.Parameters.ParamBYName('@SrlSystem').VALUE := Srl_System;
      F_Common.sp_GetSystemPermision.ExecProc;
      result := F_Common.sp_GetSystemPermision.Parameters.ParamBYName('@Is_Permision').VALUE;
   except
       ShowMessage('��� �� ���� ��� �����');
   end;  }

end;


procedure TF_Common.SystemConnectionWillExecute(Connection: TADOConnection;
  var CommandText: WideString; var CursorType: TCursorType;
  var LockType: TADOLockType; var CommandType: TCommandType;
  var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
  const Command: _Command; const Recordset: _Recordset);
begin
   Screen.Cursor := crHourGlass;
//   F_WaitAvi.Hide;
end;

procedure TF_Common.SystemConnectionExecuteComplete(
  Connection: TADOConnection; RecordsAffected: Integer; const Error: Error;
  var EventStatus: TEventStatus; const Command: _Command;
  const Recordset: _Recordset);
begin
   Screen.Cursor := crDefault;
//   F_WaitAvi.Showshowmodal;
end;

procedure TF_Common.DataModuleDestroy(Sender: TObject);
begin
   F1.Free;
   SetUserWork('End');
end;

procedure TF_Common.SystemConnectionAfterConnect(Sender: TObject);
begin
   //SetUserWork('Start');
end;

procedure TF_Common.Timer1Timer(Sender: TObject);
begin
 {  CheckPermission := Is_Permision;

   if NewSystem_Version <> F_Common.GetSystemVersion then
   begin
      WinExec(PChar(MsgEndUserTime), 1);
      Application.Terminate;
      exit;   
   end;  }

{
   if SystemIdelTime > '12:59' then
      kldfgjd
}


 end;

procedure ExecSystem(MySystemName : String);
var
   FileLocacl, FileServer : file of Byte;
   ExecFilePath : String;
   ExecName : String;
   ParamStr : String;
   FullFileNameLocal, FullFileNameServer : String;
   FileSizeLocal, FileSizeServer : Longint;
//   FileAgeLocal, FileAgeServer : integer;
begin
   ExecFilePath := '\\Hozeh-s\ServerFiles$\ExecSys\';

   if  not DirectoryExists('c:\temp')  then
       if  not  CreateDir('C:\temp')  then
          raise  Exception.Create('Cannot create c:\temp');

   ParamStr := IntToStr(Srl_User) + ' ' + IntToStr(GlobalSrl_Dprt); // + ' ' + GlobalN_Dprt;
   if MySystemName = 'Permission' then
      ExecName := 'PermisionPrj.exe'
   else
   if MySystemName = 'Dabir' then
      ExecName := 'DabirPrj.exe'
   else
   if MySystemName = 'Sonat' then
      ExecName := 'SonatPrj.exe'
   else
   if MySystemName = 'Paziresh' then
      ExecName := 'Paziresh.exe'
   else
   if MySystemName = 'Exam' then
      ExecName := 'ExamPrj.exe'
   else
   if MySystemName = 'Bime' then
      ExecName := 'BimePrj.exe'
   else
   if MySystemName = 'EdariMali' then
      ExecName := 'EdariMali.exe'
   else
   if MySystemName = 'School' then
      ExecName := 'SchoolPrj.exe'
   else
   if MySystemName = 'Research' then
      ExecName := 'ResearchPrj.exe'
   else
   if MySystemName = 'City' then
      ExecName := 'CityPrj.exe'
   else
   if MySystemName = 'Hesab' then
      ExecName := 'HesabPrj.exe'
   else
   if MySystemName = 'Mokatebat' then
      ExecName := 'MokatebatPrj.exe'
   else
   if MySystemName = 'Ravabet' then
      ExecName := 'RavabetPrj.exe'
   else
   if MySystemName = 'Mashmool' then
      ExecName := 'MashmoolPrj.exe';

   F_Common.UserCanAccessField(MySystemName, 'F_Main', K_Access);

   FullFileNameLocal := 'C:\Temp\' + ExecName;
   FullFileNameServer := ExecFilePath + ExecName;

{
   if FileExists(PChar(FullFileNameLocal)) then
   begin
      AssignFile(FileLocacl, FullFileNameLocal);
      Reset(FileLocacl);
      FileSizeLocal := FileSize(FileLocacl);
      FileAgeLocal := FileAge(PChar(FullFileNameLocal));
      CloseFile(FileLocacl);
   end
   else
   begin
      FileSizeLocal := 0;
      FileAgeLocal := 0;
   end;

   try
      if FileExists(PChar(FullFileNameServer)) then
      begin
         AssignFile(FileServer, FullFileNameServer);
         Reset(FileServer);
         FileSizeServer := FileSize(FileServer);
         FileAgeServer := FileAge(PChar(FullFileNameServer));
         CloseFile(FileServer);
      end
      else
      begin
         FileSizeServer := 0;
         FileAgeServer := 0;
      end;
   except
      ;
   end;


   try
      if (FileSizeServer <> FileSizeLocal)
         OR (FileAgeServer <> FileAgeLocal)
         OR (FileAgeLocal = 0)
         OR (FileAgeLocal = 0) then
      begin
         CopyFile(PChar(FullFileNameServer), PChar(FullFileNameLocal), False);
   //      StatusBar1.Panels[2].Text := '����';
   //    ShowMessage('�� ��� ��� ��� ������');
      end
      else
      begin;
   //      StatusBar1.Panels[2].Text := '����';
   //    ShowMessage('������ �� �������');
      end;
   except
      ;
   end;
}

   if K_Access > 1 then
   begin
      CopyFile(PChar(FullFileNameServer), PChar(FullFileNameLocal), False);
      WinExec(PChar(FullFileNameLocal + ' ' + ParamStr), 0)
   end
   else
      ShowMessage('��� ���� ������� �� ��� ������ �� ������');
end;

procedure InsertMyLetter(SrlTable, SrlRecord : integer);
begin
   if MessageDlg('��� ���� ����� ��� ?',
      mtConfirmation, [mbNo, mbYes],1) = mrNo then
      exit;

   NullAdoSPParameters(F_Common.insert_T_Letter_System);
   with F_Common.insert_T_Letter_System.Parameters do
   begin
      ParamByName('@Srl_Table').Value := SrlTable;
      ParamByName('@Srl_Record').Value := SrlRecord;
      ParamByName('@Srl_User').Value := Srl_User;
   end;
   try
      F_Common.insert_T_Letter_System.ExecProc;
      GlobalSrl_Letter := F_Common.insert_T_Letter_System.Parameters[0].Value;
      ShowMessage('���� ����� ��');
   except
      ShowMessage('��� �� ����� ����');
   end;
end;

procedure ViewPicPer(SrlStudent, KStudent, KPicture : integer;var PicStd : TImage);
begin
 PicStd.Picture.LoadFromFile('C:\Temp\A.Jpg');
 {  if SrlStudent < 1 then
   begin
      PicStd.Picture := nil;
      exit;
   end;

   PicStd.Picture := nil;

   with F_Common.BROWSE_Student_Pic do
   begin
      NullAdoSPParameters(F_Common.BROWSE_Student_Pic);
      try
         Parameters.ParamByName('@Srl_Student').Value := SrlStudent;
         Parameters.ParamByName('@K_Student').Value := KStudent;
         Parameters.ParamByName('@K_Picture').Value := KPicture;
         Open;

         if F_Common.BROWSE_Student_Pic.RecordCount > 0  then
            if Not F_Common.BROWSE_Student_PicPic_Student.IsNull then
            begin
               F_Common.BROWSE_Student_PicPic_Student.SaveToFile('C:\Temp\A.Jpg');
               PicStd.Picture.LoadFromFile('C:\Temp\A.Jpg');
            end
            else
               PicStd.Picture := nil
         else
             PicStd.Picture := nil;
      except
//         ShowMessage('��� ������ �����');
      end;
   end; }
end;

procedure ViewPicPer(SrlStudent, KStudent, KPicture : integer; PicStd : TppImage);overload;
begin
   if SrlStudent < 1 then exit;

   with F_Common.BROWSE_Student_Pic do
   begin
      NullAdoSPParameters(F_Common.BROWSE_Student_Pic);
      try
         Parameters.ParamByName('@Srl_Student').Value := SrlStudent;
         Parameters.ParamByName('@K_Student').Value := KStudent;
         Parameters.ParamByName('@K_Picture').Value := KPicture;
         Open;

         if F_Common.BROWSE_Student_Pic.RecordCount > 0  then
            if Not F_Common.BROWSE_Student_PicPic_Student.IsNull then
            begin
               F_Common.BROWSE_Student_PicPic_Student.SaveToFile('C:\Temp\A.Jpg');
               PicStd.Picture.LoadFromFile('C:\Temp\A.Jpg');
            end
            else
               PicStd.Picture := nil
          else
             PicStd.Picture := nil;
      except
//         ShowMessage('��� ������ �����');
      end;
   end;
end;


procedure AssignPicToPer(SrlStudent, KStudent, KPicture : integer; PicStd : TImage);overload;
begin
   F_Common.OpenPictureDialog1.Title := '���� ����� �� ������ ����';
   if F_Common.OpenPictureDialog1.Execute  then
   begin
      PicStd.Picture.LoadFromFile(F_Common.OpenPictureDialog1.FileName);

      with F_Common.insert_T_Student_Pic_1 do
      begin
         NullAdoSPParameters(F_Common.insert_T_Student_Pic_1);
         try
            Parameters.ParamByName('@FName_2').Value := '';
            Parameters.ParamByName('@Srl_Student_3').Value := SrlStudent;
            Parameters.ParamByName('@K_Student').Value := KStudent;
            Parameters.ParamByName('@K_Picture').Value := KPicture;
            Parameters.ParamByName('@Pic_Student_4').LoadFromFile(F_Common.OpenPictureDialog1.FileName, ftGraphic);
            ExecProc;
         except
            ShowMessage('��� ������ �����');
         end;
      end;
   end;
end;

function ShowCloseCycle: boolean;
var
   MyRs : boolean;
begin
   if IsCloseCycle = 1 then
   begin
      ShowMessage('���� ���� ��� ��� � ���� ������ ����� � ��� ��� ����');
      MyRs := True;
   end
   else
   begin
      MyRs := False;
   end;

   Result := MyRs;
end;

procedure TF_Common.PerImagePrint(Sender: TObject);
begin
 if FileExists('c:\temp\A.jpg') then
  TppImage(Sender).Picture.LoadFromFile('c:\temp\A.jpg');
//   ViewPicPer(110110, 1, 1, TppImage(Sender));
end;

procedure TF_Common.CompanyMarkImagePrint(Sender: TObject);
begin
   TppImage(Sender).Picture.LoadFromFile('arm.Jpg');
end;


procedure TF_Common.ppLabelD_CurrentGetText(Sender: TObject;
  var Text: String);
begin
   Text := CurrDate;
end;

procedure TF_Common.ppLabelNumberStrGetText(Sender: TObject;
  var Text: String);
begin
   Text := '(' + Number2StrDecimal(GlobalNumberVal1) + ')';
end;

procedure TF_Common.ppLabelNumberValGetText(Sender: TObject;
  var Text: String);
begin
   if Trim(Text) <> '' then
      GlobalNumberVal1 := StrToFloat(Text);
end;

procedure TF_Common.MyDBGrid1Enter(Sender: TObject);
begin
   GlobalDBGrid := TDBGrid(Sender);
   GlobalDBGridName := GlobalDBGrid.Name;
//   GlobalDBGrid.Color := $00D9D8D7;
   MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TF_Common.MyDBGrid1Exit(Sender: TObject);
begin
   GlobalDBGridOld := TDBGrid(Sender);
   GlobalDBGridOldName := GlobalDBGridOld.Name;
//   GlobalDBGridOld.Color := clWhite;
end;


procedure TF_Common.MyDSBrowse1DataChange(Sender: TObject; Field: TField);
begin
   MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TF_Common.DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
  Field: TField; State: TGridDrawState);
begin
   MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TF_Common.DBGrid1ColEnter(Sender: TObject);
begin
   MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TF_Common.DBGrid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TF_Common.DBGrid1CellClick(Column: TColumn);
begin

   MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TF_Common.DBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   MyDBGridRecordCount(GlobalDBGrid);
end;



procedure TF_Common.EditSearchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key = vk_Up then
   begin
      GlobalDBGrid.DataSource.DataSet.Prior;
      TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
      Key := 0;
   end
   else if Key = vk_Down then
   begin
      GlobalDBGrid.DataSource.DataSet.Next;
      TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
      Key := 0;
   end
end;

Procedure TF_Common.CheckSystemVersion;
begin
   NullAdoSPParameters(Common_SP_GetSystemVersion);

   Common_SP_GetSystemVersion.Parameters.ParamByName('@Srl_System').value := Srl_System;
   try
      Common_SP_GetSystemVersion.ExecProc;
      OldSystem_Version := Common_SP_GetSystemVersion.Parameters.ParamByName('@Last_Version').value;
   except
      OldSystem_Version := -1;
      ShowMessage('����� �� ���� ������');
   end;

   if NewSystem_Version <> OldSystem_Version then
   begin
      CountMsgTermUser := CountMsgTermUser + 1;
//      if CountMsgTermUser Mod 3 = 0 then
      WinExec(PChar(MsgEndUserTime), 1);

      if CountMsgTermUser > 3 then
         Application.Terminate;
      exit;
   end;
end;

procedure BrowseTStudentOne(SrlStudent : integer);
begin
   NullAdoSPParameters(F_Common.Browse_T_StudentOne);
   with F_Common.Browse_T_StudentOne do
   begin
      try
         Parameters.ParamByName('@SrlStudent').Value := SrlStudent;
         Open;;
      except
         ShowMessage('���');
      end;
   end;
end;


procedure InsertPicture(OldFullFileName : String; IsMoveFile : Boolean;
            MyKFile, MyKSource, MySrlSource : integer; IsShowMessage : boolean; var Result : integer);
{
   OldFullFileName : Current Path of File
   IsMoveFile : Delete File From Source Directory ?
   MyKFile : 1 - Picture,
   MyKSource : ... ,
   MySrlSource : Serial Parent of Picture
   Result : Is Moved File ?
}
var
   FileIndex : integer;
   MyExactFileName : String;
   MyFileExtention : String;
   MyNewPathName : String;
   MyNewFileName : String;
   MyNewFilePathName : String;
   PosDot : integer;
   NewDirectory : TDirectoryListBox;
   SearchRec : TSearchRec;
   DirectoryIndex : integer;
begin
   Result := 0;

   MyProject_Picture_Path := '\\hozeh-s\Picture$\DabirPrj';
   MyNewPathName := MyProject_Picture_Path;

   DirectoryIndex := 0;

   while FileExists(PChar(MyNewPathName + IntToStr(DirectoryIndex) + '\' + 'EndFile.Txt')) do
      Inc(DirectoryIndex);
      
   MyNewPathName := MyNewPathName + IntToStr(DirectoryIndex) + '\';
   if  not DirectoryExists(MyNewPathName)  then
       if  not  ForceDirectories(MyNewPathName)  then
          raise  Exception.Create(MyNewPathName);

{
   if FindFirst(MyNewPathName + '*.*', faAnyFile and not faDirectory, SearchRec) = 0 then
   begin
      Inc(FileCount);
      while FindNext(SearchRec) = 0 do
         Inc(FileCount);
   end;
}

   if FileExists(PChar(OldFullFileName)) then
   begin
      MyExactFileName := ExtractFilename(OldFullFileName);
      MyFileExtention := ExtractFileExt(OldFullFileName);
      PosDot := Pos('.', MyExactFileName);
      Delete(MyExactFileName, PosDot, (Length(MyExactFileName) - PosDot + 1));

      MyNewFileName :=
                  IntToStr(MyKSource) + '_'
                  + IntToStr(MySrlSource) + '_'
                  + ExtractFileName(MyExactFileName);
      FileIndex := 0;
      MyNewFilePathName := MyNewPathName + MyNewFileName + '_' + IntToStr(FileIndex) + MyFileExtention;

      While FileExists(PChar(MyNewFilePathName)) do
      begin
         FileIndex := FileIndex + 1;
         MyNewFilePathName := MyNewPathName + MyNewFileName + '_' + IntToStr(FileIndex) + MyFileExtention;
      end;

      MyNewFileName := MyNewFileName + '_' + IntToStr(FileIndex);
      MyNewFilePathName := MyNewPathName + MyNewFileName + MyFileExtention;
      with F_Common.insert_T_Source_Pic_1 do
      begin
         NullAdoSPParameters(F_Common.insert_T_Source_Pic_1);
         try
            Parameters.ParamByName('@K_File').Value := MyKFile;
            Parameters.ParamByName('@K_Source').Value := MyKSource;
            Parameters.ParamByName('@Srl_Source').Value := MySrlSource;
            Parameters.ParamByName('@N_Path').Value := MyNewPathName;
            Parameters.ParamByName('@N_File').Value := MyNewFileName + MyFileExtention;
            ExecProc;
//                      asda
            if IsMoveFile then
            begin
               if not MoveFileEX(PChar(OldFullFileName), PChar(MyNewFilePathName), MOVEFILE_COPY_ALLOWED) then
               begin
                  ShowMessage('�Ԙ�� �� ����� ����'
                              + OldFullFileName + ' : '
                              + #13 + MyNewFileName);
                  Result := 0;
               end
               else
               begin
                  Result := 1;
               end;
            end
            else
            begin
               if not CopyFile(PChar(OldFullFileName), PChar(MyNewFilePathName), False) then
               begin
                  ShowMessage('�Ԙ�� �� ����� ����'
                              + OldFullFileName + ' : '
                              + #13 + MyNewFileName);
                  Result := 0;
               end
               else
               begin
                  Result := 1;
               end;
            end

         except
            ShowMessage('��� ������ �����');
            Result := 0;
         end;

         if (Result = 1) AND (IsShowMessage) then
            ShowMessage('����� ������ ���� ��');
      end;
   end;
end;

Procedure ShowAttachPic(KSourcePic, SrlSourcePic : integer);
begin
   if ((KSourcePic < 1) OR (SrlSourcePic < 1)) then
      exit;

   GlobalK_Source := KSourcePic;
   GlobalSrl_Source := SrlSourcePic;
   FormShow2('F_Picture', 'F_Main', 2, True);
end;

Procedure ShowAttachDoc(KSourceDoc, SrlSourceDoc : integer);
begin
   if ((KSourceDoc < 1) OR (SrlSourceDoc < 1)) then
      exit;

   GlobalK_Source := KSourceDoc;
   GlobalSrl_Source := SrlSourceDoc;
   FormShow2('F_Docum', 'F_Main', 2, True);
end;

procedure ChangeActiveBitBtnCount(MyBitBtn : TBitBtn; RecordCount : integer);
var
   LastCaption : String;
   Pos1, Pos2 : integer;
begin
   LastCaption := Trim(MyBitBtn.Caption);
   Pos1 := Pos('(', LastCaption);
   Pos2 := Pos(')', LastCaption);

   if ((Pos1 > 0) AND (Pos2 > 0)) then
      LastCaption := Trim(Copy(LastCaption, 1, Pos1 - 1));

   MyBitBtn.Caption := LastCaption + ' (' + Trim(IntToStr(RecordCount)) + ')';
   if RecordCount = 0 then
      MyBitBtn.Enabled := False
   else
      MyBitBtn.Enabled := True;
end;

function  Is_Student_Can_Register(KCheck, SrlStudent, SrlCycleOld, SrlCycleNew : integer) : integer;
var
   Res : integer;
begin
   Res := 1;
   NullAdoSPParameters(F_Common.Exam_Student_Can_Register);
   with F_Common.Exam_Student_Can_Register do
   begin
      try
         Parameters.ParamByName('@KCheck').Value := KCheck;
         Parameters.ParamByName('@SrlStudent').Value := SrlStudent;
         Parameters.ParamByName('@SrlCycleOld').Value := SrlCycleOld;
         Parameters.ParamByName('@SrlCycleNew').Value := SrlCycleNew;
         ExecProc;
         Res := Parameters.ParamByName('@ResRegister').Value;
      except
         ShowMessage('���');
         Res := 0;
      end;
   end;
   Result := Res;
end;


procedure TF_Common.N1Click(Sender: TObject);
begin
   GlobalDBGrid.DataSource.DataSet.First;

end;

procedure TF_Common.N2Click(Sender: TObject);
begin
   GlobalDBGrid.DataSource.DataSet.Last;

end;

procedure TF_Common.N3Click(Sender: TObject);
var
   RowNumber : integer;
begin
   RowNumber := StrToInt(InputBox('���� ��', '', IntToStr(GlobalDBGrid.DataSource.DataSet.RecNo)));
   if RowNumber > 0 then
   begin
      GlobalDBGrid.DataSource.DataSet.RecNo := RowNumber;
   end;
end;

procedure TF_Common.N4Click(Sender: TObject);
begin
   PrintDBGrid(GlobalDBGrid)

end;

procedure TF_Common.N5Click(Sender: TObject);
begin
   OptionDBGrid(GlobalDBGrid)

end;

procedure TF_Common.N6Click(Sender: TObject);
begin
   FilterClearDBGrid(GlobalDBGrid)

end;

procedure TF_Common.N7Click(Sender: TObject);
begin
   FilterSelectedDBGrid(GlobalDBGrid)

end;

procedure TF_Common.N8Click(Sender: TObject);
begin
   FilterSelectedDBGrid(GlobalDBGrid)

end;

procedure TF_Common.N9Click(Sender: TObject);
begin
   ShowMessage(SumDBGrid(GlobalDBGrid));

end;

procedure TF_Common.Access1Click(Sender: TObject);
begin
   DBGridSaveToAccess(GlobalDBGrid);

end;

procedure TF_Common.Excel1Click(Sender: TObject);
begin
   DBGridSaveToExcelFile(GlobalDBGrid);

end;

procedure TF_Common.WORD1Click(Sender: TObject);
begin
   DBGridSaveToWordFile(GlobalDBGrid);

end;
procedure SetGridColumnWidths(Grid: Tdbgrid);
const
  DEFBORDER = 10;
var
  temp, n: Integer;
  lmax: array [0..30] of Integer;
begin
  with Grid do
  begin
    Canvas.Font := Font;
    for n := 0 to Columns.Count - 1 do

      lmax[n] := Canvas.TextWidth(Fields[n].FieldName) + DEFBORDER;
    grid.DataSource.DataSet.First;
    while not grid.DataSource.DataSet.EOF do
    begin
      for n := 0 to Columns.Count - 1 do
      begin

        temp := Canvas.TextWidth(trim(Columns[n].Field.DisplayText)) + DEFBORDER;
        if temp > lmax[n] then lmax[n] := temp;

      end;
      grid.DataSource.DataSet.Next;
    end;
    grid.DataSource.DataSet.First;
    for n := 0 to Columns.Count - 1 do
      if lmax[n] > 0 then
        Columns[n].Width := lmax[n];
  end;
end;

procedure TF_Common.N10Click(Sender: TObject);
begin
   SetGridColumnWidths(GlobalDBGrid)
end;

end.



program ExamPrj;
uses
  Forms,
  Types,
  Windows,
  Dialogs,
  SysUtils,
  U_Main in 'U_Main.pas' {F_Main},
  U_Common in '..\..\ShoraCommFile\U_Common.pas' {F_Common: TDataModule};

{$R *.RES}

var
   UserName : array[0..255] of char;
   size : dword;
begin
  size:= 256;
  GetUserName(N_UserNT, size);
  N_UserNT2 := Trim(N_UserNT);

  Application.Initialize;
  Srl_System := 4;
  NewSystem_Version := 31;

//  IsFullAccess := 20;

  if (ParamCount = 2) then
  begin
     Srl_User := StrToInt(ParamStr(1));
     GlobalSrl_Dprt := StrToInt(ParamStr(2));
  end
  else
  begin
     GlobalSrl_Dprt := -1;
     Srl_User := -6;
  end;

  if (UpperCase(N_UserNT2) = 'USER11') then Srl_User := 6;

  if Srl_User > -1 then
  begin
  Application.CreateForm(TF_Common, F_Common);
  Application.CreateForm(TF_Main, F_Main);
  end
  else
     ShowMessage('��� ���� ������� �� ����� �� ������');

  Application.Run;
end.

