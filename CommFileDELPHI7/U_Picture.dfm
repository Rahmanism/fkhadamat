object F_Picture: TF_Picture
  Left = 31
  Top = 103
  Width = 971
  Height = 686
  BiDiMode = bdRightToLeft
  Caption = #1604#1610#1587#1578' '#1578#1589#1575#1608#1610#1585
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object DBText1: TDBText
    Left = 29
    Top = 624
    Width = 60
    Height = 19
    AutoSize = True
    BiDiMode = bdLeftToRight
    DataField = 'Pic_Path'
    DataSource = DSBrowse_T_Source_Pic
    ParentBiDiMode = False
  end
  object DBNavigator1: TDBNavigator
    Left = 280
    Top = 536
    Width = 220
    Height = 33
    DataSource = DSBrowse_T_Source_Pic
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbPost]
    TabOrder = 1
  end
  object BitBtnAddPic: TBitBtn
    Left = 128
    Top = 536
    Width = 137
    Height = 33
    Caption = #1575#1590#1575#1601#1607' '#1603#1585#1583#1606' '#1578#1589#1608#1610#1585
    TabOrder = 2
    OnClick = BitBtnAddPicClick
  end
  object BitBtnDelOne: TBitBtn
    Left = 512
    Top = 536
    Width = 137
    Height = 35
    Caption = #1581#1584#1601' '#1578#1589#1608#1610#1585' '
    TabOrder = 3
    OnClick = BitBtnDelOneClick
  end
  object DBGrid1: TDBGrid
    Left = 648
    Top = 8
    Width = 305
    Height = 385
    DataSource = DSBrowse_T_Source_Pic
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = ARABIC_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'FCode'
        Title.Caption = #1588#1605#1575#1585#1607
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Des_Pic'
        Title.Caption = #1605#1608#1590#1608#1593
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FName'
        Title.Caption = #1578#1608#1590#1610#1581#1575#1578
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pic_Path'
        Width = 10
        Visible = True
      end>
  end
  object DBEdit1: TDBEdit
    Left = 648
    Top = 400
    Width = 305
    Height = 27
    BiDiMode = bdRightToLeft
    DataField = 'FName'
    DataSource = DSBrowse_T_Source_Pic
    ParentBiDiMode = False
    TabOrder = 5
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 648
    Top = 432
    Width = 305
    Height = 27
    DataField = 'Srl_Des_Pic'
    DataSource = DSBrowse_T_Source_Pic
    KeyField = 'FSerial'
    ListField = 'FName'
    ListSource = DataSource1
    TabOrder = 6
    OnClick = DBLookupComboBox1Click
  end
  object BitBtnViewPic: TBitBtn
    Left = 128
    Top = 576
    Width = 121
    Height = 33
    Caption = #1575#1606#1583#1575#1586#1607' '#1608#1575#1602#1593#1610
    TabOrder = 7
    OnClick = BitBtnViewPicClick
  end
  object BitBtnPrintPic: TBitBtn
    Left = 384
    Top = 576
    Width = 121
    Height = 33
    Caption = #1670#1575#1662
    TabOrder = 8
    OnClick = BitBtnPrintPicClick
  end
  object BitBtnDelAll: TBitBtn
    Left = 512
    Top = 576
    Width = 137
    Height = 35
    Caption = #1581#1584#1601' '#1607#1605#1607' '#1578#1589#1575#1608#1610#1585' '
    TabOrder = 9
    OnClick = BitBtnDelAllClick
  end
  object BitBtnEditPic: TBitBtn
    Left = 256
    Top = 576
    Width = 121
    Height = 33
    Caption = #1608#1610#1585#1575#1610#1588' '#1578#1589#1608#1610#1585
    TabOrder = 10
    OnClick = BitBtnEditPicClick
  end
  object IsMoveFile: TCheckBox
    Left = 648
    Top = 486
    Width = 305
    Height = 23
    Caption = #1570#1610#1575' '#1601#1575#1610#1604' '#1605#1606#1578#1602#1604' '#1588#1608#1583
    TabOrder = 11
  end
  object BitBtnClose: TBitBtn
    Left = 16
    Top = 576
    Width = 97
    Height = 33
    Caption = #1582#1585#1608#1580
    TabOrder = 12
    OnClick = BitBtnCloseClick
  end
  object Panel1: TPanel
    Left = 9
    Top = 4
    Width = 624
    Height = 525
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 622
      Height = 523
      Align = alClient
      Stretch = True
    end
  end
  object BitBtn4: TBitBtn
    Left = 32
    Top = 535
    Width = 33
    Height = 33
    Caption = '+'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clTeal
    Font.Height = -35
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 13
    OnClick = BitBtn4Click
  end
  object BitBtn5: TBitBtn
    Left = 72
    Top = 535
    Width = 33
    Height = 33
    Caption = '-'
    Font.Charset = ARABIC_CHARSET
    Font.Color = clTeal
    Font.Height = -35
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
    OnClick = BitBtn5Click
  end
  object Browse_T_Source_Pic: TADOStoredProc
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    ProcedureName = 'Common_SP_Browse_T_Source_Pic;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@SrlSource'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 194487
      end
      item
        Name = '@KFile'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@KSource'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 4009
      end>
    Left = 160
    Top = 35
    object Browse_T_Source_PicFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse_T_Source_PicFCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse_T_Source_PicFName: TStringField
      FieldName = 'FName'
      Size = 200
    end
    object Browse_T_Source_PicSrl_Des_Pic: TIntegerField
      FieldName = 'Srl_Des_Pic'
    end
    object Browse_T_Source_PicDes_Pic: TStringField
      FieldName = 'Des_Pic'
      Size = 100
    end
    object Browse_T_Source_PicPic_Path: TStringField
      FieldName = 'Pic_Path'
      Size = 200
    end
  end
  object DSBrowse_T_Source_Pic: TDataSource
    DataSet = Browse_T_Source_Pic
    OnDataChange = DSBrowse_T_Source_PicDataChange
    Left = 161
    Top = 84
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'JPEG Image File (*.jpg)|*.jpg'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 336
    Top = 72
  end
  object Delete1: TADOStoredProc
    ProcedureName = 'delete_T_Source_Pic_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 160
    Top = 136
  end
  object ADOTable1: TADOTable
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    TableName = 'Pub_Des_Picture'
    Left = 728
    Top = 248
    object ADOTable1FSerial: TIntegerField
      FieldName = 'FSerial'
    end
    object ADOTable1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object ADOTable1FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object ADOTable1U_DateTime: TDateTimeField
      FieldName = 'U_DateTime'
    end
    object ADOTable1Srl_Connection: TIntegerField
      FieldName = 'Srl_Connection'
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 728
    Top = 296
  end
  object ADOTable2: TADOTable
    Connection = F_Common.SystemConnection
    CursorType = ctStatic
    TableName = 'Pub_Des_Picture'
    Left = 752
    Top = 96
    object ADOTable2FSerial: TIntegerField
      FieldName = 'FSerial'
    end
    object ADOTable2FCode: TIntegerField
      FieldName = 'FCode'
    end
    object ADOTable2FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object ADOTable2U_DateTime: TDateTimeField
      FieldName = 'U_DateTime'
    end
    object ADOTable2Srl_Connection: TIntegerField
      FieldName = 'Srl_Connection'
    end
  end
  object DataSource2: TDataSource
    DataSet = ADOTable2
    Left = 744
    Top = 152
  end
  object PrintDialog1: TPrintDialog
    Left = 321
    Top = 220
  end
end
