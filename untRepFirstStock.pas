unit untRepFirstStock;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untParentReport, DB, ADODB, StdCtrls, Buttons, ExtCtrls, Grids,
  DBGrids, ComCtrls;

type
  TfrmRepFirstStock = class(TfrmParentReport)
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRepFirstStock: TfrmRepFirstStock;

implementation

uses U_Common, untDm;

{$R *.dfm}

procedure TfrmRepFirstStock.BitBtn1Click(Sender: TObject);
begin
  inherited;
    Browse1.Close;
    Browse1.Parameters.ParamByName('@srl_year1').Value:=srlYearRep1;
    Browse1.Parameters.ParamByName('@srl_year2').Value:=srlYearRep2;
    Browse1.Parameters.ParamByName('@num_rep').Value:=0;
    Browse1.Open;

end;

procedure TfrmRepFirstStock.FormShow(Sender: TObject);
begin
  inherited;
    Browse1.Close;
    Browse1.Parameters.ParamByName('@srl_year1').Value:=srlYear;
    Browse1.Parameters.ParamByName('@srl_year2').Value:=srlYear;
    Browse1.Parameters.ParamByName('@num_rep').Value:=8;
    Browse1.Open;

end;

end.
