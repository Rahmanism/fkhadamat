object frmDm: TfrmDm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 454
  Top = 335
  Height = 350
  Width = 432
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 40
    Top = 16
  end
  object MainMenu1: TMainMenu
    Left = 112
    Top = 16
  end
  object insert_TObject: TADOStoredProc
    Connection = ADOConnection
    ProcedureName = 'insert_TObject_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fserial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@frmName_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@frmCaption_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@objName_4'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@objCaption_5'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@objCode_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 192
    Top = 16
  end
  object stpDate: TADOStoredProc
    Connection = ADOConnection
    ProcedureName = 'ConvertToFarsiDate;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FDate'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 8
        Value = Null
      end>
    Left = 40
    Top = 72
  end
  object UserPermit: TADOStoredProc
    Connection = ADOConnection
    ProcedureName = 'Browes_TUser1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 40
    Top = 136
    object UserPermitFserial: TAutoIncField
      FieldName = 'Fserial'
      ReadOnly = True
    end
    object UserPermitFCode: TStringField
      FieldName = 'FCode'
      Size = 50
    end
    object UserPermitFName: TStringField
      FieldName = 'FName'
      Size = 70
    end
    object UserPermitMessage: TWideStringField
      FieldName = 'Message'
      Size = 150
    end
    object UserPermitIsActive: TBooleanField
      FieldName = 'IsActive'
    end
    object UserPermitYearActive: TIntegerField
      FieldName = 'YearActive'
    end
    object UserPermitHouseActive: TIntegerField
      FieldName = 'HouseActive'
    end
    object UserPermitPass: TWideStringField
      FieldName = 'Pass'
      Size = 25
    end
    object UserPermitYearName: TWideStringField
      FieldName = 'YearName'
      Size = 50
    end
    object UserPermitYearCode: TIntegerField
      FieldName = 'YearCode'
    end
    object UserPermitHouseCode: TIntegerField
      FieldName = 'HouseCode'
    end
    object UserPermitHouseName: TWideStringField
      FieldName = 'HouseName'
      Size = 50
    end
    object UserPermityearclose: TBooleanField
      FieldName = 'yearclose'
    end
  end
  object checkuser: TADOStoredProc
    Connection = ADOConnection
    ProcedureName = 'Browes_CheckUser;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@userName'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@pass'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 25
        Value = Null
      end>
    Left = 112
    Top = 72
  end
  object Browse_per: TADOStoredProc
    Connection = ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browes_TUserObiect;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srluser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 112
    Top = 133
    object Browse_perfrmName: TStringField
      FieldName = 'frmName'
      Size = 50
    end
    object Browse_perfrmCaption: TStringField
      FieldName = 'frmCaption'
      Size = 50
    end
    object Browse_perobjName: TStringField
      FieldName = 'objName'
      Size = 50
    end
    object Browse_perobjCaption: TStringField
      FieldName = 'objCaption'
      Size = 50
    end
    object Browse_perobjCode: TIntegerField
      FieldName = 'objCode'
    end
    object Browse_persrlObj: TIntegerField
      FieldName = 'srlObj'
    end
    object Browse_perISvisible: TBooleanField
      FieldName = 'ISvisible'
    end
    object Browse_perIsEnable: TBooleanField
      FieldName = 'IsEnable'
    end
    object Browse_perIsReadOnly: TBooleanField
      FieldName = 'IsReadOnly'
    end
    object Browse_persrlUser: TIntegerField
      FieldName = 'srlUser'
    end
    object Browse_perFserial: TAutoIncField
      FieldName = 'Fserial'
      ReadOnly = True
    end
    object Browse_perFserialUObj: TAutoIncField
      FieldName = 'FserialUObj'
      ReadOnly = True
    end
  end
  object permit11: TADOStoredProc
    Connection = ADOConnection
    ProcedureName = 'Permit11;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 104
  end
  object Browse_Pub_systems: TADOStoredProc
    Connection = ADOConnection
    ProcedureName = 'Browse_Pub_systems;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    Left = 216
    Top = 176
  end
end
