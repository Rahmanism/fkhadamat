unit untRecPay;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt1Panel, DB, ADODB, Buttons, Grids, DBGrids, ExtCtrls,
  unt2Panels, ComCtrls, StdCtrls, U_Common, AppEvnts, IniFiles, Mask;

type
  TfrmRecPay = class(Tfrm1Panel)
    FCode1: TEdit;
    MRecieve1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DtDate1: TMaskEdit;
    Label5: TLabel;
    FactorCode1: TEdit;
    lblFactor: TLabel;
    Label6: TLabel;
    TypeCode1: TEdit;
    lblType: TLabel;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1Type: TIntegerField;
    Browse1srlStd: TIntegerField;
    Browse1MPayment: TBCDField;
    Browse1MRecieve: TBCDField;
    Browse1DtDate: TStringField;
    Browse1srlFactor: TIntegerField;
    Browse1srlYear: TIntegerField;
    Browse1StdCode: TIntegerField;
    Browse1FactorCode: TIntegerField;
    Browse1StdName: TWideStringField;
    Browse1TypeCode: TIntegerField;
    Browse1TypeName: TWideStringField;
    Browse1TypeSerial: TAutoIncField;
    Browse1FactorSerial: TAutoIncField;
    Browse1FactorName: TWideStringField;
    Browse1StdSerial: TAutoIncField;
    MPayment1: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure bSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure FactorCode1Change(Sender: TObject);
    procedure FactorCode1Exit(Sender: TObject);
    procedure FactorCode1KeyPress(Sender: TObject; var Key: Char);
    procedure TypeCode1Change(Sender: TObject);
    procedure TypeCode1Exit(Sender: TObject);
    procedure TypeCode1KeyPress(Sender: TObject; var Key: Char);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    procedure refreshPanel(bfs: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRecPay: TfrmRecPay;
  srlDataSetMaxRecord, srlDataSetPayment, srlDataSetFactor: Integer;

implementation

uses untDm, untPayment;

{$R *.dfm}

procedure TfrmRecPay.bSaveClick(Sender: TObject);
begin
  if (Trim(FCode1.Text) = '') or
    (Trim(FactorCode1.Text) = '') or
    (Trim(DtDate1.Text) = '') or
    (Trim(MPayment1.Text) = '') or
    (Trim(MRecieve1.Text) = '') or
    (Trim(TypeCode1.Text) = '')
    then
  begin
    ShowMessage('������� ���� ����!');
    Exit;
  end;
  inherited;
  if MyState = sAdd then
  begin
    Insert1.Close;
    with Insert1.Parameters do
    begin
      ParamByName('@FCode_1').Value := FCode1.Text;
      ParamByName('@Type_2').Value := srlType;
//      ParamByName('@srlStd_3').Value := srlStd;
      if Self.Tag = 1 then  // Revieve
      begin
        ParamByName('@MPayment_4').Value := 0;
        ParamByName('@MRecieve_5').Value := MRecieve1.Text;
      end
      else if Self.Tag = 0 then // Payment
      begin
        ParamByName('@MPayment_4').Value := MPayment1.Text;
        ParamByName('@MRecieve_5').Value := 0;
      end;
      ParamByName('@DtDate_6').Value := DTDate1.Text;
      ParamByName('@srlFactor_7').Value := srlFactor;
      ParamByName('@srlYear_8').Value := srlYear;
    end;
    Insert1.ExecProc;
    refreshPanel(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value);
  end;
end;

procedure TfrmRecPay.FormShow(Sender: TObject);
begin
  inherited;
  MPayment1.Visible := False;
  MRecieve1.Visible := False;
  if Self.Tag = 1 then //Recieve
  begin
    srlDataSetMaxRecord := 33;
    srlDataSetPayment := 12;
    srlDataSetFactor := 35;
    Caption := '������';
    MRecieve1.Visible := True;
  end
  else if Self.Tag = 0 then // Payment
  begin
    srlDataSetMaxRecord := 32;
    srlDataSetPayment := 12;
    srlDataSetFactor := 34;
    Caption := '������';
    MPayment1.Visible := True;
  end;
  refreshPanel(0);
end;

procedure TfrmRecPay.bAddClick(Sender: TObject);
begin
  inherited;
  FCode1.Text := GetMaxCodeRecord(srlDataSetMaxRecord, srlYear, -1, -1, -1);
  DtDate1.Text := CurrDate;
  MPayment1.Text := '0';
  MRecieve1.Text := '0';
end;

procedure TfrmRecPay.FactorCode1Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, FactorCode1, lblFactor, srlFactor, srlDataSetFactor);
end;

procedure TfrmRecPay.FactorCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, FactorCode1, lblFactor);
end;

procedure TfrmRecPay.FactorCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, FactorCode1, lblFactor, srlFactor, srlDataSetFactor);
end;

procedure TfrmRecPay.TypeCode1Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, TypeCode1, lblType, srlType, 38);
end;

procedure TfrmRecPay.TypeCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, TypeCode1, lblType);
end;

procedure TfrmRecPay.TypeCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, TypeCode1, lblType, srlType, 38);
end;

procedure TfrmRecPay.refreshPanel(bfs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Parameters.ParamByName('@PayOrRec').Value := Self.Tag;
  Browse1.Parameters.ParamByName('@srlYear').Value := srlYear;
  Browse1.Open;
  Browse1.Locate('fserial', bfs, []);
end;

procedure TfrmRecPay.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  srlFactor := Browse1srlFactor.Value;
  inherited;
  srlFactor := Browse1srlFactor.Value;
end;

procedure TfrmRecPay.SpeedButton1Click(Sender: TObject);
begin
  inherited;
    SBFindClick(MyState, FactorCode1, lblFactor, srlFactor, srlDataSetFactor);
end;

procedure TfrmRecPay.SpeedButton2Click(Sender: TObject);
begin
  inherited;
   SBFindClick(MyState, TypeCode1, lblType, srlType, 38);
end;

end.
