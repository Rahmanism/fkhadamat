inherited frmGoods: TfrmGoods
  Left = 201
  Top = 136
  Caption = #1603#1575#1604#1575#1607#1575
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    object Label1: TLabel [1]
      Left = 597
      Top = 44
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object Label2: TLabel [2]
      Left = 595
      Top = 84
      Width = 30
      Height = 13
      Caption = #1593#1606#1608#1575#1606':'
    end
    inherited DBGrid1: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Visible = True
        end>
    end
    object Fcode1: TEdit
      Left = 464
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Fname1: TEdit
      Left = 464
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  inherited Panel2: TPanel
    object Label3: TLabel [1]
      Left = 324
      Top = 128
      Width = 31
      Height = 13
      Caption = 'Label3'
    end
    object lblUnit: TLabel [2]
      Left = 107
      Top = 52
      Width = 20
      Height = 13
      Caption = '.....'
    end
    object Label5: TLabel [3]
      Left = 613
      Top = 19
      Width = 15
      Height = 13
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label6: TLabel [4]
      Left = 613
      Top = 51
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = #1581#1583#1575#1603#1579#1585' '#1605#1608#1580#1608#1583#1610':'
    end
    object Label7: TLabel [5]
      Left = 405
      Top = 51
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = #1581#1583#1575#1602#1604' '#1605#1608#1580#1608#1583#1610':'
    end
    object Label8: TLabel [6]
      Left = 405
      Top = 19
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = #1593#1606#1608#1575#1606':'
    end
    object Label9: TLabel [7]
      Left = 181
      Top = 19
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = #1605#1608#1580#1608#1583#1610':'
    end
    object Label10: TLabel [8]
      Left = 181
      Top = 51
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = #1608#1575#1581#1583':'
    end
    object DBCheckBox1: TDBCheckBox [9]
      Left = 48
      Top = 176
      Width = 15
      Height = 17
      DataField = 'Blocked'
      DataSource = DataSource2
      TabOrder = 8
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      Visible = False
    end
    inherited DBGrid2: TDBGrid
      OnDrawColumnCell = DBGrid2DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Stock'
          Title.Caption = #1605#1608#1580#1608#1583#1610
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlCategory'
          Title.Caption = #1587' '#1711#1585#1608#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MaxStock'
          Title.Caption = #1581#1583#1575#1603#1579#1585' '#1605#1608#1580#1608#1583#1610
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MinStock'
          Title.Caption = #1581#1583#1575#1602#1604' '#1605#1608#1580#1608#1583#1610
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlUnit'
          Title.Caption = #1587' '#1608#1575#1581#1583
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Blocked'
          Title.Caption = #1605#1587#1583#1608#1583
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnitName'
          Title.Caption = #1608#1575#1581#1583
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UnitCode'
          Title.Caption = #1603#1583' '#1608#1575#1581#1583
          Width = 50
          Visible = True
        end>
    end
    object FCode2: TEdit
      Left = 486
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object FName2: TEdit
      Left = 281
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object Stock2: TEdit
      Left = 55
      Top = 17
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object MaxStock2: TEdit
      Left = 487
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object MinStock2: TEdit
      Left = 282
      Top = 47
      Width = 121
      Height = 21
      TabOrder = 5
      Text = 'MinStock2'
    end
    object UnitCode2: TEdit
      Left = 135
      Top = 48
      Width = 42
      Height = 21
      TabOrder = 6
      OnChange = UnitCode2Change
      OnExit = UnitCode2Exit
      OnKeyPress = UnitCode2KeyPress
    end
    object Blocked2: TCheckBox
      Left = 92
      Top = 78
      Width = 84
      Height = 17
      Caption = #1605#1587#1583#1608#1583
      TabOrder = 7
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TGoodsCategory_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browes_TGoodCategory;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TGoodsCategory_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 1
        Value = Null
      end>
  end
  inherited Update2: TADOStoredProc
    ProcedureName = 'update_TGoods_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Stock_5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlCategory_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxStock_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MinStock_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlUnit_9'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Blocked_10'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
  inherited Browse2: TADOStoredProc
    ProcedureName = 'Browes_TGoods;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlCategory'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    object Browse2FSerial: TIntegerField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse2FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse2FName: TWideStringField
      FieldName = 'FName'
      Size = 150
    end
    object Browse2Stock: TIntegerField
      FieldName = 'Stock'
    end
    object Browse2srlCategory: TIntegerField
      FieldName = 'srlCategory'
    end
    object Browse2MaxStock: TIntegerField
      FieldName = 'MaxStock'
    end
    object Browse2MinStock: TIntegerField
      FieldName = 'MinStock'
    end
    object Browse2srlUnit: TIntegerField
      FieldName = 'srlUnit'
    end
    object Browse2Blocked: TBooleanField
      FieldName = 'Blocked'
    end
    object Browse2UnitName: TWideStringField
      FieldName = 'UnitName'
      Size = 50
    end
    object Browse2UnitCode: TIntegerField
      FieldName = 'UnitCode'
    end
  end
  inherited Insert2: TADOStoredProc
    ProcedureName = 'insert_TGoods_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Stock_4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlCategory_5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxStock_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MinStock_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlUnit_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Blocked_9'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
end
