program FKhadamat;

{%ToDo 'FKhadamat.todo'}

uses
  Forms,
  untMain in 'untMain.pas' {frmMain},
  untDm in 'untDm.pas' {frmDm: TDataModule},
  U_Common in 'CommFileDELPHI7\U_Common.pas' {F_Common: TDataModule},
  unt2Panels in 'unt2Panels.pas' {frm2Panels},
  untUser in 'untUser.pas' {frmUser},
  unt1Panel in 'unt1Panel.pas' {frm1Panel},
  untSellBuy in 'untSellBuy.pas' {frmSellBuy},
  untAbout in 'untAbout.pas' {AboutBox},
  untPass in 'untPass.pas' {frmPass},
  untLoan in 'untLoan.pas' {frmLoan},
  untBuyCenters in 'untBuyCenters.pas' {frmBuyCenters},
  untStudents in 'untStudents.pas' {frmStudents},
  untRecPay in 'untRecPay.pas' {frmRecPay},
  untReceive in 'untReceive.pas' {frmReceive},
  untPayment in 'untPayment.pas' {frmPayment},
  untParentReport in 'untParentReport.pas' {frmParentReport},
  untRepsell in 'untRepsell.pas' {frmRepsell},
  untBuy in 'untBuy.pas' {frmBuy},
  untBackBuy in 'untBackBuy.pas' {frmBackBuy},
  untSell in 'untSell.pas' {frmSell},
  untBackSell in 'untBackSell.pas' {frmBackSell},
  untPreFactor in 'untPreFactor.pas' {frmPreFactor},
  untFirstStock in 'untFirstStock.pas' {frmFirstStock},
  untMoveFromWarehouse in 'untMoveFromWarehouse.pas' {frmMoveFromWarehouse},
  Convert_Dates in 'Convert_Dates.pas' {DateForm},
  untHelp in 'untHelp.pas' {frmHelp},
  untGift in 'untGift.pas' {frmGift},
  untParcel in 'untParcel.pas' {frmParcel};

{$R *.res}

begin
//  Application.CreateForm(TfrmMain, frmMain);
  Application.Title := '�������� ���Ԑ�� ������ ���� ����� ������� �����';
  Application.CreateForm(TfrmDm, frmDm);
  Application.CreateForm(TF_Common, F_Common);
  frmPass := TfrmPass.Create(Application);
  frmPass.ShowModal;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TDateForm, DateForm);
  Application.Initialize;
  Application.CreateForm(Tfrm1Panel, frm1Panel);
  Application.Run;
end.
