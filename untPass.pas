unit untPass;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmPass = class(TForm)
    pass: TEdit;
    Label1: TLabel;
    btnEnter: TButton;
    btnExit: TButton;
    Image1: TImage;
    Label2: TLabel;
    username: TEdit;
    procedure btnExitClick(Sender: TObject);
    procedure btnEnterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  p = '1';
  maxPassCounter = 3;

var
  frmPass: TfrmPass;
  canEnter: Boolean;
  passCounter: Byte;

implementation

uses untDm, U_Common;

{$R *.dfm}

procedure TfrmPass.btnExitClick(Sender: TObject);
begin
  Halt;
end;

procedure TfrmPass.btnEnterClick(Sender: TObject);
begin
  Inc(passCounter);
  frmDm.checkuser.Close;
  frmDM.checkuser.Parameters.ParamByName('@userName').Value := username.Text;
  frmDM.checkuser.Parameters.ParamByName('@pass').Value := pass.Text;
  frmDm.checkuser.Open;
  if frmDm.checkuser.RecordCount > 0 then
  begin
    srlUser := frmDm.checkuser.FieldValues['FSerial'];
    canEnter := True;
    frmDm.UserPermission;
    Close;
  end
  else
  begin
    if passCounter < maxPassCounter then
    begin
      ShowMessage('��� ���� �� ��� ������ ������ ���!' + #13 + '��� '+
      IntToStr(maxPassCounter - passCounter) + ' ��� ��� ���� �����.');
    end
    else
    begin
      ShowMessage('��� ��� ����� ������.');
      Halt;
    end
  end
end;

procedure TfrmPass.FormCreate(Sender: TObject);
begin
  canEnter := False;
  passCounter := 0;
end;

procedure TfrmPass.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := canEnter;
end;

procedure TfrmPass.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmPass.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then
    Perform(7388420, vk_Tab, 0);
end;

end.
