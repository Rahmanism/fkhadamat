unit untWareHouse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt1Panel, DB, ADODB, Buttons, Grids, DBGrids, ExtCtrls,
  unt2Panels, ComCtrls, StdCtrls, U_Common, AppEvnts, IniFiles;

type
  TfrmWareHouse = class(Tfrm1Panel)
    FCode1: TEdit;
    FName1: TEdit;
    Boss1: TEdit;
    Browse1FSerial: TIntegerField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1Boss: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure bSaveClick(Sender: TObject);
    procedure FCode1KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
  private
    procedure refreshPanel(bfs: Integer);
    { Private declarations }
  public
    { Public declarations }

  end;

var
  frmWareHouse: TfrmWareHouse;

implementation

uses untDm;

{$R *.dfm}

procedure TfrmWareHouse.bSaveClick(Sender: TObject);
begin
  inherited;
  if (MyState=sAdd)  then
        begin
            Insert1.Parameters.ParamByName('@Fcode_2').Value:=Fcode1.Text;
            Insert1.Parameters.ParamByName('@Fname_3').Value:=Fname1.Text;
            Insert1.Parameters.ParamByName('@Boss_4').Value:=Boss1.Text;
            Insert1.ExecProc;
            Browse1.Close;
            Browse1.Open;
            Browse1.Locate('fserial',Insert1.Parameters.ParamByName('@RETURN_VALUE').Value,[]);
        end;
    if (MyState=sedit) then
      begin
            Update1.Parameters.ParamByName('@FSerial_1').Value:=srltemp;
            Update1.Parameters.ParamByName('@FCode_3').Value:=Fcode1.Text;
            Update1.Parameters.ParamByName('@FName_4').Value:=Fname1.Text;
            Update1.Parameters.ParamByName('@Boss_5').Value:=Boss1.Text;
            Update1.ExecProc;
            Browse1.Close;
            Browse1.Open;
            Browse1.Locate('fserial',srltemp,[]);
     end;
end;

procedure TfrmWareHouse.FCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ValidKey) then

end;

procedure TfrmWareHouse.Button1Click(Sender: TObject);
//var
//  f: TIniFile
begin
  inherited;
  F1.WriteInteger('Year', 'CurrentYear', Browse1FSerial.Value);
end;

procedure TfrmWareHouse.refreshPanel(bfs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', bfs, []);
end;

procedure TfrmWareHouse.FormShow(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmWareHouse.bDelClick(Sender: TObject);
begin
  inherited;
  DeleteRecord1(13, Browse1FSerial.Value, Browse1);
end;

procedure TfrmWareHouse.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmWareHouse.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
    srltemp:=Browse1FSerial.Value;
end;

end.
