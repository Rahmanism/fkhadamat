unit untUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt1Panel, DB, ADODB, Buttons, Grids, DBGrids, ExtCtrls,
  unt2Panels, ComCtrls, StdCtrls, U_Common, AppEvnts;

type
  TfrmUnit = class(Tfrm1Panel)
    FCode1: TEdit;
    FName1: TEdit;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Label1: TLabel;
    Label2: TLabel;
    procedure bSaveClick(Sender: TObject);
    procedure FCode1KeyPress(Sender: TObject; var Key: Char);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure bAddClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
  private
    procedure refreshPanel(bfs: Integer);
    { Private declarations }
  public
    { Public declarations }
    
  end;

var
  frmUnit: TfrmUnit;

implementation

uses untDm;

{$R *.dfm}

procedure TfrmUnit.bSaveClick(Sender: TObject);
var
  i:Integer;
begin
  inherited;
  if MyState = sAdd then
  begin
    NullAdoSPParameters(Insert1);
   with Insert1.Parameters do
    begin
      ParamByName('@FCode_2').Value := FCode1.Text;
      ParamByName('@FName_3').Value := FName1.Text;
       end;
    Insert1.ExecProc;
    Browse1.Close;
    Browse1.Open;
    Browse1.Locate('fserial',Insert1.Parameters.ParamByName('@RETURN_VALUE').Value,[]);
  end;
  if MyState = sedit then
    begin
    i:=srltemp;
   NullAdoSPParameters(Update1);
    with Update1.Parameters do
    begin
      ParamByName('@FSerial_1').Value := srltemp;
      ParamByName('@FCode_3').Value := FCode1.Text;
      ParamByName('@FName_4').Value := FName1.Text;
       end;
    Update1.ExecProc;
    Browse1.Close;
    Browse1.Open;
    Browse1.Locate('fserial',i,[]) ;
    end;


end;

procedure TfrmUnit.FCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ValidKey) then

end;

procedure TfrmUnit.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
  inherited;
  if Msg.message = WM_RBUTTONDOWN then
    ShowMessage('sdfdsfd');
end;

procedure TfrmUnit.bAddClick(Sender: TObject);
begin
  inherited;
    Fcode1.Text:=GetMaxCodeRecord(16) ;
end;

procedure TfrmUnit.bDelClick(Sender: TObject);
begin
  inherited;
  DeleteRecord1(16, Browse1FSerial.Value, Browse1);
end;

procedure TfrmUnit.refreshPanel(bfs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', bfs, []);
end;

procedure TfrmUnit.FormShow(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmUnit.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmUnit.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
     srltemp:=Browse1FSerial.Value;
end;

end.
