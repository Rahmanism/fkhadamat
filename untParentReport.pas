unit untParentReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, Grids, DBGrids, ADODB, StdCtrls, Buttons, ComCtrls;

type
  TfrmParentReport = class(TForm)
    Browse1: TADOStoredProc;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Edit1: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    StatusBar2: TStatusBar;
    Edit2: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1Change(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure Edit2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParentReport: TfrmParentReport;
  srlYearRep1,srlYearRep2 :integer;

implementation
uses
U_Common, untDm;



{$R *.dfm}

procedure TfrmParentReport.DBGrid1DblClick(Sender: TObject);
begin
//   SetGridColumnWidths(DBGrid1);
end;

procedure TfrmParentReport.DBGrid1TitleClick(Column: TColumn);
begin
     SortDBGrid1(Column);
end;

procedure TfrmParentReport.FormShow(Sender: TObject);
begin
//  N_System:='FKhadamat';
  GlobalFormName:=Self.Name;
  GlobalFormCaption:=self.Caption;
  GlobalDBGrid:=DBGrid1;
  AddmyComponentToTable(Sender);
  CheckMyPermission(Sender);
  ReSizeDBGrid(DBGrid1);
end;

procedure TfrmParentReport.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     GlobalDBGrid:=DBGrid1;
 if key=VK_RETURN then
 Perform(7388420, vk_Tab, 0);

  if Key = vk_up then
      Browse1.Prior
    else if Key = vk_down then
      Browse1.Next;
  if ((char(key) in ['f','F'])  and (Shift = [ssCtrl])) then
    FilterDBGrid2(GlobalDBGrid)
  else  if ((char(key) in ['h','H'])  and (Shift = [ssCtrl])) then
    FilterDBGrid3(GlobalDBGrid)
  else  if ((char(key) in ['g','G'])  and (Shift = [ssCtrl])) then
    FilterDBGrid(GlobalDBGrid)
  else  if ((char(key) in ['p','P'])  and (Shift = [ssCtrl])) then
    PrintDBGrid(GlobalDBGrid)
  else  if ((char(key) in ['o','O'])  and (Shift = [ssCtrl])) then
    OptionDBGrid(GlobalDBGrid)
  else if (key = vk_escape) then
    TDBGrid(Sender).DataSource.DataSet.Filtered:=false
  else if (key = vk_F6) then
    FilterByEditDBGrid(GlobalDBGrid)
end;

procedure TfrmParentReport.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   DBGridSaveToFile(DBGrid1);
   Action := caFree;
end;

procedure TfrmParentReport.Edit1Change(Sender: TObject);

begin
 EditCodeChange(sView,Edit1,Label2,srlYearRep1,40);
end;

procedure TfrmParentReport.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
    MyDBGridRecordCount(GlobalDBGrid);
end;

procedure TfrmParentReport.Edit2Change(Sender: TObject);
begin
      EditCodeChange(sView,Edit2,Label3,srlYearRep2,40);
end;

end.
