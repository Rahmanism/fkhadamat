inherited frmTransfer: TfrmTransfer
  Left = 211
  Top = 121
  Caption = #1575#1606#1578#1602#1575#1604' '#1605#1608#1580#1608#1583#1610
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    object lblStd: TLabel [0]
      Left = 249
      Top = 38
      Width = 32
      Height = 13
      Caption = #1582#1585#1610#1583#1575#1585':'
    end
    object Label1: TLabel [2]
      Left = 631
      Top = 38
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object Label2: TLabel [3]
      Left = 438
      Top = 38
      Width = 30
      Height = 13
      Caption = #1593#1606#1608#1575#1606':'
    end
    object Label4: TLabel [4]
      Left = 631
      Top = 60
      Width = 25
      Height = 13
      Caption = #1578#1575#1585#1610#1582':'
    end
    object Label5: TLabel [5]
      Left = 438
      Top = 60
      Width = 63
      Height = 13
      Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578':'
    end
    object Label6: TLabel [6]
      Left = 247
      Top = 60
      Width = 30
      Height = 13
      Caption = #1607#1586#1610#1606#1607':'
    end
    object Label7: TLabel [7]
      Left = 631
      Top = 84
      Width = 34
      Height = 13
      Caption = #1578#1582#1601#1610#1601':'
    end
    object lblSBName: TLabel [8]
      Left = 175
      Top = 38
      Width = 12
      Height = 13
      Caption = '...'
    end
    object StdCode1: TEdit [9]
      Left = 192
      Top = 32
      Width = 49
      Height = 21
      TabOrder = 3
      OnChange = StdCode1Change
      OnExit = StdCode1Exit
      OnKeyPress = StdCode1KeyPress
    end
    inherited DBGrid1: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Title.Caption = #1578#1575#1585#1610#1582
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MExtra'
          Title.Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Type'
          Title.Caption = #1606#1608#1593
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCost'
          Title.Caption = #1607#1586#1610#1606#1607
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MDiscount'
          Title.Caption = #1578#1582#1601#1610#1601
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsClose'
          Title.Caption = #1605#1587#1583#1608#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlYear'
          Title.Caption = #1587' '#1587#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlLoan'
          Title.Caption = #1587' '#1608#1575#1605
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlStd'
          Title.Caption = #1587' '#1591#1604#1576#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlBuyCenter'
          Title.Caption = #1587' '#1605#1585#1603#1586' '#1582#1585#1610#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BuyCenterCode'
          Title.Caption = #1603#1583' '#1605#1585#1603#1586' '#1582#1585#1610#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdCode'
          Title.Caption = #1603#1583' '#1591#1604#1576#1607
          Width = 50
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 504
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object FName1: TEdit
      Left = 312
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object MCost1: TEdit
      Left = 120
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 6
    end
    object MExtra1: TEdit
      Left = 312
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 5
    end
    object MDiscount1: TEdit
      Left = 504
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 7
    end
    object IsClose1: TCheckBox
      Left = 336
      Top = 80
      Width = 97
      Height = 17
      Caption = #1576#1587#1578#1607' '#1575#1587#1578
      TabOrder = 8
    end
    object DtDate1: TMaskEdit
      Left = 504
      Top = 56
      Width = 122
      Height = 21
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 4
      Text = '  /  /  '
    end
  end
  inherited Panel2: TPanel
    object Label3: TLabel [1]
      Left = 249
      Top = 38
      Width = 27
      Height = 13
      Caption = #1578#1593#1583#1575#1583':'
    end
    object Label8: TLabel [2]
      Left = 244
      Top = 60
      Width = 56
      Height = 13
      Caption = #1601#1610' '#1610#1603' '#1593#1583#1583':'
    end
    object Label9: TLabel [3]
      Left = 645
      Top = 60
      Width = 48
      Height = 13
      Caption = #1602#1610#1605#1578' '#1606#1602#1583':'
    end
    object Label12: TLabel [4]
      Left = 645
      Top = 38
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object lblGood: TLabel [5]
      Left = 581
      Top = 37
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label10: TLabel [6]
      Left = 441
      Top = 62
      Width = 58
      Height = 13
      Caption = #1602#1610#1605#1578' '#1606#1587#1610#1607':'
    end
    object Label11: TLabel [7]
      Left = 244
      Top = 14
      Width = 51
      Height = 13
      Caption = #1575#1606#1576#1575#1585' '#1605#1602#1589#1583':'
    end
    object lblWarehouse2: TLabel [8]
      Left = 168
      Top = 15
      Width = 12
      Height = 13
      Caption = '...'
    end
    inherited DBGrid2: TDBGrid
      TitleFont.Height = -13
      TitleFont.Name = 'B Nazanin'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlGood'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'GoodCode'
          Title.Caption = #1603#1583' '#1603#1575#1604#1575
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1606#1575#1605' '#1603#1575#1604#1575
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NCount'
          Title.Caption = #1578#1593#1583#1575#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NRemain'
          Title.Caption = #1605#1575#1606#1583#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unit'
          Title.Caption = #1608#1575#1581#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCash'
          Title.Caption = #1606#1602#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCredit'
          Title.Caption = #1606#1587#1610#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MPrice'
          Title.Caption = #1601#1610
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WarehouseCode'
          Title.Caption = #1603#1583' '#1575#1606#1576#1575#1585
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlWarehouse'
          Title.Caption = #1587' '#1575#1606#1576#1575#1585
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlFactor'
          Title.Caption = #1587' '#1601#1575#1603#1578#1608#1585
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WarehouseName'
          Title.Caption = #1606#1575#1605' '#1575#1606#1576#1575#1585
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlSource'
          Title.Caption = #1587' '#1601#1575#1603#1578#1608#1585' '#1575#1589#1604#1610
          Visible = True
        end>
    end
    object GoodCode2: TEdit
      Left = 600
      Top = 32
      Width = 41
      Height = 21
      TabOrder = 1
      OnChange = GoodCode2Change
      OnExit = GoodCode2Exit
      OnKeyPress = GoodCode2KeyPress
    end
    object MCash2: TEdit
      Left = 520
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object MPrice2: TEdit
      Left = 120
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 6
    end
    object NCount2: TEdit
      Left = 120
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object MCredit2: TEdit
      Left = 312
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 5
    end
    object WarehouseCode2: TEdit
      Left = 184
      Top = 8
      Width = 57
      Height = 21
      TabOrder = 3
      OnChange = WarehouseCode2Change
      OnExit = WarehouseCode2Exit
      OnKeyPress = WarehouseCode2KeyPress
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TFactor_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@srlStd_4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DtDate_6'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MExtra_7'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Type_8'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@MCost_9'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MDiscount_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@IsClose_11'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@srlYear_12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlLoan_13'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browse_TFactor;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Type'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlWarehouse'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object Browse1DtDate: TStringField
      FieldName = 'DtDate'
      FixedChar = True
      Size = 8
    end
    object Browse1MExtra: TBCDField
      FieldName = 'MExtra'
      Precision = 19
    end
    object Browse1Type: TSmallintField
      FieldName = 'Type'
    end
    object Browse1MCost: TBCDField
      FieldName = 'MCost'
      Precision = 19
    end
    object Browse1MDiscount: TBCDField
      FieldName = 'MDiscount'
      Precision = 19
    end
    object Browse1IsClose: TBooleanField
      FieldName = 'IsClose'
    end
    object Browse1srlYear: TIntegerField
      FieldName = 'srlYear'
    end
    object Browse1srlLoan: TIntegerField
      FieldName = 'srlLoan'
    end
    object Browse1srlStd: TIntegerField
      FieldName = 'srlStd'
    end
    object Browse1srlBuyCenter: TIntegerField
      FieldName = 'srlBuyCenter'
    end
    object Browse1BuyCenterCode: TIntegerField
      FieldName = 'BuyCenterCode'
    end
    object Browse1StdCode: TIntegerField
      FieldName = 'StdCode'
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TFactor_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@srlStd_4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DtDate_7'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MExtra_8'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MCost_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MDiscount_11'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@IsClose_12'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
  inherited Update2: TADOStoredProc
    ProcedureName = 'update_TDetFac_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MCash_9'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MCredit_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MPrice_11'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end>
  end
  inherited Browse2: TADOStoredProc
    ProcedureName = 'Browse_TDetFac;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    object Browse2FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse2srlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object Browse2NCount: TIntegerField
      FieldName = 'NCount'
    end
    object Browse2NRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object Browse2srlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object Browse2srlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object Browse2srlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object Browse2MCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object Browse2MCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object Browse2MPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object Browse2FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object Browse2WarehouseName: TWideStringField
      FieldName = 'WarehouseName'
      Size = 50
    end
    object Browse2Unit: TWideStringField
      FieldName = 'Unit'
      Size = 50
    end
    object Browse2GoodCode: TIntegerField
      FieldName = 'GoodCode'
    end
    object Browse2WarehouseCode: TIntegerField
      FieldName = 'WarehouseCode'
    end
  end
  inherited Insert2: TADOStoredProc
    ProcedureName = 'insert_TDetFac_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlGood_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NCount_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlSource_5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlWarehouse_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MCash_8'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MCredit_9'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MPrice_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end>
  end
  object dtsGoods: TDataSource
    DataSet = GoodsBrowse
    Left = 24
    Top = 480
  end
  object GoodsBrowse: TADOStoredProc
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browes_TDetFacRemains;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlGood1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 24
    Top = 432
    object GoodsBrowseFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object GoodsBrowsesrlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object GoodsBrowseNCount: TIntegerField
      FieldName = 'NCount'
    end
    object GoodsBrowseNRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object GoodsBrowsesrlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object GoodsBrowsesrlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object GoodsBrowsesrlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object GoodsBrowseMCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object GoodsBrowseMCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object GoodsBrowseMPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object GoodsBrowseType: TSmallintField
      FieldName = 'Type'
    end
  end
  object GoodsCountUpdate: TADOStoredProc
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TDetFacRemain_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 105
    Top = 432
  end
  object PreToFactor: TADOStoredProc
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TFactor_PreToFactor_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 97
    Top = 97
  end
  object dtsDetPreToFactor: TDataSource
    DataSet = DetPreToFactor
    Left = 280
    Top = 432
  end
  object DetPreToFactor: TADOStoredProc
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_TDetFac;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 195
    Top = 432
    object DetPreToFactorFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object DetPreToFactorsrlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object DetPreToFactorNCount: TIntegerField
      FieldName = 'NCount'
    end
    object DetPreToFactorNRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object DetPreToFactorsrlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object DetPreToFactorsrlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object DetPreToFactorsrlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object DetPreToFactorMCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object DetPreToFactorMCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object DetPreToFactorMPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object DetPreToFactorFName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object DetPreToFactorWarehouseName: TWideStringField
      FieldName = 'WarehouseName'
      Size = 50
    end
    object DetPreToFactorUnit: TWideStringField
      FieldName = 'Unit'
      Size = 50
    end
    object DetPreToFactorGoodCode: TIntegerField
      FieldName = 'GoodCode'
    end
    object DetPreToFactorWarehouseCode: TIntegerField
      FieldName = 'WarehouseCode'
    end
  end
  object ADOStoredProc2: TADOStoredProc
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TDetFacRemain_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 196
    Top = 481
  end
end
