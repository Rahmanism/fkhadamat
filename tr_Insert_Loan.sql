CREATE TRIGGER [tr_Insert_Loan] ON [TLoan] 
FOR INSERT
AS
DECLARE @srlLoan INT
DECLARE @DtDate CHAR(8)
DECLARE @MAmount MONEY
DECLARE @NInstalment SMALLINT
DECLARE @srlStd INT
DECLARE @srlYear INT
DECLARE @srlBail1 INT
DECLARE @srlBail2 INT
DECLARE @NCheque1 NVARCHAR(50)
DECLARE @NCheque2 NVARCHAR(50)

SELECT @srlLoan = (SELECT FSerial FROM TLoan)
SELECT @DtDate = (SELECT DtDate FROM TLoan)
SELECT @MAmount = (SELECT MAmount FROM TLoan)
SELECT @NInstalment = (SELECT NInstalment FROM TLoan)
SELECT @srlStd = (SELECT srlStd FROM TLoan)
SELECT @srlYear = (SELECT srlYear FROM TLoan)
SELECT @srlBail1 = (SELECT srlBail1 FROM TLoan)
SELECT @srlBail2 = (SELECT srlBail2 FROM TLoan)
SELECT @NCheque1 = (SELECT NCheque1 FROM TLoan)
SELECT @NCheque2 = (SELECT NCheque2 FROM TLoan)

DECLARE @MAmountForFirstInstalment MONEY
DECLARE @MInstalmentAmount MONEY
DECLARE @MInstalmentAmount2 MONEY
SELECT @MInstalmentAmount = (@MAmount / @NInstalment)
SELECT @MInstalmentAmount2 = @MInstalmentAmount - (FLOOR(@MInstalmentAmount / 1000) * 1000)
SELECT @MInstalmentAmount = @MInstalmentAmount - @MInstalmentAmount2
SELECT @MAmountForFirstInstalment = (@MInstalmentAmount + (@MInstalmentAmount2 * @NInstalment))

DECLARE @NextDate char(8)
SELECT @NextDate = substring(dbo.Fn_GetNextMonthDate('13' + @DtDate, 1), 3, 8)
INSERT INTO TInstalment
	(DtDate, DtPayment, srlLoan, MAmount, Paid)
	VALUES (@DtDate, @NextDate, @srlLoan, @MAmountForFirstInstalment, 0)

DECLARE @Counter int
SELECT @Counter = 2

WHILE (@Counter <= @NInstalment)
BEGIN
	SELECT @NextDate = substring(dbo.Fn_GetNextMonthDate('13' + @NextDate, 1), 3, 8)
	INSERT INTO TInstalment
		(DtDate, DtPayment, srlLoan, MAmount, Paid)
		VALUES (@DtDate, @NextDate, @srlLoan, @MInstalmentAmount, 0)
	SELECT @Counter = (@Counter + 1)
END
