unit untLoan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt2Panels, DB, ADODB, Buttons, ComCtrls, Grids, DBGrids,
  ExtCtrls, StdCtrls, Mask, untDm, U_Common, Math, DBCtrls;

type
  TfrmLoan = class(Tfrm2Panels)
    FCode1: TEdit;
    FName1: TEdit;
    MAmount1: TEdit;
    NInstalment1: TEdit;
    StdFCode1: TEdit;
    Bail1FCode1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    lblStd: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lblStdFName: TLabel;
    MAmount2: TEdit;
    Label9: TLabel;
    DtDate1: TMaskEdit;
    dtsGoods: TDataSource;
    GoodsBrowse: TADOStoredProc;
    GoodsCountUpdate: TADOStoredProc;
    GoodsBrowseFSerial: TAutoIncField;
    GoodsBrowsesrlGood: TIntegerField;
    GoodsBrowseNCount: TIntegerField;
    GoodsBrowseNRemain: TIntegerField;
    GoodsBrowsesrlSource: TIntegerField;
    GoodsBrowsesrlWarehouse: TIntegerField;
    GoodsBrowsesrlFactor: TIntegerField;
    GoodsBrowseMCash: TBCDField;
    GoodsBrowseMCredit: TBCDField;
    GoodsBrowseMPrice: TBCDField;
    GoodsBrowseType: TSmallintField;
    dtsDetPreToFactor: TDataSource;
    DetPreToFactor: TADOStoredProc;
    ADOStoredProc2: TADOStoredProc;
    DetPreToFactorFSerial: TAutoIncField;
    DetPreToFactorsrlGood: TIntegerField;
    DetPreToFactorNCount: TIntegerField;
    DetPreToFactorNRemain: TIntegerField;
    DetPreToFactorsrlSource: TIntegerField;
    DetPreToFactorsrlWarehouse: TIntegerField;
    DetPreToFactorsrlFactor: TIntegerField;
    DetPreToFactorMCash: TBCDField;
    DetPreToFactorMCredit: TBCDField;
    DetPreToFactorMPrice: TBCDField;
    DetPreToFactorFName: TWideStringField;
    DetPreToFactorWarehouseName: TWideStringField;
    DetPreToFactorUnit: TWideStringField;
    DetPreToFactorGoodCode: TIntegerField;
    DetPreToFactorWarehouseCode: TIntegerField;
    Label11: TLabel;
    Bail2FCode1: TEdit;
    lblBail1FName: TLabel;
    lblBail2FName: TLabel;
    Label13: TLabel;
    NCheque11: TEdit;
    Label14: TLabel;
    NCheque21: TEdit;
    Label12: TLabel;
    DtDate2: TMaskEdit;
    Label3: TLabel;
    DtPayment2: TMaskEdit;
    Browse2FSerial: TAutoIncField;
    Browse2DtDate: TStringField;
    Browse2DtPayment: TStringField;
    Browse2srlLoan: TIntegerField;
    Browse2MAmount: TBCDField;
    Browse2Paid: TBooleanField;
    Paid2: TCheckBox;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1DtDate: TStringField;
    Browse1MAmount: TBCDField;
    Browse1NInstalment: TSmallintField;
    Browse1IsClose: TBooleanField;
    Browse1srlStd: TIntegerField;
    Browse1srlYear: TIntegerField;
    Browse1srlBail1: TIntegerField;
    Browse1srlBail2: TIntegerField;
    Browse1NCheque1: TWideStringField;
    Browse1NCheque2: TWideStringField;
    Browse1StdFCode: TIntegerField;
    Browse1StdFName: TWideStringField;
    Browse1Bail1FCode: TIntegerField;
    Browse1Bail1FName: TWideStringField;
    Browse1Bail2FCode: TIntegerField;
    Browse1Bail2FName: TWideStringField;
    bbnPayment: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure StdFCode1Change(Sender: TObject);
    procedure StdFCode1Exit(Sender: TObject);
    procedure StdFCode1KeyPress(Sender: TObject; var Key: Char);
    procedure bSaveClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Bail1FCode1Change(Sender: TObject);
    procedure Bail1FCode1Exit(Sender: TObject);
    procedure Bail1FCode1KeyPress(Sender: TObject; var Key: Char);
    procedure Bail2FCode1Change(Sender: TObject);
    procedure Bail2FCode1Exit(Sender: TObject);
    procedure Bail2FCode1KeyPress(Sender: TObject; var Key: Char);
    procedure bbnPaymentClick(Sender: TObject);
    procedure DataSource2DataChange(Sender: TObject; Field: TField);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bCancelClick(Sender: TObject);
  private
    procedure refreshPanels(b1fs, b2fs: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLoan: TfrmLoan;
  srlDataSetGoods, srlDataSetMaxRecord, srlDataSetStd,
  srlBail1, srlBail2, srlLoan: Integer;

implementation

{$R *.dfm}

procedure TfrmLoan.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  srlLoan := Browse1FSerial.Value;
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlLoan').Value := Browse1FSerial.Value;
  Browse2.Open;
end;

procedure TfrmLoan.StdFCode1Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, StdFCode1, lblStdFName, srlStd, srlDataSetStd);
end;

procedure TfrmLoan.StdFCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, StdFCode1, lblStdFName);
end;

procedure TfrmLoan.StdFCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, StdFCode1, lblStdFName, srlStd, srlDataSetStd);
end;

procedure TfrmLoan.bSaveClick(Sender: TObject);
begin
  inherited;
  if panelfocus = 'Panel1' then
  begin
    if MyState = sAdd then
    begin
      Insert1.Parameters.ParamByName('@Fcode_2').Value := Fcode1.Text;
      Insert1.Parameters.ParamByName('@FName_3').Value := Fname1.Text;
      Insert1.Parameters.ParamByName('@DtDate_4').Value := DtDate1.Text;
      Insert1.Parameters.ParamByName('@MAmount_5').Value := MAmount1.Text;
      Insert1.Parameters.ParamByName('@NInstalment_6').Value := NInstalment1.Text;
      Insert1.Parameters.ParamByName('@IsClose_7').Value := False; //IsClose1.Checked;
      Insert1.Parameters.ParamByName('@srlStd_8').Value := srlStd;
      Insert1.Parameters.ParamByName('@srlYear_9').Value := srlYear;
      Insert1.Parameters.ParamByName('@srlBail1_10').Value := srlBail1;
      Insert1.Parameters.ParamByName('@srlBail2_11').Value := srlBail2;
      Insert1.Parameters.ParamByName('@NCheque1_12').Value := NCheque11.Text;
      Insert1.Parameters.ParamByName('@NCheque2_13').Value := NCheque21.Text;
      Insert1.ExecProc;
      refreshPanels(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value, 0);
    end
  end
  else
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  if panelfocus = 'Panel2' then  ///////////
  begin
    if MyState = sEdit then
    begin
    end;
  end;
  MyState := sView;
end;

procedure TfrmLoan.bAddClick(Sender: TObject);
begin
  inherited;
  DtDate1.Text := CurrDate;
  if panelfocus = 'Panel1' then
    Fcode1.Text := GetMaxCodeRecord(srlDataSetMaxRecord, srlYear, -1, -1, -1)
  else
    bCancel.OnClick(bCancel);
end;

procedure TfrmLoan.bDelClick(Sender: TObject);
begin
  inherited;
//  DeleteRecord1(23, Browse2FSerial.Value, Browse2);
end;

procedure TfrmLoan.FormShow(Sender: TObject);
begin
  inherited;
  srlDataSetMaxRecord := 36;
  srlDataSetStd := 12;
  refreshPanels(0, 0);
end;

procedure TfrmLoan.Bail1FCode1Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, Bail1FCode1, lblBail1FName, srlBail1, srlDataSetStd);
end;

procedure TfrmLoan.Bail1FCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, Bail1FCode1, lblBail1FName);
end;

procedure TfrmLoan.Bail1FCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, Bail1FCode1, lblBail1FName, srlBail1, srlDataSetStd);
end;

procedure TfrmLoan.Bail2FCode1Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, Bail2FCode1, lblBail2FName, srlBail2, srlDataSetStd);
end;

procedure TfrmLoan.Bail2FCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, Bail2FCode1, lblBail2FName);
end;

procedure TfrmLoan.Bail2FCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, Bail2FCode1, lblBail2FName, srlBail2, srlDataSetStd);
end;

procedure TfrmLoan.bbnPaymentClick(Sender: TObject);
var
  b1fs, b2fs: Integer;
begin
  IfNullThenZero(Browse1FSerial.Value, b2fs);
  inherited;
  if MessageDlgTranslated('��� ������Ͽ', mtConfirmation, [mbYes, mbNo], mbNo, 0) = mrYes then
  begin
    if Browse2.FieldByName('Paid').AsBoolean then
    begin
      DtPayment2.Text := '00/00/00';
      Paid2.Checked := False;
    end
    else
    begin
      DtPayment2.Text := CurrDate;
      Paid2.Checked := True;
    end;
    Update2.Parameters.ParamByName('@FSerial_1').Value := Browse2.FieldValues['FSerial'];
    Update2.Parameters.ParamByName('@DtPayment_2').Value := DtPayment2.Text;
    Update2.Parameters.ParamByName('@Paid_3').Value := Paid2.Checked;
    Update2.ExecProc;
    b1fs := Browse2.FieldValues['FSerial'];
    refreshPanels(b1fs, b2fs);
  end
end;

procedure TfrmLoan.DataSource2DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Browse2.FieldByName('Paid').AsBoolean  then
    bbnPayment.Caption := '��� ������      (F9)'
  else
    bbnPayment.Caption := '������ ���      (F9)';
end;

procedure TfrmLoan.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const IsChecked : array[Boolean] of Integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  DrawState: Integer;
  DrawRect: TRect;
begin
  if (gdFocused in State) then
  begin
    if (Column.Field.FieldName = DBCheckBox1.DataField) then
    begin

     DBCheckBox1.Left := Rect.Left + TDBGrid(Sender).Left + 2;
     DBCheckBox1.Top := Rect.Top + TDBGrid(Sender).top + 2;
     DBCheckBox1.Width := Rect.Right - Rect.Left;
     DBCheckBox1.Height := Rect.Bottom - Rect.Top;

     DBCheckBox1.Visible := True;
    end;
 {   if (Column.Field.FieldName = DBLookupComboBox1.DataField) then
    with DBLookupComboBox1 do begin
      Left := Rect.Left + DBGrid1.Left + 2;
      Top := Rect.Top + DBGrid1.Top + 2;
      Width := Rect.Right - Rect.Left;
      Width := Rect.Right - Rect.Left;
      Height := Rect.Bottom - Rect.Top;

      Visible := True;
    end      }
  end
  else {in this else area draw any "stay behind" bitmaps}
  begin
    if (Column.Field.FieldName = DBCheckBox1.DataField) then
    begin
      DrawRect:=Rect;
      InflateRect(DrawRect,-1,-1);

      DrawState := ISChecked[Column.Field.AsBoolean];

      TDBGrid(Sender).Canvas.FillRect(Rect);
      DrawFrameControl(TDBGrid(Sender).Canvas.Handle, DrawRect, DFC_BUTTON, DrawState);
    end;
  end; //if focused
end;

procedure TfrmLoan.refreshPanels(b1fs, b2fs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlLoan').Value := Browse1FSerial.Value;
  Browse2.Open;
  Browse2.Locate('FSerial', b2fs, []);
end;

procedure TfrmLoan.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_F9) then
    bbnPayment.OnClick(Sender);
end;

procedure TfrmLoan.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanels(0, 0);
end;

end.
