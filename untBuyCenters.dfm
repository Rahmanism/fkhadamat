inherited frmBuyCenters: TfrmBuyCenters
  Left = 195
  Top = 186
  Caption = #1605#1585#1575#1603#1586' '#1582#1585#1610#1583
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 27
  inherited Panel1: TPanel
    object Label1: TLabel [0]
      Left = 601
      Top = 18
      Width = 17
      Height = 27
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 602
      Top = 58
      Width = 33
      Height = 27
      Alignment = taRightJustify
      Caption = #1593#1606#1608#1575#1606':'
    end
    object Label3: TLabel [2]
      Left = 275
      Top = 19
      Width = 41
      Height = 27
      Alignment = taRightJustify
      Caption = #1605#1587#1572#1608#1604':'
    end
    object Label4: TLabel [3]
      Left = 601
      Top = 98
      Width = 34
      Height = 27
      Alignment = taRightJustify
      Caption = #1606#1588#1575#1606#1610':'
    end
    object Label5: TLabel [4]
      Left = 275
      Top = 55
      Width = 27
      Height = 27
      Alignment = taRightJustify
      Caption = #1578#1604#1601#1606':'
    end
    object Label6: TLabel [5]
      Left = 275
      Top = 144
      Width = 87
      Height = 27
      Alignment = taRightJustify
      Caption = #1662#1587#1578' '#1575#1604#1603#1578#1585#1608#1606#1610#1603':'
    end
    object Label7: TLabel [6]
      Left = 275
      Top = 183
      Width = 76
      Height = 27
      Alignment = taRightJustify
      Caption = #1587#1575#1610#1578' '#1575#1610#1606#1578#1585#1606#1578#1610':'
    end
    object Label8: TLabel [7]
      Left = 275
      Top = 223
      Width = 30
      Height = 27
      Alignment = taRightJustify
      Caption = #1575#1593#1578#1576#1575#1585':'
    end
    inherited DBGrid1: TDBGrid
      Top = 264
      Height = 212
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 221
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Manager'
          Title.Caption = #1605#1587#1572#1608#1604
          Width = 131
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Address'
          Title.Caption = #1606#1588#1575#1606#1610
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tel'
          Title.Caption = #1578#1604#1601#1606
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Email'
          Title.Caption = #1662#1587#1578' '#1575#1604#1603#1578#1585#1608#1606#1610#1603
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Site'
          Title.Caption = #1587#1575#1610#1578
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credit'
          Title.Caption = #1575#1593#1578#1576#1575#1585
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 464
      Top = 11
      Width = 130
      Height = 35
      TabOrder = 1
    end
    object FName1: TEdit
      Left = 376
      Top = 51
      Width = 219
      Height = 35
      TabOrder = 2
    end
    object Manager1: TEdit
      Left = 17
      Top = 11
      Width = 250
      Height = 35
      TabOrder = 3
    end
    object Address1: TEdit
      Left = 16
      Top = 94
      Width = 579
      Height = 35
      TabOrder = 4
    end
    object Tel1: TEdit
      Left = 17
      Top = 51
      Width = 250
      Height = 35
      TabOrder = 5
    end
    object Email1: TEdit
      Left = 17
      Top = 135
      Width = 250
      Height = 35
      TabOrder = 6
    end
    object Site1: TEdit
      Left = 17
      Top = 177
      Width = 250
      Height = 35
      TabOrder = 7
    end
    object Credit1: TEdit
      Left = 17
      Top = 219
      Width = 250
      Height = 35
      TabOrder = 8
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TBuyCenter_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Manager_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Address_5'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Tel_6'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Email_7'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Site_8'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Credit_9'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browes_TBuyCenter;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object Browse1Manager: TWideStringField
      FieldName = 'Manager'
      Size = 50
    end
    object Browse1Address: TWideStringField
      FieldName = 'Address'
      Size = 100
    end
    object Browse1Tel: TWideStringField
      FieldName = 'Tel'
      Size = 50
    end
    object Browse1Email: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object Browse1Site: TWideStringField
      FieldName = 'Site'
      Size = 100
    end
    object Browse1Credit: TWideStringField
      FieldName = 'Credit'
      Size = 50
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TBuyCenter_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Manager_5'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Address_6'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Tel_7'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@Email_8'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Site_9'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@Credit_10'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
  end
end
