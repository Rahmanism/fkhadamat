inherited frmUser: TfrmUser
  Left = 202
  Top = 104
  Caption = #1578#1593#1585#1610#1601' '#1581#1602' '#1583#1587#1578#1585#1587#1610
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Left = -5
    Width = 701
    inherited Splitter2: TSplitter
      Top = 131
      Width = 699
      Height = 5
    end
    object Label1: TLabel [1]
      Left = 641
      Top = 19
      Width = 11
      Height = 13
      Caption = #1603#1583
    end
    object Label2: TLabel [2]
      Left = 529
      Top = 22
      Width = 36
      Height = 13
      Caption = #1606#1575#1605' '#1603#1575#1585#1576#1585
    end
    object Label5: TLabel [3]
      Left = 336
      Top = 76
      Width = 23
      Height = 13
      Caption = #1662#1610#1594#1575#1605
    end
    object Label6: TLabel [4]
      Left = 110
      Top = 78
      Width = 11
      Height = 13
      Caption = #1603#1583
    end
    object Label7: TLabel [5]
      Left = 631
      Top = 72
      Width = 17
      Height = 13
      Caption = #1575#1606#1576#1575#1585
    end
    object Label8: TLabel [6]
      Left = 290
      Top = 25
      Width = 47
      Height = 13
      Caption = #1587#1575#1604' '#1605#1575#1604#1610
    end
    object Label4: TLabel [7]
      Left = 212
      Top = 46
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label9: TLabel [8]
      Left = 424
      Top = 22
      Width = 15
      Height = 13
      Caption = #1585#1605#1586
    end
    object Label10: TLabel [9]
      Left = 549
      Top = 93
      Width = 12
      Height = 13
      Caption = '...'
    end
    object SpeedButton3: TSpeedButton [10]
      Left = 231
      Top = 39
      Width = 23
      Height = 22
      OnClick = SpeedButton3Click
    end
    object SpeedButton4: TSpeedButton [11]
      Left = 565
      Top = 88
      Width = 23
      Height = 22
      OnClick = SpeedButton4Click
    end
    inherited DBGrid1: TDBGrid
      Top = 136
      Width = 699
      Height = 135
      TabOrder = 7
      Columns = <
        item
          Expanded = False
          FieldName = 'Fserial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 5
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Message'
          Title.Caption = #1662#1610#1575#1605
          Width = 158
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsActive'
          Title.Caption = #1601#1593#1575#1604
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FcodeYear'
          Title.Caption = #1587' '#1587#1575#1604
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FcodeWarehouse'
          Title.Caption = #1587' '#1575#1606#1576#1575#1585
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FnameYear'
          Title.Caption = #1587#1575#1604' '#1605#1575#1604#1610
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FnameWarehouse'
          Title.Caption = #1606#1575#1605' '#1575#1606#1576#1575#1585
          Width = 72
          Visible = True
        end>
    end
    object Fcode1: TEdit
      Left = 575
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 0
    end
    object Fname1: TEdit
      Left = 447
      Top = 40
      Width = 122
      Height = 21
      TabOrder = 1
    end
    object FcodeWarehouse1: TEdit
      Left = 590
      Top = 88
      Width = 63
      Height = 21
      TabOrder = 4
      OnChange = FcodeWarehouse1Change
      OnExit = FcodeWarehouse1Exit
      OnKeyPress = FcodeWarehouse1KeyPress
    end
    object Message1: TEdit
      Left = 129
      Top = 91
      Width = 232
      Height = 21
      TabOrder = 5
    end
    object FcodeYear1: TEdit
      Left = 255
      Top = 40
      Width = 86
      Height = 21
      TabOrder = 3
      OnChange = FcodeYear1Change
      OnExit = FcodeYear1Exit
      OnKeyPress = FcodeYear1KeyPress
    end
    object IsActive1: TCheckBox
      Left = 107
      Top = 94
      Width = 20
      Height = 24
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      TabOrder = 6
    end
    object pass1: TEdit
      Left = 344
      Top = 40
      Width = 99
      Height = 21
      TabOrder = 2
    end
  end
  inherited Panel2: TPanel
    Top = 272
    Height = 268
    inherited Splitter1: TSplitter
      Top = 68
      Height = 5
    end
    object Label3: TLabel [1]
      Left = 324
      Top = 128
      Width = 31
      Height = 13
      Caption = 'Label3'
    end
    object DBCheckBox1: TDBCheckBox [2]
      Left = 128
      Top = 168
      Width = 18
      Height = 17
      DataField = 'ISvisible'
      DataSource = DataSource2
      TabOrder = 2
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      Visible = False
    end
    object DBCheckBox2: TDBCheckBox [3]
      Left = 128
      Top = 192
      Width = 18
      Height = 17
      DataField = 'IsEnable'
      DataSource = DataSource2
      TabOrder = 3
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      Visible = False
    end
    object DBCheckBox3: TDBCheckBox [4]
      Left = 128
      Top = 210
      Width = 18
      Height = 17
      DataField = 'IsReadOnly'
      DataSource = DataSource2
      TabOrder = 4
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      Visible = False
    end
    inherited DBGrid2: TDBGrid
      Top = 73
      Height = 194
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit, dgMultiSelect]
      OnDrawColumnCell = DBGrid2DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'frmName'
          Title.Caption = #1606#1575#1605' '#1601#1585#1605
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'frmCaption'
          Title.Caption = #1593#1606#1608#1575#1606' '#1601#1585#1605
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'objName'
          Title.Caption = #1606#1575#1605' '#1588#1574
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'objCaption'
          Title.Caption = #1593#1606#1608#1575#1606' '#1588#1574
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'objCode'
          Title.Caption = #1603#1583' '#1588#1574
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlObj'
          Title.Caption = #1587' '#1588#1574
          Width = 7
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ISvisible'
          Title.Caption = #1606#1605#1575#1610#1588
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsEnable'
          Title.Caption = #1601#1593#1575#1604
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsReadOnly'
          Title.Caption = #1601#1602#1591' '#1582#1608#1575#1606#1583#1606#1610
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlUser'
          Title.Caption = #1587' '#1603#1575#1585#1576#1585
          Width = 2
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fserial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 3
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 2
      Top = 11
      Width = 691
      Height = 53
      TabOrder = 1
      object SpeedButton1: TSpeedButton
        Left = 150
        Top = 8
        Width = 91
        Height = 38
        Caption = '&'#1578#1575#1610#1610#1583
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 41
        Top = 8
        Width = 88
        Height = 38
        Caption = #1575#1606#1578#1582#1575#1576' &'#1607#1605#1607
        OnClick = SpeedButton2Click
      end
      object CheckBox1: TCheckBox
        Left = 584
        Top = 24
        Width = 97
        Height = 17
        BiDiMode = bdRightToLeft
        Caption = #1606#1605#1575#1610#1588
        ParentBiDiMode = False
        TabOrder = 0
      end
      object CheckBox2: TCheckBox
        Left = 472
        Top = 24
        Width = 97
        Height = 17
        BiDiMode = bdRightToLeft
        Caption = #1601#1593#1575#1604
        ParentBiDiMode = False
        TabOrder = 1
      end
      object CheckBox3: TCheckBox
        Left = 365
        Top = 24
        Width = 97
        Height = 17
        BiDiMode = bdRightToLeft
        Caption = #1601#1602#1591' '#1582#1608#1575#1606#1583#1606#1610
        ParentBiDiMode = False
        TabOrder = 2
      end
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_Tuser_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fserial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@Message_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 150
        Value = Null
      end
      item
        Name = '@IsActive_5'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@YearActive_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HouseActive_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pass'
        Size = -1
        Value = Null
      end>
    Top = 36
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browes_TUser;1'
    Left = 55
    Top = 88
    object Browse1Fserial: TAutoIncField
      DisplayWidth = 12
      FieldName = 'Fserial'
      ReadOnly = True
    end
    object Browse1FCode: TStringField
      DisplayWidth = 16
      FieldName = 'FCode'
      Size = 50
    end
    object Browse1FName: TStringField
      DisplayWidth = 15
      FieldName = 'FName'
      Size = 70
    end
    object Browse1Message: TWideStringField
      DisplayWidth = 31
      FieldName = 'Message'
      Size = 150
    end
    object Browse1IsActive: TBooleanField
      DisplayWidth = 8
      FieldName = 'IsActive'
    end
    object Browse1YearActive: TIntegerField
      DisplayWidth = 12
      FieldName = 'YearActive'
    end
    object Browse1HouseActive: TIntegerField
      DisplayWidth = 12
      FieldName = 'HouseActive'
    end
    object Browse1pass: TWideStringField
      FieldName = 'pass'
      Size = 50
    end
    object Browse1FcodeWarehouse: TIntegerField
      FieldName = 'FcodeWarehouse'
    end
    object Browse1FnameWarehouse: TWideStringField
      FieldName = 'FnameWarehouse'
      Size = 50
    end
    object Browse1Boss: TWideStringField
      FieldName = 'Boss'
      Size = 50
    end
    object Browse1FcodeYear: TIntegerField
      FieldName = 'FcodeYear'
    end
    object Browse1FnameYear: TWideStringField
      FieldName = 'FnameYear'
      Size = 50
    end
  end
  inherited DataSource1: TDataSource
    Top = 0
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_Tuser_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fserial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftString
        Size = 70
        Value = Null
      end
      item
        Name = '@Message_5'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 150
        Value = Null
      end
      item
        Name = '@IsActive_6'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@YearActive_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@HouseActive_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@pass'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
  inherited Update2: TADOStoredProc
    ProcedureName = 'update_TGoods_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Stock_5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlCategory_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MaxStock_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MinStock_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlUnit_9'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Blocked_10'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
  inherited Browse2: TADOStoredProc
    ProcedureName = 'Browes_TUserObiect;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srluser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 55
    Top = 296
    object Browse2frmName: TStringField
      FieldName = 'frmName'
      Size = 50
    end
    object Browse2frmCaption: TStringField
      FieldName = 'frmCaption'
      Size = 50
    end
    object Browse2objName: TStringField
      FieldName = 'objName'
      Size = 50
    end
    object Browse2objCaption: TStringField
      FieldName = 'objCaption'
      Size = 50
    end
    object Browse2objCode: TIntegerField
      FieldName = 'objCode'
    end
    object Browse2srlObj: TIntegerField
      FieldName = 'srlObj'
    end
    object Browse2ISvisible: TBooleanField
      FieldName = 'ISvisible'
    end
    object Browse2IsEnable: TBooleanField
      FieldName = 'IsEnable'
    end
    object Browse2IsReadOnly: TBooleanField
      FieldName = 'IsReadOnly'
    end
    object Browse2srlUser: TIntegerField
      FieldName = 'srlUser'
    end
    object Browse2Fserial: TAutoIncField
      FieldName = 'Fserial'
      ReadOnly = True
    end
    object Browse2FserialUObj: TAutoIncField
      FieldName = 'FserialUObj'
      ReadOnly = True
    end
  end
  inherited Insert2: TADOStoredProc
    ProcedureName = 'insert_TUserObject_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srluser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
  end
  object Update3: TADOStoredProc
    Connection = frmDm.ADOConnection
    ProcedureName = 'Update_TuserOBJ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Fserial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@isvisible'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@isEnable'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@IsReadOnly'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 15
    Top = 423
  end
end
