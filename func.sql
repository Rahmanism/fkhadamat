if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_ChangeCheck]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_ChangeCheck]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_ConvertDateToNumber]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_ConvertDateToNumber]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_DateDif]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_DateDif]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetCorrectName]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetCorrectName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetDateName]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetDateName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetDateNameByID]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetDateNameByID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetDayHoureMin]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetDayHoureMin]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetDifDay]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetDifDay]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetExactName]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetExactName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetFirstPart]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetFirstPart]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetFirstPart2]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetFirstPart2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetKAccesUserField]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetKAccesUserField]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetKDaftarBime]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetKDaftarBime]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetMonthName]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetMonthName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetNextMonthDate]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetNextMonthDate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetQtyCombo]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetQtyCombo]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetSecundPart2]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetSecundPart2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrExamHozoor]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrExamHozoor]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrIsAccept]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrIsAccept]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrKJens]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrKJens]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrKJens2]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrKJens2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrKLesRegM]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrKLesRegM]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrKLesRegW]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrKLesRegW]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrKPay]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrKPay]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrKTitleCountMark]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrKTitleCountMark]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrLesRegMark]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrLesRegMark]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrLocatiron]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrLocatiron]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrMoney]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrMoney]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrNumber]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrNumber]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrNumberNew]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrNumberNew]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_GetStrYesNo]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_GetStrYesNo]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_MyFunc10]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_MyFunc10]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Fn_MyFunc6]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[Fn_MyFunc6]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MinToTimeStr]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[MinToTimeStr]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sum11]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[sum11]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_ChangeCheck (@MyCheck1 char(30), @MyPosition int,  @MyNewCheck char(1))  
RETURNS char(30)
AS  
BEGIN 
	Declare @Str1 char(30)
	Declare @Str2 char(30)
	Select @Str1 = SubString(@MyCheck1,1,@MyPosition-1 ) 
	Select @Str2 = SubString(@MyCheck1,@MyPosition+1, 30 - @MyPosition) 
	Select @MyCheck1 = LTrim(RTrim(@Str1))  + @MyNewCheck + LTrim(RTrim(@Str2 )) 
	RETURN(@MyCheck1)
END








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_ConvertDateToNumber(@MyDate char(8))  
RETURNS int
AS  
BEGIN 
	RETURN(13000000 + SubString(@MyDate,1,2)* 10000 + SubString(@MyDate,4,2)* 100 +  SubString(@MyDate,7,2))
END












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.Fn_DateDif(@MyDate1 char(8), @MyDate2 char(8))  
RETURNS int
AS  
BEGIN 
	Declare @DateId1 int,  @DateId2 int
	SELECT     @DateId1  = Srl_Date
	FROM         Pub_Calendar
	WHERE     (S_Date = @MyDate1)

	SELECT     @DateId2  = Srl_Date
	FROM         Pub_Calendar
	WHERE     (S_Date = @MyDate2)

	RETURN @DateId1 - @DateId2
END















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.Fn_GetCorrectName (@MainName varchar(200))  
RETURNS varchar(200)
AS  
BEGIN 
	SELECT @MainName = REPLACE(@MainName, CHAR(152), CHAR(223))
/*


 	WHILE CHARINDEX(char(157), @MainName) > 0 		SELECT @MainName = Replace(@MainName, char(157), '')   
 	WHILE CHARINDEX(char(152), @MainName) > 0 		SELECT @MainName = Replace(@MainName, char(152), char(223))  --  ك

 	WHILE CHARINDEX(char(223), @MainName) > 0 		SELECT @MainName = Replace(@MainName, char(223), char(152))
 	WHILE CHARINDEX(char(32), @MainName) > 0 			SELECT @MainName = Replace(@MainName, char(32), '')

 	WHILE CHARINDEX('  ', @MainName) > 0 			SELECT @MainName = Replace(@MainName, '  ', char(32))
*/


	SELECT @MainName =  LTrim(RTrim(@MainName))
	RETURN(@MainName )
END









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


CREATE FUNCTION dbo.Fn_GetDateName (@MyDate DateTime)  
RETURNS varchar(200)
AS  
BEGIN 
	DECLARE @MyDayName varchar(200)
	SELECT @MyDayName = DATENAME(weekday, @MyDate) 
	SELECT @MyDayName = 
				CASE @MyDayName 
					WHEN 'Saturday' THEN 'شنبه'
					WHEN 'Sunday' THEN 'يكشنبه'
					WHEN 'Monday' THEN 'دو شنبه'
					WHEN 'Tuesday' THEN 'سه شنبه'
					WHEN 'Wednesday' THEN 'چهار شنبه'
					WHEN 'Thursday' THEN 'پنج شنبه'
					WHEN 'Friday' THEN 'جمعه'
					ELSE  'NONAME' 
				END

	RETURN(@MyDayName )
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.Fn_GetDateNameByID (@MyDateID int)  
RETURNS varchar(10)
AS  
BEGIN 
	DECLARE @MyDayName varchar(10)
	SELECT @MyDateID = @MyDateID % 7
	SELECT @MyDayName = 
				CASE @MyDateID 
					WHEN 0 THEN 'شنبه'
					WHEN 1 THEN 'يكشنبه'
					WHEN 2 THEN 'دو شنبه'
					WHEN 3 THEN 'سه شنبه'
					WHEN 4 THEN 'چهار شنبه'
					WHEN 5 THEN 'پنج شنبه'
					WHEN 6 THEN 'جمعه'
					ELSE  'NONAME' 
				END

	RETURN(@MyDayName )
END







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE FUNCTION dbo.Fn_GetDayHoureMin (@Houre float)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
DECLARE @temp Float

Select @temp =  Floor(@Houre / 7.33333)
Select @Houre =  @Houre - @temp * 7.33333

Select @Str  = LTrim(RTrim(Str(@temp))) + ':'

Select @temp =  Floor(@Houre)
Select @Houre =  @Houre - @temp 

Select @Str  = @Str  +  LTrim(RTrim(Str(@temp)))  + ':'

Select @temp =  @Houre * 60

Select @Str  = @Str  +  LTrim(RTrim(Str(@temp)))

RETURN(@Str)

END








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.Fn_GetDifDay (@D1 char(8), @D2 char(8)  )  
RETURNS int
AS  
BEGIN 
DECLARE @DifDay int
DECLARE @SrlD1 int
DECLARE @SrlD2 int

SELECT     @SrlD1 = Srl_Date
FROM         Pub_Calendar
WHERE     (S_Date = @D1)

SELECT     @SrlD2 = Srl_Date
FROM         Pub_Calendar
WHERE     (S_Date = @D2)

SELECT @DifDay  = IsNull(@SrlD2, 0) - IsNull(@SrlD1, 0)  + 1

RETURN(@DifDay )
END






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetExactName (@MainName varchar(200))  
RETURNS varchar(200)
AS  
BEGIN 
 	WHILE CHARINDEX(char(32), @MainName) > 0 
		SELECT @MainName = Replace(@MainName, char(32), '')
 	WHILE CHARINDEX(char(253), @MainName) > 0 
		SELECT @MainName = Replace(@MainName, char(253), '')
 	WHILE CHARINDEX(char(254), @MainName) > 0 
		SELECT @MainName = Replace(@MainName, char(254), '')
 	WHILE CHARINDEX(char(157), @MainName) > 0 
		SELECT @MainName = Replace(@MainName, char(157), '')
 	WHILE CHARINDEX(char(240), @MainName) > 0 
		SELECT @MainName = Replace(@MainName, char(240), '')
	RETURN(@MainName )
END











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetFirstPart (@MainName varchar(200))  
RETURNS varchar(200)
AS  
BEGIN 
	DECLARE @S char(1)
	DECLARE @i int
	DECLARE @FirstName varchar(50)
	DECLARE @Kind int

	SELECT @FirstName =  NULL
	SELECT @MainName = LTrim(RTrim(@MainName ))
	SELECT @i  = 1

	SELECT @S  =SubString(@MainName,@i ,1)

	IF (	(Ascii(@S) >= 48) 
		AND (Ascii(@S) <= 57 )
	)
		SELECT @Kind = 1  --  Number
	ELSE 	
		SELECT @Kind = 2


--  Number
	IF (@Kind = 1)
 	WHILE ((Ascii(@S) >= 48) AND (Ascii(@S) <= 57) AND (@i < len(@MainName)))
	BEGIN
		SELECT @FirstName =  IsNull(@FirstName, ' ')  + @S
		SELECT @i  = @i + 1
		SELECT @S  = SubString(@MainName, @i, 1)
	END	
/*
--  Letter
	IF (@Kind = 2)
 	WHILE (  (Ascii(@S) >= 58) AND (@i <= len(@MainName)))
	BEGIN
		SELECT @i  = @i + 1
		SELECT @FirstName = IsNull(@FirstName , ' ') + @S
		SELECT @S  = SubString(@MainName, @i, 1)
	END	
*/
	RETURN(LTrim(RTrim(@FirstName)))
END


























GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetFirstPart2 (@MainName varchar(200), @MyChar char)  
RETURNS varchar(200)
AS  
BEGIN 
	DECLARE @i int
	DECLARE @FirstName varchar(50)

	SELECT @FirstName =  NULL
	SELECT @MainName = LTrim(RTrim(@MainName ))
	SELECT @i  = -1

	SELECT @i  =  CHARINDEX(@MyChar, @MainName)
	IF @i > 0 
		SELECT @FirstName =  SubString(@MainName, 1, @i -1)
	ELSE
		SELECT @FirstName =  @MainName

	RETURN(LTrim(RTrim(@FirstName)))
END

























GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetKAccesUserField (@Srl_User int , @Srl_Field int)  
RETURNS int
AS  
BEGIN 

	Declare  @KAccess int

	Select @KAccess = 1

	SELECT @KAccess = @KAccess * Pub_Role_Field.K_Access
	FROM Pub_Role_Field 
			INNER     JOIN    Pub_Role ON     Pub_Role_Field.Srl_Role = Pub_Role.Srl_Role 
			INNER JOIN    Pub_User_Role ON     Pub_Role.Srl_Role = Pub_User_Role.Srl_Role 
	WHERE 
		(Pub_User_Role.Srl_User = @Srl_User   )
		AND Pub_Role_Field.Srl_Field = @Srl_Field 
	 
	RETURN(@KAccess)
END














GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetKDaftarBime (@ItemIndex int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @ItemIndex
		WHEN 0 THEN 'اهل سنت'
		WHEN 1 THEN 'خواهران'
		WHEN 2 THEN 'شهرستانها'
		WHEN 3 THEN 'غير ايراني'
		WHEN 4 THEN 'مرحومين'
		WHEN 5 THEN 'مشهد'
		ELSE  'نامعلوم'
		END
	RETURN(@Str)
END










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE FUNCTION dbo.Fn_GetMonthName (@MonthId int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @MonthId 
		WHEN 1 THEN 'فروردين'
		WHEN 2 THEN 'ارديبهشت'
		WHEN 3 THEN 'خرداد'
		WHEN 4 THEN 'تير'
		WHEN 5 THEN 'مرداد'
		WHEN 6 THEN 'شهريور'
		WHEN 7 THEN 'مهر'
		WHEN 8 THEN 'آبان'
		WHEN 9 THEN 'آذر'
		WHEN 10 THEN 'دي'
		WHEN 11 THEN 'بهمن'
		WHEN 12 THEN 'اسفند'
		END
	RETURN(@Str)
END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.Fn_GetNextMonthDate(@MyDate1 char(10), @MyDifMonth int)  
RETURNS char(10)
AS  
BEGIN 

	Declare @NextDate char(10) 
	Declare @M1 int
	Declare @Y1 int
	

	SELECT     @Y1 = SUBSTRING(@MyDate1, 1, 4)
	SELECT     @M1 = SUBSTRING(@MyDate1, 6, 2)
	SELECT     @M1 = @M1 + @MyDifMonth 
	IF @M1 > 12 
	BEGIN
		SELECT     @Y1 = SUBSTRING(@MyDate1, 1, 4)
		SELECT     @Y1 = @Y1  + @M1  / 12
		SELECT     @M1 = (@M1 % 12 ) 
		IF @M1  = 0 
		BEGIN
			SELECT     @Y1 = @Y1  - 1
			SELECT     @M1 = 12 
		END	
	END

	IF @M1 < 1
	BEGIN
		SELECT     @Y1 = SUBSTRING(@MyDate1, 1, 4)
		SELECT     @Y1 = @Y1  - 1
		SELECT     @M1 = @M1 + 12 
	END

	IF @M1 < 10 
	 	SELECT @NextDate = LTRIM(RTRIM(STR(@Y1))) + '/0' + LTRIM(RTRIM(STR(@M1))) + SUBSTRING(@MyDate1, 8, 3)
	ELSE 
	 	SELECT @NextDate = LTRIM(RTRIM(STR(@Y1))) + '/' + LTRIM(RTRIM(STR(@M1)))  + SUBSTRING(@MyDate1, 8, 3)

	
	IF @NextDate is null Select @NextDate = '0000/00/00'

	RETURN @NextDate 
END







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetQtyCombo (@ItemIndex int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @ItemIndex
		WHEN -1 THEN ''
		WHEN 0 THEN 'عالي'
		WHEN 1 THEN 'خوب'
		WHEN 2 THEN 'متوسط'
		WHEN 3 THEN 'ضعيف'
		WHEN 4 THEN 'بد'
		ELSE  'نامعلوم'
		END
	RETURN(@Str)
END







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetSecundPart2 (@MainName varchar(200), @MyChar char)  
RETURNS varchar(200)
AS  
BEGIN 
	DECLARE @i int
	DECLARE @FirstName varchar(50)

	SELECT @FirstName =  NULL
	SELECT @MainName = LTrim(RTrim(@MainName ))
	SELECT @i  = -1

	SELECT @i  =  CHARINDEX(@MyChar, @MainName)
	IF @i > 0 
		SELECT @FirstName =  SubString(@MainName, @i +1, Len(@MainName))
	ELSE
		SELECT @FirstName =  @MainName

	RETURN(LTrim(RTrim(@FirstName)))
END


























GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrExamHozoor(@ItemIndex int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @ItemIndex
		when -1 then ' '
		when 0 then ' '
		when 1 then 'غیبت موجه'
		when 2 then 'غیبت غیر موجه'
		when Null then ' '
		else 'نام معلوم'
		END
	RETURN(@Str)
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrIsAccept (@IsAccept int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @IsAccept
		WHEN 0 THEN 'مردود'
		WHEN 1 THEN 'تجديد'
		WHEN 2 THEN 'قبول'
		ELSE  'نامعلوم'
		END
	RETURN(@Str)
END












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetStrKJens(@KJens int)  
RETURNS Varchar(4)
AS  
BEGIN 
	RETURN(Case @KJens  when 1 then 'آقاي' when 2 then 'خانم' else '----' end)
END















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetStrKJens2(@KJens int)  
RETURNS Varchar(10)
AS  
BEGIN 
	RETURN(Case @KJens  when 1 then 'جناب آقاي' when 2 then 'سركار خانم' else '----' end)
END


















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrKLesRegM (@ItemIndex int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @ItemIndex
		when 0 then ' '
		when 1 then 'تجديدي'
		when 2 then 'مطالعاتي'
		when 3 then 'شرط ارتقائي'
		when 4 then 'غیبت موجه'
		when 5 then 'موارد خاص'
		when 6 then 'تابستاني'
		when 7 then 'غير مجاز'
		else 'نام معلوم'
		END
	RETURN(@Str)
END














GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrKLesRegW (@ItemIndex int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @ItemIndex
		when 0 then 'غير مؤثر'
		when 1 then 'مؤثر'
		else 'نام معلوم'
		END
	RETURN(@Str)
END













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetStrKPay(@KPay int)  
RETURNS Varchar(30)
AS  
BEGIN 

	RETURN(Case @KPay  
			when 0 then 'نقدي' 
			when 1 then 'واريز به حساب' 
			when 2 then 'نا منظم' 
			when 3 then 'با تاخير' 
			when 4 then 'به موقع' 
			else '----' 
		end)
END

















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrKTitleCountMark (@ItemIndex int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @ItemIndex
		when 0 then 'مطالعاتی'
		when 1 then 'کم حجم'
		when 2 then 'یک مرحله ای'
		when 3 then 'دو مرحله ای'
		when 4 then 'سه مرحله ای'
		else 'نام معلوم'
		END
	RETURN(@Str)
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrLesRegMark (@SrlStd int, @SrlCycle int)  
RETURNS varchar(3000)
AS  
BEGIN 
 
	DECLARE @StrLesRegMark varchar(3000)


	SELECT     @StrLesRegMark = IsNull(@StrLesRegMark, '')  + '[' + dbo.T_Title.FName + ' : ' + Cast(IsNull(dbo.T_Les_Reg.Total_Mark,00) AS varchar(10)) + ']'

	FROM         dbo.T_Register 
		INNER JOIN	                      dbo.T_Les_Reg ON dbo.T_Register.FSerial = dbo.T_Les_Reg.Srl_Reg 
		INNER JOIN		                      dbo.T_Title ON dbo.T_Les_Reg.Srl_Title = dbo.T_Title.FSerial
	WHERE     
		(dbo.T_Register.Srl_Per = @SrlStd) 
		AND (dbo.T_Register.Srl_Cycle = @SrlCycle)

	RETURN @StrLesRegMark
END







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrLocatiron (@Location int)  
RETURNS varchar(30)
AS  
BEGIN 
 
DECLARE @Str varchar(30)
SELECT 
	@Str = CASE @Location
		WHEN 1 THEN 'شمالي'
		WHEN 2 THEN 'رضوي'
		WHEN 3 THEN 'جنوبي'
		ELSE  'نامعلوم'
		END
	RETURN(@Str)
END













GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_GetStrMoney(@MyPrice money)  
RETURNS nVarchar(1000)
AS  
BEGIN 
	RETURN('آقاي' )
END
















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrNumber(@MyNumber int)  
RETURNS varchar(500)
AS  
BEGIN 
 
DECLARE @StrNumber varchar(500)
DECLARE @a1 int
DECLARE @a2 int
DECLARE @a3 int
DECLARE @a4 int
DECLARE @a5 int
DECLARE @a6 int
DECLARE @a7 int
DECLARE @a8 int
DECLARE @a9 int
DECLARE @a10 int
DECLARE @a11 int
DECLARE @a12 int

DECLARE @Stra1 varchar(20)
DECLARE @Stra2 varchar(20)
DECLARE @Stra3 varchar(20)
DECLARE @Stra4 varchar(20)
DECLARE @Stra5 varchar(20)
DECLARE @Stra6 varchar(20)
DECLARE @Stra7 varchar(20)
DECLARE @Stra8 varchar(20)
DECLARE @Stra9 varchar(20)
DECLARE @Stra10 varchar(20)
DECLARE @Stra11 varchar(20)
DECLARE @Stra12 varchar(20)

Select @StrNumber = ''


Select @a1 = @MyNumber % 10
Select @a2 = @MyNumber / 10
Select @a3 = @MyNumber / 100
Select @a4 = @MyNumber / 1000
Select @a5 = @MyNumber / 10000
Select @a6 = @MyNumber / 100000
Select @a7 = @MyNumber / 1000000
Select @a8 = @MyNumber / 10000000
Select @a9 = @MyNumber / 100000000
Select @a10 = @MyNumber / 1000000000
Select @a11 = @MyNumber / 10000000000
Select @a12 = @MyNumber / 100000000000


SELECT 
	@Stra1  = CASE @a1
			WHEN 0 THEN 'صفر'
			WHEN 1 THEN 'يك'
			WHEN 2 THEN 'دو'
			WHEN 3 THEN 'سه'
			WHEN 4 THEN 'چهار'
			WHEN 5 THEN 'پنج'
			WHEN 6 THEN 'شش'
			WHEN 7 THEN 'هفت'
			WHEN 8 THEN 'هشت'
			WHEN 9 THEN 'نه'
			ELSE  'نامعلوم'
		END


Select @StrNumber = @Stra1

SELECT 
	@Stra2  = CASE @a2
			WHEN 0 THEN ' '
			WHEN 1 THEN 
					CASE @a1
							WHEN 0 THEN 'ده'
							WHEN 1 THEN 'يازده'
							WHEN 2 THEN 'دوازده'
							WHEN 3 THEN 'سيزده'
							WHEN 4 THEN 'چهارده'
							WHEN 5 THEN 'پانزده'
							WHEN 6 THEN 'شانزده'
							WHEN 7 THEN 'هفده'
							WHEN 8 THEN 'هيجده'
							WHEN 9 THEN 'نوزده'
							ELSE  'نامعلوم'
						END
					
			WHEN 2 THEN 'بيست'
			WHEN 3 THEN 'سي'
			WHEN 4 THEN 'چهل'
			WHEN 5 THEN 'پنجاه'
			WHEN 6 THEN 'شصت'
			WHEN 7 THEN 'هفتاد'
			WHEN 8 THEN 'هشتاد'
			WHEN 9 THEN 'نود'
			ELSE  'نامعلوم'
		END


IF  @a2 = 1 SELECT @StrNumber  = ' '
IF  (@a1 = 0 AND @a2 <> 0) SELECT @StrNumber  = ' '


IF  (LTrim(RTrim(@StrNumber)) <> '')  AND (LTrim(RTrim(@Stra2)) <> '')
	SELECT @StrNumber = @Stra2  + ' و ' +  @StrNumber 
ELSE
	Select @StrNumber = @Stra2  + @StrNumber 

SELECT 
	@Stra3  = CASE @a3
			WHEN 0 THEN ' '
			WHEN 1 THEN 'صد'
			WHEN 2 THEN 'دويست'
			WHEN 3 THEN 'سيصد'
			WHEN 4 THEN 'چهارصد'
			WHEN 5 THEN 'پانصد'
			WHEN 6 THEN 'ششصد'
			WHEN 7 THEN 'هفتصد'
			WHEN 8 THEN 'هشتصد'
			WHEN 9 THEN 'نهصد'
			ELSE  'نامعلوم'
		END



IF  (LTrim(RTrim(@StrNumber)) <> '')  AND (LTrim(RTrim(@Stra3)) <> '')
	SELECT @StrNumber = @Stra3  + ' و ' +  @StrNumber 
ELSE
	Select @StrNumber = @Stra3  + @StrNumber 

RETURN(@StrNumber )


END















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrNumberNew(@MyNumber int)  
RETURNS varchar(500)
AS  
BEGIN 
 
DECLARE @StrNumber varchar(500)
DECLARE @a1 int
DECLARE @a2 int
DECLARE @a3 int
DECLARE @a4 int
DECLARE @a5 int
DECLARE @a6 int
DECLARE @a7 int
DECLARE @a8 int
DECLARE @a9 int
DECLARE @a10 int
DECLARE @a11 int
DECLARE @a12 int

DECLARE @Stra1 varchar(20)
DECLARE @Stra2 varchar(20)
DECLARE @Stra3 varchar(20)
DECLARE @Stra4 varchar(20)
DECLARE @Stra5 varchar(20)
DECLARE @Stra6 varchar(20)
DECLARE @Stra7 varchar(20)
DECLARE @Stra8 varchar(20)
DECLARE @Stra9 varchar(20)
DECLARE @Stra10 varchar(20)
DECLARE @Stra11 varchar(20)
DECLARE @Stra12 varchar(20)

Select @StrNumber = ''


Select @a1 = @MyNumber % 10
Select @a2 = @MyNumber / 10
Select @a3 = @MyNumber / 100
Select @a4 = @MyNumber / 1000
Select @a5 = @MyNumber / 10000
Select @a6 = @MyNumber / 100000
Select @a7 = @MyNumber / 1000000
Select @a8 = @MyNumber / 10000000
Select @a9 = @MyNumber / 100000000
Select @a10 = @MyNumber / 1000000000
Select @a11 = @MyNumber / 10000000000
Select @a12 = @MyNumber / 100000000000


SELECT 
	@Stra12  = CASE @a12
			WHEN 0 THEN ' '
			WHEN 1 THEN 'يك ميليارد'
			WHEN 2 THEN 'دو ميليارد'
			WHEN 3 THEN 'سه ميليارد'
			WHEN 4 THEN 'چهار ميليارد'
			WHEN 5 THEN 'پانصد'
			WHEN 6 THEN 'ششصد'
			WHEN 7 THEN 'هفتصد'
			WHEN 8 THEN 'هشتصد'
			WHEN 9 THEN 'نهصد'
			ELSE  'نامعلوم'
		END


SELECT 
	@Stra1  = CASE @a1
			WHEN 0 THEN 'صفر'
			WHEN 1 THEN 'يك'
			WHEN 2 THEN 'دو'
			WHEN 3 THEN 'سه'
			WHEN 4 THEN 'چهار'
			WHEN 5 THEN 'پنج'
			WHEN 6 THEN 'شش'
			WHEN 7 THEN 'هفت'
			WHEN 8 THEN 'هشت'
			WHEN 9 THEN 'نه'
			ELSE  'نامعلوم'
		END


Select @StrNumber = @Stra1

SELECT 
	@Stra2  = CASE @a2
			WHEN 0 THEN ' '
			WHEN 1 THEN 
					CASE @a1
							WHEN 0 THEN 'ده'
							WHEN 1 THEN 'يازده'
							WHEN 2 THEN 'دوازده'
							WHEN 3 THEN 'سيزده'
							WHEN 4 THEN 'چهارده'
							WHEN 5 THEN 'پانزده'
							WHEN 6 THEN 'شانزده'
							WHEN 7 THEN 'هفده'
							WHEN 8 THEN 'هيجده'
							WHEN 9 THEN 'نوزده'
							ELSE  'نامعلوم'
						END
					
			WHEN 2 THEN 'بيست'
			WHEN 3 THEN 'سي'
			WHEN 4 THEN 'چهل'
			WHEN 5 THEN 'پنجاه'
			WHEN 6 THEN 'شصت'
			WHEN 7 THEN 'هفتاد'
			WHEN 8 THEN 'هشتاد'
			WHEN 9 THEN 'نود'
			ELSE  'نامعلوم'
		END


IF  @a2 = 1 SELECT @StrNumber  = ' '
IF  (@a1 = 0 AND @a2 <> 0) SELECT @StrNumber  = ' '


IF  (LTrim(RTrim(@StrNumber)) <> '')  AND (LTrim(RTrim(@Stra2)) <> '')
	SELECT @StrNumber = @Stra2  + ' و ' +  @StrNumber 
ELSE
	Select @StrNumber = @Stra2  + @StrNumber 

SELECT 
	@Stra3  = CASE @a3
			WHEN 0 THEN ' '
			WHEN 1 THEN 'صد'
			WHEN 2 THEN 'دويست'
			WHEN 3 THEN 'سيصد'
			WHEN 4 THEN 'چهارصد'
			WHEN 5 THEN 'پانصد'
			WHEN 6 THEN 'ششصد'
			WHEN 7 THEN 'هفتصد'
			WHEN 8 THEN 'هشتصد'
			WHEN 9 THEN 'نهصد'
			ELSE  'نامعلوم'
		END



IF  (LTrim(RTrim(@StrNumber)) <> '')  AND (LTrim(RTrim(@Stra3)) <> '')
	SELECT @StrNumber = @Stra3  + ' و ' +  @StrNumber 
ELSE
	Select @StrNumber = @Stra3  + @StrNumber 

RETURN(@StrNumber )


END















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.Fn_GetStrYesNo (@YesNo int)  
RETURNS varchar(3)
AS  
BEGIN 
 
DECLARE @Str varchar(3)
SELECT 
	@Str = CASE @YesNo
		WHEN 0 THEN 'خير'
		WHEN 1 THEN 'بلي'
		ELSE  '---'
		END
	RETURN(@Str)
END












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_MyFunc10 (@MainName varchar(200))  
RETURNS varchar(200)
AS  
BEGIN 
	RETURN(SubString(@MainName, 3,2)+ '/' + SubString(@MainName, 6,2) + '/' +  SubString(@MainName, 9,2) )
END










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.Fn_MyFunc6 (@MainName varchar(200))  
RETURNS varchar(200)
AS  
BEGIN 
	RETURN(SubString(LTrim(RTrim(Str(@MainName))), 1,2)+ '/' + SubString(LTrim(RTrim(Str(@MainName))), 3,2) + '/' +  SubString(LTrim(RTrim(Str(@MainName))), 5,2) )
END










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.MinToTimeStr  (@Min int)  
RETURNS Char(5) AS  
BEGIN 
declare @St char(5)
Select  @St = Str(@Min / 60) + ':' + Str(@Min % 60) 
return   @St 
END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


CREATE FUNCTION dbo.sum11 (@a int , @b int)  
RETURNS int AS  
BEGIN 


return @a+@b +5

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

