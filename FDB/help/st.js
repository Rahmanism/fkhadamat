/*function myGetCurrentFolder()
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	return fso.GetParentFolderName(location.pathname);
}

	fileC = myGetCurrentFolder() + "/c.txt";
	fileC = fileC.substring(1, fileC.length);
	fileC = fileC.replace(/\//g, "//");*/
	fileC = "c://temp//c.txt";

function setStyle(n)
{
	document.body.className = "c" + n;
	changeTagStyle("h1", n);
	changeTagStyle("h2", n);
	changeTagStyle("h3", n);
	writeToFile(n);
}

function changeTagStyle(theTagName, styleNo)
{
	var theTag = document.getElementsByTagName(theTagName);

	for (var i = 0; i < theTag.length; i++)
	{
		theTag[i].className = "c" + styleNo;
	}
}

function writeToFile(msgToFile)
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var s = fso.CreateTextFile(fileC, true);
	s.WriteLine(msgToFile);
	s.Close();
}

function readFromFile()
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var FileObject = fso.OpenTextFile(fileC, 1, true,0);
	var strRet;
	strRet = FileObject.ReadLine();
	FileObject.Close();
	return strRet;
}

function defaultStyle()
{
	var n;

	try
	{
		n = readFromFile();
	}
	catch (e)
	{
		n = 3;
	}

	try
	{
		setStyle(n);
	}
	catch (e)
	{
		//alert('Error: Can\'t write to file.');
	}
}
