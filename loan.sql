CREATE PROCEDURE Browse_TLoan

AS

SELECT TLoan.*, TStudents.FCode AS StdCode
	FROM TLoan
	INNER join TStudents ON TLoan.srlStd = TStudents.FSerial
	INNER JOIN TStudents ON TLoan.srlBail1 = TStudents.FSerial
	INNER JOIN TStudents ON TLoan.srlBail2 = TStudents.FSerial

GO
