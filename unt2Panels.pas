unit unt2Panels;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DB,DBGrids, StdCtrls, U_Common,
  Buttons, ADODB, ComCtrls;

type
  Tfrm2Panels = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    pnlTools: TPanel;
    Insert1: TADOStoredProc;
    Browse1: TADOStoredProc;
    DataSource1: TDataSource;
    Update1: TADOStoredProc;
    Panel2: TPanel;
    DBGrid2: TDBGrid;
    Update2: TADOStoredProc;
    Browse2: TADOStoredProc;
    Insert2: TADOStoredProc;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    StatusBar1: TStatusBar;
    bAdd: TSpeedButton;
    bEdit: TSpeedButton;
    bSave: TSpeedButton;
    bDel: TSpeedButton;
    bSearch: TSpeedButton;
    bPrint: TSpeedButton;
    bCancel: TSpeedButton;
    bClose: TSpeedButton;
    DataSource2: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure bAddClick(Sender: TObject);
    procedure bCloseClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure DBGrid2TitleClick(Column: TColumn);
    procedure Panel1Enter(Sender: TObject);
    procedure Panel2Enter(Sender: TObject);
    procedure Panel1Exit(Sender: TObject);
    procedure Panel2Exit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bEditClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure DataSource2DataChange(Sender: TObject; Field: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bCancelClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure bSearchClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private

OldGridProc: TWndMethod;
procedure GridWindowProc(var Message: TMessage);



 { Private declarations }
  public
    { Public declarations }
  end;

var
  frm2Panels: Tfrm2Panels;

implementation

uses untDm;

{$R *.dfm}

procedure Tfrm2Panels.GridWindowProc(var Message: TMessage);
var
  Pos: SmallInt;
begin
  OldGridProc(Message);
  if Message.Msg = WM_VSCROLL then  //or WM_HSCROLL
  begin
    Pos          := Message.WParamHi;  //Scrollbox position
    TADOStoredProc(FindComponent('Browes1')).RecNo := Pos;
   // TADOStoredProc(FindComponent('Browes2')).RecNo := Pos;
  end;
end;

procedure Tfrm2Panels.FormShow(Sender: TObject);
begin
  GlobalFormName := Self.Name;
  GlobalFormCaption:=self.Caption;
  AddmyComponentToTable(Sender);
  CheckMyPermission(Sender);
  ReSizeDBGrid(DBGrid1);
  ReSizeDBGrid(DBGrid2);
end;

procedure Tfrm2Panels.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if DBGrid1.DataSource.DataSet.RecNo mod 2=0  then
  begin
    DBGrid1.Canvas.Font.Color := clblue;
    DBGrid1.Canvas.Brush.Color := clCream;
  end;
  DBGrid1.DefaultDrawColumnCell(Rect,DataCol,Column,State);
end;

procedure Tfrm2Panels.bAddClick(Sender: TObject);
begin
 If YearClose then
  begin
  ShowMessage('��� ���� ���� ��� ����� ������� ������� �� ���� ���� ���� Ȑ����');
     exit
   end
   else
   begin

   MyState := sAdd;
  EmptyEdits(Self,panelfocus);
  SpeedBtnClick(Self, TSpeedButton(Sender));
  end;
end;

procedure Tfrm2Panels.bCloseClick(Sender: TObject);
begin
  Close;
end;

procedure Tfrm2Panels.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  if not DataSource2.Enabled then
    DataSource2.Enabled := True;
  FillEdits(Self,'Panel1');
end;

procedure Tfrm2Panels.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  GlobalDBGrid:=TDBGrid(Sender);

  if ((char(key) in ['f','F'])  and (Shift = [ssCtrl])) then
    FilterDBGrid2(TDBGrid(Sender))
  else  if ((char(key) in ['h','H'])  and (Shift = [ssCtrl])) then
    FilterDBGrid3(TDBGrid(Sender))
  else  if ((char(key) in ['g','G'])  and (Shift = [ssCtrl])) then
    FilterDBGrid(TDBGrid(Sender))
  else  if ((char(key) in ['p','P'])  and (Shift = [ssCtrl])) then
    PrintDBGrid(TDBGrid(Sender))
  else  if ((char(key) in ['o','O'])  and (Shift = [ssCtrl])) then
    OptionDBGrid(TDBGrid(Sender))
  else if (key = vk_escape) then
    TDBGrid(Sender).DataSource.DataSet.Filtered:=false
  else if (key = vk_F6) then
    FilterByEditDBGrid(TDBGrid(Sender))
end;

procedure Tfrm2Panels.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  GlobalDBGrid:=TDBGrid(Sender);

  if ((char(key) in ['f','F'])  and (Shift = [ssCtrl])) then
    FilterDBGrid2(TDBGrid(Sender))
  else  if ((char(key) in ['h','H'])  and (Shift = [ssCtrl])) then
    FilterDBGrid3(TDBGrid(Sender))
  else  if ((char(key) in ['g','G'])  and (Shift = [ssCtrl])) then
    FilterDBGrid(TDBGrid(Sender))
  else  if ((char(key) in ['p','P'])  and (Shift = [ssCtrl])) then
    PrintDBGrid(TDBGrid(Sender))
  else  if ((char(key) in ['o','O'])  and (Shift = [ssCtrl])) then
    OptionDBGrid(TDBGrid(Sender))
  else if (key = vk_escape) then
    TDBGrid(Sender).DataSource.DataSet.Filtered:=false
  else if (key = vk_F6) then
    FilterByEditDBGrid(TDBGrid(Sender))
end;

procedure Tfrm2Panels.DBGrid1TitleClick(Column: TColumn);
begin
  SortDBGrid1(Column);
end;

procedure Tfrm2Panels.DBGrid2TitleClick(Column: TColumn);
begin
  SortDBGrid1(Column);
end;

procedure Tfrm2Panels.Panel1Enter(Sender: TObject);
begin
  panelfocus:='Panel1';
  TPanel(Sender).Color:=clMoneyGreen;
  GlobalDBGrid := DBGrid1;
end;

procedure Tfrm2Panels.Panel2Enter(Sender: TObject);
begin
  panelfocus:='Panel2';
  TPanel(Sender).Color:=clMoneyGreen;
  GlobalDBGrid := DBGrid2;
end;

procedure Tfrm2Panels.Panel1Exit(Sender: TObject);
begin
  TPanel(Sender).Color := clCream;
  if (MyState = sAdd) or (MyState = sEdit)  then
  begin
    bSave.OnClick(Sender);
    EnableBtn(Self);
  end;
  MyState := sView;
  DataSource1.OnDataChange(DataSource1,DataSource1.DataSet.Fields[0]);
end;

procedure Tfrm2Panels.Panel2Exit(Sender: TObject);
begin
    TPanel(Sender).Color:=clCream;
    DataSource2.OnDataChange(DataSource2,DataSource2.DataSet.Fields[0])
end;

procedure Tfrm2Panels.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DBGridSaveToFile(DBGrid1);
  DBGridSaveToFile(DBGrid2);
  Action := caFree;
end;

procedure Tfrm2Panels.bEditClick(Sender: TObject);
begin
  If YearClose then
  begin
  ShowMessage('��� ���� ���� ��� ����� ������� ������� �� ���� ���� ���� Ȑ����');

   end
   else
   begin
  MyState := sEdit;
  SpeedBtnClick(Self, TSpeedButton(Sender));
  end;
end;

procedure Tfrm2Panels.bSaveClick(Sender: TObject);
begin
  SpeedBtnClick(Self, TSpeedButton(Sender));
  NullAdoSPParameters(Browse1);
  NullAdoSPParameters(Insert1);
  NullAdoSPParameters(Update1);
  NullAdoSPParameters(Browse2);
  NullAdoSPParameters(Insert2);
  NullAdoSPParameters(Update2);
//  MyState:=sView;
end;

procedure Tfrm2Panels.bDelClick(Sender: TObject);
begin

  SpeedBtnClick(Self, TSpeedButton(Sender));
end;

procedure Tfrm2Panels.DataSource2DataChange(Sender: TObject;
  Field: TField);
begin
  FillEdits(Self, 'Panel2');
end;

procedure Tfrm2Panels.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MYFormKeyDownF(Sender, Self, key, Shift);
  if key = vk_PRIOR then
    Panel1.SetFocus;
  if key = vk_next then
    Panel2.SetFocus;

  if panelfocus = 'Panel1' then
  begin
    if Key = vk_up then
      Browse1.Prior
    else if Key = vk_down then
      Browse1.Next;
  end
  else if panelfocus = 'Panel2' then
  begin
    if Key = vk_up then
      Browse2.Prior
    else if Key = vk_down then
      Browse2.Next;
  end;
end;

procedure Tfrm2Panels.bCancelClick(Sender: TObject);
begin
  MyState := sView;
  SpeedBtnClick(Self,TSpeedButton(Sender));
  DataSource1.OnDataChange(DataSource1,DataSource1.DataSet.Fields[0]);
  DataSource2.OnDataChange(DataSource2,DataSource2.DataSet.Fields[0]);
end;

procedure Tfrm2Panels.DBGrid1DblClick(Sender: TObject);
begin
//  SetGridColumnWidths(DBGrid1);
end;

procedure Tfrm2Panels.DBGrid2DblClick(Sender: TObject);
begin
//  SetGridColumnWidths(DBGrid2);
end;

procedure Tfrm2Panels.bSearchClick(Sender: TObject);
begin
  FilterDBGrid2(GlobalDBGrid);
end;

procedure Tfrm2Panels.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = '+') and (panelfocus = 'Panel2') and ((MyState = sAdd) or (MyState = sView)) and (TComponent(Sender).Name<>'Comment2') then
  begin
    bSave.OnClick(Sender);
    bAdd.OnClick(Sender);
  end;
end;

end.
