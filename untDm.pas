unit untDm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DB, DBGrids, StdCtrls, U_Common, ExtActns,
  Buttons, ADODB, ComCtrls, AppEvnts, Menus, Mask, DBCtrls;

const
  ///////////// Pictures \\\\\\\\\\\\\\\\\\
  PIC_FOLDER_DIV_NO = 20000;
 // IMAGES_FOLDER_NAME = 'FDB\Images';
//  PIC_FOLDER_NAME = IMAGES_FOLDER_NAME + '\Pic';
  /////////////////////////////////////////

type
  TFactorType = (ftBuy, ftSell, ftBackBuy, ftBackSell, ftMoveStockFromYear,
                 ftPreFactor, ftMoveFromWarehouse, ftMoveToWarehouse,
                 ftFirstStock, ftMoveStockToYear, ftGift, ftParcel);
                 //Buy  ����,  Sell ����
                 //BackBuy  �ѐ�� �� ����, BackSell �ѐ�� �� ����
                 //MoveStockFromYear  ������ �� ���
                 //PreFactor  ��� ������
                 //MoveFromWarehouse  ������ �� �����
                 //MoveToWarehouse  ������ �� �����
                 //FirstStock  ������ ��� ����
                 //MoveStockToYear ������ �� ���
                 //Gift  �����, Parcel  �����
  TfrmDm = class(TDataModule)
    ADOConnection: TADOConnection;
    MainMenu1: TMainMenu;
    insert_TObject: TADOStoredProc;
    stpDate: TADOStoredProc;
    UserPermit: TADOStoredProc;
    UserPermitFserial: TAutoIncField;
    UserPermitFCode: TStringField;
    UserPermitFName: TStringField;
    UserPermitMessage: TWideStringField;
    UserPermitIsActive: TBooleanField;
    UserPermitYearActive: TIntegerField;
    UserPermitHouseActive: TIntegerField;
    checkuser: TADOStoredProc;
    Browse_per: TADOStoredProc;
    Browse_perfrmName: TStringField;
    Browse_perfrmCaption: TStringField;
    Browse_perobjName: TStringField;
    Browse_perobjCaption: TStringField;
    Browse_perobjCode: TIntegerField;
    Browse_persrlObj: TIntegerField;
    Browse_perISvisible: TBooleanField;
    Browse_perIsEnable: TBooleanField;
    Browse_perIsReadOnly: TBooleanField;
    Browse_persrlUser: TIntegerField;
    Browse_perFserial: TAutoIncField;
    Browse_perFserialUObj: TAutoIncField;
    UserPermitPass: TWideStringField;
    UserPermitYearName: TWideStringField;
    UserPermitYearCode: TIntegerField;
    UserPermitHouseCode: TIntegerField;
    UserPermitHouseName: TWideStringField;
    permit11: TADOStoredProc;
    UserPermityearclose: TBooleanField;
    Browse_Pub_systems: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UserPermission;
  end;

function MessageDlgTranslated(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; DefaultButton: TMsgDlgBtn; HelpCtx: Longint = 0): TModalResult;
procedure SpeedBtnClick(SenderForm: TForm; Send: TSpeedButton);
procedure EnableBtn(SenderForm: TForm);
procedure EmptyEdits(Sender: TObject; panelFocus1:string);// overload;
procedure MYFormKeyDownF(Sender: TObject; SenderForm:tform;var Key: Word; Shift: TShiftState);overload;
procedure FillEdits(sender:TObject;panelfocus1:string);
procedure SetGridColumnWidths(Grid: Tdbgrid);
Procedure AddMyComponentToTable(Sender: TObject);
Procedure CheckMyPermission(Sender: TObject);
procedure CheckBoxInDBGrid(MyDBCheckBox: TDBCheckBox; Sender: TObject;
  const  Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
procedure MyFileRun(mfr: String);
function IfNullThenZero(NullParam: Variant; var ZeroParam: Integer): Variant;
procedure CheckDate(Sender: TObject);
procedure CheckValidEntry(var Key: Char);
procedure CheckYearClose();
procedure Clock(H, M, S, HLen, MLen, SLen: longint; L: TLabel);

var
  frmDm: TfrmDm;
  CurrDate: String;
  MyState: TState;
  panelfocus: string;
  srlUnit: Integer;
  srlCategory: Integer;
  srlStd, srlBuyCenter: Integer;
  srlFactor, srlSource: Integer;
  srlGood: Integer;
  srlWarehouse: Integer;
  FactorType: TFactorType;
  srlYear,srltemp: Integer;  // It must be setted with DB -
                     // but now I give its value manually.    -- on Dm OnCreate
 // srlWarehouse: Integer; // It muset be valued with DB -
                         // but now I give its value manually.
  srlUser: Integer;
  Permitted,YearClose: Boolean;
  srlType,srlyearUser,srlHouseUser, srlCasher: Integer;
  USER_PIC_FOLDER_NAME, PIC_FOLDER_NAME: String;


implementation

uses Math, Masks;

{$R *.dfm}

procedure MyFileRun(mfr: String);
var
  fr: TFileRun;
begin
  fr := TFileRun.Create(Application);
  fr.FileName := mfr;
  fr.Execute;
  fr.Free;
end;

function MessageDlgTranslated(const Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; DefaultButton: TMsgDlgBtn; HelpCtx: Longint = 0): TModalResult;
const
  rsCaptionWarning      = '�����';
  rsCaptionError        = '���';
  rsCaptionInformation  = '�������';
  rsCaptionConfirmation = '�����';
  rsMsgYes              = '���';
  rsMsgNo               = '���';
  rsMsgCancel           = '���';
  rsMsgOk               = '�����';
  rsMsgAbort            = '��� ���';
  rsMsgRetry            = '���� ����';
  rsMsgIgnore           = '������ �����';
  rsMsgYesToAll         = '��� �� ���';
  rsMsgNoToAll          = '��� �� ���';
  rsMsgHelp             = '������';
var img:TImage;
    lbl:TLabel;
    btn:TButton;

begin
  with CreateMessageDialog(Msg, DlgType, Buttons) do
  try
    Position := poDesktopCenter;

    // Persian
    BiDiMode := bdRightToLeft;

    img := TImage(Components[0]); // MessageDlg Icon
    lbl := TLabel(Components[1]); // MessageDlg Message Lable
    btn := TButton.Create(nil);
    btn.Left := 5;
    btn.Top := 5;

    //btn.Left:=round(Width div 2)-btn.Width;

    img.Left := img.Parent.Width - img.Width - 15;
    lbl.Left := img.Left - lbl.Width - 12;
    // End Persian

    case DlgType of
      mtWarning: Caption := rsCaptionWarning;
      mtError: Caption := rsCaptionError;
      mtInformation: Caption := rsCaptionInformation;
      mtConfirmation: Caption := rsCaptionConfirmation;
    end;

    with FindChildControl('Yes') as TButton do
    begin
      Caption := rsMsgYes;
      if DefaultButton = mbYes then
        TabOrder := 0;
    end;
    with FindChildControl('No') as TButton do
    begin
      Caption := rsMsgNo;
      if DefaultButton = mbNo then
        TabOrder := 0;
    end;
    with FindChildControl('Cancel') as TButton do
    begin
      Caption := rsMsgCancel;
      if DefaultButton = mbCancel then
        TabOrder := 0;
    end;
    with FindChildControl('OK') as TButton do
    begin
      Caption := rsMsgOk;
      if DefaultButton = mbOK then
        TabOrder := 0;
    end;
    with FindChildControl('Abort') as TButton do
    begin
      Caption := rsMsgAbort;
      if DefaultButton = mbAbort then
        TabOrder := 0;
    end;
    with FindChildControl('Retry') as TButton do
    begin
      Caption := rsMsgRetry;
      if DefaultButton = mbRetry then
        TabOrder := 0;
    end;
    with FindChildControl('Ignore') as TButton do
    begin
      Caption := rsMsgIgnore;
      if DefaultButton = mbIgnore then
        TabOrder := 0;
    end;
    with FindChildControl('Yes to All') as TButton do
    begin
      Caption := rsMsgYesToAll;
      if DefaultButton = mbYesToAll then
        TabOrder := 0;
    end;
    with FindChildControl('No to All') as TButton do
    begin
      Caption := rsMsgNoToAll;
      if DefaultButton = mbNoToAll then
        TabOrder := 0;
    end;
    with FindChildControl('Help') as TButton do
    begin
      Caption := rsMsgHelp;
      if DefaultButton = mbHelp then
        TabOrder := 0;
    end;
    Result := ShowModal;
  finally
    Free;
  end;
end;

procedure FillEdits(sender:TObject;panelfocus1:string);
var
  i: Integer;
  d: String;
begin
  EmptyEdits(sender, panelfocus1);
  For i := 0 to TForm(Sender).ComponentCount - 1 do
    if (TForm(Sender).Components[i] is TEdit) then
    begin
      d:=copy(TForm(Sender).Components[i].Name,0, strlen(pchar(TForm(Sender).Components[i].Name)) - 1);
      if TForm(Sender).Components[i].GetParentComponent.Name = 'Panel1' then
      begin
        if (TADOStoredProc(TForm(Sender).FindComponent('Browse1')).Active) and (TADOStoredProc(TForm(Sender).FindComponent('Browse1')).FieldValues[d]<>null) then
          TEdit((TForm(Sender).Components[i])).Text := TADOStoredProc(TForm(Sender).FindComponent('Browse1')).FieldValues[d]
      end else if TForm(Sender).Components[i].GetParentComponent.Name = 'Panel2' then
      begin
          if (TADOStoredProc(TForm(Sender).FindComponent('Browse2')).Active) and (TADOStoredProc(TForm(Sender).FindComponent('Browse2')).FieldValues[d]<>null) then
            TEdit((TForm(Sender).Components[i])).Text := TADOStoredProc(TForm(Sender).FindComponent('Browse2')).FieldValues[d]
      end
    end
    else if (TForm(Sender).Components[i] is TCheckBox) then
    begin
      d:=copy(TForm(Sender).Components[i].Name,0, strlen(pchar(TForm(Sender).Components[i].Name)) - 1);
      if TForm(Sender).Components[i].GetParentComponent.Name = 'Panel1' then
      begin
        if (TADOStoredProc(TForm(Sender).FindComponent('Browse1')).Active) and (TADOStoredProc(TForm(Sender).FindComponent('Browse1')).FieldValues[d]<>null) then
          TCheckBox((TForm(Sender).Components[i])).Checked := TADOStoredProc(TForm(Sender).FindComponent('Browse1')).FieldValues[d]
      end else if TForm(Sender).Components[i].GetParentComponent.Name = 'Panel2' then
      begin
          if (TADOStoredProc(TForm(Sender).FindComponent('Browse2')).Active) and (TADOStoredProc(TForm(Sender).FindComponent('Browse2')).FieldValues[d]<>null) then
            tCheckBox((TForm(Sender).Components[i])).Checked := TADOStoredProc(TForm(Sender).FindComponent('Browse2')).FieldValues[d]
      end
    end
    else if (TForm(Sender).Components[i] is TMaskEdit) then
    begin
      d := copy(TForm(Sender).Components[i].Name,0, strlen(pchar(TForm(Sender).Components[i].Name)) - 1);
      if TForm(Sender).Components[i].GetParentComponent.Name = 'Panel1' then
      begin
        if (TADOStoredProc(TForm(Sender).FindComponent('Browse1')).Active) and (TADOStoredProc(TForm(Sender).FindComponent('Browse1')).FieldValues[d]<>null) then
          TMaskEdit((TForm(Sender).Components[i])).Text := TADOStoredProc(TForm(Sender).FindComponent('Browse1')).FieldValues[d]
      end else if TForm(Sender).Components[i].GetParentComponent.Name = 'Panel2' then
      begin
        if (TADOStoredProc(TForm(Sender).FindComponent('Browse2')).Active) and (TADOStoredProc(TForm(Sender).FindComponent('Browse2')).FieldValues[d]<>null) then
          TMaskEdit((TForm(Sender).Components[i])).Text := TADOStoredProc(TForm(Sender).FindComponent('Browse2')).FieldValues[d]
      end
    end
end;
//-------------------------------------------------------
procedure SpeedBtnClick(SenderForm: TForm; Send: TSpeedButton);
begin
  if send.Name='bAdd' then
    begin
      EnableBtn(SenderForm);
      TSpeedButton(SenderForm.FindComponent('bEdit')).Enabled := false;
      TSpeedButton(SenderForm.FindComponent('bDel')).Enabled := false;
      TSpeedButton(SenderForm.FindComponent('bSearch')).Enabled := false;
      TSpeedButton(SenderForm.FindComponent('bPrint')).Enabled := false;
    end
  else if send.Name='bEdit' then
    begin
      EnableBtn(SenderForm);
      TSpeedButton(SenderForm.FindComponent('bAdd')).Enabled := false;
      TSpeedButton(SenderForm.FindComponent('bDel')).Enabled := false;
      TSpeedButton(SenderForm.FindComponent('bSearch')).Enabled := false;
      TSpeedButton(SenderForm.FindComponent('bPrint')).Enabled := false;
    end
  else if send.Name = 'bSave' then
    begin
      EnableBtn(SenderForm);
    end
    else if send.Name='bCancel' then
    begin
      EnableBtn(SenderForm);
    end;
end;
//-------------------------------------------------------
procedure EnableBtn(SenderForm: TForm);
begin
   TSpeedButton(SenderForm.FindComponent('bAdd')).Enabled := True;
   TSpeedButton(SenderForm.FindComponent('bEdit')).Enabled := True;
   TSpeedButton(SenderForm.FindComponent('bSave')).Enabled := True;
   TSpeedButton(SenderForm.FindComponent('bDel')).Enabled := True;
   TSpeedButton(SenderForm.FindComponent('bSearch')).Enabled := True;
   TSpeedButton(SenderForm.FindComponent('bPrint')).Enabled := True;
end;
//-------------------------------------------------------

procedure EmptyEdits(Sender: TObject ; panelFocus1:string);
var
  i: Integer;
begin
  for i := 0 to TForm(Sender).ComponentCount - 1 do
  begin
    if TForm(Sender).Components[i] is TEdit then
    begin
      if TForm(Sender).Components[i].GetParentComponent.Name = panelFocus1 then
        TEdit(TForm(Sender).Components[i]).Text := '';
    end
    else
    if TForm(Sender).Components[i] is TMaskEdit then
    begin
      if TForm(Sender).Components[i].GetParentComponent.Name = panelFocus1 then
        TMaskEdit(TForm(Sender).Components[i]).Text := ''
    end
    else
    if TForm(Sender).Components[i] is TCheckBox then
      if TForm(Sender).Components[i].GetParentComponent.Name = panelFocus1 then
        TCheckBox(TForm(Sender).Components[i]).Checked := False;
  end;
end;

procedure MYFormKeyDownF(Sender: TObject; SenderForm:tform ;var Key: Word;
  Shift: TShiftState);overload
var
  s:string;
begin
  

  if key = vk_Return then
    TForm(SenderForm).Perform(7388420, vk_Tab, 0);

  while not (Sender.InheritsFrom(TForm)) do
    Sender := TComponent(Sender).GetParentComponent;
  s:=TComponent(Sender).Name;
  if (key = vk_F4) and (SenderForm.FindComponent('bAdd') as TSpeedButton).Enabled then
    TSpeedButton(SenderForm.FindComponent('bAdd')).onclick(SenderForm.FindComponent('bAdd'))
  else if (key = vk_F5) and (SenderForm.FindComponent('bEdit') as TSpeedButton).Enabled then
    TSpeedButton(SenderForm.FindComponent('bEdit')).onclick(SenderForm.FindComponent('bEdit'))
  else if (key = vk_F2) and (SenderForm.FindComponent('bSave') as TSpeedButton).Enabled then
    TSpeedButton(SenderForm.FindComponent('bSave')).onclick(SenderForm.FindComponent('bSave'))
  else if (key = vk_F8) and (SenderForm.FindComponent('bDel') as TSpeedButton).Enabled then
    TSpeedButton(SenderForm.FindComponent('bDel')).onclick(SenderForm.FindComponent('bDel'))
  else if (key = vk_Escape) and (SenderForm.FindComponent('bCancel') as TSpeedButton).Enabled then
    TSpeedButton(SenderForm.FindComponent('bCancel')).onclick(SenderForm.FindComponent('bCancel'))
  else if (key = vk_F10) and (SenderForm.FindComponent('bPrint') as TSpeedButton).Enabled then
    TSpeedButton(SenderForm.FindComponent('bPrint')).onclick(SenderForm.FindComponent('bPrint'));

end;

procedure SetGridColumnWidths(Grid: Tdbgrid);
const
  DEFBORDER = 10;
var
  temp, n: Integer;
  lmax: array [0..30] of Integer;
begin
  with Grid do
  begin
    Canvas.Font := Font;
    for n := 0 to Columns.Count - 1 do

      lmax[n] := Canvas.TextWidth(Fields[n].FieldName) + DEFBORDER;
    grid.DataSource.DataSet.First;
    while not grid.DataSource.DataSet.EOF do
    begin
      for n := 0 to Columns.Count - 1 do
      begin

        temp := Canvas.TextWidth(trim(Columns[n].Field.DisplayText)) + DEFBORDER;
        if temp > lmax[n] then lmax[n] := temp;

      end;
      grid.DataSource.DataSet.Next;
    end;
    grid.DataSource.DataSet.First;
    for n := 0 to Columns.Count - 1 do
      if lmax[n] > 0 then
        Columns[n].Width := lmax[n];
  end;
end;

Procedure AddMyComponentToTable(Sender: TObject);
Var
   N_Form : String;
   Cap_Form : String;
   N_Field : String;
   Cap_Field : String;
   i, j, k, m : integer;
   a : TForm;
Begin
   if Not Add_Components then exit;
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      a := TForm(Sender);
      N_Form := a.name;
//      N_Form := Copy(Sender.ClassName, 2, Length(Sender.ClassName) -1);
      Cap_Form := TForm(Sender).Caption;
      For i := 0 to ComponentCount - 1 do
      begin
         if (Components[i] is TControl) then
         begin
            N_Field := Components[i].Name;

            if (Components[i] is TLabel) then
               Cap_Field := TLabel(Components[i]).Caption
            else if (Components[i] is TButton) then
               Cap_Field := TButton(Components[i]).Caption
            else if (Components[i] is TSpeedButton) then
            begin
               Cap_Field := TSpeedButton(Components[i]).Caption;
               if Trim(Cap_Field) = '' then
                  Cap_Field := TSpeedButton(Components[i]).Hint;
            end
            else if (Components[i] is TCheckBox) then
               Cap_Field := TCheckBox(Components[i]).Caption;

            if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
            begin
               frmDm.insert_TObject.Close;
               for j := 0 to frmDm.insert_TObject.Parameters.Count-1 do
                    frmDm.insert_TObject.Parameters[j].Value := null;

               frmDm.insert_TObject.Parameters.ParamByName('@frmname_2').VALUE := N_Form;
               frmDm.insert_TObject.Parameters.ParamByName('@frmCaption_3').VALUE := Cap_Form;
               frmDm.insert_TObject.Parameters.ParamByName('@objName_4').VALUE := N_Field;
               frmDm.insert_TObject.Parameters.ParamByName('@objCaption_5').VALUE := Cap_Field;
               frmDm.insert_TObject.Parameters.ParamByName('@ObjCode_6').VALUE := 1;
               try
                  frmDm.insert_TObject.ExecProc;
                  N_Field := '';
                  Cap_Field := '';
               Except
                  ShowMessage('��� �� ����� ���� �����');
                  exit;
               end;
            end;
         end
         else if (Components[i] is TMainMenu) then
         begin
            for k := 0 to TMainMenu(Components[i]).Items.Count - 1 do
            begin
               N_Field := TMainMenu(Components[i]).Items[k].Name;
               Cap_Field := TMainMenu(Components[i]).Items[k].Caption;

               if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
               begin
                  frmDm.insert_TObject.Close;
                  for j := 0 to frmDm.insert_TObject.Parameters.Count-1 do
                       frmDm.insert_TObject.Parameters[j].Value := null;

                  frmDm.insert_TObject.Parameters.ParamByName('@frmname_2').VALUE := N_Form;
               frmDm.insert_TObject.Parameters.ParamByName('@frmCaption_3').VALUE := Cap_Form;
               frmDm.insert_TObject.Parameters.ParamByName('@objName_4').VALUE := N_Field;
               frmDm.insert_TObject.Parameters.ParamByName('@objCaption_5').VALUE := Cap_Field;
               frmDm.insert_TObject.Parameters.ParamByName('@ObjCode_6').VALUE := 1;
                  try
                      frmDm.insert_TObject.ExecProc;
                     N_Field := '';
                     Cap_Field := '';
                  Except
                     ShowMessage('��� �� ����� ���� �����');
                     exit;
                  end;
               end;

               with TMainMenu(Components[i]).Items[k] do
               for m := 0 to Count - 1 do
               begin
                  N_Field := Items[m].Name;
                  Cap_Field := Items[m].Caption;

                  if (Trim(N_Form) <> '') and (Trim(N_Field) <> '') then
                  begin
                     frmDm.insert_TObject.Close;
                     for j := 0 to  frmDm.insert_TObject.Parameters.Count-1 do
                           frmDm.insert_TObject.Parameters[j].Value := null;

                      frmDm.insert_TObject.Parameters.ParamByName('@frmname_2').VALUE := N_Form;
                      frmDm.insert_TObject.Parameters.ParamByName('@frmCaption_3').VALUE := Cap_Form;
                      frmDm.insert_TObject.Parameters.ParamByName('@objName_4').VALUE := N_Field;
                      frmDm.insert_TObject.Parameters.ParamByName('@objCaption_5').VALUE := Cap_Field;
                      frmDm.insert_TObject.Parameters.ParamByName('@ObjCode_6').VALUE := 1;
                     try
                         frmDm.insert_TObject.ExecProc;
                        N_Field := '';
                        Cap_Field := '';

                     Except
                        ShowMessage('��� �� ����� ���� �����');
                        exit;
                     end;
                  end;
               end;
            end;
         end;
      end;
   end;
end;

procedure TfrmDm.DataModuleCreate(Sender: TObject);
var
  s: String;
begin
  N_System:='Fkhadamat';
  s := GetCurrentDir + '\' + 'path.udl';
  ADOConnection.Provider := s;
  ADOConnection.ConnectionString := 'FILE NAME=' + s;
  ADOConnection.Open;
  stpDate.Close;
  stpDate.ExecProc;
  CurrDate := stpDate.Parameters.ParamByName('@FDate').Value;
  Browse_Pub_systems.Close;
  Browse_Pub_systems.Open;
  PIC_FOLDER_NAME := 'FDB\Images\Pic';
  USER_PIC_FOLDER_NAME := 'FDB\Images\Users';
  if Browse_Pub_systems.FieldValues['Pth_Picture'] <> Null then
  begin
    PIC_FOLDER_NAME := Browse_Pub_systems.FieldValues['Pth_Picture'] + '\Pic';
//    USER_PIC_FOLDER_NAME := Browse_Pub_systems.FieldValues['Pth_Picture'] + '\Pic';
  end;
  if Browse_Pub_systems.FieldValues['Pth_User_Pic'] <> Null then
    USER_PIC_FOLDER_NAME := Browse_Pub_systems.FieldValues['Pth_User_Pic'];// + '\Users';
  if Browse_Pub_systems.FieldValues['Cap_System'] <> Null then
    Application.Title := Browse_Pub_systems.FieldValues['Cap_System'];
end;

Procedure CheckMyPermission(Sender: TObject);
Var
   N_Form : String;
   Cap_Form : String;
   N_Field : String;
   Cap_Field : String;
   i, j, k, m : integer;
   a : TForm;
Begin
//   if Not CheckPermission then exit;
   if (Sender <> nil) and (Sender.ClassType <> nil)
    and (Sender.InheritsFrom(TForm)) then
   with TForm(Sender) Do
   begin
      a := TForm(Sender);
      N_Form := a.name;
//      N_Form := Copy(Sender.ClassName, 2, Length(Sender.ClassName) -1);
      Cap_Form := TForm(Sender).Caption;
      For i := 0 to ComponentCount - 1 do
      begin

         if (Components[i] is TControl) then
          //begin
            if frmDm.Browse_per.Locate('frmName;ObjName',VarArrayOf([N_Form,components[i].Name]),[]) then
              begin
                   TControl(Components[i]).Visible:=frmDm.Browse_perISvisible.Value;
                   TControl(Components[i]).Enabled:=frmDm.Browse_perISEnable.Value;
              end
            else if (Components[i] is TLabel) then
                  if frmDm.Browse_per.Locate('frmName;ObjName',VarArrayOf([N_Form,components[i].name]),[]) then
                     TLabel(Components[i]).Visible:=frmDm.Browse_perISvisible.Value
            else if (Components[i] is TButton) then
               if frmDm.Browse_per.Locate('frmName;ObjName',VarArrayOf([N_Form,components[i].Name]),[]) then
                 begin
                  TButton(Components[i]).Visible:=frmDm.Browse_perISvisible.Value ;
                  TButton(Components[i]).Enabled:=frmDm.Browse_perISEnable.Value
                 end
            else  if (Components[i] is TSpeedButton) then
            begin
                TSpeedButton(Components[i]).Visible:=frmDm.Browse_perISvisible.Value;;

            end
            else if (Components[i] is TCheckBox) then
               if frmDm.Browse_per.Locate('frmName;ObjName',VarArrayOf([N_Form,components[i].Name]),[]) then
                begin
                TCheckBox(Components[i]).Visible:=frmDm.Browse_perISvisible.Value;
                TCheckBox(Components[i]).Enabled:=frmDm.Browse_perIsEnable.Value;
                end ;


       if (Components[i] is TMainMenu) then
         begin
            for k := 0 to TMainMenu(Components[i]).Items.Count - 1 do
                 begin
                  if    frmDm.Browse_per.Locate('frmName;ObjName',VarArrayOf([N_Form,TMainMenu(Components[i]).Items[k].Name]),[]) then
                  TMainMenu(Components[i]).Items[k].Visible:=frmDm.Browse_perISvisible.Value;
                {   for m := 0 to TMainMenu(Components[i]).Items[k].Count - 1 do
                     begin
                       if    frmDm.Browse_per.Locate('frmName;ObjName',VarArrayOf([N_Form,TMainMenu(Components[i]).Items[m].Name]),[]) then
                       //N_Field := Items[m].Name;
                       BEGIN
                        TMainMenu(Components[i]).Items[M].Visible:=frmDm.Browse_perISvisible.Value;
                        TMainMenu(Components[i]).Items[M].Enabled:=frmDm.Browse_perIsEnable.Value;
                     // Cap_Field := Items[m].Caption;
                       end;

                     end;  }
                 end;
              end;
           end;
        // end;
      end;
 //   end;
end;


procedure CheckBoxInDBGrid(MyDBCheckBox: TDBCheckBox; Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const IsChecked : array[Boolean] of Integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  DrawState: Integer;
  DrawRect: TRect;
begin
  if (gdFocused in State) then
  begin
    if (Column.Field.FieldName = MyDBCheckBox.DataField) then
    begin

     MyDBCheckBox.Left := Rect.Left + TDBGrid(Sender).Left + 2;
     MyDBCheckBox.Top := Rect.Top + TDBGrid(Sender).top + 2;
     MyDBCheckBox.Width := Rect.Right - Rect.Left;
     MyDBCheckBox.Height := Rect.Bottom - Rect.Top;

     MyDBCheckBox.Visible := True;
    end;
 {   if (Column.Field.FieldName = DBLookupComboBox1.DataField) then
    with DBLookupComboBox1 do begin
      Left := Rect.Left + DBGrid1.Left + 2;
      Top := Rect.Top + DBGrid1.Top + 2;
      Width := Rect.Right - Rect.Left;
      Width := Rect.Right - Rect.Left;
      Height := Rect.Bottom - Rect.Top;

      Visible := True;
    end      }
  end
  else {in this else area draw any "stay behind" bitmaps}
  begin
    if (Column.Field.FieldName = MyDBCheckBox.DataField) then
    begin
      DrawRect:=Rect;
      InflateRect(DrawRect,-1,-1);

      DrawState := ISChecked[Column.Field.AsBoolean];

      TDBGrid(Sender).Canvas.FillRect(Rect);
      DrawFrameControl(TDBGrid(Sender).Canvas.Handle, DrawRect, DFC_BUTTON, DrawState);
    end;
  end; //if focused
end;

function IfNullThenZero(NullParam: Variant; var ZeroParam: Integer): Variant;
begin
  if NullParam = Null then
    ZeroParam := 0
  else
    ZeroParam := NullParam;
  Result := ZeroParam;
end;

procedure CheckDate(Sender: TObject);
begin
  if IsCorrectDate((Sender as TMaskEdit).Text) <> 1 then
  begin
    ShowMessage('����� �� ������ ���� �������.');
    (Sender as TMaskEdit).Text := '';
    (Sender as TMaskEdit).SetFocus;
  end;
end;

procedure TfrmDm.UserPermission;
begin
  UserPermit.Parameters.ParamByName('@srlUser').Value := srlUser;
  UserPermit.Open;
  if (UserPermit.RecordCount > 0) then
    Permitted := True
  else
    Permitted := False;
  permit11.ExecProc;
end;

procedure CheckValidEntry(var Key: Char);
begin
  if not (Key in ValidKey) then
    Key := #0;
end;
procedure CheckYearClose();
begin
    
end;
procedure Clock(H, M, S, HLen, MLen, SLen: longint; L: TLabel);
var
  R: Real;
  X, Y, Nt: Integer;
begin
  Nt := M;

  L.Canvas.Pen.Color := clRed;
  S := S + 45;
  R := (0.10461* S);
  X := round(SLen * Cos(R));
  Y := round(SLen * Sin(R));
  L.Repaint;
  L.Canvas.Pen.Width := 1;
  L.Canvas.MoveTo(SLen, SLen);
  L.Canvas.LineTo(X + SLen, Y + SLen);
  M := M + 45;
  R := (0.10461 * M);
  X := round(MLen * Cos(R));
  Y := round(MLen * Sin(R));
//  L.Repaint;
  L.Canvas.Pen.Color := clblack;
  L.Canvas.Pen.Width := 2;
  L.Canvas.MoveTo(SLen, SLen);
  L.Canvas.LineTo(X + SLen, Y + SLen);
  if (H >= 12) then H := H - 12;
  H := ((H * 5) + (Nt div 12)) + 45;
  R := (0.10461 * H);
  X := round(HLen * Cos(R));
  Y := round(HLen * Sin(R));
//  L.Repaint;
  L.Canvas.Pen.Color := $00370000;
  L.Canvas.Pen.Width := 3;
  L.Canvas.MoveTo(SLen, SLen);
  L.Canvas.LineTo(X + SLen, Y + SLen);
end;

end.
