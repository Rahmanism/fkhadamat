object frmParentReport: TfrmParentReport
  Left = 0
  Top = 35
  Width = 1024
  Height = 703
  BiDiMode = bdRightToLeft
  Caption = 'parentReport'
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Zar'
  Font.Style = [fsBold]
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 26
  object Splitter1: TSplitter
    Left = 0
    Top = 197
    Width = 1008
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 208
    Width = 1008
    Height = 434
    Align = alBottom
    BiDiMode = bdRightToLeft
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentBiDiMode = False
    PopupMenu = F_Common.PopupMenu1
    TabOrder = 0
    TitleFont.Charset = ARABIC_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = 'Zar'
    TitleFont.Style = [fsBold]
    OnDblClick = DBGrid1DblClick
    OnTitleClick = DBGrid1TitleClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 197
    Align = alTop
    BiDiMode = bdRightToLeft
    ParentBiDiMode = False
    TabOrder = 1
    object Label1: TLabel
      Left = 658
      Top = 58
      Width = 54
      Height = 26
      Caption = #1587#1575#1604' '#1605#1575#1604#1610
    end
    object Label2: TLabel
      Left = 559
      Top = 64
      Width = 12
      Height = 26
      Caption = '...'
    end
    object Label3: TLabel
      Left = 327
      Top = 64
      Width = 12
      Height = 26
      Caption = '...'
    end
    object Edit1: TEdit
      Left = 584
      Top = 56
      Width = 65
      Height = 34
      TabOrder = 0
      Text = '0'
      OnChange = Edit1Change
    end
    object BitBtn1: TBitBtn
      Left = 136
      Top = 48
      Width = 97
      Height = 33
      Caption = #1578#1575#1610#1610#1583
      TabOrder = 2
    end
    object Edit2: TEdit
      Left = 344
      Top = 56
      Width = 65
      Height = 34
      TabOrder = 1
      Text = '0'
      OnChange = Edit2Change
    end
  end
  object StatusBar2: TStatusBar
    Left = 0
    Top = 642
    Width = 1008
    Height = 25
    AutoHint = True
    Panels = <
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end
      item
        Alignment = taCenter
        Width = 100
      end>
    ParentFont = True
    UseSystemFont = False
  end
  object Browse1: TADOStoredProc
    Connection = frmDm.ADOConnection
    Parameters = <>
  end
  object DataSource1: TDataSource
    DataSet = Browse1
    OnDataChange = DataSource1DataChange
    Left = 32
  end
end
