inherited frmWareHouse: TfrmWareHouse
  Left = 232
  Top = 156
  Caption = #1578#1593#1585#1610#1601' '#1575#1606#1576#1575#1585#1607#1575
  ClientHeight = 520
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 27
  inherited Panel1: TPanel
    Height = 501
    object Label1: TLabel [0]
      Left = 529
      Top = 28
      Width = 17
      Height = 27
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 531
      Top = 82
      Width = 33
      Height = 27
      Alignment = taRightJustify
      Caption = #1593#1606#1608#1575#1606':'
    end
    object Label3: TLabel [2]
      Left = 529
      Top = 136
      Width = 41
      Height = 27
      Alignment = taRightJustify
      Caption = #1605#1587#1572#1608#1604':'
    end
    inherited DBGrid1: TDBGrid
      Top = 243
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Boss'
          Title.Caption = #1605#1587#1572#1608#1604
          Width = 200
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 265
      Top = 28
      Width = 250
      Height = 35
      TabOrder = 1
      OnKeyPress = FCode1KeyPress
    end
    object FName1: TEdit
      Left = 265
      Top = 78
      Width = 250
      Height = 35
      TabOrder = 2
    end
    object Boss1: TEdit
      Left = 265
      Top = 128
      Width = 250
      Height = 35
      TabOrder = 3
    end
  end
  inherited pnlTools: TPanel
    Height = 501
  end
  inherited StatusBar1: TStatusBar
    Top = 501
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TWarehouse_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Boss_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browes_TWareHouse;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    object Browse1FSerial: TIntegerField
      FieldName = 'FSerial'
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object Browse1Boss: TWideStringField
      FieldName = 'Boss'
      Size = 50
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TWarehouse_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@Boss_5'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
end
