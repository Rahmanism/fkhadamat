unit untBuyCenters;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt1Panel, DB, ADODB, ComCtrls, Buttons, Grids, DBGrids,
  ExtCtrls, StdCtrls, U_Common, untDm;

type
  TfrmBuyCenters = class(Tfrm1Panel)
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1Manager: TWideStringField;
    Browse1Address: TWideStringField;
    Browse1Tel: TWideStringField;
    Browse1Email: TWideStringField;
    Browse1Site: TWideStringField;
    Browse1Credit: TWideStringField;
    FCode1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    FName1: TEdit;
    Label3: TLabel;
    Manager1: TEdit;
    Label4: TLabel;
    Address1: TEdit;
    Label5: TLabel;
    Tel1: TEdit;
    Label6: TLabel;
    Email1: TEdit;
    Label7: TLabel;
    Site1: TEdit;
    Label8: TLabel;
    Credit1: TEdit;
    procedure bAddClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure refreshPanel(bfs: Integer);
  public
    { Public declarations }
  end;

var
  frmBuyCenters: TfrmBuyCenters;

implementation

{$R *.dfm}

procedure TfrmBuyCenters.bAddClick(Sender: TObject);
begin
  inherited;
  FCode1.Text := GetMaxCodeRecord(20, srlYear, -1, -1, -1);
end;

procedure TfrmBuyCenters.bSaveClick(Sender: TObject);
begin
  inherited;
if MyState=sadd then
  begin
  Insert1.Parameters.ParamByName('@FCode_2').Value := FCode1.Text;
  Insert1.Parameters.ParamByName('@FName_3').Value := FName1.Text;
  Insert1.Parameters.ParamByName('@Manager_4').Value := Manager1.Text;
  Insert1.Parameters.ParamByName('@Address_5').Value := Address1.Text;
  Insert1.Parameters.ParamByName('@Tel_6').Value := Tel1.Text;
  Insert1.Parameters.ParamByName('@Email_7').Value := Email1.Text;
  Insert1.Parameters.ParamByName('@Site_8').Value := Site1.Text;
  Insert1.Parameters.ParamByName('@Credit_9').Value := Credit1.Text;
  Insert1.ExecProc;
  refreshPanel(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value);
  end;
  if MyState=sedit then
  begin
  Update1.Parameters.ParamByName('@Fserial_1').Value := srltemp;
  Update1.Parameters.ParamByName('@FCode_3').Value := FCode1.Text;
  Update1.Parameters.ParamByName('@FName_4').Value := FName1.Text;
  Update1.Parameters.ParamByName('@Manager_5').Value := Manager1.Text;
  Update1.Parameters.ParamByName('@Address_6').Value := Address1.Text;
  Update1.Parameters.ParamByName('@Tel_7').Value := Tel1.Text;
  Update1.Parameters.ParamByName('@Email_8').Value := Email1.Text;
  Update1.Parameters.ParamByName('@Site_9').Value := Site1.Text;
  Update1.Parameters.ParamByName('@Credit_10').Value := Credit1.Text;
  Update1.ExecProc;
  refreshPanel(update1.Parameters.ParamByName('@RETURN_VALUE').Value);
  end;


end;

procedure TfrmBuyCenters.refreshPanel(bfs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', bfs, []);
end;

procedure TfrmBuyCenters.FormShow(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmBuyCenters.bDelClick(Sender: TObject);
begin
  inherited;
  DeleteRecord1(20, Browse1FSerial.Value, Browse1);
end;

procedure TfrmBuyCenters.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmBuyCenters.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     srltemp:=Browse1FSerial.Value;
end;

end.
