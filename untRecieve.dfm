inherited frmRecPay: TfrmRecPay
  Left = 261
  Top = 193
  Caption = #1583#1585#1610#1575#1601#1578' / '#1662#1585#1583#1575#1582#1578
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    object Label1: TLabel [0]
      Left = 518
      Top = 56
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 518
      Top = 105
      Width = 24
      Height = 13
      Caption = #1605#1576#1604#1594':'
    end
    object Label3: TLabel [2]
      Left = 518
      Top = 131
      Width = 25
      Height = 13
      Caption = #1578#1575#1585#1610#1582':'
    end
    object Label5: TLabel [3]
      Left = 518
      Top = 84
      Width = 30
      Height = 13
      Caption = #1601#1575#1603#1578#1608#1585':'
    end
    object lblFactor: TLabel [4]
      Left = 457
      Top = 87
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label6: TLabel [5]
      Left = 518
      Top = 156
      Width = 19
      Height = 13
      Alignment = taRightJustify
      Caption = #1606#1608#1593':'
    end
    object lblType: TLabel [6]
      Left = 457
      Top = 159
      Width = 12
      Height = 13
      Caption = '...'
    end
    inherited DBGrid1: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Type'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlStd'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MPayment'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MRecieve'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlFactor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlYear'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdCode'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FactorCode'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdName'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TypeCode'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TypeName'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TypeSerial'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FactorSerial'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FactorName'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdSerial'
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 392
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object MRecieve1: TEdit
      Left = 392
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object DtDate1: TMaskEdit
      Left = 392
      Top = 128
      Width = 122
      Height = 21
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 5
      Text = '  /  /  '
    end
    object FactorCode1: TEdit
      Left = 472
      Top = 80
      Width = 41
      Height = 21
      TabOrder = 2
      OnChange = FactorCode1Change
      OnExit = FactorCode1Exit
      OnKeyPress = FactorCode1KeyPress
    end
    object TypeCode1: TEdit
      Left = 472
      Top = 152
      Width = 41
      Height = 21
      TabOrder = 6
      OnChange = TypeCode1Change
      OnExit = TypeCode1Exit
      OnKeyPress = TypeCode1KeyPress
    end
    object MPayment1: TEdit
      Left = 392
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 3
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TPayment_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Type_2'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@MPayment_4'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MRecieve_5'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@DtDate_6'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@srlFactor_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browse_TPayment;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@PayOrRec'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1Type: TIntegerField
      FieldName = 'Type'
    end
    object Browse1srlStd: TIntegerField
      FieldName = 'srlStd'
    end
    object Browse1MPayment: TBCDField
      FieldName = 'MPayment'
      Precision = 19
    end
    object Browse1MRecieve: TBCDField
      FieldName = 'MRecieve'
      Precision = 19
    end
    object Browse1DtDate: TStringField
      FieldName = 'DtDate'
      FixedChar = True
      Size = 8
    end
    object Browse1srlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object Browse1srlYear: TIntegerField
      FieldName = 'srlYear'
    end
    object Browse1StdCode: TIntegerField
      FieldName = 'StdCode'
    end
    object Browse1FactorCode: TIntegerField
      FieldName = 'FactorCode'
    end
    object Browse1StdName: TWideStringField
      FieldName = 'StdName'
      Size = 100
    end
    object Browse1TypeCode: TIntegerField
      FieldName = 'TypeCode'
    end
    object Browse1TypeName: TWideStringField
      FieldName = 'TypeName'
      Size = 50
    end
    object Browse1TypeSerial: TAutoIncField
      FieldName = 'TypeSerial'
      ReadOnly = True
    end
    object Browse1FactorSerial: TAutoIncField
      FieldName = 'FactorSerial'
      ReadOnly = True
    end
    object Browse1FactorName: TWideStringField
      FieldName = 'FactorName'
      Size = 100
    end
    object Browse1StdSerial: TAutoIncField
      FieldName = 'StdSerial'
      ReadOnly = True
    end
  end
end
