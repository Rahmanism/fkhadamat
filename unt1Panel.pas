unit unt1Panel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DB,DBGrids, StdCtrls, U_Common,
  Buttons, ADODB, ComCtrls, AppEvnts;

type
  Tfrm1Panel = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    pnlTools: TPanel;
    bClose: TSpeedButton;
    bCancel: TSpeedButton;
    bPrint: TSpeedButton;
    bSearch: TSpeedButton;
    bDel: TSpeedButton;
    bSave: TSpeedButton;
    bEdit: TSpeedButton;
    bAdd: TSpeedButton;
    Insert1: TADOStoredProc;
    Browse1: TADOStoredProc;
    DataSource1: TDataSource;
    Update1: TADOStoredProc;
    StatusBar1: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bCloseClick(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure Panel1Enter(Sender: TObject);
    procedure bEditClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bSaveClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure bSearchClick(Sender: TObject);
    procedure bPrintClick(Sender: TObject);
  private
   OldGridProc: TWndMethod;
    procedure GridWindowProc(var Message: TMessage);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm1Panel: Tfrm1Panel;

implementation

uses untDm;

{$R *.dfm}

procedure Tfrm1Panel.FormShow(Sender: TObject);
begin
  GlobalFormName := Self.Name;
  GlobalFormCaption:=self.Caption;
  AddMyComponentToTable(Sender);
  CheckMyPermission(Sender);
  ReSizeDBGrid(DBGrid1);
end;

procedure Tfrm1Panel.bAddClick(Sender: TObject);
begin
  MyState := sAdd;
  SpeedBtnClick(Self, TSpeedButton(Sender));
  EmptyEdits(Self,panelfocus);
end;

procedure Tfrm1Panel.bCloseClick(Sender: TObject);
begin
  Close;
end;

procedure Tfrm1Panel.DataSource1DataChange(Sender: TObject; Field: TField);
var
  i: Integer;
  d: String;
begin
  FillEdits(self,'Panel1');
end;

procedure Tfrm1Panel.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  GlobalDBGrid:=TDBGrid(Sender);
  // Perform(7388420, vk_Tab, 0);
  if ((char(key) in ['f','F'])  and (Shift = [ssCtrl])) then
    FilterDBGrid2(TDBGrid(Sender))
  else  if ((char(key) in ['h','H'])  and (Shift = [ssCtrl])) then
    FilterDBGrid3(TDBGrid(Sender))
  else  if ((char(key) in ['g','G'])  and (Shift = [ssCtrl])) then
    FilterDBGrid(TDBGrid(Sender))
  else  if ((char(key) in ['p','P'])  and (Shift = [ssCtrl])) then
    PrintDBGrid(TDBGrid(Sender))
  else  if ((char(key) in ['o','O'])  and (Shift = [ssCtrl])) then
    OptionDBGrid(TDBGrid(Sender))
  else if (key = vk_escape) then
    TDBGrid(Sender).DataSource.DataSet.Filtered:=false
  else if (key = vk_F6) then
    FilterByEditDBGrid(TDBGrid(Sender))
end;

procedure Tfrm1Panel.DBGrid1TitleClick(Column: TColumn);
begin
  SortDBGrid1(Column);
end;

procedure Tfrm1Panel.Panel1Enter(Sender: TObject);
begin
  panelfocus:='Panel1';
  GlobalDBGrid := DBGrid1;
end;

procedure Tfrm1Panel.bEditClick(Sender: TObject);
begin
  MyState:=sEdit;
  SpeedBtnClick(Self, TSpeedButton(Sender));
end;

procedure Tfrm1Panel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DBGridSaveToFile(DBGrid1);
  Action := caFree;
end;

procedure Tfrm1Panel.bSaveClick(Sender: TObject);
begin
  SpeedBtnClick(Self, TSpeedButton(Sender));
  NullAdoSPParameters(Browse1);
  NullAdoSPParameters(Insert1);
  NullAdoSPParameters(Update1);
end;

procedure Tfrm1Panel.bDelClick(Sender: TObject);
begin
  SpeedBtnClick(Self, TSpeedButton(Sender));
end;

procedure Tfrm1Panel.bCancelClick(Sender: TObject);
begin
  MyState:=sView;
  DataSource1.OnDataChange(DataSource1,DataSource1.DataSet.Fields[0]);
  SpeedBtnClick(Self, TSpeedButton(Sender));
end;

procedure Tfrm1Panel.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     MYFormKeyDownF(Sender,Self,key,shift);
end;

procedure Tfrm1Panel.GridWindowProc(var Message: TMessage);
var
  Pos: SmallInt;
begin
{  OldGridProc(Message);
  if Message.Msg = WM_VSCROLL then  //or WM_HSCROLL
  begin
    Pos          := Message.WParamHi;  //Scrollbox position
    //TADOStoredProc(FindComponent('Browes1')).RecNo := Pos;
    Browse1.RecNo := Pos;
  end;}
end;


procedure Tfrm1Panel.DBGrid1DblClick(Sender: TObject);
begin
      SetGridColumnWidths(DBGrid1);
end;

procedure Tfrm1Panel.bSearchClick(Sender: TObject);
begin
  FilterDBGrid2(GlobalDBGrid);
end;

procedure Tfrm1Panel.bPrintClick(Sender: TObject);
begin
  PrintDBGrid(GlobalDBGrid);
end;

end.
