unit untSellBuy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt2Panels, DB, ADODB, Buttons, ComCtrls, Grids, DBGrids,
  ExtCtrls, StdCtrls, Mask, untDm, U_Common, Math, ppDB, ppDBPipe, ppComm,
  ppRelatv, ppProd, ppClass, ppReport, ppPrnabl, ppCtrls, ppBands, ppCache,
  ppParameter, jpeg, ppModule, raCodMod, ppChrt;

type
  TfrmSellBuy = class(Tfrm2Panels)
    FCode1: TEdit;
    FName1: TEdit;
    MExtra1: TEdit;
    MCost1: TEdit;
    MDiscount1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    lblStd: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lblSBName: TLabel;
    GoodCode2: TEdit;
    MPrice2: TEdit;
    MTPrice2: TEdit;
    NCount2: TEdit;
    Label3: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    lblGood: TLabel;
    Label10: TLabel;
    MCredit2: TEdit;
    DtDate1: TMaskEdit;
    dtsGoods: TDataSource;
    GoodsBrowse: TADOStoredProc;
    GoodsCountUpdate: TADOStoredProc;
    Browse2FSerial: TAutoIncField;
    Browse2srlGood: TIntegerField;
    Browse2NCount: TIntegerField;
    Browse2NRemain: TIntegerField;
    Browse2srlSource: TIntegerField;
    Browse2srlWarehouse: TIntegerField;
    Browse2srlFactor: TIntegerField;
    Browse2MCash: TBCDField;
    Browse2MCredit: TBCDField;
    Browse2MPrice: TBCDField;
    Browse2FName: TWideStringField;
    Browse2WarehouseName: TWideStringField;
    Browse2Unit: TWideStringField;
    Browse2GoodCode: TIntegerField;
    Browse2WarehouseCode: TIntegerField;
    GoodsBrowseFSerial: TAutoIncField;
    GoodsBrowsesrlGood: TIntegerField;
    GoodsBrowseNCount: TIntegerField;
    GoodsBrowseNRemain: TIntegerField;
    GoodsBrowsesrlSource: TIntegerField;
    GoodsBrowsesrlWarehouse: TIntegerField;
    GoodsBrowsesrlFactor: TIntegerField;
    GoodsBrowseMCash: TBCDField;
    GoodsBrowseMCredit: TBCDField;
    GoodsBrowseMPrice: TBCDField;
    GoodsBrowseType: TSmallintField;
    spbPreToFactor: TSpeedButton;
    PreToFactor: TADOStoredProc;
    dtsDetPreToFactor: TDataSource;
    DetPreToFactor: TADOStoredProc;
    DetPreToFactorFSerial: TAutoIncField;
    DetPreToFactorsrlGood: TIntegerField;
    DetPreToFactorNCount: TIntegerField;
    DetPreToFactorNRemain: TIntegerField;
    DetPreToFactorsrlSource: TIntegerField;
    DetPreToFactorsrlWarehouse: TIntegerField;
    DetPreToFactorsrlFactor: TIntegerField;
    DetPreToFactorMCash: TBCDField;
    DetPreToFactorMCredit: TBCDField;
    DetPreToFactorMPrice: TBCDField;
    DetPreToFactorFName: TWideStringField;
    DetPreToFactorWarehouseName: TWideStringField;
    DetPreToFactorUnit: TWideStringField;
    DetPreToFactorGoodCode: TIntegerField;
    DetPreToFactorWarehouseCode: TIntegerField;
    deactiveStudent: TADOStoredProc;
    deactiveStudentFSerial: TAutoIncField;
    deactiveStudentFName: TWideStringField;
    deactiveGoods: TADOStoredProc;
    deactiveGoodsFSerial: TAutoIncField;
    deactiveGoodsFName: TWideStringField;
    deactiveGoodsBlocked: TBooleanField;
    imgStd: TImage;
    dtsGoodsSource: TDataSource;
    GoodsBrowseSource: TADOStoredProc;
    ADOStoredProc2: TADOStoredProc;
    Label11: TLabel;
    WarehouseCode2: TEdit;
    lblWarehouse2: TLabel;
    CustomerCode1: TEdit;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1DtDate: TStringField;
    Browse1MExtra: TBCDField;
    Browse1Type: TSmallintField;
    Browse1MCost: TBCDField;
    Browse1MDiscount: TBCDField;
    Browse1IsClose: TBooleanField;
    Browse1srlYear: TIntegerField;
    Browse1srlLoan: TIntegerField;
    Browse1srlStd: TIntegerField;
    Browse1srlBuyCenter: TIntegerField;
    Browse1CustomerCode: TIntegerField;
    Browse1CustomerName: TWideStringField;
    deactiveStudentBlocked: TBooleanField;
    ppReport1: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    StudentPic1: TppImage;
    Comment2: TEdit;
    Label13: TLabel;
    Browse2Comment: TWideStringField;
    Browse1SumCash: TBCDField;
    Browse1SumCredit: TBCDField;
    Browse1SumPrice: TBCDField;
    ppShape1: TppShape;
    ppDBImage1: TppDBImage;
    Browse2MTPrice: TBCDField;
    Browse1srlCasher: TIntegerField;
    Browse1FserialCasher: TAutoIncField;
    Browse1FcodeCasher: TIntegerField;
    Browse1FnameCasher: TWideStringField;
    FcodeCasher1: TEdit;
    Label16: TLabel;
    lblCasher: TLabel;
    Browse2Percent1: TIntegerField;
    Browse2ExtraPrice: TBCDField;
    Browse2IsTaghsit: TBooleanField;
    Browse1srlWarehouse: TIntegerField;
    Label17: TLabel;
    Nesieh1: TEdit;
    Browse1Nesieh: TBCDField;
    Browse1srlUser: TIntegerField;
    IsTaghsit2: TCheckBox;
    Label18: TLabel;
    Percent12: TEdit;
    Label19: TLabel;
    Label20: TLabel;
    ExtraPrice2: TEdit;
    Label21: TLabel;
    MCash2: TEdit;
    BOk: TSpeedButton;
    Update_Ok_Fac: TADOStoredProc;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure StdCode1Change(Sender: TObject);
    procedure StdCode1Exit(Sender: TObject);
    procedure StdCode1KeyPress(Sender: TObject; var Key: Char);
    procedure GoodCode2Change(Sender: TObject);
    procedure GoodCode2Exit(Sender: TObject);
    procedure GoodCode2KeyPress(Sender: TObject; var Key: Char);
    procedure bSaveClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure spbPreToFactorClick(Sender: TObject);
    procedure bPrintClick(Sender: TObject);
    procedure DataSource2DataChange(Sender: TObject; Field: TField);
    procedure bCancelClick(Sender: TObject);
    procedure bEditClick(Sender: TObject);
    procedure WarehouseCode2Change(Sender: TObject);
    procedure WarehouseCode2Exit(Sender: TObject);
    procedure WarehouseCode2KeyPress(Sender: TObject; var Key: Char);
    procedure FCode1KeyPress(Sender: TObject; var Key: Char);
    procedure MExtra1KeyPress(Sender: TObject; var Key: Char);
    procedure MCost1KeyPress(Sender: TObject; var Key: Char);
    procedure MDiscount1KeyPress(Sender: TObject; var Key: Char);
    procedure NCount2KeyPress(Sender: TObject; var Key: Char);
    procedure MTPrice2KeyPress(Sender: TObject; var Key: Char);
    procedure MCredit2KeyPress(Sender: TObject; var Key: Char);
    procedure MPrice2KeyPress(Sender: TObject; var Key: Char);
    procedure FcodeCasher1Change(Sender: TObject);
    procedure FcodeCasher1Exit(Sender: TObject);
    procedure FcodeCasher1KeyPress(Sender: TObject; var Key: Char);
    procedure BOkClick(Sender: TObject);
  private
    { Private declarations }
    procedure procPreToFactor(srlG, nv, srlF: Integer);
    procedure refreshPanels(b1fs, b2fs:Integer);
    procedure ShowStdPic;
  public
    { Public declarations }
  end;

var
  frmSellBuy: TfrmSellBuy;
  srlDataSetGoods, srlDataSetMaxRecord, srlDataSetSellerBuyer,
  srlDataSetWarehouse2, srlWarehouse2, srlDataSetCasher: Integer;

implementation

{$R *.dfm}

procedure TfrmSellBuy.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  IfNullThenZero(Browse1FSerial.Value, srlFactor);
//  IfNullThenZero(Browse1srlBuyCenter.Value, srlBuyCenter);
//  IfNullThenZero(Browse1srlStd.Value, srlStd);
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlFactor').Value := Browse1FSerial.Value;
  Browse2.Open;
end;

procedure TfrmSellBuy.StdCode1Change(Sender: TObject);
begin
  inherited;
  if FactorType in [ftBuy, ftFirstStock, ftBackBuy, ftMoveFromWarehouse, ftGift, ftParcel] then
    EditCodeChange(sView, CustomerCode1, lblSBName, srlBuyCenter, srlDataSetSellerBuyer)
  else if FactorType in [ftSell, ftBackSell, ftPreFactor] then
  begin
    EditCodeChange(sView, CustomerCode1, lblSBName, srlStd, srlDataSetSellerBuyer);
    ShowStdPic;
  end;
end;

procedure TfrmSellBuy.StdCode1Exit(Sender: TObject);
begin
  inherited;
  if FactorType in [ftBuy, ftFirstStock, ftBackBuy, ftMoveFromWarehouse, ftGift, ftParcel] then
    EditCodeExit(MyState, CustomerCode1, lblSBName)
  else if FactorType in [ftSell, ftBackSell, ftPreFactor] then
    EditCodeExit(MyState, CustomerCode1, lblSBName)
end;

procedure TfrmSellBuy.StdCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
  if FactorType in [ftBuy, ftFirstStock, ftBackBuy, ftMoveFromWarehouse, ftGift, ftParcel] then
    EditCodeKeyPress(Key, MyState, CustomerCode1, lblSBName, srlBuyCenter, srlDataSetSellerBuyer)
  else if FactorType in [ftSell, ftBackSell, ftPreFactor] then
    EditCodeKeyPress(Key, MyState, CustomerCode1, lblSBName, srlStd, srlDataSetSellerBuyer);
end;

procedure TfrmSellBuy.GoodCode2Change(Sender: TObject);
begin
  inherited;
  if FactorType = ftBackBuy then
    EditCodeChange(sView, GoodCode2, lblGood, srlSource, srlDataSetGoods, srlBuyCenter, -1, -1, -1)
  else if FactorType = ftBackSell then
    EditCodeChange(sView, GoodCode2, lblGood, srlSource, srlDataSetGoods, srlStd, -1, -1, -1)
  else
    EditCodeChange(sView, GoodCode2, lblGood, srlGood, srlDataSetGoods);
end;

procedure TfrmSellBuy.GoodCode2Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, GoodCode2, lblGood);
end;

procedure TfrmSellBuy.GoodCode2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
  if FactorType = ftBackBuy then
    EditCodeKeyPress(Key, MyState, GoodCode2, lblGood, srlSource, srlDataSetGoods, srlBuyCenter, -1, -1, -1)
  else if FactorType = ftBackSell then
    EditCodeKeyPress(Key, MyState, GoodCode2, lblGood, srlSource, srlDataSetGoods, srlStd, -1, -1, -1)
  else
    EditCodeKeyPress(Key, MyState, GoodCode2, lblGood, srlGood, srlDataSetGoods);
end;

procedure TfrmSellBuy.bSaveClick(Sender: TObject);
var
  i, t, nvalue, nremain, newnremain, grc, sgrc, b1fs, b2fs:Integer;
  srlDetFac4Remain: Integer;
begin
  IfNullThenZero(Browse1.FieldValues['FSerial'], b1fs);
  IfNullThenZero(Browse2.FieldValues['FSerial'], b2fs);
  if panelfocus = 'Panel1' then
  begin
    if (Trim(CustomerCode1.Text) = '') or
      (Trim(FCode1.Text) = '') or
      (Trim(DtDate1.Text) = '') or
      (Trim(MExtra1.Text) = '') or
      (Trim(MCost1.Text) = '') or
      (Trim(Nesieh1.Text) = '') or
      (Trim(FcodeCasher1.Text) = '') or
      (Trim(MDiscount1.Text) = '')
      then
    begin
      ShowMessage('������� ���� ����!');
      Exit;
    end;
  end
  else if panelfocus = 'Panel2' then
  begin
    if (Trim(GoodCode2.Text) = '') or
      ((WarehouseCode2.Visible) and (Trim(WarehouseCode2.Text) = '')) or
      (Trim(MCash2.Text) = '') or
      (Trim(MCredit2.Text) = '') or
      (Trim(MPrice2.Text) = '')
      then
    begin
      ShowMessage('������� ���� ����!');
      Exit;
    end;
  end;
  inherited;
  NullAdoSPParameters(deactiveStudent);
  NullAdoSPParameters(Insert1);
  if (FactorType in [ftSell, ftBackSell, ftPreFactor]) then
  begin
    deactiveStudent.Parameters.ParamByName('@FSerial').Value := srlStd;
    deactiveStudent.Open;
    if deactiveStudentBlocked.Value then
    begin
      ShowMessage(deactiveStudentFName.Value + ' ����� ���.');
      bCancelClick(Sender);
      refreshPanels(b1fs, b2fs);
      MyState := sView;
      Exit;
    end;
  end;
  if panelfocus = 'Panel1' then
  begin
    if MyState = sAdd then
    begin
      Insert1.Parameters.ParamByName('@Fcode_2').Value := Fcode1.Text;
      Insert1.Parameters.ParamByName('@FName_3').Value := Fname1.Text;
      Insert1.Parameters.ParamByName('@DtDate_4').Value := DtDate1.Text;
      Insert1.Parameters.ParamByName('@MExtra_5').Value := MExtra1.Text;
      Insert1.Parameters.ParamByName('@Type_6').Value := Ord(FactorType);
      Insert1.Parameters.ParamByName('@MCost_7').Value := MCost1.Text;
      Insert1.Parameters.ParamByName('@MDiscount_8').Value := MDiscount1.Text;
      Insert1.Parameters.ParamByName('@IsClose_9').Value := False;// IsClose1.Checked;
      Insert1.Parameters.ParamByName('@srlYear_10').Value := srlYear;
      Insert1.Parameters.ParamByName('@srlLoan_11').Value := 0;
      Insert1.Parameters.ParamByName('@srlUser').Value := srlUser;
      Insert1.Parameters.ParamByName('@srlCasher').Value := srlCasher;
      Insert1.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
      Insert1.Parameters.ParamByName('@Nesieh').Value := Nesieh1.Text;


      if FactorType in [ftBuy, ftMoveFromWarehouse, ftGift, ftParcel] then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := Null;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := srlBuyCenter;
      end
      else if (FactorType = ftSell) or (FactorType = ftPreFactor) then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := srlStd;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := Null;
      end
      else if FactorType = ftFirstStock then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := Null;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := srlBuyCenter;
      end
      else if FactorType = ftBackBuy then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := Null;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := srlBuyCenter;
      end
      else if FactorType = ftBackSell then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := srlStd;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := Null;
      end;
      Insert1.ExecProc;
      refreshPanels(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value, 0);
    end
    else if (MyState = sEdit) and (panelfocus = 'Panel1') then
    begin
      Update1.Parameters.ParamByName('@FSerial_1').Value := b1fs;
      Update1.Parameters.ParamByName('@FCode_3').Value := Fcode1.Text;
      Update1.Parameters.ParamByName('@FName_4').Value := Fname1.Text;
    //  Update1.Parameters.ParamByName('@srlStd_4').Value := srlStd;
      Update1.Parameters.ParamByName('@DtDate_7').Value := DtDate1.Text;
      Update1.Parameters.ParamByName('@MExtra_8').Value := MExtra1.Text;
      Update1.Parameters.ParamByName('@MCost_10').Value := MCost1.Text;
      Update1.Parameters.ParamByName('@srlCasher').Value := srlCasher;
      Update1.Parameters.ParamByName('@Nesieh').Value := Nesieh1.Text;
      Update1.Parameters.ParamByName('@MDiscount_11').Value := MDiscount1.Text;
     // Update1.Parameters.ParamByName('@IsClose_12').Value := False;// IsClose1.Checked;
      Update1.ExecProc;
      refreshPanels(b1fs, 0);
    end;
  end
  else
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  if panelfocus = 'Panel2' then  ///////////
  begin
    /////////////// BLOCKED GOODS
    NullAdoSPParameters(deactiveGoods);
    NullAdoSPParameters(Insert2);
    deactiveGoods.Parameters.ParamByName('@FSerial').Value := srlGood;
    deactiveGoods.Open;
    if deactiveGoodsBlocked.Value then
    begin
      ShowMessage(deactiveGoodsFName.Value + ' ����� ���.');
      bCancelClick(Sender);
      refreshPanels(b1fs, b2fs);
      MyState := sView;
      Exit;
    end;
    ///////////////////////////////
    if MyState = sAdd then
    begin
      nvalue := StrToInt(NCount2.Text);
      ////////////////////////////////////
      if (FactorType = ftSell) then
      begin
        NullAdoSPParameters(GoodsBrowse);
        GoodsBrowse.Parameters.ParamByName('@srlGood1').Value := srlGood;
        GoodsBrowse.Parameters.ParamByName('@srlYear').Value := srlYear;
        GoodsBrowse.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
        GoodsBrowse.Open;
        if GoodsBrowse.RecordCount > 0 then
        begin
          sgrc := 0;
          for grc := 0 to GoodsBrowse.RecordCount - 1 do
          begin
            sgrc := sgrc + GoodsBrowse.FieldValues['NRemain'];
            GoodsBrowse.Next;
          end;
          GoodsBrowse.First;
          if sgrc < nvalue then
          begin
            grc := MessageDlg('����� ������� ��� �� ������ ���.'#13'��� �� ����� ' + IntToStr(sgrc) + ' ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
            if grc = mrCancel then
              exit
            else
            begin
              nvalue := sgrc;
              NCount2.Text := IntToStr(nvalue);
            end;
          end;
          repeat
          begin
            //srlDetFac4Remain := GoodsBrowse.FieldValues['FSerial'];
//            ShowMessage('.����� ���� ����� �� ����� ���� �� �� ���� ���'#13'���� ��������� �� ����� ���� ������ ���� ��������� �� �����.');
       //     NCount2.Text := IntToStr(GoodsBrowse.FieldValues['NRemain']);
         //   GoodsBrowse.Locate('FSerial', srlDetFac4Remain, []);
            nremain := StrToInt(GoodsBrowse.FieldValues['NRemain']);
            newnremain := nremain - nvalue;
            if newnremain < 0 then
              newnremain := 0
            else
              nremain := nvalue;
            GoodsCountUpdate.Parameters.ParamByName('@FSerial_1').Value := GoodsBrowse.FieldValues['FSerial'];
            GoodsCountUpdate.Parameters.ParamByName('@NRemain_2').Value := newnremain;
            GoodsCountUpdate.ExecProc;
            Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
            Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
            Insert2.Parameters.ParamByName('@NRemain_4').Value := nremain;
            Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
            Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
            Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
            Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
            Insert2.Parameters.ParamByName('@MCredit_9').Value := MCredit2.Text;//GoodsBrowse.FieldValues['MCredit'];
            if (IsTaghsit2.Checked) and (GoodsBrowse.FieldValues['MCredit'] > 0) then
              Insert2.Parameters.ParamByName('@MPrice_10').Value := GoodsBrowse.FieldValues['MCredit']
            else
            begin
              Insert2.Parameters.ParamByName('@MPrice_10').Value := GoodsBrowse.FieldValues['MCash'];
              if IsTaghsit2.Checked then
              begin
                IsTaghsit2.Checked := False;
                ShowMessage('��� ���� ����� ����� �����.');
              end;
            end;
            if (MPrice2.Text <> '') and (StrToInt(MPrice2.Text) > 0) then
              Insert2.Parameters.ParamByName('@MPrice_10').Value := MPrice2.Text;
            Insert2.Parameters.ParamByName('@isTaghsit').Value := IsTaghsit2.Checked;
            Insert2.Parameters.ParamByName('@Percent1').Value := Percent12.Text;
            Insert2.Parameters.ParamByName('@comment').Value :=Comment2.Text ;
            Insert2.ExecProc;
            nvalue := nvalue - nremain;
            GoodsBrowse.Next;
 //           Exit;
          end
          until (nvalue <= 0);
        end
        else // GoodsBrowse.RecordCount = 0
        begin
          ShowMessage('�� ��� ���� ������ ������.');
//          Exit;
        end;
        GoodsBrowse.Close;
      end
      else if FactorType in [ftBackBuy, ftBackSell] then
      begin
        GoodsBrowseSource.Close;
        GoodsBrowseSource.Parameters.ParamByName('@srlSource').Value := srlSource;
        GoodsBrowse.Parameters.ParamByName('@srlYear').Value := srlYear;
        GoodsBrowse.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
        GoodsBrowseSource.Open;
        sgrc := GoodsBrowseSource.FieldValues['NRemain'];
        nvalue := StrToInt(NCount2.Text);
        if sgrc < nvalue then
        begin
          grc := MessageDlg('����� ������� ��� �� �� ���� ���.'#13'��� �� ����� ���� ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
          if grc = mrCancel then
            exit
          else
          begin
            nvalue := sgrc;
            NCount2.Text := IntToStr(nvalue);
          end;
        end;
        Insert2.Parameters.ParamByName('@srlGood_2').Value := GoodCode2.Text;
        Insert2.Parameters.ParamByName('@isTaghsit').Value := False;
        Insert2.Parameters.ParamByName('@NCount_3').Value := NCount2.Text;
        Insert2.Parameters.ParamByName('@NRemain_4').Value := NCount2.Text;
        Insert2.Parameters.ParamByName('@srlSource_5').Value := srlSource;
        Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
        Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
        Insert2.Parameters.ParamByName('@MCash_8').Value := MCash2.Text;
        Insert2.Parameters.ParamByName('@MCredit_9').Value := MCredit2.Text;
        Insert2.Parameters.ParamByName('@MPrice_10').Value := MPrice2.Text;
        Insert2.Parameters.ParamByName('@Percent1').Value := 0;
        Insert2.Parameters.ParamByName('@comment').Value :=Comment2.Text ;
        Insert2.ExecProc;
        refreshPanels(b1fs, Insert2.Parameters.ParamByName('@RETURN_VALUE').Value);
      end
      else if (FactorType = ftMoveFromWarehouse) then
      begin
        GoodsBrowse.Close;
        GoodsBrowse.Parameters.ParamByName('@srlGood1').Value := srlGood;
        GoodsBrowse.Parameters.ParamByName('@srlYear').Value := srlYear;
        GoodsBrowse.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
        GoodsBrowse.Open;
        if GoodsBrowse.RecordCount > 0 then
        begin
          sgrc := 0;
          for grc := 0 to GoodsBrowse.RecordCount - 1 do
          begin
            sgrc := sgrc + GoodsBrowse.FieldValues['NRemain'];
            GoodsBrowse.Next;
          end;
          GoodsBrowse.First;
          if sgrc < nvalue then
          begin
            grc := MessageDlg('����� ������� ��� �� ������ ���.'#13'��� �� ����� ' + IntToStr(sgrc) + ' ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
            if grc = mrCancel then
              exit
            else
            begin
              nvalue := sgrc;
              NCount2.Text := IntToStr(nvalue);
            end;
          end;
          repeat
          begin
            //srlDetFac4Remain := GoodsBrowse.FieldValues['FSerial'];
//            ShowMessage('.����� ���� ����� �� ����� ���� �� �� ���� ���'#13'���� ��������� �� ����� ���� ������ ���� ��������� �� �����.');
       //     NCount2.Text := IntToStr(GoodsBrowse.FieldValues['NRemain']);
         //   GoodsBrowse.Locate('FSerial', srlDetFac4Remain, []);
            nremain := StrToInt(GoodsBrowse.FieldValues['NRemain']);
            newnremain := nremain - nvalue;
            if newnremain < 0 then
              newnremain := 0
            else
              nremain := nvalue;
            GoodsCountUpdate.Parameters.ParamByName('@FSerial_1').Value := GoodsBrowse.FieldValues['FSerial'];
            GoodsCountUpdate.Parameters.ParamByName('@NRemain_2').Value := newnremain;
            GoodsCountUpdate.ExecProc;

            Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
            Insert2.Parameters.ParamByName('@IsTaghsit').Value := False;
            Insert2.Parameters.ParamByName('@Percent1').Value := 0;
            Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
            Insert2.Parameters.ParamByName('@NRemain_4').Value := 0;
            Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
            Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
            Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
            Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
            Insert2.Parameters.ParamByName('@MCredit_9').Value := GoodsBrowse.FieldValues['MCredit'];
            Insert2.Parameters.ParamByName('@MPrice_10').Value := 0;// GoodsBrowse.FieldValues['MPrice'];
            Insert2.Parameters.ParamByName('@comment').Value :=Comment2.Text ;
            Insert2.ExecProc;

            Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
            Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
            Insert2.Parameters.ParamByName('@NRemain_4').Value := nremain;
            Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
            Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse2;
            Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
            Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
            Insert2.Parameters.ParamByName('@MCredit_9').Value := GoodsBrowse.FieldValues['MCredit'];
            Insert2.Parameters.ParamByName('@MPrice_10').Value := 0;// GoodsBrowse.FieldValues['MPrice'];
            Insert2.Parameters.ParamByName('@comment').Value :=Comment2.Text ;
            Insert2.ExecProc;

            nvalue := nvalue - nremain;
            GoodsBrowse.Next;
 //           Exit;
          end
          until (nvalue <= 0);
        end
        else // GoodsBrowse.RecordCount = 0
        begin
          ShowMessage('�� ��� ���� ������ ������.');
//          Exit;
        end;
        GoodsBrowse.Close;
      end
      else // O F   if FactorType=ftSell
      begin
        Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
        Insert2.Parameters.ParamByName('@NCount_3').Value := nvalue;
            Insert2.Parameters.ParamByName('@IsTaghsit').Value := False;
            Insert2.Parameters.ParamByName('@Percent1').Value := 0;
        Insert2.Parameters.ParamByName('@NRemain_4').Value := StrToInt(NCount2.Text);
        Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
        Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
        Insert2.Parameters.ParamByName('@MCash_8').Value := MCash2.Text;
        Insert2.Parameters.ParamByName('@MCredit_9').Value := MCredit2.Text;
        Insert2.Parameters.ParamByName('@MPrice_10').Value := MPrice2.Text;
        Insert2.Parameters.ParamByName('@comment').Value :=Comment2.Text ;
        Insert2.ExecProc;
        refreshPanels(b1fs, Insert2.Parameters.ParamByName('@RETURN_VALUE').Value);
      end;
      ////////////////////////////////////
    end
    else if (MyState = sEdit) and (panelfocus='Panel2') then
    begin
      Update2.Parameters.ParamByName('@FSerial_1').Value := b2fs;
      Update2.Parameters.ParamByName('@MCash_9').Value := MCash2.Text;
      Update2.Parameters.ParamByName('@MCredit_10').Value := MCredit2.Text;
      Update2.Parameters.ParamByName('@isTaghsit').Value := IsTaghsit2.Checked;
      Update2.Parameters.ParamByName('@Percent1').Value := Percent12.Text;
      Update2.Parameters.ParamByName('@MPrice_11').Value := MPrice2.Text;
       Update2.Parameters.ParamByName('@Comment').Value := Comment2.Text;
      Update2.ExecProc;
    end; // end if MyState.
    RefreshPanels(b1fs, IfNullThenZero(Update2.Parameters.ParamByName('@RETURN_VALUE').Value, b2fs));
  end; // end if panel.
  MyState := sView;
end;

procedure TfrmSellBuy.bAddClick(Sender: TObject);
begin
  inherited;
  if panelfocus = 'Panel1' then
  begin
    Fcode1.Text := GetMaxCodeRecord(srlDataSetMaxRecord, srlYear, -1, -1, -1);
    DtDate1.Text := CurrDate;
    MExtra1.Text := '0';
    MCost1.Text := '0';
    MDiscount1.Text := '0';
    Nesieh1.Text := '0';
    FCode1.SetFocus;
  end
  else if panelfocus = 'Panel2' then
  begin
    if IsTaghsit2.Visible then
      IsTaghsit2.SetFocus
    else
      GoodCode2.SetFocus;
    NCount2.Text := '1';
    MCash2.Text := '0';
    Percent12.Text := '0';
    MCash2.Text := '0';
    MCredit2.Text := '0';
    MPrice2.Text := '0';
  end;
end;

procedure TfrmSellBuy.bDelClick(Sender: TObject);
begin
  inherited;
  if panelfocus='Panel1'  then
    DeleteRecord1(39, Browse1FSerial.Value, Browse1)
  else if panelfocus='Panel2'  then
    DeleteRecord1(23, Browse2FSerial.Value, Browse2);
end;

procedure TfrmSellBuy.FormShow(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  spbPreToFactor.Visible := False;
  spbPreToFactor.Enabled := False;
  WarehouseCode2.Visible := False;
  Label11.Visible := False;
  lblWarehouse2.Visible := False;
  srlDataSetGoods := 18;
  srlDataSetCasher := 45;
  if (FactorType = ftSell) then
  begin
    lblStd.Caption := '������:';
    srlDataSetMaxRecord := 21;
    srlDataSetSellerBuyer := 12;
    MCash2.Visible:=false;
    Label21.Visible:=false;
  end
  else if FactorType = ftBuy then
  begin
    lblStd.Caption := '�������:';
    srlDataSetMaxRecord := 22;
    srlDataSetSellerBuyer := 20;
  end
  else if FactorType = ftGift then
  begin
    lblStd.Caption := '���������:';
    srlDataSetMaxRecord := 43;
    srlDataSetSellerBuyer := 20;
  end
  else if FactorType = ftParcel then
  begin
    lblStd.Caption := '����� �����:';
    srlDataSetMaxRecord := 44;
    srlDataSetSellerBuyer := 20;
  end
  else if FactorType = ftPreFactor then
  begin
    lblStd.Caption := '������:';
    srlDataSetMaxRecord := 28;
    srlDataSetSellerBuyer := 12;
    spbPreToFactor.Visible := True;
    spbPreToFactor.Enabled := True;
    MCash2.Visible:=false;
    Label21.Visible := False;
  end
  else if (FactorType = ftFirstStock) then
  begin
    lblStd.Caption := '�������:';
    srlDataSetMaxRecord := 37;
    srlDataSetSellerBuyer := 20;
  end
  else if (FactorType = ftBackSell) then
  begin
    lblStd.Caption := '������:';
    srlDataSetGoods := 27;
    srlDataSetMaxRecord := 25;
    srlDataSetSellerBuyer := 12;
  end
  else if FactorType = ftBackBuy then
  begin
    lblStd.Caption := '�������:';
    srlDataSetGoods := 26;
    srlDataSetMaxRecord := 24;
    srlDataSetSellerBuyer := 20;
  end
  else if FactorType = ftMoveFromWarehouse then
  begin
    lblStd.Caption := '�������:';
    srlDataSetGoods := 18;
    srlDataSetWarehouse2 := 29;
    srlDataSetMaxRecord := 30;
    srlDataSetSellerBuyer := 20;
    Label11.Visible := True;
    WarehouseCode2.Visible := True;
    lblWarehouse2.Visible := True;
  end;
  for i := 0 to Browse1.Parameters.Count - 1 do
    Browse1.Parameters[i].Value := null;

  if not (FactorType in [ftSell, ftPreFactor]) then
  begin
    ExtraPrice2.Visible := False;
    Label20.Visible := False;
    Label18.Visible := False;
    Label8.Visible := False;
    Label19.Visible := False;
    Percent12.Visible := False;
    MTPrice2.Visible := False;
    IsTaghsit2.Visible:=False;
  end;
  refreshPanels(0, 0);
end;

procedure TfrmSellBuy.procPreToFactor(srlG, nv, srlF: Integer);
var
  sgrc, grc, nvalue, nremain, newnremain: Integer;
begin
  nvalue := nv;
  GoodsBrowse.Close;
  GoodsBrowse.Parameters.ParamByName('@srlGood1').Value := srlG;
  GoodsBrowse.Parameters.ParamByName('@srlYear').Value := srlYear;
  GoodsBrowse.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
  GoodsBrowse.Open;
  if GoodsBrowse.RecordCount > 0 then
  begin
    sgrc := 0;
    for grc := 0 to GoodsBrowse.RecordCount - 1 do
    begin
      sgrc := sgrc + GoodsBrowse.FieldValues['NRemain'];
      GoodsBrowse.Next;
    end;
    GoodsBrowse.First;
    if sgrc < nvalue then
    begin
    //  grc := MessageDlg('����� ������� ��� �� ������ ���.'#13'��� �� ����� ' +
      //  IntToStr(sgrc) +
        //' ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
     // if grc = mrCancel then
       // exit
     // else
      begin
        nvalue := sgrc;
        NCount2.Text := IntToStr(nvalue);
      end;
    end;
    repeat
      nremain := StrToInt(GoodsBrowse.FieldValues['NRemain']);
      newnremain := nremain - nvalue;
      if newnremain < 0 then
        newnremain := 0
      else
        nremain := nvalue;
      GoodsCountUpdate.Parameters.ParamByName('@FSerial_1').Value := GoodsBrowse.FieldValues['FSerial'];
      GoodsCountUpdate.Parameters.ParamByName('@NRemain_2').Value := newnremain;
      GoodsCountUpdate.ExecProc;
      Insert2.Parameters.ParamByName('@srlGood_2').Value := srlG;
      Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
      Insert2.Parameters.ParamByName('@NRemain_4').Value := nremain;
      Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
      Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
      Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlF;
      Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
      Insert2.Parameters.ParamByName('@MCredit_9').Value := GoodsBrowse.FieldValues['MCredit'];
      Insert2.Parameters.ParamByName('@MPrice_10').Value := 0;// GoodsBrowse.FieldValues['MPrice'];
      Insert2.ExecProc;
      nvalue := nvalue - nremain;
      GoodsBrowse.Next;
    until (nvalue <= 0);
  end
  else // GoodsBrowse.RecordCount = 0
  begin
    ShowMessage('�� ����� ' + Browse2FName.Value + ' ������ ������.');
  end;
  GoodsBrowse.Close;
end;

procedure TfrmSellBuy.spbPreToFactorClick(Sender: TObject);
var
  detFacCounter, detsrlFactor: Integer;
begin
  inherited;
  if (FactorType = ftPreFactor) and (not Browse1IsClose.Value) then
  begin
    PreToFactor.Parameters.ParamByName('@srlFactor').Value := srlFactor;
    PreToFactor.ExecProc;
//    DetPreToFactor.Parameters.ParamByName('@srlFactor').Value :=
//      PreToFactor.Parameters.ParamByName('@RETURN_VALUE').Value;
    //Browse2.Open;
    detsrlFactor := PreToFactor.Parameters.ParamByName('@RETURN_VALUE').Value;
    if Browse2.RecordCount > 0 then
    begin
      Browse2.First;
      for detFacCounter := 1 to Browse2.RecordCount do
      begin
        procPreToFactor(Browse2srlGood.Value, Browse2NCount.Value, detsrlFactor);
        Browse2.Next;
      end;
    end;
  end
  else if Browse1IsClose.Value then
    ShowMessage('��� ��ԝ������ ����� ����� ��� ���.');
end;

procedure TfrmSellBuy.bPrintClick(Sender: TObject);
begin
  inherited;
  PreviewReport(ord(factortype)+1, DataSource2,DataSource1,DataSource2, 2);
  //ppReport1.Print
end;

procedure TfrmSellBuy.DataSource2DataChange(Sender: TObject;
  Field: TField);
begin
  srlGood := Browse2srlGood.Value;
  srlSource := Browse2srlSource.Value;
  inherited;
end;

procedure TfrmSellBuy.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanels(0, 0);
end;

procedure TfrmSellBuy.refreshPanels(b1fs, b2fs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Parameters.ParamByName('@Type').Value := FactorType;
  Browse1.Parameters.ParamByName('@srlYear').Value := srlYear;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlFactor').Value := srlFactor;
  Browse2.Open;
  Browse2.Locate('FSerial', b2fs, []);
end;

procedure TfrmSellBuy.bEditClick(Sender: TObject);
var
  b1fs, b2fs: Integer;
begin
  IfNullThenZero(Browse1.FieldValues['FSerial'], b1fs);
  IfNullThenZero(Browse2.FieldValues['FSerial'], b2fs);
  inherited;

  if Browse1IsClose.Value then
  begin
    ShowMessage('��� ������ ���� ���.');
    bCancelClick(bCancel);
    refreshPanels(b1fs, b2fs);
    MyState := sView;
    Exit;
  end;
end;

procedure TfrmSellBuy.ShowStdPic;
var
  picFolderNo: Integer;
  picFolder, picFile: String;
begin
  picFolderNo := srlStd div PIC_FOLDER_DIV_NO;
  if not ((srlStd mod PIC_FOLDER_DIV_NO) = 0) then
    Inc(picFolderNo);
  picFolder := IntToStr(picFolderNo);
  if picFolderNo < 10 then
    picFolder := '0' + picFolder;
//  picFolder := GetCurrentDir + '\' + PIC_FOLDER_NAME + picFolder + '\';
  picFolder := PIC_FOLDER_NAME + picFolder + '\';
  picFile := picFolder + IntToStr(srlStd) + '.jpg';
  if FileExists(picFile) then
    begin
    imgStd.Picture.LoadFromFile(picFile);
    imgstd.Picture.savetofile('c:\temp\A.jpg') ;
    end

  else
    begin
    imgStd.Picture.Graphic := nil;
    imgstd.Picture.savetofile('c:\temp\A.jpg') ;
    end;
end;

procedure TfrmSellBuy.WarehouseCode2Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, WarehouseCode2, lblWarehouse2, srlWarehouse2, srlDataSetWarehouse2, srlWarehouse, -1, -1, -1)
end;

procedure TfrmSellBuy.WarehouseCode2Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, WarehouseCode2, lblWarehouse2);
end;

procedure TfrmSellBuy.WarehouseCode2KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
  EditCodeKeyPress(Key, MyState, WarehouseCode2, lblWarehouse2, srlWarehouse2, srlDataSetWarehouse2, srlWarehouse, -1, -1, -1);
end;

procedure TfrmSellBuy.FCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.MExtra1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.MCost1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.MDiscount1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.NCount2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.MTPrice2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.MCredit2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.MPrice2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
end;

procedure TfrmSellBuy.FcodeCasher1Change(Sender: TObject);
begin
  inherited;
  //if FactorType in [ftBuy, ftFirstStock, ftBackBuy, ftMoveFromWarehouse, ftGift, ftParcel] then
  EditCodeChange(MyState, FcodeCasher1, lblCasher, srlCasher, srlDataSetCasher)
  ;
  //else
  if FactorType in [ftSell, ftBackSell, ftPreFactor] then
  begin
  //  EditCodeChange(sView, CustomerCode1, lblSBName, srlStd, srlDataSetSellerBuyer);
    ShowStdPic;
  end;
end;

procedure TfrmSellBuy.FcodeCasher1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, FcodeCasher1, lblCasher);
end;

procedure TfrmSellBuy.FcodeCasher1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckValidEntry(Key);
  //if FactorType in [ftBuy, ftFirstStock, ftBackBuy, ftMoveFromWarehouse, ftGift, ftParcel] then
  EditCodeKeyPress(Key, MyState, FcodeCasher1, lblCasher, srlCasher, srlDataSetCasher)
  //else if FactorType in [ftSell, ftBackSell, ftPreFactor] then
  //EditCodeKeyPress(Key, MyState, CustomerCode1, lblSBName, srlStd, srlDataSetSellerBuyer);
end;

procedure TfrmSellBuy.BOkClick(Sender: TObject);
var
  b1fs: Integer;
begin
  inherited;
   if MessageDlg('                              '+'��� ������Ͽ'#13'�� ���� ����� ������ ������ ���� ������ ������ ���.', mtWarning, [mbYes, mbNo], 0) = mrYes then
   begin
    b1fs := Browse1FSerial.Value;
    NullAdoSPParameters(Update_Ok_Fac);
    Update_Ok_Fac.Close;
    Update_Ok_Fac.Parameters.ParamByName('@fserial').value:=Browse1FSerial.Value;
    Update_Ok_Fac.ExecProc;
    refreshPanels(b1fs, 0);
   end;
end;

end.
