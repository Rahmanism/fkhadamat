inherited frmYear: TfrmYear
  Left = 256
  Top = 191
  Caption = #1587#1575#1604' '#1605#1575#1604#1610
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 27
  inherited Panel1: TPanel
    object Label1: TLabel [0]
      Left = 480
      Top = 14
      Width = 17
      Height = 27
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 480
      Top = 50
      Width = 33
      Height = 27
      Alignment = taRightJustify
      Caption = #1593#1606#1608#1575#1606':'
    end
    object Label3: TLabel [2]
      Left = 480
      Top = 124
      Width = 60
      Height = 27
      Alignment = taRightJustify
      Caption = #1578#1575#1585#1610#1582' '#1662#1575#1610#1575#1606':'
    end
    object Label4: TLabel [3]
      Left = 480
      Top = 87
      Width = 63
      Height = 27
      Alignment = taRightJustify
      Caption = #1578#1575#1585#1610#1582' '#1588#1585#1608#1593':'
    end
    object DBCheckBox2: TDBCheckBox [4]
      Left = -346
      Top = 320
      Width = 21
      Height = 24
      DataField = 'IsActive'
      DataSource = DataSource1
      TabOrder = 8
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object DBCheckBox1: TDBCheckBox [5]
      Left = -346
      Top = 287
      Width = 21
      Height = 24
      DataField = 'IsClose'
      DataSource = DataSource1
      TabOrder = 7
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    inherited DBGrid1: TDBGrid
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 84
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtStart'
          Title.Caption = #1578#1575#1585#1610#1582' '#1588#1585#1608#1593
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtEnd'
          Title.Caption = #1578#1575#1585#1610#1582' '#1662#1575#1610#1575#1606
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsClose'
          Title.Caption = #1576#1587#1578#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsActive'
          Title.Caption = #1601#1593#1575#1604
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 408
      Top = 12
      Width = 67
      Height = 35
      TabOrder = 1
      OnKeyPress = FCode1KeyPress
    end
    object FName1: TEdit
      Left = 306
      Top = 48
      Width = 169
      Height = 35
      TabOrder = 2
    end
    object IsClose1: TCheckBox
      Left = 340
      Top = 156
      Width = 135
      Height = 24
      Caption = #1576#1587#1578#1607
      TabOrder = 5
    end
    object IsActive1: TCheckBox
      Left = 340
      Top = 186
      Width = 135
      Height = 24
      Caption = #1601#1593#1575#1604
      TabOrder = 6
    end
    object DtStart1: TMaskEdit
      Left = 408
      Top = 84
      Width = 67
      Height = 35
      EditMask = '!99/99/99;1;_'
      MaxLength = 8
      TabOrder = 3
      Text = '  /  /  '
      OnExit = DtStart1Exit
    end
    object DtEnd1: TMaskEdit
      Left = 408
      Top = 120
      Width = 67
      Height = 35
      EditMask = '!99/99/99;1;_'
      MaxLength = 8
      TabOrder = 4
      Text = '  /  /  '
      OnExit = DtEnd1Exit
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TYear_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@DtStart'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@DtEnd'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@IsClose'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@IsActive'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browse_TYear;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object Browse1DtStart: TStringField
      FieldName = 'DtStart'
      FixedChar = True
      Size = 8
    end
    object Browse1DtEnd: TStringField
      FieldName = 'DtEnd'
      FixedChar = True
      Size = 8
    end
    object Browse1IsClose: TBooleanField
      FieldName = 'IsClose'
    end
    object Browse1IsActive: TBooleanField
      FieldName = 'IsActive'
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TYear_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@DtStart'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@DtEnd'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@IsClose'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@IsActive'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
end
