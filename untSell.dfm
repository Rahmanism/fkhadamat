inherited frmSell: TfrmSell
  Left = 205
  Top = 264
  Caption = #1601#1585#1608#1588
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited DBGrid1: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Title.Caption = #1578#1575#1585#1610#1582
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Type'
          Title.Caption = #1606#1608#1593
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'CustomerCode'
          Title.Caption = #1603#1583' '#1605#1588#1578#1585#1610
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumCash'
          Title.Caption = #1580#1605#1593' '#1603#1604' '#1606#1602#1583
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumCredit'
          Title.Caption = #1580#1605#1593' '#1603#1604' '#1602#1587#1591#1610
          Width = 83
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumPrice'
          Title.Alignment = taCenter
          Title.Caption = #1580#1605#1593' '#1603#1604
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustomerName'
          Title.Caption = #1606#1575#1605' '#1605#1588#1578#1585#1610
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumCash'
          Title.Caption = #1580#1605#1593' '#1603#1604' '#1606#1602#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumCredit'
          Title.Caption = #1580#1605#1593' '#1603#1604' '#1606#1587#1610#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumPrice'
          Title.Caption = #1580#1605#1593' '#1603#1604
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCost'
          Title.Caption = #1607#1586#1610#1606#1607
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MDiscount'
          Title.Caption = #1578#1582#1601#1610#1601
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsClose'
          Title.Caption = #1576#1587#1578#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MExtra'
          Title.Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlYear'
          Title.Caption = #1587' '#1587#1575#1604
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlLoan'
          Title.Caption = #1587' '#1608#1575#1605
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlStd'
          Title.Caption = #1587' '#1591#1604#1576#1607
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlBuyCenter'
          Title.Caption = #1587' '#1605#1585#1603#1586' '#1582#1585#1610#1583
          Visible = False
        end>
    end
  end
  inherited Browse2: TADOStoredProc
    Active = True
  end
end
