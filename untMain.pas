unit untMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ToolWin, XPMan, ExtCtrls, StdCtrls, untWareHouse,
  OleCtnrs, Buttons, IdBaseComponent, IdComponent, IdSocks, Convert_Dates,
  ActnMan, ActnColorMaps, ImgList, jpeg;

type
  TfrmMain = class(TForm)
    MainMenu1: TMainMenu;
    ul1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N8: TMenuItem;
    N7: TMenuItem;
    N9: TMenuItem;
    N11: TMenuItem;
    mnuBuy: TMenuItem;
    mnuSell: TMenuItem;
    mnuBackSell: TMenuItem;
    mnuBackBuy: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    mnuMoveStock: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    XPManifest1: TXPManifest;
    StatusBar1: TStatusBar;
    mnuPreFactor: TMenuItem;
    N32: TMenuItem;
    Timer1: TTimer;
    N23: TMenuItem;
    mnuFirstStock: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    XPColorMap1: TXPColorMap;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ImageList1: TImageList;
    Image1: TImage;
    N14: TMenuItem;
    N15: TMenuItem;
    N18: TMenuItem;
    Panel1: TPanel;
    Image2: TImage;
    Bevel1: TBevel;
    imgUser: TImage;
    lblClock: TLabel;
    GroupBox1: TGroupBox;
    memMsg: TMemo;
    tmrPanel: TTimer;
    procedure N4Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mnuSellClick(Sender: TObject);
    procedure mnuBuyClick(Sender: TObject);
    procedure mnuBackBuyClick(Sender: TObject);
    procedure mnuBackSellClick(Sender: TObject);
    procedure mnuPreFactorClick(Sender: TObject);
    procedure mnuMoveStockClick(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure mnuFirstStockClick(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure N35Click(Sender: TObject);
    procedure N36Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure Label1MouseEnter(Sender: TObject);
    procedure Label1MouseLeave(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure tmrPanelTimer(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
procedure AppClose;

var
  frmMain: TfrmMain;
  PanelVisible: Boolean;

implementation

uses U_Common, untYear, unt2Panels, untGoods,
  unt1Panel,untUnit, untDm, untSellBuy, untBackBuySell, untAbout,
  untTransfer, U_DynamicReport, untPayment, untLoan, untUser,
  untBuyCenters, untStudents, untReceive, untRepFirstStock, untRepBuy,
  untRepsell, untBuy, untBackBuy, untSell, untBackSell, untPreFactor,
  untFirstStock, untMoveFromWarehouse, untHelp, untParcel, untGift;

{$R *.dfm}

procedure TfrmMain.N4Click(Sender: TObject);
begin
  frmYear := TfrmYear.Create(Self);
  frmYear.ShowModal;
end;

procedure TfrmMain.N9Click(Sender: TObject);
begin
  AppClose;
end;

procedure AppClose;
begin
  Application.Terminate;
end;

procedure TfrmMain.N6Click(Sender: TObject);
begin
  frmgoods := TfrmGoods.Create(Self);
  frmgoods.ShowModal;
end;

procedure TfrmMain.N8Click(Sender: TObject);
begin
  // FormShowDM(frmUnit,0,false);
//   RegisterClass(tfrmUnit);
//   FormShow2('frmUnit','frmMain',1,true);
  frmUnit := TfrmUnit.Create(Self);
  frmUnit.ShowModal;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  AddMyComponentToTable(Sender);
  CheckMyPermission(Sender);
  StatusBar1.Panels[0].Text := CurrDate;
  Self.WindowState := wsMaximized;

  DateForm.Show;
end;

procedure TfrmMain.mnuSellClick(Sender: TObject);
begin
  FactorType := ftSell;
  frmSell := TfrmSell.Create(Self);
  frmSell.ShowModal;
end;

procedure TfrmMain.mnuBuyClick(Sender: TObject);
begin
  FactorType := ftBuy;
  frmBuy := TfrmBuy.Create(Self);
  frmBuy.ShowModal;
end;

procedure TfrmMain.mnuBackBuyClick(Sender: TObject);
begin
  FactorType := ftBackBuy;
  frmBackBuy := TfrmBackBuy.Create(Self);
  frmBackBuy.ShowModal;
end;

procedure TfrmMain.mnuBackSellClick(Sender: TObject);
begin
  FactorType := ftBackSell;
  frmBackSell := TfrmBackSell.Create(Self);
  frmBackSell.ShowModal;
end;

procedure TfrmMain.mnuPreFactorClick(Sender: TObject);
begin
  FactorType := ftPreFactor;
  frmPreFactor := TfrmPreFactor.Create(Self);
  frmPreFactor.ShowModal;
end;

procedure TfrmMain.mnuMoveStockClick(Sender: TObject);
begin
  FactorType := ftMoveFromWarehouse;
  frmMoveFromWarehouse := TfrmMoveFromWarehouse.Create(Self);
  frmMoveFromWarehouse.ShowModal;
end;

procedure TfrmMain.N32Click(Sender: TObject);
begin
  CurDate := CurrDate;
  ShowDynamicQueryForm(6);
//  F_DynamicReport := TF_DynamicReport.Create(Self);
//  F_DynamicReport.ShowModal;
end;

procedure TfrmMain.N20Click(Sender: TObject);
begin
  frmPayment := TfrmPayment.Create(Self);
  frmPayment.ShowModal;
end;

procedure TfrmMain.N21Click(Sender: TObject);
begin
  frmReceive := TfrmReceive.Create(Self);
  frmReceive.ShowModal;
end;

procedure TfrmMain.N22Click(Sender: TObject);
begin
  frmLoan := TfrmLoan.Create(Self);
  frmLoan.ShowModal;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  t: String;
begin
  Screen.Cursors[1] := LoadCursor(0, IDC_HAND);
  Caption := Application.Title;
  frmDm.Browse_per.Close;
  frmDm.Browse_per.Parameters.ParamByName('@srluser').Value:=srlUser;
  frmDm.Browse_per.Open;
  frmDm.UserPermit.Close;
  frmDm.UserPermit.Parameters.ParamByName('@srluser').Value := srlUser;
  frmDm.UserPermit.Open;
  if frmDm.UserPermit.RecordCount > 0 then
  begin
    srlYear := frmDm.UserPermit.FieldValues['YearActive'];
    srlWarehouse := frmDm.UserPermit.FieldValues['HouseActive'];
    YearClose:=frmDm.UserPermit.FieldValues['Yearclose'];
    with StatusBar1.Panels do
    begin
      Items[1].Text := CurrDate;
      Items[2].Text := '�� ��� ����: ' + IntToStr(frmDm.UserPermit.FieldValues['YearCode']);
      Items[3].Text := frmDm.UserPermit.FieldValues['YearName'];
      Items[4].Text := '�� �����: ' + IntToStr(frmDm.UserPermit.FieldValues['HouseCode']);
      Items[5].Text := frmDm.UserPermit.FieldValues['HouseName'];
      Items[6].Text := frmDm.UserPermit.FieldValues['FName'];
    end;
    memMsg.Text := frmDm.UserPermit.FieldValues['Message'];
    t := USER_PIC_FOLDER_NAME + '\' + IntToStr(frmDm.UserPermit.FieldValues['FCode']) + '.jpg';
    if FileExists(t) then
      imgUser.Picture.LoadFromFile(t);
  end;

  PanelVisible := True;
end;

procedure TfrmMain.N25Click(Sender: TObject);
begin
  frmUser := TfrmUser.Create(Self);
  frmUser.ShowModal;
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
 var
  h, m, s, ms: Word ;
begin
  StatusBar1.Panels.Items[0].Text := TimeToStr(Time);
  DecodeTime(Time, h, m, s, ms);
  Clock(longint(h),longint( m), longint(s), 28, 37, 45, lblClock);
end;

procedure TfrmMain.N5Click(Sender: TObject);
begin
  frmWareHouse := TfrmWareHouse.Create(Self);
  frmWarehouse.ShowModal;
end;

procedure TfrmMain.N29Click(Sender: TObject);
begin
  MyFileRun('calc.exe');
end;

procedure TfrmMain.N11Click(Sender: TObject);
begin
  frmBuyCenters := TfrmBuyCenters.Create(Self);
  frmBuyCenters.ShowModal;
end;

procedure TfrmMain.N10Click(Sender: TObject);
begin
  frmStudents := TfrmStudents.Create(Self);
  frmStudents.ShowModal;
end;

procedure TfrmMain.mnuFirstStockClick(Sender: TObject);
begin
  FactorType := ftFirstStock;
  frmFirstStock := TfrmFirstStock.Create(Self);
  frmFirstStock.ShowModal;
end;

procedure TfrmMain.N34Click(Sender: TObject);
begin
  frmRepFirstStock:=TfrmRepFirstStock.Create(Self);
  frmRepFirstStock.Show;
end;

procedure TfrmMain.N35Click(Sender: TObject);
begin
  frmRepBuy:=TfrmRepBuy.Create(Self);
  frmRepBuy.Show;
end;

procedure TfrmMain.N36Click(Sender: TObject);
begin
  frmRepsell:=TfrmRepsell.Create(Self);
  frmRepsell.Show;
end;

procedure TfrmMain.N28Click(Sender: TObject);
begin
 DateForm:=TDateForm.Create(Application);
  DateForm.ShowModal;
end;

procedure TfrmMain.N12Click(Sender: TObject);
begin
  AboutBox := TAboutBox.Create(Self);
  AboutBox.ShowModal;
end;

procedure TfrmMain.N13Click(Sender: TObject);
begin
  frmHelp := TfrmHelp.Create(Self);
  frmHelp.ShowModal;
end;

procedure TfrmMain.Label1MouseEnter(Sender: TObject);
begin
    CoolBar1.Visible:=True;
end;

procedure TfrmMain.Label1MouseLeave(Sender: TObject);
begin
    CoolBar1.Visible:=False
end;

procedure TfrmMain.Label1Click(Sender: TObject);
begin
      CoolBar1.Visible:=False
end;

procedure TfrmMain.tmrPanelTimer(Sender: TObject);
const
  Offset = 4;
begin
  if PanelVisible then
  begin
    Panel1.Width := Panel1.Width - Offset;
    if Panel1.Width <= 20 then
    begin
      Panel1.Width := 20;
      tmrPanel.Enabled := False;
      PanelVisible := False;
    end
  end
  else
  begin
    Panel1.Width := Panel1.Width + Offset;
    if Panel1.Width >= 185 then
    begin
      Panel1.Width := 185;
      tmrPanel.Enabled := False;
      PanelVisible := True;
    end
  end;
end;

procedure TfrmMain.Panel1Click(Sender: TObject);
begin
  tmrPanel.Enabled := True;
end;

procedure TfrmMain.N15Click(Sender: TObject);
begin
  FactorType := ftParcel;
  frmParcel := TfrmParcel.Create(Self);
  frmParcel.ShowModal;
end;

procedure TfrmMain.N14Click(Sender: TObject);
begin
  FactorType := ftGift;
  frmGift := TfrmGift.Create(Self);
  frmGift.ShowModal;
end;

end.
