inherited frmRecPay: TfrmRecPay
  Left = 261
  Top = 193
  Caption = #1583#1585#1610#1575#1601#1578' / '#1662#1585#1583#1575#1582#1578
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 27
  inherited Panel1: TPanel
    object Label1: TLabel [0]
      Left = 602
      Top = 46
      Width = 17
      Height = 27
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 602
      Top = 131
      Width = 25
      Height = 27
      Alignment = taRightJustify
      Caption = #1605#1576#1604#1594':'
    end
    object Label3: TLabel [2]
      Left = 282
      Top = 83
      Width = 30
      Height = 27
      Alignment = taRightJustify
      Caption = #1578#1575#1585#1610#1582':'
    end
    object Label5: TLabel [3]
      Left = 602
      Top = 90
      Width = 34
      Height = 27
      Alignment = taRightJustify
      Caption = #1601#1575#1603#1578#1608#1585':'
    end
    object lblFactor: TLabel [4]
      Left = 352
      Top = 88
      Width = 66
      Height = 27
      Caption = '...'
    end
    object Label6: TLabel [5]
      Left = 282
      Top = 128
      Width = 20
      Height = 27
      Alignment = taRightJustify
      Caption = #1606#1608#1593':'
    end
    object lblType: TLabel [6]
      Left = 97
      Top = 133
      Width = 9
      Height = 27
      Caption = '...'
    end
    object SpeedButton1: TSpeedButton [7]
      Left = 456
      Top = 88
      Width = 23
      Height = 22
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton [8]
      Left = 128
      Top = 136
      Width = 23
      Height = 22
      OnClick = SpeedButton2Click
    end
    inherited DBGrid1: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Type'
          Title.Caption = #1587' '#1606#1608#1593
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlStd'
          Title.Caption = #1587' '#1591#1604#1576#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MPayment'
          Title.Caption = #1662#1585#1583#1575#1582#1578
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MRecieve'
          Title.Caption = #1583#1585#1610#1575#1601#1578
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Title.Caption = #1578#1575#1585#1610#1582
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlFactor'
          Title.Caption = #1587' '#1601#1575#1603#1578#1608#1585
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlYear'
          Title.Caption = #1587' '#1587#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdCode'
          Title.Caption = #1603#1583' '#1591#1604#1576#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FactorCode'
          Title.Caption = #1603#1583' '#1601#1575#1603#1578#1608#1585
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdName'
          Title.Caption = #1606#1575#1605' '#1591#1604#1576#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TypeCode'
          Title.Caption = #1603#1583' '#1606#1608#1593
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TypeName'
          Title.Caption = #1606#1608#1593
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TypeSerial'
          Title.Caption = #1587' '#1606#1608#1593
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FactorSerial'
          Title.Caption = #1587' '#1601#1575#1603#1578#1608#1585
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FactorName'
          Title.Caption = #1593#1606#1608#1575#1606' '#1601#1575#1603#1578#1608#1585
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdSerial'
          Title.Caption = #1587' '#1591#1604#1576#1607
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 488
      Top = 38
      Width = 101
      Height = 35
      TabOrder = 1
    end
    object MRecieve1: TEdit
      Left = 347
      Top = 128
      Width = 242
      Height = 35
      TabOrder = 4
      Visible = False
    end
    object DtDate1: TMaskEdit
      Left = 159
      Top = 80
      Width = 118
      Height = 35
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 5
      Text = '  /  /  '
    end
    object FactorCode1: TEdit
      Left = 488
      Top = 82
      Width = 101
      Height = 35
      TabOrder = 2
      OnChange = FactorCode1Change
      OnExit = FactorCode1Exit
      OnKeyPress = FactorCode1KeyPress
    end
    object TypeCode1: TEdit
      Left = 160
      Top = 126
      Width = 117
      Height = 35
      TabOrder = 6
      OnChange = TypeCode1Change
      OnExit = TypeCode1Exit
      OnKeyPress = TypeCode1KeyPress
    end
    object MPayment1: TEdit
      Left = 347
      Top = 128
      Width = 242
      Height = 35
      TabOrder = 3
      Visible = False
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TPayment_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Type_2'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@MPayment_4'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MRecieve_5'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@DtDate_6'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@srlFactor_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browse_TPayment;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@PayOrRec'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 71
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1Type: TIntegerField
      FieldName = 'Type'
    end
    object Browse1srlStd: TIntegerField
      FieldName = 'srlStd'
    end
    object Browse1MPayment: TBCDField
      FieldName = 'MPayment'
      Precision = 19
    end
    object Browse1MRecieve: TBCDField
      FieldName = 'MRecieve'
      Precision = 19
    end
    object Browse1DtDate: TStringField
      FieldName = 'DtDate'
      FixedChar = True
      Size = 8
    end
    object Browse1srlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object Browse1srlYear: TIntegerField
      FieldName = 'srlYear'
    end
    object Browse1StdCode: TIntegerField
      FieldName = 'StdCode'
    end
    object Browse1FactorCode: TIntegerField
      FieldName = 'FactorCode'
    end
    object Browse1StdName: TWideStringField
      FieldName = 'StdName'
      Size = 100
    end
    object Browse1TypeCode: TIntegerField
      FieldName = 'TypeCode'
    end
    object Browse1TypeName: TWideStringField
      FieldName = 'TypeName'
      Size = 50
    end
    object Browse1TypeSerial: TAutoIncField
      FieldName = 'TypeSerial'
      ReadOnly = True
    end
    object Browse1FactorSerial: TAutoIncField
      FieldName = 'FactorSerial'
      ReadOnly = True
    end
    object Browse1FactorName: TWideStringField
      FieldName = 'FactorName'
      Size = 100
    end
    object Browse1StdSerial: TAutoIncField
      FieldName = 'StdSerial'
      ReadOnly = True
    end
  end
end
