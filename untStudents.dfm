inherited frmStudents: TfrmStudents
  Left = 288
  Top = 162
  Caption = #1591#1604#1575#1576
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    object Label1: TLabel [0]
      Left = 606
      Top = 30
      Width = 15
      Height = 13
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 406
      Top = 30
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = #1606#1575#1605':'
    end
    object Label3: TLabel [2]
      Left = 206
      Top = 30
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = #1606#1575#1605' '#1582#1575#1606#1608#1575#1583#1711#1610':'
    end
    object Label4: TLabel [3]
      Left = 206
      Top = 62
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = #1588#1605#1575#1585#1607' '#1605#1604#1610':'
    end
    object Label5: TLabel [4]
      Left = 406
      Top = 62
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = #1588' '#1588#1606#1575#1587#1606#1575#1605#1607':'
    end
    object Label6: TLabel [5]
      Left = 606
      Top = 62
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = #1606#1575#1605' '#1662#1583#1585':'
    end
    object Label7: TLabel [6]
      Left = 206
      Top = 126
      Width = 29
      Height = 13
      Alignment = taRightJustify
      Caption = #1601#1603#1587':'
    end
    object Label8: TLabel [7]
      Left = 406
      Top = 126
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = #1607#1605#1585#1575#1607':'
    end
    object Label9: TLabel [8]
      Left = 606
      Top = 126
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = #1578#1604#1601#1606':'
    end
    object Label10: TLabel [9]
      Left = 206
      Top = 158
      Width = 29
      Height = 13
      Alignment = taRightJustify
      Caption = #1575#1593#1578#1576#1575#1585':'
    end
    object Label11: TLabel [10]
      Left = 406
      Top = 158
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = #1587#1575#1610#1578':'
    end
    object Label12: TLabel [11]
      Left = 606
      Top = 158
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = #1575#1610#8204#1605#1610#1604':'
    end
    object Label13: TLabel [12]
      Left = 606
      Top = 94
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = #1606#1588#1575#1606#1610':'
    end
    object FirstName1: TEdit
      Left = 280
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object LastName1: TEdit
      Left = 80
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object Address1: TEdit
      Left = 80
      Top = 88
      Width = 521
      Height = 21
      TabOrder = 7
    end
    object Tel1: TEdit
      Left = 480
      Top = 120
      Width = 121
      Height = 21
      TabOrder = 8
    end
    object Mobile1: TEdit
      Left = 280
      Top = 120
      Width = 121
      Height = 21
      TabOrder = 9
    end
    object Fax1: TEdit
      Left = 80
      Top = 120
      Width = 121
      Height = 21
      TabOrder = 10
    end
    object Email1: TEdit
      Left = 480
      Top = 152
      Width = 121
      Height = 21
      TabOrder = 11
    end
    object Site1: TEdit
      Left = 280
      Top = 152
      Width = 121
      Height = 21
      TabOrder = 12
    end
    object Credit1: TEdit
      Left = 80
      Top = 152
      Width = 121
      Height = 21
      TabOrder = 13
    end
    object FatherName1: TEdit
      Left = 480
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object Sn1: TEdit
      Left = 280
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 5
    end
    object NationalNo1: TEdit
      Left = 80
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 6
    end
    object Blocked1: TCheckBox
      Left = 544
      Top = 184
      Width = 57
      Height = 17
      Caption = #1605#1587#1583#1608#1583
      TabOrder = 14
    end
    object FCode1: TEdit
      Left = 480
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 1
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TStudents_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@FirstName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end
      item
        Name = '@LastName_5'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end
      item
        Name = '@Address_6'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Tel_7'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Mobile_8'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 25
        Value = Null
      end
      item
        Name = '@Fax_9'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Email_10'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Site_11'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Credit_12'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@FatherName_13'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sn_14'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NationalNo_15'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@Blocked_16'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browes_TStudents;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object Browse1FirstName: TWideStringField
      FieldName = 'FirstName'
      Size = 30
    end
    object Browse1LastName: TWideStringField
      FieldName = 'LastName'
      Size = 30
    end
    object Browse1Address: TWideStringField
      FieldName = 'Address'
      Size = 100
    end
    object Browse1Tel: TWideStringField
      FieldName = 'Tel'
    end
    object Browse1Mobile: TWideStringField
      FieldName = 'Mobile'
      Size = 25
    end
    object Browse1Fax: TWideStringField
      FieldName = 'Fax'
    end
    object Browse1Email: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object Browse1Site: TWideStringField
      FieldName = 'Site'
      Size = 100
    end
    object Browse1Credit: TWideStringField
      FieldName = 'Credit'
      Size = 50
    end
    object Browse1FatherName: TWideStringField
      FieldName = 'FatherName'
      Size = 30
    end
    object Browse1Sn: TWideStringField
      FieldName = 'Sn'
    end
    object Browse1NationalNo: TWideStringField
      FieldName = 'NationalNo'
      Size = 10
    end
    object Browse1Blocked: TBooleanField
      FieldName = 'Blocked'
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TStudents_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@FirstName_5'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end
      item
        Name = '@LastName_6'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end
      item
        Name = '@Address_7'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Tel_8'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Mobile_9'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 25
        Value = Null
      end
      item
        Name = '@Fax_10'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@Email_11'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Site_12'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@Credit_13'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@FatherName_14'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 30
        Value = Null
      end
      item
        Name = '@Sn_15'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NationalNo_16'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 10
        Value = Null
      end
      item
        Name = '@Blocked_17'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
end
