unit untGoods;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt2Panels, DB, ADODB, Buttons, ComCtrls, Grids, DBGrids,
  ExtCtrls, StdCtrls, DBCtrls;

type
  TfrmGoods = class(Tfrm2Panels)
    Fcode1: TEdit;
    Fname1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    FCode2: TEdit;
    FName2: TEdit;
    Stock2: TEdit;
    MaxStock2: TEdit;
    UnitCode2: TEdit;
    MinStock2: TEdit;
    Blocked2: TCheckBox;
    Label3: TLabel;
    lblUnit: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBCheckBox1: TDBCheckBox;
    Browse2FSerial: TIntegerField;
    Browse2FCode: TIntegerField;
    Browse2FName: TWideStringField;
    Browse2Stock: TIntegerField;
    Browse2srlCategory: TIntegerField;
    Browse2MaxStock: TIntegerField;
    Browse2MinStock: TIntegerField;
    Browse2srlUnit: TIntegerField;
    Browse2Blocked: TBooleanField;
    Browse2UnitName: TWideStringField;
    Browse2UnitCode: TIntegerField;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure UnitCode2Change(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure UnitCode2KeyPress(Sender: TObject; var Key: Char);
    procedure UnitCode2Exit(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure DataSource2DataChange(Sender: TObject; Field: TField);
  private
    procedure refreshPanels(b1fs, b2fs: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGoods: TfrmGoods;

implementation

uses U_Common, untDm, Math;

{$R *.dfm}

procedure TfrmGoods.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  srlCategory := Browse1FSerial.Value;
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlCategory').Value := srlCategory;
  Browse2.Open;
end;

procedure TfrmGoods.UnitCode2Change(Sender: TObject);
var
  i:Integer;
begin
  inherited;
  EditCodeChange(sAdd, UnitCode2, lblUnit, srlUnit, 16);
end;

procedure TfrmGoods.bSaveClick(Sender: TObject);
var
  i:Integer;
begin
  inherited;
  i := Browse1FSerial.Value;
  if panelfocus = 'Panel1' then
  begin
    if MyState = sAdd then
    begin
      Insert1.Parameters.ParamByName('@Fcode_2').Value := Fcode1.Text;
      Insert1.Parameters.ParamByName('@Fname_3').Value := Fname1.Text;
      Insert1.ExecProc;
      Browse1.Close;
      Browse1.Open;
      Browse1.Locate('fserial', Insert1.Parameters.ParamByName('@RETURN_VALUE').Value, []);
    end
    else if MyState = sEdit then
    begin
      i := srlCategory;
      Update1.Parameters.ParamByName('@FSerial_1').Value := srlCategory;
      Update1.Parameters.ParamByName('@FCode_2').Value := Fcode1.Text;
      Update1.Parameters.ParamByName('@FName_3').Value := Fname1.Text;
      Update1.ExecProc;
      Browse1.Close;
      Browse1.Open;
      Browse1.Locate('fserial', i, []);
    end;
  end
  else
  if panelfocus = 'Panel2' then
  begin
    if MyState = sAdd then
    begin
      Insert2.Parameters.ParamByName('@Fcode_2').Value := Fcode2.Text;
      Insert2.Parameters.ParamByName('@Fname_3').Value := Fname2.Text;
      Insert2.Parameters.ParamByName('@Stock_4').Value := Stock2.Text;
      Insert2.Parameters.ParamByName('@srlcategory_5').Value := srlcategory;
      Insert2.Parameters.ParamByName('@Maxstock_6').Value := MaxStock2.Text;
      Insert2.Parameters.ParamByName('@MinStock_7').Value := MinStock2.Text;
      if Trim(UnitCode2.Text) <> '' then
        Insert2.Parameters.ParamByName('@srlUnit_8').Value := srlUnit
      else
        Insert2.Parameters.ParamByName('@srlUnit_8').Value := null;
      Insert2.Parameters.ParamByName('@Blocked_9').Value := Blocked2.Checked;
      Insert2.ExecProc;
      refreshPanels(Browse1FSerial.Value, Insert2.Parameters.ParamByName('@RETURN_VALUE').Value);
    end
    else if MyState = sEdit then
    begin
      i := srltemp;
      NullAdoSPParameters(Update2);
      Update2.Parameters.ParamByName('@Fserial_1').Value := srltemp;
      Update2.Parameters.ParamByName('@Fcode_2').Value := Fcode2.Text;
      Update2.Parameters.ParamByName('@Fname_3').Value := Fname2.Text;
      Update2.Parameters.ParamByName('@Stock_5').Value := Stock2.Text;
      Update2.Parameters.ParamByName('@srlcategory_6').Value := srlcategory;
      Update2.Parameters.ParamByName('@Maxstock_7').Value := MaxStock2.Text;
      Update2.Parameters.ParamByName('@MinStock_8').Value := MinStock2.Text;
      if Trim(UnitCode2.Text) <> ''  then
        Update2.Parameters.ParamByName('@srlUnit_9').Value := srlUnit;
      Update2.Parameters.ParamByName('@Blocked_10').Value := Blocked2.Checked;
      Update2.ExecProc;
      refreshPanels(Browse1FSerial.Value, i);
    end;
  end; // end if.
  MyState := sView;
end;

procedure TfrmGoods.bAddClick(Sender: TObject);
begin
  inherited;
  if panelfocus='Panel1' then
   Fcode1.Text:=GetMaxCodeRecord(17) ;
  if panelfocus='Panel2' then
  FCode2.Text:=GetMaxCodeRecord(18) ;
end;

procedure TfrmGoods.UnitCode2KeyPress(Sender: TObject; var Key: Char);
var
  i:Integer;
begin
  inherited;
  EditCodeKeyPress(key, MyState, UnitCode2, lblUnit, srlUnit, 16);
end;

procedure TfrmGoods.UnitCode2Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, UnitCode2, lblUnit);
end;

procedure TfrmGoods.bDelClick(Sender: TObject);
begin
  inherited;
  if panelfocus='Panel1'  then
    DeleteRecord1(17, Browse1FSerial.Value, Browse1)
  else if panelfocus='Panel2'  then
    DeleteRecord1(18, Browse2FSerial.Value, Browse2);
end;

procedure TfrmGoods.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  CheckBoxInDBGrid(DBCheckBox1, Sender, Rect, DataCol, Column, State);
end;

procedure TfrmGoods.refreshPanels(b1fs, b2fs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
  Browse2.Locate('FSerial', b2fs, []);
end;

procedure TfrmGoods.FormShow(Sender: TObject);
begin
  inherited;
  refreshPanels(0, 0);
end;

procedure TfrmGoods.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanels(0, 0);
end;

procedure TfrmGoods.DataSource2DataChange(Sender: TObject; Field: TField);
begin
  inherited;
    srltemp:=Browse2FSerial.Value;
end;

end.
