inherited frmUnit: TfrmUnit
  Left = 217
  Top = 135
  Caption = #1608#1575#1581#1583#1607#1575
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 27
  inherited Panel1: TPanel
    object Label1: TLabel [0]
      Left = 556
      Top = 75
      Width = 17
      Height = 27
      Alignment = taRightJustify
      Caption = #1603#1583':'
    end
    object Label2: TLabel [1]
      Left = 556
      Top = 122
      Width = 33
      Height = 27
      Alignment = taRightJustify
      Caption = #1593#1606#1608#1575#1606':'
    end
    inherited DBGrid1: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 424
      Top = 68
      Width = 117
      Height = 35
      TabOrder = 1
      OnKeyPress = FCode1KeyPress
    end
    object FName1: TEdit
      Left = 290
      Top = 118
      Width = 251
      Height = 35
      TabOrder = 2
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TUnits_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browes_TUnit;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TUnits_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
end
