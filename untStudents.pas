unit untStudents;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt1Panel, DB, ADODB, ComCtrls, Buttons, Grids, DBGrids,
  ExtCtrls, StdCtrls, U_Common, untDm;

type
  TfrmStudents = class(Tfrm1Panel)
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1FirstName: TWideStringField;
    Browse1LastName: TWideStringField;
    Browse1Address: TWideStringField;
    Browse1Tel: TWideStringField;
    Browse1Mobile: TWideStringField;
    Browse1Fax: TWideStringField;
    Browse1Email: TWideStringField;
    Browse1Site: TWideStringField;
    Browse1Credit: TWideStringField;
    Browse1FatherName: TWideStringField;
    Browse1Sn: TWideStringField;
    Browse1NationalNo: TWideStringField;
    Browse1Blocked: TBooleanField;
    FirstName1: TEdit;
    LastName1: TEdit;
    Address1: TEdit;
    Tel1: TEdit;
    Mobile1: TEdit;
    Fax1: TEdit;
    Email1: TEdit;
    Site1: TEdit;
    Credit1: TEdit;
    FatherName1: TEdit;
    Sn1: TEdit;
    NationalNo1: TEdit;
    Blocked1: TCheckBox;
    FCode1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    procedure bAddClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
  private
    procedure refreshPanel(bfs: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStudents: TfrmStudents;

implementation

{$R *.dfm}

procedure TfrmStudents.bAddClick(Sender: TObject);
begin
  inherited;
  FCode1.Text := GetMaxCodeRecord(12, srlYear, -1, -1, -1);
end;

procedure TfrmStudents.refreshPanel(bfs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', bfs, []);
end;

procedure TfrmStudents.bSaveClick(Sender: TObject);
begin
  inherited;
  Insert1.Parameters.ParamByName('@FCode_2').Value := FCode1.Text;
  Insert1.Parameters.ParamByName('@FName_3').Value := FirstName1.Text + ' ' + LastName1.Text;
  Insert1.Parameters.ParamByName('@FirstName_4').Value := FirstName1.Text;
  Insert1.Parameters.ParamByName('@LastName_5').Value := LastName1.Text;
  Insert1.Parameters.ParamByName('@Address_6').Value := Address1.Text;
  Insert1.Parameters.ParamByName('@Tel_7').Value := Tel1.Text;
  Insert1.Parameters.ParamByName('@Mobile_8').Value := Mobile1.Text;
  Insert1.Parameters.ParamByName('@Fax_9').Value := Fax1.Text;
  Insert1.Parameters.ParamByName('@Email_10').Value := Email1.Text;
  Insert1.Parameters.ParamByName('@Site_11').Value := Site1.Text;
  Insert1.Parameters.ParamByName('@Credit_12').Value := Credit1.Text;
  Insert1.Parameters.ParamByName('@FatherName_13').Value := FatherName1.Text;
  Insert1.Parameters.ParamByName('@Sn_14').Value := Sn1.Text;
  Insert1.Parameters.ParamByName('@NationalNo_15').Value := NationalNo1.Text;
  Insert1.Parameters.ParamByName('@Blocked_16').Value := Blocked1.Checked;
  Insert1.ExecProc;
  refreshPanel(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value);
end;

end.
