unit untRepsell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untParentReport, DB, ADODB, StdCtrls, Buttons, ExtCtrls, Grids,
  DBGrids, ComCtrls;

type
  TfrmRepsell = class(TfrmParentReport)
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRepsell: TfrmRepsell;

implementation

uses U_Common, untDm;

{$R *.dfm}

procedure TfrmRepsell.BitBtn1Click(Sender: TObject);
begin
  inherited;
    Browse1.Close;
    Browse1.Parameters.ParamByName('@srl_year1').Value:=srlYearRep1;
    Browse1.Parameters.ParamByName('@srl_year2').Value:=srlYearRep2;
    Browse1.Parameters.ParamByName('@num_rep').Value:=1;
    Browse1.Open;


end;

procedure TfrmRepsell.FormShow(Sender: TObject);
begin
  inherited;
    Browse1.Close;
    Browse1.Parameters.ParamByName('@srl_year1').Value:=srlYear;
    Browse1.Parameters.ParamByName('@srl_year2').Value:=srlYear;
    Browse1.Parameters.ParamByName('@num_rep').Value:=1;
    Browse1.Open;

end;

end.
