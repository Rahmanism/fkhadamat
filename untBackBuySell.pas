unit untBackBuySell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt2Panels, DB, ADODB, Buttons, ComCtrls, Grids, DBGrids,
  ExtCtrls, StdCtrls, Mask, untDm, U_Common, Math;

type
  TfrmBackBuySell = class(Tfrm2Panels)
    FCode1: TEdit;
    FName1: TEdit;
    MExtra1: TEdit;
    MCost1: TEdit;
    MDiscount1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    GoodCode2: TEdit;
    MCash2: TEdit;
    MPrice2: TEdit;
    NCount2: TEdit;
    Label3: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    lblGood: TLabel;
    Label10: TLabel;
    MCredit2: TEdit;
    IsClose1: TCheckBox;
    DtDate1: TMaskEdit;
    dtsGoodsSource: TDataSource;
    GoodsBrowseSource: TADOStoredProc;
    Browse2FSerial: TAutoIncField;
    Browse2srlGood: TIntegerField;
    Browse2NCount: TIntegerField;
    Browse2NRemain: TIntegerField;
    Browse2srlSource: TIntegerField;
    Browse2srlWarehouse: TIntegerField;
    Browse2srlFactor: TIntegerField;
    Browse2MCash: TBCDField;
    Browse2MCredit: TBCDField;
    Browse2MPrice: TBCDField;
    Browse2FName: TWideStringField;
    Browse2WarehouseName: TWideStringField;
    Browse2Unit: TWideStringField;
    Browse2GoodCode: TIntegerField;
    Browse2WarehouseCode: TIntegerField;
    lblFName: TLabel;
    lblStd: TLabel;
    StdCode1: TEdit;
    lblSBName: TLabel;
    GoodsBrowseSourceFSerial: TAutoIncField;
    GoodsBrowseSourcesrlGood: TIntegerField;
    GoodsBrowseSourceNCount: TIntegerField;
    GoodsBrowseSourceNRemain: TIntegerField;
    GoodsBrowseSourcesrlSource: TIntegerField;
    GoodsBrowseSourcesrlWarehouse: TIntegerField;
    GoodsBrowseSourcesrlFactor: TIntegerField;
    GoodsBrowseSourceMCash: TBCDField;
    GoodsBrowseSourceMCredit: TBCDField;
    GoodsBrowseSourceMPrice: TBCDField;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1DtDate: TStringField;
    Browse1MExtra: TBCDField;
    Browse1Type: TSmallintField;
    Browse1MCost: TBCDField;
    Browse1MDiscount: TBCDField;
    Browse1IsClose: TBooleanField;
    Browse1srlYear: TIntegerField;
    Browse1srlLoan: TIntegerField;
    Browse1srlStd: TIntegerField;
    Browse1srlBuyCenter: TIntegerField;
    Browse1BuyCenterCode: TIntegerField;
    Browse1StdCode: TIntegerField;
    imgStd: TImage;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure GoodCode2Change(Sender: TObject);
    procedure GoodCode2Exit(Sender: TObject);
    procedure GoodCode2KeyPress(Sender: TObject; var Key: Char);
    procedure bSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure StdCode1Change(Sender: TObject);
    procedure StdCode1Exit(Sender: TObject);
    procedure StdCode1KeyPress(Sender: TObject; var Key: Char);
    procedure bDelClick(Sender: TObject);
  private
    procedure refreshPanels(b1fs, b2fs: Integer);
    procedure ShowStdPic;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBackBuySell: TfrmBackBuySell;
  srlDataSet1, srlDataSet2, srlDataSet3: Integer;

implementation

uses untSellBuy;

{$R *.dfm}

procedure TfrmBackBuySell.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  srlFactor := Browse1FSerial.Value;
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlFactor').Value := Browse1FSerial.Value;
  Browse2.Open;
end;                                        

procedure TfrmBackBuySell.GoodCode2Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sAdd, GoodCode2, lblGood, srlSource, srlDataSet1)
end;

procedure TfrmBackBuySell.GoodCode2Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, GoodCode2, lblGood);
end;

procedure TfrmBackBuySell.GoodCode2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, GoodCode2, lblGood, srlSource, srlDataSet1);
end;

procedure TfrmBackBuySell.bSaveClick(Sender: TObject);
var
  i, t, nvalue, nremain, newnremain, grc, sgrc, b1fs, b2fs:Integer;
  srlDetFac4Remain: Integer;
begin
  IfNullThenZero(Browse1.FieldValues['FSerial'], b1fs);
  IfNullThenZero(Browse2.FieldValues['FSerial'], b2fs);
  inherited;
  if panelfocus = 'Panel1' then
  begin
    if MyState = sAdd then
    begin
      Insert1.Parameters.ParamByName('@Fcode_2').Value := Fcode1.Text;
      Insert1.Parameters.ParamByName('@FName_3').Value := Fname1.Text;
      Insert1.Parameters.ParamByName('@DtDate_4').Value := DtDate1.Text;
      Insert1.Parameters.ParamByName('@MExtra_5').Value := MExtra1.Text;
      Insert1.Parameters.ParamByName('@Type_6').Value := Ord(FactorType);
      Insert1.Parameters.ParamByName('@MCost_7').Value := MCost1.Text;
      Insert1.Parameters.ParamByName('@MDiscount_8').Value := MDiscount1.Text;
      Insert1.Parameters.ParamByName('@IsClose_9').Value := IsClose1.Checked;
      Insert1.Parameters.ParamByName('@srlYear_10').Value := srlYear;
      Insert1.Parameters.ParamByName('@srlLoan_11').Value := 0;
      if FactorType = ftBackBuy then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := Null;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := srlBuyCenter;
      end
      else if FactorType = ftBackSell then
      begin
        Insert1.Parameters.ParamByName('@srlStd_12').Value := srlStd;
        Insert1.Parameters.ParamByName('@srlBuyCenter_13').Value := Null;
      end;
      Insert1.ExecProc;
      refreshPanels(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value, 0);
    end
    else if MyState = sEdit then
    begin
      Update1.Parameters.ParamByName('@FSerial_1').Value := b1fs;
      Update1.Parameters.ParamByName('@FCode_3').Value := Fcode1.Text;
      Update1.Parameters.ParamByName('@FName_4').Value := Fname1.Text;
      Update1.Parameters.ParamByName('@srlBuyer_5').Value := srlStd;
      Update1.Parameters.ParamByName('@DtDate_7').Value := DtDate1.Text;
      Update1.Parameters.ParamByName('@MExtra_8').Value := MExtra1.Text;
      Update1.Parameters.ParamByName('@MCost_10').Value := MCost1.Text;
      Update1.Parameters.ParamByName('@MDiscount_11').Value := MDiscount1.Text;
      Update1.Parameters.ParamByName('@IsClose_12').Value := IsClose1.Checked;
      Update1.ExecProc;
      refreshPanels(b1fs, 0);
    end;
  end
  else
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  if panelfocus = 'Panel2' then
  begin
    if MyState = sAdd then
    begin
      GoodsBrowseSource.Close;
      GoodsBrowseSource.Parameters.ParamByName('@srlSource').Value := srlSource;
      GoodsBrowseSource.Open;
      sgrc := GoodsBrowseSource.FieldValues['NRemain'];
      nvalue := StrToInt(NCount2.Text);
      if sgrc < nvalue then
      begin
        grc := MessageDlg('����� ������� ��� �� �� ���� ���.'#13'��� �� ����� ���� ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
        if grc = mrCancel then
          exit
        else
        begin
          nvalue := sgrc;
          NCount2.Text := IntToStr(nvalue);
        end;
      end;
      Insert2.Parameters.ParamByName('@srlGood_2').Value := GoodCode2.Text;
      Insert2.Parameters.ParamByName('@NCount_3').Value := NCount2.Text;
      Insert2.Parameters.ParamByName('@NRemain_4').Value := 0;
      Insert2.Parameters.ParamByName('@srlSource_5').Value := srlSource;
      Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
      Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
      Insert2.Parameters.ParamByName('@MCash_8').Value := MCash2.Text;
      Insert2.Parameters.ParamByName('@MCredit_9').Value := MCredit2.Text;
      Insert2.Parameters.ParamByName('@MPrice_10').Value := MPrice2.Text;
      Insert2.ExecProc;
      Browse2.Close;
      Browse2.Parameters.ParamByName('@srlFactor').Value := srlFactor;
      Browse2.Open;
      Browse2.Locate('fserial', Insert2.Parameters.ParamByName('@RETURN_VALUE').Value, []);
    end
    else if MyState = sEdit then
    begin
      Update2.Parameters.ParamByName('@FSerial_1').Value := b2fs;
      Update2.Parameters.ParamByName('@srlGood_2').Value := GoodCode2.Text;
      Update2.Parameters.ParamByName('@NCount_3').Value := NCount2.Text;
      Update2.Parameters.ParamByName('@NRemain_4').Value := 0;
      Update2.Parameters.ParamByName('@srlSource_5').Value := srlSource;
      Update2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
      Update2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
      Update2.Parameters.ParamByName('@MCash_8').Value := MCash2.Text;
      Update2.Parameters.ParamByName('@MCredit_9').Value := MCredit2.Text;
      Update2.Parameters.ParamByName('@MPrice_10').Value := MPrice2.Text;
      Update2.ExecProc;
      Browse2.Close;
      Browse2.Open;
      Browse2.Locate('fserial', b2fs, []);
    end;
  end
end;

procedure TfrmBackBuySell.FormShow(Sender: TObject);
begin
  inherited;
  if (FactorType = ftBackSell) then
  begin
    frmBackBuySell.Caption := '�ѐ�� �� ����';
    srlDataSet1 := 27;
    srlDataSet2 := 25;
    srlDataSet3 := 12;
  end
  else if FactorType = ftBackBuy then
  begin
    frmBackBuySell.Caption := '�ѐ�� �� ����';
    srlDataSet1 := 26;
    srlDataSet2 := 24;
    srlDataSet3 := 20;
  end;
  refreshPanels(0, 0);
end;

procedure TfrmBackBuySell.bAddClick(Sender: TObject);
begin
  inherited;
  if panelfocus = 'Panel1' then
    Fcode1.Text := GetMaxCodeRecord(srlDataSet2, srlYear, -1, -1, -1); // Back from Buy.
  DtDate1.Text := CurrDate;
end;

procedure TfrmBackBuySell.StdCode1Change(Sender: TObject);
begin
  inherited;
  if FactorType = ftBackBuy then
    EditCodeChange(sView, StdCode1, lblSBName, srlBuyCenter, srlDataSet3)
  else if FactorType = ftBackSell then
  begin
    EditCodeChange(sAdd, StdCode1, lblSBName, srlStd, srlDataSet3);
    ShowStdPic;
  end;
end;

procedure TfrmBackBuySell.StdCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, StdCode1, lblSBName);
end;

procedure TfrmBackBuySell.StdCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if FactorType = ftBackBuy then
    EditCodeKeyPress(Key, MyState, StdCode1, lblSBName, srlBuyCenter, srlDataSet3)
  else if FactorType = ftBackSell then
    EditCodeKeyPress(Key, MyState, StdCode1, lblSBName, srlStd, srlDataSet3);
end;

procedure TfrmBackBuySell.bDelClick(Sender: TObject);
begin
  inherited;
  if panelfocus='Panel1'  then
    DeleteRecord1(39, Browse1FSerial.Value, Browse1);
  if panelfocus='Panel2'  then
    DeleteRecord1(23, Browse2FSerial.Value, Browse2);
end;

procedure TfrmBackBuySell.refreshPanels(b1fs, b2fs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Parameters.ParamByName('@Type').Value := FactorType;
  Browse1.Parameters.ParamByName('@srlYear').Value := srlYear;
  Browse1.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
  Browse1.Parameters.ParamByName('@srlUser').Value := srlUser;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlFactor').Value := srlFactor;
  Browse2.Open;
  Browse2.Locate('FSerial', b2fs, []);
end;

procedure TfrmBackBuySell.ShowStdPic;
var
  picFolderNo: Integer;
  picFolder, picFile: String;
begin
  picFolderNo := srlStd div PIC_FOLDER_DIV_NO;
  if not ((srlStd mod PIC_FOLDER_DIV_NO) = 0) then
    Inc(picFolderNo);
  picFolder := IntToStr(picFolderNo);
  if picFolderNo < 10 then
    picFolder := '0' + picFolder;
  picFolder := GetCurrentDir + '\' + PIC_FOLDER_NAME + picFolder + '\';
  picFile := picFolder + IntToStr(srlStd) + '.jpg';
  if FileExists(picFile) then
    imgStd.Picture.LoadFromFile(picFile)
  else
    imgStd.Picture.Graphic := nil;
end;

end.
