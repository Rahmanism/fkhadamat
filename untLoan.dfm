inherited frmLoan: TfrmLoan
  Left = 252
  Top = 139
  Caption = #1608#1575#1605
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 692
    object lblStd: TLabel [0]
      Left = 248
      Top = 38
      Width = 50
      Height = 13
      Caption = #1608#1575#1605' '#1711#1610#1585#1606#1583#1607':'
    end
    inherited Splitter2: TSplitter
      Width = 690
    end
    object Label1: TLabel [2]
      Left = 631
      Top = 38
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object Label2: TLabel [3]
      Left = 438
      Top = 38
      Width = 30
      Height = 13
      Caption = #1593#1606#1608#1575#1606':'
    end
    object Label4: TLabel [4]
      Left = 631
      Top = 60
      Width = 25
      Height = 13
      Caption = #1578#1575#1585#1610#1582':'
    end
    object Label5: TLabel [5]
      Left = 439
      Top = 60
      Width = 24
      Height = 13
      Caption = #1605#1576#1604#1594':'
    end
    object Label6: TLabel [6]
      Left = 247
      Top = 60
      Width = 60
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1578#1593#1583#1575#1583' '#1575#1602#1587#1575#1591':'
      ParentBiDiMode = False
    end
    object Label7: TLabel [7]
      Left = 631
      Top = 84
      Width = 39
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1590#1575#1605#1606' 1:'
      ParentBiDiMode = False
    end
    object lblStdFName: TLabel [8]
      Left = 175
      Top = 38
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label11: TLabel [9]
      Left = 439
      Top = 84
      Width = 39
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1590#1575#1605#1606' 2:'
      ParentBiDiMode = False
    end
    object lblBail1FName: TLabel [10]
      Left = 551
      Top = 86
      Width = 12
      Height = 13
      Caption = '...'
    end
    object lblBail2FName: TLabel [11]
      Left = 359
      Top = 86
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label13: TLabel [12]
      Left = 631
      Top = 108
      Width = 53
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1588#1605#1575#1585#1607' '#1670#1603':'
      ParentBiDiMode = False
    end
    object Label14: TLabel [13]
      Left = 439
      Top = 108
      Width = 53
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1588#1605#1575#1585#1607' '#1670#1603':'
      ParentBiDiMode = False
    end
    object StdFCode1: TEdit [14]
      Left = 192
      Top = 32
      Width = 49
      Height = 21
      TabOrder = 3
      OnChange = StdFCode1Change
      OnExit = StdFCode1Exit
      OnKeyPress = StdFCode1KeyPress
    end
    inherited DBGrid1: TDBGrid
      Width = 690
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 104
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Title.Caption = #1578#1575#1585#1610#1582
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAmount'
          Title.Caption = #1605#1602#1583#1575#1585
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NInstalment'
          Title.Caption = #1575#1602#1587#1575#1591
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsClose'
          Title.Caption = #1576#1587#1578#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlStd'
          Title.Caption = #1587' '#1591#1604#1576#1607
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlYear'
          Title.Caption = #1587' '#1587#1575#1604
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlBail1'
          Title.Caption = #1587' '#1590#1575#1605#1606' 1'
          Width = 49
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlBail2'
          Title.Caption = #1587' '#1590#1575#1605#1606' 2'
          Width = 41
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NCheque1'
          Title.Caption = #1588' '#1670#1603' 1'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NCheque2'
          Title.Caption = #1588' '#1670#1603' 2'
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdFCode'
          Title.Caption = #1603#1583' '#1591#1604#1576#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'StdFName'
          Title.Caption = #1606#1575#1605' '#1591#1604#1576#1607
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bail1FCode'
          Title.Caption = #1603#1583' '#1590#1575#1605#1606' 1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bail1FName'
          Title.Caption = #1606#1575#1605' '#1590#1575#1605#1606' 1'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bail2FCode'
          Title.Caption = #1603#1583' '#1590#1575#1605#1606' 2'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bail2FName'
          Title.Caption = #1606#1575#1605' '#1590#1575#1605#1606' 2'
          Width = 77
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 504
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object FName1: TEdit
      Left = 312
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object NInstalment1: TEdit
      Left = 120
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 6
    end
    object MAmount1: TEdit
      Left = 312
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 5
    end
    object Bail1FCode1: TEdit
      Left = 569
      Top = 80
      Width = 57
      Height = 21
      TabOrder = 7
      OnChange = Bail1FCode1Change
      OnExit = Bail1FCode1Exit
      OnKeyPress = Bail1FCode1KeyPress
    end
    object DtDate1: TMaskEdit
      Left = 504
      Top = 56
      Width = 122
      Height = 21
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 4
      Text = '  /  /  '
    end
    object Bail2FCode1: TEdit
      Left = 376
      Top = 80
      Width = 57
      Height = 21
      TabOrder = 8
      OnChange = Bail2FCode1Change
      OnExit = Bail2FCode1Exit
      OnKeyPress = Bail2FCode1KeyPress
    end
    object NCheque11: TEdit
      Left = 504
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 9
    end
    object NCheque21: TEdit
      Left = 312
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 10
    end
  end
  inherited Panel2: TPanel
    Width = 694
    Height = 272
    inherited Splitter1: TSplitter
      Top = 87
      Width = 692
    end
    object Label9: TLabel [1]
      Left = 605
      Top = 52
      Width = 24
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1605#1576#1604#1594':'
      ParentBiDiMode = False
    end
    object Label12: TLabel [2]
      Left = 605
      Top = 28
      Width = 64
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1605#1608#1593#1583' '#1662#1585#1583#1575#1582#1578':'
      ParentBiDiMode = False
    end
    object Label3: TLabel [3]
      Left = 401
      Top = 28
      Width = 61
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = #1578#1575#1585#1610#1582' '#1662#1585#1583#1575#1582#1578':'
      ParentBiDiMode = False
    end
    object DBCheckBox1: TDBCheckBox [4]
      Left = 168
      Top = 112
      Width = 17
      Height = 17
      DataField = 'Paid'
      TabOrder = 6
      ValueChecked = 'True'
      ValueUnchecked = 'False'
      Visible = False
    end
    inherited DBGrid2: TDBGrid
      Top = 90
      Width = 692
      Height = 181
      TitleFont.Height = -13
      TitleFont.Name = 'B Nazanin'
      TitleFont.Style = [fsBold]
      OnDrawColumnCell = DBGrid2DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Title.Caption = #1605#1608#1593#1583' '#1662#1585#1583#1575#1582#1578
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtPayment'
          Title.Caption = #1578#1575#1585#1610#1582' '#1662#1585#1583#1575#1582#1578
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlLoan'
          Title.Caption = #1587#1585#1610#1575#1604' '#1608#1575#1605
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAmount'
          Title.Caption = #1605#1576#1604#1594
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Paid'
          Title.Caption = #1662#1585#1583#1575#1582#1578' '#1588#1583
          Visible = True
        end>
    end
    object MAmount2: TEdit
      Left = 480
      Top = 48
      Width = 120
      Height = 21
      TabOrder = 1
    end
    object DtDate2: TMaskEdit
      Left = 480
      Top = 24
      Width = 121
      Height = 21
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 2
      Text = '  /  /  '
    end
    object DtPayment2: TMaskEdit
      Left = 272
      Top = 24
      Width = 121
      Height = 21
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 3
      Text = '  /  /  '
    end
    object Paid2: TCheckBox
      Left = 296
      Top = 48
      Width = 97
      Height = 17
      Caption = #1662#1585#1583#1575#1582#1578' '#1588#1583
      TabOrder = 4
    end
    object bbnPayment: TBitBtn
      Left = 120
      Top = 32
      Width = 137
      Height = 33
      Caption = #1662#1585#1583#1575#1582#1578'      (F9)'
      TabOrder = 5
      OnClick = bbnPaymentClick
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TLoan_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@DtDate_4'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MAmount_5'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@NInstalment_6'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@IsClose_7'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@srlStd_8'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear_9'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlBail1_10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlBail2_11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NCheque1_12'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end
      item
        Name = '@NCheque2_13'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 50
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    ProcedureName = 'Browse_TLoan;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object Browse1DtDate: TStringField
      FieldName = 'DtDate'
      FixedChar = True
      Size = 8
    end
    object Browse1MAmount: TBCDField
      FieldName = 'MAmount'
      Precision = 19
    end
    object Browse1NInstalment: TSmallintField
      FieldName = 'NInstalment'
    end
    object Browse1IsClose: TBooleanField
      FieldName = 'IsClose'
    end
    object Browse1srlStd: TIntegerField
      FieldName = 'srlStd'
    end
    object Browse1srlYear: TIntegerField
      FieldName = 'srlYear'
    end
    object Browse1srlBail1: TIntegerField
      FieldName = 'srlBail1'
    end
    object Browse1srlBail2: TIntegerField
      FieldName = 'srlBail2'
    end
    object Browse1NCheque1: TWideStringField
      FieldName = 'NCheque1'
      Size = 50
    end
    object Browse1NCheque2: TWideStringField
      FieldName = 'NCheque2'
      Size = 50
    end
    object Browse1StdFCode: TIntegerField
      FieldName = 'StdFCode'
    end
    object Browse1StdFName: TWideStringField
      FieldName = 'StdFName'
      Size = 100
    end
    object Browse1Bail1FCode: TIntegerField
      FieldName = 'Bail1FCode'
    end
    object Browse1Bail1FName: TWideStringField
      FieldName = 'Bail1FName'
      Size = 100
    end
    object Browse1Bail2FCode: TIntegerField
      FieldName = 'Bail2FCode'
    end
    object Browse1Bail2FName: TWideStringField
      FieldName = 'Bail2FName'
      Size = 100
    end
  end
  inherited Update2: TADOStoredProc
    ProcedureName = 'update_TInstalment_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@DtPayment_2'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@Paid_3'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
  end
  inherited Browse2: TADOStoredProc
    ProcedureName = 'Browse_TInstalment;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlLoan'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    object Browse2FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse2DtDate: TStringField
      FieldName = 'DtDate'
      FixedChar = True
      Size = 8
    end
    object Browse2DtPayment: TStringField
      FieldName = 'DtPayment'
      FixedChar = True
      Size = 8
    end
    object Browse2srlLoan: TIntegerField
      FieldName = 'srlLoan'
    end
    object Browse2MAmount: TBCDField
      FieldName = 'MAmount'
      Precision = 19
    end
    object Browse2Paid: TBooleanField
      FieldName = 'Paid'
    end
  end
  object dtsGoods: TDataSource [11]
    DataSet = GoodsBrowse
    Left = 80
    Top = 440
  end
  object GoodsBrowse: TADOStoredProc [12]
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browes_TDetFacRemains;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlGood1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 40
    Top = 440
    object GoodsBrowseFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object GoodsBrowsesrlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object GoodsBrowseNCount: TIntegerField
      FieldName = 'NCount'
    end
    object GoodsBrowseNRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object GoodsBrowsesrlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object GoodsBrowsesrlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object GoodsBrowsesrlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object GoodsBrowseMCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object GoodsBrowseMCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object GoodsBrowseMPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object GoodsBrowseType: TSmallintField
      FieldName = 'Type'
    end
  end
  object GoodsCountUpdate: TADOStoredProc [13]
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TDetFacRemain_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 41
    Top = 489
  end
  object dtsDetPreToFactor: TDataSource [14]
    DataSet = DetPreToFactor
    Left = 184
    Top = 440
  end
  object DetPreToFactor: TADOStoredProc [15]
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_TDetFac;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 144
    Top = 440
    object DetPreToFactorFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object DetPreToFactorsrlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object DetPreToFactorNCount: TIntegerField
      FieldName = 'NCount'
    end
    object DetPreToFactorNRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object DetPreToFactorsrlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object DetPreToFactorsrlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object DetPreToFactorsrlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object DetPreToFactorMCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object DetPreToFactorMCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object DetPreToFactorMPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object DetPreToFactorFName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object DetPreToFactorWarehouseName: TWideStringField
      FieldName = 'WarehouseName'
      Size = 50
    end
    object DetPreToFactorUnit: TWideStringField
      FieldName = 'Unit'
      Size = 50
    end
    object DetPreToFactorGoodCode: TIntegerField
      FieldName = 'GoodCode'
    end
    object DetPreToFactorWarehouseCode: TIntegerField
      FieldName = 'WarehouseCode'
    end
  end
  object ADOStoredProc2: TADOStoredProc [16]
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TDetFacRemain_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 145
    Top = 489
  end
end
