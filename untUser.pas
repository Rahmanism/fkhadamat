
unit untUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt2Panels, DB, ADODB, Buttons, ComCtrls, Grids, DBGrids,
  ExtCtrls, StdCtrls, DBCtrls, dxtree, dxdbtree;

type
  TfrmUser = class(Tfrm2Panels)
    Fcode1: TEdit;
    Fname1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Browse1Fserial: TAutoIncField;
    Browse1FCode: TStringField;
    Browse1FName: TStringField;
    Browse1Message: TWideStringField;
    Browse1IsActive: TBooleanField;
    Browse1YearActive: TIntegerField;
    Browse1HouseActive: TIntegerField;
    FcodeWarehouse1: TEdit;
    Message1: TEdit;
    Label5: TLabel;
    FcodeYear1: TEdit;
    IsActive1: TCheckBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Browse2frmName: TStringField;
    Browse2frmCaption: TStringField;
    Browse2objName: TStringField;
    Browse2objCaption: TStringField;
    Browse2objCode: TIntegerField;
    Browse2srlObj: TIntegerField;
    Browse2ISvisible: TBooleanField;
    Browse2IsEnable: TBooleanField;
    Browse2IsReadOnly: TBooleanField;
    Browse2srlUser: TIntegerField;
    Browse2Fserial: TAutoIncField;
    Browse2FserialUObj: TAutoIncField;
    Update3: TADOStoredProc;
    Panel3: TPanel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    Label4: TLabel;
    pass1: TEdit;
    Label9: TLabel;
    Browse1pass: TWideStringField;
    Label10: TLabel;
    Browse1FcodeWarehouse: TIntegerField;
    Browse1FnameWarehouse: TWideStringField;
    Browse1Boss: TWideStringField;
    Browse1FcodeYear: TIntegerField;
    Browse1FnameYear: TWideStringField;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure bSaveClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FcodeYear1KeyPress(Sender: TObject; var Key: Char);
    procedure FcodeYear1Change(Sender: TObject);
    procedure FcodeYear1Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bSearchClick(Sender: TObject);
    procedure FcodeWarehouse1Change(Sender: TObject);
    procedure FcodeWarehouse1Exit(Sender: TObject);
    procedure FcodeWarehouse1KeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    procedure refreshPanels(b1fs, b2fs: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUser: TfrmUser;

implementation

uses U_Common, untDm, Math;

{$R *.dfm}

procedure TfrmUser.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  srluser := Browse1FSerial.Value;
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlUser').Value:=Browse1FSerial.Value;
  Browse2.Open;
end;

procedure TfrmUser.bSaveClick(Sender: TObject);
var
  i:Integer;
begin
  inherited;
 // i:=Browse1FSerial.Value;
  if (MyState=sAdd)  and (panelfocus='Panel1') then
  begin
    Insert1.Parameters.ParamByName('@Fcode_2').Value:=Fcode1.Text;
    Insert1.Parameters.ParamByName('@Fname_3').Value:=Fname1.Text;
    Insert1.Parameters.ParamByName('@Message_4').Value:=Message1.Text;
    Insert1.Parameters.ParamByName('@IsActive_5').Value:=IsActive1.Checked;
    Insert1.Parameters.ParamByName('@YearActive_6').Value:=srlyearUser;
    Insert1.Parameters.ParamByName('@HouseActive_7').Value:=srlHouseUser;
    Insert1.Parameters.ParamByName('@pass').Value:=pass1.Text;
    Insert1.ExecProc;
    Insert2.Close;
    Insert2.Parameters.ParamByName('@srluser').Value:=Insert1.Parameters.ParamByName('@RETURN_VALUE').Value;
    Insert2.ExecProc;
    refreshPanels(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value, Insert2.Parameters.ParamByName('@RETURN_VALUE').Value);
  end;
  if (MyState=sedit)  and (panelfocus='Panel1') then
  begin
     NullAdoSPParameters(Update1);

   // i:=Browse1FSerial.Value;
    Update1.Parameters.ParamByName('@FSerial_1').Value:=srluser;//Browse1FSerial.Value;
    Update1.Parameters.ParamByName('@Fcode_3').Value:=Fcode1.Text;
    Update1.Parameters.ParamByName('@Fname_4').Value:=Fname1.Text;
    Update1.Parameters.ParamByName('@Message_5').Value:=Message1.Text;
    Update1.Parameters.ParamByName('@IsActive_6').Value:=IsActive1.Checked;
    Update1.Parameters.ParamByName('@YearActive_7').Value:=srlyearUser;
    Update1.Parameters.ParamByName('@HouseActive_8').Value:=srlHouseUser;
    Update1.Parameters.ParamByName('@pass').Value:=pass1.Text;
    Update1.ExecProc;
    refreshPanels(i, 0);
  end;
/////////////////////////////////////////////////////////////////////////////////////
  { if (MyState=sAdd)  and (panelfocus='Panel2') then
        begin
            Insert2.Parameters.ParamByName('@Fcode_2').Value:=Fcode2.Text;
            Insert2.Parameters.ParamByName('@Fname_3').Value:=Fname2.Text;
            Insert2.Parameters.ParamByName('@Stock_4').Value:=Stock2.Text;
            Insert2.Parameters.ParamByName('@srlcategory_5').Value:=srlcategory;
            Insert2.Parameters.ParamByName('@Maxstock_6').Value:=MaxStock2.Text;
            Insert2.Parameters.ParamByName('@MinStock_7').Value:=MinStock2.Text;
            if srlUnit2.Text<>'' then
            Insert2.Parameters.ParamByName('@srlUnit_8').Value:=srlUnit else  Insert2.Parameters.ParamByName('@srlUnit_8').Value:=null;
            Insert2.Parameters.ParamByName('@Blocked_9').Value:=Blocked2.Checked;
            Insert2.ExecProc;
            Browse2.Close;
            Browse2.Open;
            Browse2.Locate('fserial',Insert2.Parameters.ParamByName('@RETURN_VALUE').Value,[]);
        end;
    if (MyState=sedit)  and (panelfocus='Panel2') then
      begin
            i:=Browse2FSerial.Value;
            Update2.Parameters.ParamByName('@Fcode_2').Value:=Fcode2.Text;
            Update2.Parameters.ParamByName('@Fname_3').Value:=Fname2.Text;
            Update2.Parameters.ParamByName('@Stock_5').Value:=Stock2.Text;
            Update2.Parameters.ParamByName('@srlcategory_6').Value:=srlcategory;
            Update2.Parameters.ParamByName('@Maxstock_7').Value:=MaxStock2.Text;
            Update2.Parameters.ParamByName('@MinStock_8').Value:=MinStock2.Text;
            if srlUnit2.Text<>''  then
            Update2.Parameters.ParamByName('@srlUnit_9').Value:=srlUnit else Update2.Parameters.ParamByName('@srlUnit_9').Value:='';
            Update2.Parameters.ParamByName('@Blocked_10').Value:=Blocked2.Checked;
            Update2.ExecProc;
            Browse2.Close;
            Browse2.Open;
            Browse2.Locate('fserial',i,[]);
     end; }

  MyState:=sView;
end;

procedure TfrmUser.bAddClick(Sender: TObject);
begin
  inherited;
  if panelfocus='Panel1' then
    Fcode1.Text:=GetMaxCodeRecord(19) ;
 // if panelfocus='Panel2' then
 // FCode2.Text:=GetMaxCodeRecord(19) ;
end;

procedure TfrmUser.bDelClick(Sender: TObject);
begin
  inherited;
  if panelfocus='Panel1'  then
    DeleteRecord1(19, Browse1FSerial.Value, Browse1)
  else if panelfocus='Panel2'  then
   begin
     Browse2.First;
  while not Browse2.Eof do
  begin
    if  DBGrid2.SelectedRows.CurrentRowSelected then
    begin
     DeleteRecord1(41, Browse2FSerial.Value, Browse2);
    end;
    Browse2.Next;
  end;

    end;
end;

procedure TfrmUser.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  Browse2.First;
  while not Browse2.Eof do
  begin
    if  DBGrid2.SelectedRows.CurrentRowSelected then
    begin
      Update3.Close;
      Update3.Parameters.ParamByName('@Fserial').Value:=Browse2FserialUObj.Value;
      Update3.Parameters.ParamByName('@isvisible').Value:=CheckBox1.Checked;
      Update3.Parameters.ParamByName('@isenable').Value:= CheckBox2.Checked;
      Update3.Parameters.ParamByName('@isReadonly').Value:=CheckBox3.Checked;
      Update3.ExecProc;
    end;
    Browse2.Next;
  end;
  Browse2.Close;
  Browse2.Open;
end;

procedure TfrmUser.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  Browse2.First;
  while not Browse2.Eof do
  begin
    DBGrid2.SelectedRows.CurrentRowSelected:=true ;
    Browse2.Next;
  end;
end;

procedure TfrmUser.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  CheckBoxInDBGrid(DBCheckBox1, Sender, Rect, DataCol, Column, State);
  CheckBoxInDBGrid(DBCheckBox2, Sender, Rect, DataCol, Column, State);
end;

procedure TfrmUser.FcodeYear1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
    EditCodeKeyPress(Key, MyState, FcodeYear1, Label4, srlyearUser, 40)
end;

procedure TfrmUser.FcodeYear1Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, FcodeYear1, Label4, srlyearUser, 40)
end;

procedure TfrmUser.FcodeYear1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, FcodeYear1, Label4);
end;

procedure TfrmUser.refreshPanels(b1fs, b2fs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlUser').Value:=Browse1FSerial.Value;
  Browse2.Open;
  Browse2.Locate('FSerial', b2fs, []);
end;

procedure TfrmUser.FormShow(Sender: TObject);
begin
  inherited;
  refreshPanels(0, 0);
end;

procedure TfrmUser.bSearchClick(Sender: TObject);
begin
  inherited;
 //    DeleteRecord1();
end;

procedure TfrmUser.FcodeWarehouse1Change(Sender: TObject);
begin
  inherited;
     EditCodeChange(sView, FcodeWarehouse1, Label10, srlHouseUser, 13)
end;

procedure TfrmUser.FcodeWarehouse1Exit(Sender: TObject);
begin
  inherited;
      EditCodeExit(MyState, FcodeWarehouse1, Label10);
end;

procedure TfrmUser.FcodeWarehouse1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
    EditCodeKeyPress(Key, MyState, FcodeWarehouse1, Label10, srlHouseUser, 13)
end;

procedure TfrmUser.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  SBFindClick(MyState, FcodeYear1, Label4, srlyearUser, 40);
end;

procedure TfrmUser.SpeedButton4Click(Sender: TObject);
begin
  inherited;
     SBFindClick(mystate, FcodeWarehouse1, Label10, srlHouseUser, 13);
end;

end.
