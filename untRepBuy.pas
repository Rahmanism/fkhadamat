unit untRepBuy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, untParentReport, DB, ADODB, StdCtrls, Buttons, ExtCtrls, Grids,
  DBGrids, ComCtrls;

type
  TfrmRepBuy = class(TfrmParentReport)
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRepBuy: TfrmRepBuy;

implementation

uses U_Common, untDm;

{$R *.dfm}

procedure TfrmRepBuy.BitBtn1Click(Sender: TObject);
begin
  inherited;
    Browse1.Close;
    Browse1.Parameters.ParamByName('@srl_year1').Value:=srlYearRep1;
    Browse1.Parameters.ParamByName('@srl_year2').Value:=srlYearRep2;
    Browse1.Parameters.ParamByName('@num_rep').Value:=0;
    Browse1.Open;

end;

procedure TfrmRepBuy.FormShow(Sender: TObject);
begin
  inherited;
    Browse1.Close;
    Browse1.Parameters.ParamByName('@srl_year1').Value:=srlYear;
    Browse1.Parameters.ParamByName('@srl_year2').Value:=srlYear;
    Browse1.Parameters.ParamByName('@num_rep').Value:=0;
    Browse1.Open;

end;

end.
