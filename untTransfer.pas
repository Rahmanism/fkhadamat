unit untTransfer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt2Panels, DB, ADODB, Buttons, ComCtrls, Grids, DBGrids,
  ExtCtrls, StdCtrls, Mask, untDm, U_Common, Math;

type
  TfrmTransfer = class(Tfrm2Panels)
    FCode1: TEdit;
    FName1: TEdit;
    MExtra1: TEdit;
    MCost1: TEdit;
    StdCode1: TEdit;
    MDiscount1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    lblStd: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lblSBName: TLabel;
    GoodCode2: TEdit;
    MCash2: TEdit;
    MPrice2: TEdit;
    NCount2: TEdit;
    Label3: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    lblGood: TLabel;
    Label10: TLabel;
    MCredit2: TEdit;
    IsClose1: TCheckBox;
    DtDate1: TMaskEdit;
    dtsGoods: TDataSource;
    GoodsBrowse: TADOStoredProc;
    GoodsCountUpdate: TADOStoredProc;
    Browse2FSerial: TAutoIncField;
    Browse2srlGood: TIntegerField;
    Browse2NCount: TIntegerField;
    Browse2NRemain: TIntegerField;
    Browse2srlSource: TIntegerField;
    Browse2srlWarehouse: TIntegerField;
    Browse2srlFactor: TIntegerField;
    Browse2MCash: TBCDField;
    Browse2MCredit: TBCDField;
    Browse2MPrice: TBCDField;
    Browse2FName: TWideStringField;
    Browse2WarehouseName: TWideStringField;
    Browse2Unit: TWideStringField;
    Browse2GoodCode: TIntegerField;
    Browse2WarehouseCode: TIntegerField;
    GoodsBrowseFSerial: TAutoIncField;
    GoodsBrowsesrlGood: TIntegerField;
    GoodsBrowseNCount: TIntegerField;
    GoodsBrowseNRemain: TIntegerField;
    GoodsBrowsesrlSource: TIntegerField;
    GoodsBrowsesrlWarehouse: TIntegerField;
    GoodsBrowsesrlFactor: TIntegerField;
    GoodsBrowseMCash: TBCDField;
    GoodsBrowseMCredit: TBCDField;
    GoodsBrowseMPrice: TBCDField;
    GoodsBrowseType: TSmallintField;
    PreToFactor: TADOStoredProc;
    dtsDetPreToFactor: TDataSource;
    DetPreToFactor: TADOStoredProc;
    ADOStoredProc2: TADOStoredProc;
    DetPreToFactorFSerial: TAutoIncField;
    DetPreToFactorsrlGood: TIntegerField;
    DetPreToFactorNCount: TIntegerField;
    DetPreToFactorNRemain: TIntegerField;
    DetPreToFactorsrlSource: TIntegerField;
    DetPreToFactorsrlWarehouse: TIntegerField;
    DetPreToFactorsrlFactor: TIntegerField;
    DetPreToFactorMCash: TBCDField;
    DetPreToFactorMCredit: TBCDField;
    DetPreToFactorMPrice: TBCDField;
    DetPreToFactorFName: TWideStringField;
    DetPreToFactorWarehouseName: TWideStringField;
    DetPreToFactorUnit: TWideStringField;
    DetPreToFactorGoodCode: TIntegerField;
    DetPreToFactorWarehouseCode: TIntegerField;
    WarehouseCode2: TEdit;
    Label11: TLabel;
    lblWarehouse2: TLabel;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1DtDate: TStringField;
    Browse1MExtra: TBCDField;
    Browse1Type: TSmallintField;
    Browse1MCost: TBCDField;
    Browse1MDiscount: TBCDField;
    Browse1IsClose: TBooleanField;
    Browse1srlYear: TIntegerField;
    Browse1srlLoan: TIntegerField;
    Browse1srlStd: TIntegerField;
    Browse1srlBuyCenter: TIntegerField;
    Browse1BuyCenterCode: TIntegerField;
    Browse1StdCode: TIntegerField;
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure StdCode1Change(Sender: TObject);
    procedure StdCode1Exit(Sender: TObject);
    procedure StdCode1KeyPress(Sender: TObject; var Key: Char);
    procedure GoodCode2Change(Sender: TObject);
    procedure GoodCode2Exit(Sender: TObject);
    procedure GoodCode2KeyPress(Sender: TObject; var Key: Char);
    procedure bSaveClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bDelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WarehouseCode2Change(Sender: TObject);
    procedure WarehouseCode2Exit(Sender: TObject);
    procedure WarehouseCode2KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure procPreToFactor(srlG, nv, srlF: Integer);
    procedure refreshPanels(b1fs, b2fs: Integer);
  public
    { Public declarations }
  end;

var
  frmTransfer: TfrmTransfer;
  srlDataSetGoods, srlDataSetMaxRecord, srlDataSetSellerBuyer,
  srlWarehouse2, srlDataSetWarehouse2: Integer;

implementation

{$R *.dfm}

procedure TfrmTransfer.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  srlFactor := Browse1FSerial.Value;
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlFactor').Value := Browse1FSerial.Value;
  Browse2.Open;
end;

procedure TfrmTransfer.StdCode1Change(Sender: TObject);
begin
  inherited;
//  srlStd := 17;
  EditCodeChange(sView, StdCode1, lblSBName, srlStd, srlDataSetSellerBuyer);
end;

procedure TfrmTransfer.StdCode1Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, StdCode1, lblSBName);
end;

procedure TfrmTransfer.StdCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, StdCode1, lblSBName, srlStd, srlDataSetSellerBuyer);
end;

procedure TfrmTransfer.GoodCode2Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, GoodCode2, lblGood, srlGood, srlDataSetGoods)
end;

procedure TfrmTransfer.GoodCode2Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, GoodCode2, lblGood);
end;

procedure TfrmTransfer.GoodCode2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, GoodCode2, lblGood, srlGood, srlDataSetGoods);
end;

procedure TfrmTransfer.bSaveClick(Sender: TObject);
var
  i, t, nvalue, nremain, newnremain, grc, sgrc, b1fs, b2fs:Integer;
  srlDetFac4Remain: Integer;
begin
  IF Browse1.FieldValues['FSerial'] <> null then
    b1fs := Browse1.FieldValues['FSerial'];
  if Browse2.FieldValues['FSerial'] <> null then
    b2fs := Browse2.FieldValues['FSerial'];
  inherited;
  if panelfocus = 'Panel1' then
  begin
    if MyState = sAdd then
    begin
      Insert1.Parameters.ParamByName('@Fcode_2').Value := Fcode1.Text;
      Insert1.Parameters.ParamByName('@FName_3').Value := Fname1.Text;
      Insert1.Parameters.ParamByName('@srlStd_4').Value := srlStd;
      Insert1.Parameters.ParamByName('@DtDate_6').Value := DtDate1.Text;
      Insert1.Parameters.ParamByName('@MExtra_7').Value := MExtra1.Text;
      Insert1.Parameters.ParamByName('@Type_8').Value := Ord(FactorType);
      Insert1.Parameters.ParamByName('@MCost_9').Value := MCost1.Text;
      Insert1.Parameters.ParamByName('@MDiscount_10').Value := MDiscount1.Text;
      Insert1.Parameters.ParamByName('@IsClose_11').Value := IsClose1.Checked;
      Insert1.Parameters.ParamByName('@srlYear_12').Value := srlYear;
      Insert1.Parameters.ParamByName('@srlLoan_13').Value := 0;
      Insert1.ExecProc;
      refreshPanels(IfNullThenZero(Insert1.Parameters.ParamByName('@RETURN_VALUE').Value, b1fs), 0);
    end
    else if MyState = sEdit then
    begin
      Update1.Parameters.ParamByName('@FSerial_1').Value := b1fs;
      Update1.Parameters.ParamByName('@FCode_3').Value := Fcode1.Text;
      Update1.Parameters.ParamByName('@FName_4').Value := Fname1.Text;
      Update1.Parameters.ParamByName('@srlStd_4').Value := srlStd;
      Update1.Parameters.ParamByName('@DtDate_7').Value := DtDate1.Text;
      Update1.Parameters.ParamByName('@MExtra_8').Value := MExtra1.Text;
      Update1.Parameters.ParamByName('@MCost_10').Value := MCost1.Text;
      Update1.Parameters.ParamByName('@MDiscount_11').Value := MDiscount1.Text;
      Update1.Parameters.ParamByName('@IsClose_12').Value := IsClose1.Checked;
      Update1.ExecProc;
      Browse1.Close;
      Browse1.Open;
      Browse1.Locate('fserial', b1fs, []);
    end;
  end
  else
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  if panelfocus = 'Panel2' then  ///////////
  begin
    if MyState = sAdd then
    begin
      nvalue := StrToInt(NCount2.Text);
      ////////////////////////////////////
      if (FactorType = ftMoveFromWarehouse) then
      begin
        GoodsBrowse.Parameters.ParamByName('@srlGood1').Value := srlGood;
        GoodsBrowse.Open;
        if GoodsBrowse.RecordCount > 0 then
        begin
          sgrc := 0;
          for grc := 0 to GoodsBrowse.RecordCount - 1 do
          begin
            sgrc := sgrc + GoodsBrowse.FieldValues['NRemain'];
            GoodsBrowse.Next;
          end;
          GoodsBrowse.First;
          if sgrc < nvalue then
          begin
            grc := MessageDlg('����� ������� ��� �� ������ ���.'#13'��� �� ����� ' + IntToStr(sgrc) + ' ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
            if grc = mrCancel then
              exit
            else
            begin
              nvalue := sgrc;
              NCount2.Text := IntToStr(nvalue);
            end;
          end;
          repeat
          begin
            //srlDetFac4Remain := GoodsBrowse.FieldValues['FSerial'];
//            ShowMessage('.����� ���� ����� �� ����� ���� �� �� ���� ���'#13'���� ��������� �� ����� ���� ������ ���� ��������� �� �����.');
       //     NCount2.Text := IntToStr(GoodsBrowse.FieldValues['NRemain']);
         //   GoodsBrowse.Locate('FSerial', srlDetFac4Remain, []);
            nremain := StrToInt(GoodsBrowse.FieldValues['NRemain']);
            newnremain := nremain - nvalue;
            if newnremain < 0 then
              newnremain := 0
            else
              nremain := nvalue;
            GoodsCountUpdate.Parameters.ParamByName('@FSerial_1').Value := GoodsBrowse.FieldValues['FSerial'];
            GoodsCountUpdate.Parameters.ParamByName('@NRemain_2').Value := newnremain;
            GoodsCountUpdate.ExecProc;

            Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
            Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
            Insert2.Parameters.ParamByName('@NRemain_4').Value := 0;
            Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
            Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
            Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
            Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
            Insert2.Parameters.ParamByName('@MCredit_9').Value := GoodsBrowse.FieldValues['MCredit'];
            Insert2.Parameters.ParamByName('@MPrice_10').Value := 0;// GoodsBrowse.FieldValues['MPrice'];
            Insert2.ExecProc;

            Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
            Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
            Insert2.Parameters.ParamByName('@NRemain_4').Value := nremain;
            Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
            Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse2;
            Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
            Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
            Insert2.Parameters.ParamByName('@MCredit_9').Value := GoodsBrowse.FieldValues['MCredit'];
            Insert2.Parameters.ParamByName('@MPrice_10').Value := 0;// GoodsBrowse.FieldValues['MPrice'];
            Insert2.ExecProc;

            nvalue := nvalue - nremain;
            GoodsBrowse.Next;
 //           Exit;
          end
          until (nvalue <= 0);
        end
        else // GoodsBrowse.RecordCount = 0
        begin
          ShowMessage('�� ��� ���� ������ ������.');
//          Exit;
        end;
        GoodsBrowse.Close;
      end
     { else // of if FactorType=ftSell
      begin
        Insert2.Parameters.ParamByName('@srlGood_2').Value := srlGood;
        Insert2.Parameters.ParamByName('@NCount_3').Value := nvalue;
        Insert2.Parameters.ParamByName('@NRemain_4').Value := StrToInt(NCount2.Text);
        Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
        Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlFactor;
        Insert2.Parameters.ParamByName('@MCash_8').Value := MCash2.Text;
        Insert2.Parameters.ParamByName('@MCredit_9').Value := MCredit2.Text;
        Insert2.Parameters.ParamByName('@MPrice_10').Value := MPrice2.Text;
        Insert2.ExecProc;
      end};
      ////////////////////////////////////
//      Insert2.Parameters.ParamByName('@srlSource_5').Value := 0;
      Browse2.Close;
      Browse2.Open;
      Browse2.Locate('fserial', Insert2.Parameters.ParamByName('@RETURN_VALUE').Value, []);
    end
    else if MyState = sEdit then
    begin
      Update2.Parameters.ParamByName('@FSerial_1').Value := b2fs;
      Update2.Parameters.ParamByName('@MCash_9').Value := MCash2.Text;
      Update2.Parameters.ParamByName('@MCredit_10').Value := MCredit2.Text;
      Update2.Parameters.ParamByName('@MPrice_11').Value := MPrice2.Text;
      Update2.ExecProc;
      Browse2.Close;
      Browse2.Parameters.ParamByName('@srlFactor').Value := srlFactor;
      Browse2.Open;
      Browse2.Locate('fserial', b2fs, []);
    end; // end if MyState.
  end; // end if panel.
  MyState := sView;
  Browse1.Close;
  Browse1.Parameters.ParamByName('@Type').Value := FactorType;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
end;

procedure TfrmTransfer.bAddClick(Sender: TObject);
begin
  inherited;
  if panelfocus = 'Panel1' then
    Fcode1.Text := GetMaxCodeRecord(srlDataSetMaxRecord, srlYear, -1, -1, -1);
  DtDate1.Text := CurrDate;
end;

procedure TfrmTransfer.bDelClick(Sender: TObject);
begin
  inherited;
  DeleteRecord1(23, Browse2FSerial.Value, Browse2);
end;

procedure TfrmTransfer.FormShow(Sender: TObject);
begin
  inherited;
  srlDataSetGoods := 18;
  srlDataSetWarehouse2 := 29;
{  if (FactorType = ftSell) then
  begin
    lblStd.Caption := '������:';
    frmSellBuy.Caption := '����';
    srlDataSetMaxRecord := 21;
    srlDataSetSellerBuyer := 12;
//    DBGrid1.Columns[3].Visible := True;
  //  DBGrid1.Columns[4].Visible := False;
  end
  else if FactorType = ftBuy then
  begin
    lblStd.Caption := '�������:';
    frmSellBuy.Caption := '����';
    srlDataSetMaxRecord := 22;
    srlDataSetSellerBuyer := 20;
//    DBGrid1.Columns[4].Visible := True;
  //  DBGrid1.Columns[3].Visible := False;
  end
  else if FactorType = ftPreFactor then
  begin
    lblStd.Caption := '������:';
    frmSellBuy.Caption := '����';
    srlDataSetMaxRecord := 28;
    srlDataSetSellerBuyer := 12;
    spbPreToFactor.Visible := True;
//    DBGrid1.Columns[4].Visible := True;
  //  DBGrid1.Columns[3].Visible := False;
  end
  else}
    if FactorType = ftMoveFromWarehouse then
  begin
    lblStd.Caption := '������:';
    frmTransfer.Caption := '������ ������';
    srlDataSetGoods := 18;
    srlDataSetWarehouse2 := 29;
    srlDataSetMaxRecord := 30;
    srlDataSetSellerBuyer := 12;
   // spbPreToFactor.Visible := False;
//    DBGrid1.Columns[4].Visible := True;
  //  DBGrid1.Columns[3].Visible := False;
  end;
  Browse1.Close;
  Browse1.Parameters.ParamByName('@Type').Value := FactorType;
  Browse1.Open;
end;

procedure TfrmTransfer.procPreToFactor(srlG, nv, srlF: Integer);
var
  sgrc, grc, nvalue, nremain, newnremain: Integer;
begin
  nvalue := nv;
  GoodsBrowse.Parameters.ParamByName('@srlGood1').Value := srlG;
  GoodsBrowse.Open;
  if GoodsBrowse.RecordCount > 0 then
  begin
    sgrc := 0;
    for grc := 0 to GoodsBrowse.RecordCount - 1 do
    begin
      sgrc := sgrc + GoodsBrowse.FieldValues['NRemain'];
      GoodsBrowse.Next;
    end;
    GoodsBrowse.First;
    if sgrc < nvalue then
    begin
    //  grc := MessageDlg('����� ������� ��� �� ������ ���.'#13'��� �� ����� ' +
      //  IntToStr(sgrc) +
        //' ��� ��Ͽ', mtConfirmation, mbOKCancel, 0);
     // if grc = mrCancel then
       // exit
     // else
      begin
        nvalue := sgrc;
        NCount2.Text := IntToStr(nvalue);
      end;
    end;
    repeat
      nremain := StrToInt(GoodsBrowse.FieldValues['NRemain']);
      newnremain := nremain - nvalue;
      if newnremain < 0 then
        newnremain := 0
      else
        nremain := nvalue;
      GoodsCountUpdate.Parameters.ParamByName('@FSerial_1').Value := GoodsBrowse.FieldValues['FSerial'];
      GoodsCountUpdate.Parameters.ParamByName('@NRemain_2').Value := newnremain;
      GoodsCountUpdate.ExecProc;
      Insert2.Parameters.ParamByName('@srlGood_2').Value := srlG;
      Insert2.Parameters.ParamByName('@NCount_3').Value := nremain;
      Insert2.Parameters.ParamByName('@NRemain_4').Value := nremain;
      Insert2.Parameters.ParamByName('@srlSource_5').Value := GoodsBrowse.FieldValues['FSerial'];
      Insert2.Parameters.ParamByName('@srlWarehouse_6').Value := srlWarehouse;
      Insert2.Parameters.ParamByName('@srlFactor_7').Value := srlF;
      Insert2.Parameters.ParamByName('@MCash_8').Value := GoodsBrowse.FieldValues['MCash'];
      Insert2.Parameters.ParamByName('@MCredit_9').Value := GoodsBrowse.FieldValues['MCredit'];
      Insert2.Parameters.ParamByName('@MPrice_10').Value := 0;// GoodsBrowse.FieldValues['MPrice'];
      Insert2.ExecProc;
      nvalue := nvalue - nremain;
      GoodsBrowse.Next;
    until (nvalue <= 0);
  end
  else // GoodsBrowse.RecordCount = 0
  begin
    ShowMessage('�� ����� ' + Browse2FName.Value + ' ������ ������.');
  end;
  GoodsBrowse.Close;
end;

procedure TfrmTransfer.WarehouseCode2Change(Sender: TObject);
begin
  inherited;
  EditCodeChange(sView, WarehouseCode2, lblWarehouse2, srlWarehouse2, srlDataSetWarehouse2)
end;

procedure TfrmTransfer.WarehouseCode2Exit(Sender: TObject);
begin
  inherited;
  EditCodeExit(MyState, WarehouseCode2, lblWarehouse2);
end;

procedure TfrmTransfer.WarehouseCode2KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  EditCodeKeyPress(Key, MyState, WarehouseCode2, lblWarehouse2, srlWarehouse2, srlDataSetWarehouse2);
end;

procedure TfrmTransfer.refreshPanels(b1fs, b2fs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Parameters.ParamByName('@Type').Value := FactorType;
  Browse1.Parameters.ParamByName('@srlYear').Value := srlYear;
  Browse1.Parameters.ParamByName('@srlWarehouse').Value := srlWarehouse;
  Browse1.Parameters.ParamByName('@srlUser').Value := srlUser;
  Browse1.Open;
  Browse1.Locate('fserial', b1fs, []);
  Browse2.Close;
  Browse2.Parameters.ParamByName('@srlFactor').Value := srlFactor;
  Browse2.Open;
  Browse2.Locate('FSerial', b2fs, []);
end;

end.
