inherited frmSellBuy: TfrmSellBuy
  Left = 321
  Top = 129
  Caption = #1601#1585#1608#1588' / '#1582#1585#1610#1583
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    object lblStd: TLabel [0]
      Left = 247
      Top = 38
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = #1582#1585#1610#1583#1575#1585':'
    end
    inherited Splitter2: TSplitter
      Top = 133
    end
    object Label1: TLabel [2]
      Left = 631
      Top = 38
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object Label2: TLabel [3]
      Left = 441
      Top = 38
      Width = 27
      Height = 13
      Caption = #1588#1585#1581':'
    end
    object Label4: TLabel [4]
      Left = 631
      Top = 60
      Width = 25
      Height = 13
      Caption = #1578#1575#1585#1610#1582':'
    end
    object Label5: TLabel [5]
      Left = 438
      Top = 60
      Width = 63
      Height = 13
      Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578':'
    end
    object Label6: TLabel [6]
      Left = 247
      Top = 60
      Width = 30
      Height = 13
      Caption = #1607#1586#1610#1606#1607':'
    end
    object Label7: TLabel [7]
      Left = 631
      Top = 84
      Width = 34
      Height = 13
      Caption = #1578#1582#1601#1610#1601':'
    end
    object lblSBName: TLabel [8]
      Left = 175
      Top = 38
      Width = 12
      Height = 13
      Caption = '...'
    end
    object spbPreToFactor: TSpeedButton [9]
      Left = 96
      Top = 104
      Width = 121
      Height = 25
      Cursor = crHandPoint
      Caption = #1578#1576#1583#1610#1604' '#1576#1607' '#1601#1575#1603#1578#1608#1585' '#1575#1589#1604#1610
      Enabled = False
      Flat = True
      Font.Charset = ARABIC_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
      OnClick = spbPreToFactorClick
    end
    object imgStd: TImage [10]
      Left = 2
      Top = 3
      Width = 93
      Height = 124
      Proportional = True
    end
    object Label16: TLabel [11]
      Left = 439
      Top = 86
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = #1589#1606#1583#1608#1602' '#1583#1575#1585
    end
    object lblCasher: TLabel [12]
      Left = 367
      Top = 86
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label17: TLabel [13]
      Left = 247
      Top = 84
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = #1605#1576#1604#1594' '#1606#1587#1610#1607':'
    end
    inherited DBGrid1: TDBGrid
      Top = 136
      Height = 135
      TitleFont.Height = -13
      TitleFont.Name = 'B Nazanin'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Title.Caption = #1587#1585#1610#1575#1604
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FCode'
          Title.Caption = #1603#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1593#1606#1608#1575#1606
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DtDate'
          Title.Caption = #1578#1575#1585#1610#1582
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Type'
          Title.Caption = #1606#1608#1593
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'CustomerCode'
          Title.Caption = #1603#1583' '#1605#1588#1578#1585#1610
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumCash'
          Title.Caption = #1580#1605#1593' '#1603#1604' '#1606#1602#1583
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumCredit'
          Title.Caption = #1580#1605#1593' '#1603#1604' '#1602#1587#1591#1610
          Width = 83
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SumPrice'
          Title.Alignment = taCenter
          Title.Caption = #1580#1605#1593' '#1603#1604
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustomerName'
          Title.Caption = #1606#1575#1605' '#1605#1588#1578#1585#1610
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCost'
          Title.Caption = #1607#1586#1610#1606#1607
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MDiscount'
          Title.Caption = #1578#1582#1601#1610#1601
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IsClose'
          Title.Caption = #1576#1587#1578#1607
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MExtra'
          Title.Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlYear'
          Title.Caption = #1587' '#1587#1575#1604
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlLoan'
          Title.Caption = #1587' '#1608#1575#1605
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlStd'
          Title.Caption = #1587' '#1591#1604#1576#1607
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlBuyCenter'
          Title.Caption = #1587' '#1605#1585#1603#1586' '#1582#1585#1610#1583
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlWarehouse'
          Title.Caption = #1587' '#1575#1606#1576#1575#1585
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nesieh'
          Title.Caption = #1606#1587#1610#1607
          Visible = True
        end>
    end
    object FCode1: TEdit
      Left = 504
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 1
      OnKeyPress = FCode1KeyPress
    end
    object FName1: TEdit
      Left = 312
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object MCost1: TEdit
      Left = 120
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 6
      OnKeyPress = MCost1KeyPress
    end
    object MExtra1: TEdit
      Left = 312
      Top = 56
      Width = 121
      Height = 21
      TabStop = False
      TabOrder = 5
      OnKeyPress = MExtra1KeyPress
    end
    object MDiscount1: TEdit
      Left = 504
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 7
      OnKeyPress = MDiscount1KeyPress
    end
    object DtDate1: TMaskEdit
      Left = 504
      Top = 56
      Width = 122
      Height = 21
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 4
      Text = '  /  /  '
    end
    object CustomerCode1: TEdit
      Left = 192
      Top = 32
      Width = 49
      Height = 21
      TabOrder = 3
      OnChange = StdCode1Change
      OnExit = StdCode1Exit
      OnKeyPress = StdCode1KeyPress
    end
    object Nesieh1: TEdit
      Left = 120
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 9
      OnKeyPress = MCost1KeyPress
    end
    object FcodeCasher1: TEdit
      Left = 384
      Top = 80
      Width = 49
      Height = 21
      TabOrder = 8
      OnChange = FcodeCasher1Change
      OnExit = FcodeCasher1Exit
      OnKeyPress = FcodeCasher1KeyPress
    end
  end
  inherited Panel2: TPanel
    object Label3: TLabel [1]
      Left = 130
      Top = 14
      Width = 27
      Height = 13
      Caption = #1578#1593#1583#1575#1583':'
    end
    object Label8: TLabel [2]
      Left = 132
      Top = 84
      Width = 46
      Height = 13
      Caption = #1602#1610#1605#1578' '#1603#1604':'
    end
    object Label9: TLabel [3]
      Left = 663
      Top = 60
      Width = 30
      Height = 13
      Caption = #1602#1610#1605#1578':'
    end
    object Label12: TLabel [4]
      Left = 661
      Top = 38
      Width = 15
      Height = 13
      Caption = #1603#1583':'
    end
    object lblGood: TLabel [5]
      Left = 352
      Top = 36
      Width = 263
      Height = 13
      AutoSize = False
      Caption = '...'
    end
    object Label10: TLabel [6]
      Left = 297
      Top = 59
      Width = 60
      Height = 13
      Caption = #1605#1576#1604#1594' '#1578#1602#1587#1610#1591':'
    end
    object Label11: TLabel [7]
      Left = 338
      Top = 14
      Width = 51
      Height = 13
      Caption = #1575#1606#1576#1575#1585' '#1605#1602#1589#1583':'
      Visible = False
    end
    object lblWarehouse2: TLabel [8]
      Left = 264
      Top = 15
      Width = 12
      Height = 13
      Caption = '...'
      Visible = False
    end
    object Label13: TLabel [9]
      Left = 661
      Top = 83
      Width = 27
      Height = 13
      Caption = #1588#1585#1581':'
    end
    object Label18: TLabel [10]
      Left = 130
      Top = 62
      Width = 28
      Height = 13
      Caption = #1583#1585#1589#1583':'
    end
    object Label19: TLabel [11]
      Left = 81
      Top = 62
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label20: TLabel [12]
      Left = 130
      Top = 36
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578':'
    end
    object Label21: TLabel [13]
      Left = 297
      Top = 36
      Width = 48
      Height = 13
      Caption = #1602#1610#1605#1578' '#1606#1602#1583':'
    end
    inherited DBGrid2: TDBGrid
      TitleFont.Height = -13
      TitleFont.Name = 'B Nazanin'
      TitleFont.Style = [fsBold]
      Columns = <
        item
          Expanded = False
          FieldName = 'FSerial'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'srlGood'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'GoodCode'
          Title.Caption = #1603#1583' '#1603#1575#1604#1575
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FName'
          Title.Caption = #1606#1575#1605' '#1603#1575#1604#1575
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NCount'
          Title.Caption = #1578#1593#1583#1575#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NRemain'
          Title.Caption = #1605#1575#1606#1583#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unit'
          Title.Caption = #1608#1575#1581#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCash'
          Title.Caption = #1606#1602#1583
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MCredit'
          Title.Caption = #1606#1587#1610#1607
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MPrice'
          Title.Caption = #1601#1610
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WarehouseCode'
          Title.Caption = #1603#1583' '#1575#1606#1576#1575#1585
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlWarehouse'
          Title.Caption = #1587' '#1575#1606#1576#1575#1585
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlFactor'
          Title.Caption = #1587' '#1601#1575#1603#1578#1608#1585
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'WarehouseName'
          Title.Caption = #1606#1575#1605' '#1575#1606#1576#1575#1585
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'srlSource'
          Title.Caption = #1587' '#1601#1575#1603#1578#1608#1585' '#1575#1589#1604#1610
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Comment'
          Title.Caption = #1588#1585#1581
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MTPrice'
          Title.Caption = #1602#1610#1605#1578' '#1603#1604
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Percent1'
          Title.Caption = #1583#1585#1589#1583
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ExtraPrice'
          Title.Caption = #1575#1590#1575#1601#1607' '#1583#1585#1610#1575#1601#1578
          Visible = True
        end>
    end
    object GoodCode2: TEdit
      Left = 616
      Top = 32
      Width = 41
      Height = 21
      TabOrder = 2
      OnChange = GoodCode2Change
      OnExit = GoodCode2Exit
      OnKeyPress = GoodCode2KeyPress
    end
    object MPrice2: TEdit
      Left = 536
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 6
      OnKeyPress = MPrice2KeyPress
    end
    object MTPrice2: TEdit
      Left = 8
      Top = 80
      Width = 121
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 10
      OnKeyPress = MTPrice2KeyPress
    end
    object NCount2: TEdit
      Left = 8
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 4
      OnKeyPress = NCount2KeyPress
    end
    object MCredit2: TEdit
      Left = 200
      Top = 56
      Width = 97
      Height = 21
      TabOrder = 7
      OnKeyPress = MCredit2KeyPress
    end
    object WarehouseCode2: TEdit
      Left = 280
      Top = 8
      Width = 57
      Height = 21
      TabOrder = 3
      Visible = False
      OnChange = WarehouseCode2Change
      OnExit = WarehouseCode2Exit
      OnKeyPress = WarehouseCode2KeyPress
    end
    object Comment2: TEdit
      Left = 200
      Top = 80
      Width = 457
      Height = 21
      TabOrder = 9
    end
    object IsTaghsit2: TCheckBox
      Left = 565
      Top = 8
      Width = 97
      Height = 17
      BiDiMode = bdRightToLeft
      Caption = #1570#1610#1575#1578#1602#1587#1610#1591' '#1588#1608#1583#1567
      ParentBiDiMode = False
      TabOrder = 1
    end
    object Percent12: TEdit
      Left = 96
      Top = 56
      Width = 33
      Height = 21
      TabOrder = 8
      OnKeyPress = NCount2KeyPress
    end
    object ExtraPrice2: TEdit
      Left = 8
      Top = 32
      Width = 121
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 11
      OnKeyPress = MTPrice2KeyPress
    end
    object MCash2: TEdit
      Left = 200
      Top = 32
      Width = 97
      Height = 21
      TabOrder = 5
      OnKeyPress = MPrice2KeyPress
    end
  end
  inherited pnlTools: TPanel
    inherited bPrint: TSpeedButton
      Hint = #1670#1575#1662#13'F10'#13'Ctrl + P'
      OnClick = bPrintClick
    end
    object BOk: TSpeedButton
      Left = 2
      Top = 472
      Width = 60
      Height = 56
      Hint = #1578#1575#1610#1610#1583' '#1601#1575#1603#1578#1608#1585
      Caption = #1578#1575#1610#1610#1583' '#1601#1575#1603#1578#1608#1585
      Font.Charset = ARABIC_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = BOkClick
    end
  end
  inherited Insert1: TADOStoredProc
    ProcedureName = 'insert_TFactor_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_3'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@DtDate_4'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MExtra_5'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Type_6'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@MCost_7'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MDiscount_8'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@IsClose_9'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@srlYear_10'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlLoan_11'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlStd_12'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlBuyCenter_13'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlCasher'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srluser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlWarehouse'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nesieh'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end>
  end
  inherited Browse1: TADOStoredProc
    CommandTimeout = 20000000
    ProcedureName = 'Browse_TFactor;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Type'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@srlYear'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 86
      end>
    object Browse1FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse1FCode: TIntegerField
      FieldName = 'FCode'
    end
    object Browse1FName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object Browse1DtDate: TStringField
      FieldName = 'DtDate'
      FixedChar = True
      Size = 8
    end
    object Browse1MExtra: TBCDField
      FieldName = 'MExtra'
      Precision = 19
    end
    object Browse1Type: TSmallintField
      FieldName = 'Type'
    end
    object Browse1MCost: TBCDField
      FieldName = 'MCost'
      Precision = 19
    end
    object Browse1MDiscount: TBCDField
      FieldName = 'MDiscount'
      Precision = 19
    end
    object Browse1IsClose: TBooleanField
      FieldName = 'IsClose'
    end
    object Browse1srlYear: TIntegerField
      FieldName = 'srlYear'
    end
    object Browse1srlLoan: TIntegerField
      FieldName = 'srlLoan'
    end
    object Browse1srlStd: TIntegerField
      FieldName = 'srlStd'
    end
    object Browse1srlBuyCenter: TIntegerField
      FieldName = 'srlBuyCenter'
    end
    object Browse1CustomerCode: TIntegerField
      FieldName = 'CustomerCode'
    end
    object Browse1CustomerName: TWideStringField
      FieldName = 'CustomerName'
      Size = 50
    end
    object Browse1SumCash: TBCDField
      FieldName = 'SumCash'
      Precision = 19
    end
    object Browse1SumCredit: TBCDField
      FieldName = 'SumCredit'
      Precision = 19
    end
    object Browse1SumPrice: TBCDField
      FieldName = 'SumPrice'
      Precision = 19
    end
    object Browse1srlCasher: TIntegerField
      FieldName = 'srlCasher'
    end
    object Browse1FserialCasher: TAutoIncField
      FieldName = 'FserialCasher'
      ReadOnly = True
    end
    object Browse1FcodeCasher: TIntegerField
      FieldName = 'FcodeCasher'
    end
    object Browse1FnameCasher: TWideStringField
      FieldName = 'FnameCasher'
      Size = 50
    end
    object Browse1srlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object Browse1Nesieh: TBCDField
      FieldName = 'Nesieh'
      Precision = 19
    end
    object Browse1srlUser: TIntegerField
      FieldName = 'srlUser'
    end
  end
  inherited DataSource1: TDataSource
    Left = 7
    Top = 0
  end
  inherited Update1: TADOStoredProc
    ProcedureName = 'update_TFactor_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FCode_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@FName_4'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@DtDate_7'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8
        Value = Null
      end
      item
        Name = '@MExtra_8'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MCost_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MDiscount_11'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@srlCasher'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlUser'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nesieh'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end>
    Top = 108
  end
  inherited Update2: TADOStoredProc
    ProcedureName = 'update_TDetFac_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MCash_9'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MCredit_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MPrice_11'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Comment'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@istaghsit'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Percent1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
  end
  inherited Browse2: TADOStoredProc
    CommandTimeout = 20000000
    ProcedureName = 'Browse_TDetFac;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 55
    Top = 352
    object Browse2FSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object Browse2srlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object Browse2NCount: TIntegerField
      FieldName = 'NCount'
    end
    object Browse2NRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object Browse2srlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object Browse2srlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object Browse2srlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object Browse2MCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object Browse2MCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object Browse2MPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object Browse2FName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object Browse2WarehouseName: TWideStringField
      FieldName = 'WarehouseName'
      Size = 50
    end
    object Browse2Unit: TWideStringField
      FieldName = 'Unit'
      Size = 50
    end
    object Browse2GoodCode: TIntegerField
      FieldName = 'GoodCode'
    end
    object Browse2WarehouseCode: TIntegerField
      FieldName = 'WarehouseCode'
    end
    object Browse2Comment: TWideStringField
      FieldName = 'Comment'
      Size = 70
    end
    object Browse2MTPrice: TBCDField
      FieldName = 'MTPrice'
      ReadOnly = True
      Precision = 19
    end
    object Browse2Percent1: TIntegerField
      FieldName = 'Percent1'
    end
    object Browse2ExtraPrice: TBCDField
      FieldName = 'ExtraPrice'
      Precision = 19
    end
    object Browse2IsTaghsit: TBooleanField
      FieldName = 'IsTaghsit'
    end
  end
  inherited Insert2: TADOStoredProc
    ProcedureName = 'insert_TDetFac_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlGood_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NCount_3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_4'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlSource_5'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlWarehouse_6'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor_7'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@MCash_8'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MCredit_9'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@MPrice_10'
        Attributes = [paNullable]
        DataType = ftBCD
        Precision = 19
        Value = Null
      end
      item
        Name = '@Comment'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 100
        Value = Null
      end
      item
        Name = '@isTaghsit'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@Percent1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
  end
  object dtsGoods: TDataSource [11]
    DataSet = GoodsBrowse
    Left = 96
    Top = 440
  end
  object GoodsBrowse: TADOStoredProc [12]
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browes_TDetFacRemains;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlGood1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlWarehouse'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlYear'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 32
    Top = 440
    object GoodsBrowseFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object GoodsBrowsesrlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object GoodsBrowseNCount: TIntegerField
      FieldName = 'NCount'
    end
    object GoodsBrowseNRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object GoodsBrowsesrlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object GoodsBrowsesrlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object GoodsBrowsesrlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object GoodsBrowseMCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object GoodsBrowseMCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object GoodsBrowseMPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object GoodsBrowseType: TSmallintField
      FieldName = 'Type'
    end
  end
  object GoodsCountUpdate: TADOStoredProc [13]
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TDetFacRemain_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 57
    Top = 489
  end
  object PreToFactor: TADOStoredProc [14]
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TFactor_PreToFactor_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 249
    Top = 105
  end
  object dtsDetPreToFactor: TDataSource [15]
    DataSet = DetPreToFactor
    Left = 224
    Top = 432
  end
  object DetPreToFactor: TADOStoredProc [16]
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browse_TDetFac;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@srlFactor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 184
    Top = 432
    object DetPreToFactorFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object DetPreToFactorsrlGood: TIntegerField
      FieldName = 'srlGood'
    end
    object DetPreToFactorNCount: TIntegerField
      FieldName = 'NCount'
    end
    object DetPreToFactorNRemain: TIntegerField
      FieldName = 'NRemain'
    end
    object DetPreToFactorsrlSource: TIntegerField
      FieldName = 'srlSource'
    end
    object DetPreToFactorsrlWarehouse: TIntegerField
      FieldName = 'srlWarehouse'
    end
    object DetPreToFactorsrlFactor: TIntegerField
      FieldName = 'srlFactor'
    end
    object DetPreToFactorMCash: TBCDField
      FieldName = 'MCash'
      Precision = 19
    end
    object DetPreToFactorMCredit: TBCDField
      FieldName = 'MCredit'
      Precision = 19
    end
    object DetPreToFactorMPrice: TBCDField
      FieldName = 'MPrice'
      Precision = 19
    end
    object DetPreToFactorFName: TWideStringField
      FieldName = 'FName'
      Size = 50
    end
    object DetPreToFactorWarehouseName: TWideStringField
      FieldName = 'WarehouseName'
      Size = 50
    end
    object DetPreToFactorUnit: TWideStringField
      FieldName = 'Unit'
      Size = 50
    end
    object DetPreToFactorGoodCode: TIntegerField
      FieldName = 'GoodCode'
    end
    object DetPreToFactorWarehouseCode: TIntegerField
      FieldName = 'WarehouseCode'
    end
  end
  object deactiveStudent: TADOStoredProc [17]
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'deactiveStudent;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 105
    Top = 17
    object deactiveStudentFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object deactiveStudentFName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object deactiveStudentBlocked: TBooleanField
      FieldName = 'Blocked'
      ReadOnly = True
    end
  end
  object deactiveGoods: TADOStoredProc [18]
    Connection = frmDm.ADOConnection
    ProcedureName = 'deactiveGoods;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@FSerial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 185
    Top = 281
    object deactiveGoodsFSerial: TAutoIncField
      FieldName = 'FSerial'
      ReadOnly = True
    end
    object deactiveGoodsFName: TWideStringField
      FieldName = 'FName'
      Size = 100
    end
    object deactiveGoodsBlocked: TBooleanField
      FieldName = 'Blocked'
    end
  end
  object dtsGoodsSource: TDataSource [19]
    DataSet = GoodsBrowseSource
    Left = 600
    Top = 480
  end
  object GoodsBrowseSource: TADOStoredProc [20]
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Browes_TDetFacSourceRemain;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@srlSource'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 600
    Top = 432
  end
  object ADOStoredProc2: TADOStoredProc [21]
    Connection = frmDm.ADOConnection
    ProcedureName = 'update_TDetFacRemain_1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@FSerial_1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NRemain_2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 209
    Top = 481
  end
  inherited DataSource2: TDataSource
    Top = 265
  end
  object ppReport1: TppReport
    PrinterSetup.BinName = 'Default'
    PrinterSetup.DocumentName = 'Report'
    PrinterSetup.PaperName = 'Letter'
    PrinterSetup.PrinterName = 'Default'
    PrinterSetup.mmMarginBottom = 6350
    PrinterSetup.mmMarginLeft = 6350
    PrinterSetup.mmMarginRight = 6350
    PrinterSetup.mmMarginTop = 6350
    PrinterSetup.mmPaperHeight = 279401
    PrinterSetup.mmPaperWidth = 215900
    PrinterSetup.PaperSize = 1
    DeviceType = 'Screen'
    EmailSettings.ReportFormat = 'PDF'
    OutlineSettings.CreateNode = True
    OutlineSettings.CreatePageNodes = True
    OutlineSettings.Enabled = True
    OutlineSettings.Visible = True
    TextSearchSettings.DefaultString = '<FindText>'
    TextSearchSettings.Enabled = True
    Left = 129
    Top = 201
    Version = '10.02'
    mmColumnWidth = 0
    object ppHeaderBand1: TppHeaderBand
      mmBottomOffset = 0
      mmHeight = 81492
      mmPrintPosition = 0
    end
    object ppDetailBand1: TppDetailBand
      mmBottomOffset = 0
      mmHeight = 75142
      mmPrintPosition = 0
      object StudentPic1: TppImage
        UserName = 'StudentPic1'
        MaintainAspectRatio = False
        Border.BorderPositions = []
        Border.Color = clBlack
        Border.Style = psSolid
        Border.Visible = False
        Border.Weight = 1.000000000000000000
        mmHeight = 35983
        mmLeft = 40481
        mmTop = 0
        mmWidth = 30692
        BandType = 4
      end
      object ppShape1: TppShape
        UserName = 'Shape1'
        mmHeight = 13229
        mmLeft = 93398
        mmTop = 15346
        mmWidth = 13229
        BandType = 4
      end
      object ppDBImage1: TppDBImage
        UserName = 'DBImage1'
        DirectDraw = True
        MaintainAspectRatio = True
        Transparent = True
        Border.BorderPositions = [bpLeft, bpTop, bpRight, bpBottom]
        Border.Color = clAqua
        Border.Style = psDashDotDot
        Border.Visible = True
        Border.Weight = 3.000000000000000000
        GraphicType = 'Bitmap'
        mmHeight = 30692
        mmLeft = 120386
        mmTop = 7408
        mmWidth = 25665
        BandType = 4
      end
    end
    object ppFooterBand1: TppFooterBand
      mmBottomOffset = 0
      mmHeight = 13229
      mmPrintPosition = 0
    end
  end
  object Update_Ok_Fac: TADOStoredProc
    Connection = frmDm.ADOConnection
    CursorType = ctStatic
    ProcedureName = 'Update_Ok_Fac;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@fserial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 512
    Top = 432
  end
end
