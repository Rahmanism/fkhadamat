unit untYear;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unt1Panel, DB, ADODB, Buttons, Grids, DBGrids, ExtCtrls,
  unt2Panels, ComCtrls, StdCtrls, U_Common, AppEvnts, IniFiles, DBCtrls,
  Mask;

type
  TfrmYear = class(Tfrm1Panel)
    FCode1: TEdit;
    FName1: TEdit;
    IsClose1: TCheckBox;
    IsActive1: TCheckBox;
    Browse1FSerial: TAutoIncField;
    Browse1FCode: TIntegerField;
    Browse1FName: TWideStringField;
    Browse1DtStart: TStringField;
    Browse1DtEnd: TStringField;
    Browse1IsClose: TBooleanField;
    Browse1IsActive: TBooleanField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DtStart1: TMaskEdit;
    DtEnd1: TMaskEdit;
    procedure bSaveClick(Sender: TObject);
    procedure FCode1KeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure bDelClick(Sender: TObject);
    procedure bAddClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure DtStart1Exit(Sender: TObject);
    procedure DtEnd1Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure refreshPanel(bfs: Integer);
    { Private declarations }
  public
    { Public declarations }
    
  end;

var
  frmYear: TfrmYear;

implementation

uses untDm, untMain;

{$R *.dfm}

procedure TfrmYear.bSaveClick(Sender: TObject);

begin
  inherited;
  if MyState = sAdd then
  begin
    Insert1.Close;
    with Insert1.Parameters do
    begin
      ParamByName('@FCode').Value := FCode1.Text;
      ParamByName('@FName').Value := FName1.Text;
      ParamByName('@DtStart').Value := DtStart1.Text;
      ParamByName('@DtEnd').Value := DtEnd1.Text;
      ParamByName('@IsClose').Value := IsClose1.Checked;
      ParamByName('@IsActive').Value := IsActive1.Checked;
    end;
    Insert1.ExecProc;
    Browse1.Close;
    Browse1.Open;
    Browse1.Locate('fserial',Insert1.Parameters.ParamByName('@Fserial').Value,[]);
  end;

  if MyState = sEdit then
  begin
   // srltemp:=Browse1FSerial.Value;
    Update1.Close;
   with Update1.Parameters do
    begin
      ParamByName('@Fserial').Value :=srltemp;
      ParamByName('@FCode').Value := FCode1.Text;
      ParamByName('@FName').Value := FName1.Text;
      ParamByName('@DtStart').Value := DtStart1.Text;
      ParamByName('@DtEnd').Value := DtEnd1.Text;
      ParamByName('@IsClose').Value := IsClose1.Checked;
      ParamByName('@IsActive').Value := IsActive1.Checked;
    end;
    Update1.ExecProc;
    Browse1.Close;
    Browse1.Open;
    Browse1.Locate('fserial',srltemp,[]);
  end;
end;

procedure TfrmYear.FCode1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ValidKey) then
                    //
end;

procedure TfrmYear.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  CheckBoxInDBGrid(DBCheckBox1, Sender, Rect, DataCol, Column, State);
  CheckBoxInDBGrid(DBCheckBox2, Sender, Rect, DataCol, Column, State);
end;

procedure TfrmYear.bDelClick(Sender: TObject);
begin
  inherited;
  DeleteRecord1(40, Browse1FSerial.Value, Browse1);
end;

procedure TfrmYear.bAddClick(Sender: TObject);
begin
  inherited;
  Fcode1.Text := GetMaxCodeRecord(40, srlYear, -1, -1, -1);
end;

procedure TfrmYear.refreshPanel(bfs:Integer);
begin
  MyState := sView;
  Browse1.Close;
  Browse1.Open;
  Browse1.Locate('fserial', bfs, []);
end;

procedure TfrmYear.bCancelClick(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmYear.DtStart1Exit(Sender: TObject);
begin
  inherited;
  CheckDate(Sender);
end;

procedure TfrmYear.DtEnd1Exit(Sender: TObject);
begin
  inherited;
  CheckDate(Sender);
end;

procedure TfrmYear.FormShow(Sender: TObject);
begin
  inherited;
  refreshPanel(0);
end;

procedure TfrmYear.DataSource1DataChange(Sender: TObject; Field: TField);
begin
  inherited;
  srltemp:=Browse1FSerial.Value;
end;

procedure TfrmYear.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
     if MessageDlgTranslated('��� ������ ��� ���� ����� ���Ͽ', mtConfirmation, [mbYes, mbNo], mbNo, 0) = mrYes then
     begin
     srlYear:=Browse1FSerial.Value;
     YearClose:=Browse1IsClose.Value;
     frmMain.StatusBar1.Panels[2].Text:=IntToStr(Browse1Fcode.Value);
     frmMain.StatusBar1.Panels[3].Text:=Browse1Fname.Value;

     end;
end;

end.
